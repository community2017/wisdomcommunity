//
//  main.m
//  Keping
//
//  Created by 柯平 on 2017/6/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
