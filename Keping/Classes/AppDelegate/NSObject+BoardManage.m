//
//  NSObject+BoardManage.m
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "NSObject+BoardManage.h"
#import "AppDelegate.h"
#import "KLoginViewController.h"
#import "KNavigationController.h"
#import "KUserData.h"
#import "KTabBarController.h"
#import "KEaseMobManager.h"
#import <Hyphenate/Hyphenate.h>
#import "KTabBarController.h"

@implementation NSObject (BoardManage)

-(AppDelegate*)appDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

-(void)changeRootToLoginViewController
{
    if ([KUserData remove]) {
        KLog(@"清除用户数据");
    }
    [[KAppConfig sharedConfig] setLogined:NO];
    [KEaseMobManager easeMobLogout];
    AppDelegate* app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.window.rootViewController = [[KNavigationController alloc] initWithRootViewController:[[KLoginViewController alloc]init]];
}

-(void)changeRootToHomeViewController
{
    [KEaseMobManager loginStateChanged:YES];
    AppDelegate* app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.window.rootViewController = [[KTabBarController alloc]init];
}

//自动登录
-(void)appAutoLogin{
    [KEaseMobManager loginStateChanged:YES];
    AppDelegate* app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.window.rootViewController = [[KTabBarController alloc]init];
}

//退出登录
-(void)userChooseLogout{
    [[KAppConfig sharedConfig] setLogined:NO];
    if ([KPartyView remove]) {
        KLog(@"Clear user info success!");
    }
    [KEaseMobManager easeMobLogout];
    AppDelegate* app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.window.rootViewController = [[KNavigationController alloc] initWithRootViewController:[KLoginViewController new]];
}

//用户是否已经登录
-(BOOL)isUserLogin{
    BOOL login = NO;
    if ([KAppConfig sharedConfig].logined && [EMClient sharedClient].isLoggedIn) {
        login = YES;
        if (![[KAppConfig sharedConfig].sessionId isNotBlank]) {
            login = NO;
        }
    }
    return login;
}

//几个根视图控制器跳转
-(void)changeToRootOfIndex:(NSInteger)index{
    KTabBarController* tb = (KTabBarController*)self.appDelegate.window.rootViewController;
    tb.selectedIndex = index;
}

@end
