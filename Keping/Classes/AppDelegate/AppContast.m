//
//  AppContast.m
//  Keping
//
//  Created by 柯平 on 2017/6/17.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "AppContast.h"


#pragma mark - const

/*占位图*/
NSString* const KImagePlaceholder = @"placeholder";
//图片路径
NSString* const KImageURL =@"http://120.25.99.202:8090/intelligent-community";
/*获取图片*/
NSString* const KGetImageURL = @"http://120.25.99.202:8090/intelligent-community/upload/media/";


#pragma mark - 环信
/*API Key*/
NSString* const KHyApiKey = @"1503170824003264#intelligentcommunity";

/*Client ID*/
NSString* const KHyClientID = @"YXA6IKHloIh9Eeeas4F39O7SVQ";

/*Client Secret*/
NSString* const KHyClientSecret = @"YXA66SRd1JFLUImTpq6j2nb7lKwipH4";

/*push name*/
NSString* const KHyPushCerName = @"link_push_dev";//link_push_dis


#pragma mark  - 服务器地址
/*服务器地址*/
NSString* const KBaseURL = @"http://120.25.99.202:8090";

#pragma mark - 用户


/*验证码获取*/
NSString* const KGetSMSCodeURL = @"/intelligent-community/api/biz/gl/v1/smsHis/add.htm";


/*登录*/
NSString* const KLoginURL = @"/intelligent-community/api/biz/ou/v1/partys/login.htm";

/*注册*/
NSString* const KRegisterURL = @"/intelligent-community/api/biz/ou/v1/partys.htm";

/*找回密码*/
NSString* const KForgetPwdURL = @"/intelligent-community/api/biz/ou/v1/partys/updatePassWordNoLogin.htm";

/*更新密码*/
NSString* const KUpdatePwdURL = @"/intelligent-community/api/biz/ou/v1/partys/updatePwd.htm";

/*更新用户信息*/
NSString* const KUpdateprofileURL = @"/intelligent-community/api/biz/ou/v1/partys/update.htm";

/*实名认证*/
NSString* const KUserVeritifyURL = @"/intelligent-community/api/biz/ou/v1/partyAuthentications/add.htm";

/*更新用户信息成功*/
NSString* const KUpdateUserInfoSuccessNotification = @"KUpdateUserInfoSuccessNotificationKey";

/*定位权限发生变化*/
NSString* const KLocationAuthorizedStatusChangedNotification = @"KLocationAuthorizedStatusChangedNotification";

#pragma mark - 关于我们
/*用户协议*/
NSString* const KUserProtocolURL = @"/intelligent-community/api/biz/gl/v1/glConfigs/get.htm";

/*关于我们*/
NSString* const KAboutUsURL = @"/intelligent-community/api/biz/gl/v1/glConfigs/get.htm";

/*获取当前版本信息*/
NSString* const KVersionURL = @"/intelligent-community/api/biz/gl/v1/glAppVersions/get/versonCode.htm";

/*T&C / Privacy 和 FAQ*/
NSString* const KGetSomeInfoURL = @"/intelligent-community/api/ec/site/v1/privacyEntitys/list.htm";

/*检查新版本*/
NSString* const KCheckVersionURL = @"/intelligent-community/api/biz/gl/v1/glAppVersions/get/checkVersion.htm";

/*T&C / Privacy & FAQ*/
NSString* const KTCPrivacyFAQURL = @"/intelligent-community/api/ec/site/v1/privacyEntitys/list.htm";

#pragma mark - CCTV

/*修改用户cctv*/
NSString* const KUpdateUserCCTVURL = @"/intelligent-community/api/biz/ou/v1//cctvPartys/update/{cctvPartyId}.htm";

/*用户cctv列表*/
NSString* const KUserCCTVsURL = @"/intelligent-community/api/ec/cctv/v1/cctvEntitys/list.htm";


#pragma mark - Coounity
/*公告详情*/
NSString* const KAnnouncementDetailURL = @"/intelligent-community/api/ec/bbs/v1/bbsTopics/get/topicId.htm";

/*添加动态评论*/
NSString* const KAddCommunityStatusCommentURL = @"/intelligent-community/api/ec/bbs/v1/bbsPostss/add.htm";

/*根据条件获取社区*/
NSString* const KGetCommunityURL = @"/intelligent-community/api/ec/community/v1/communityEntitys/list.htm";

/*添加动态*/
NSString* const KAddCommunityStatusURL = @"/intelligent-community/api/ec/bbs/v1/bbsTopics/add.htm";

/*添加社区反馈*/
NSString* const KAddCommunityFeedbackURL = @"/intelligent-community/api/biz/gl/v1/glFeedbacks/add.htm";

/*点赞*/
//NSString* const KPraiseURL = @"/intelligent-community/api/biz/gl/v1/glStatisticss/add.htm";

/*社区公告*/
NSString* const KAnnouncementsURL = @"/intelligent-community/api/ec/bbs/v1/bbsTopics/list.htm";

/*社区认证*/
NSString* const KCommunityVeritifyURL = @"/intelligent-community/api/ec/community/v1/communityUserAuthentications/add.htm";

/*获取社区用户推荐信*/
NSString* const KCommunityRecommandLetterURL = @"/intelligent-community/api/biz/gl/v1/glRecommends/list.htm";
/*场馆预定*/
NSString* const KPCategoriesURL =@"/intelligent-community/api/biz/ou/v1/shops/list.htm";

/*上传图片*/
NSString* const KUploadImageURL = @"/intelligent-community/api/biz/gl/upload/upLoadImages.htm";
/*订场*/
NSString* const KPSuperstarBookURL=@"/intelligent-community/api/biz/ou/v1/shopBooks/add.htm";
/*预定列表*/
NSString* const KPSuperstarBookListURL=@"/intelligent-community/api/biz/ou/v1/shopBooks/list.htm";

/*预定详情*/
NSString* const KFacilitiesBookDetailURL = @"/intelligent-community/api/biz/ou/v1/shopBooks/get/bookId.htm";
/*商家详情*/
NSString* const KShopDetailURL=@"/intelligent-community/api/biz/ou/v1//shops/get/shopId.htm";

/*认证社区列表*/
NSString* const KVertifiedCommunitiesURL = @"/intelligent-community/api/ec/community/v1/communityUserAuthentications/list.htm";

#pragma mark - Directory

/*directory列表*/
NSString* const KDirectoryDataURL = @"/intelligent-community/api/biz/ou/v1/shops/listDirect.htm";

/*Directory详情*/
NSString* const KDirectoryDetailURL = @"/intelligent-community/api/biz/ou/v1/shops/Detail/{key}.htm";

/*Directory轮播图*/
NSString* const KDirectoryPhotosURL = @"/intelligent-community/api/biz/gl/v1/banners/lists.htm";


#pragma mark - chat

/*contacts*/
NSString* const KChatContactsURL = @"/intelligent-community/api/ec/relation/v1/phoneBooks/list.htm";

/*history*/
NSString* const KChatHistoryURL = @"/intelligent-community/api/ec/relation/v1/relationSessions/list.htm";

/*上传手机联系人*/
NSString* const KUploadContactsURL = @"/intelligent-community/";

/*sos列表*/
NSString* const KSOSsURL = @"/intelligent-community/";

/*添加SOS联系人*/
NSString* const KAddSOSURL = @"/intelligent-community/";

/*删除sos联系人*/
NSString* const KDeleteSOSURL = @"/intelligent-community/";

/*请求添加好友*/
NSString* const KAddFriendURL = @"/intelligent-community/";

/*同意添加好友*/
NSString* const KAgreeAddURL = @"/intelligent-community/";

/*搜索好友*/
NSString* const KSearchFriendURL = @"/intelligent-community/";

/*点击聊天回调*/
NSString* const KChatClickBlockURL = @"/intelligent-community/";

/*获取好友*/
NSString* const KGetFriendsURL = @"/intelligent-community/";

/*获取好友详情*/
NSString* const KGetFriendInfoURL = @"/intelligent-community/";

#pragma mark - 社区

/*公告、动态点赞*/
NSString* const KPraiseURL = @"/intelligent-community/api/biz/gl/v1/glStatisticss/add.htm";

/*公告或动态评论*/
NSString* const KAddCommentURL = @"/intelligent-community/";

/*公告或动态评论列表*/
NSString* const KCommentsURL = @"/intelligent-community/";

/*公告或动态评论的评论*/
NSString* const KCommentOfCommentObjURL = @"/intelligent-community/";

/*公告或动态评论的评论列表*/
NSString* const KCommentsOfCommentObjURL = @"/intelligent-community/";

/*公告或动态评论的评论回复*/
NSString* const KReplayOfCommentObjURL = @"/intelligent-community/";

/*公告或动态评论详情*/
NSString* const KCommentsDetailURL = @"/intelligent-community/";

/*公告详情*/
NSString* const KNoticeDetailURL = @"/intelligent-community/";

/*发布动态*/
NSString* const KPublishStatusURL = @"/intelligent-community/";

/*开锁与解锁*/
NSString* const KLockOrNotURL = @"/intelligent-community/";

/*我的门禁卡列表*/
NSString* const KDoorCardsURL = @"/intelligent-community/";

/*添加社区动态*/
NSString* const KAddCommunitURL = @"/intelligent-community/api/ec/bbs/v1/bbsTopics/add.htm";

/*获取社区动态列表*/
NSString* const KCommunityStatusURL = @"/intelligent-community/api/ec/bbs/v1/bbsTopics/listWall.htm";

/*获取社区用户私信*/
NSString* const KCommunityPrivateLetterURL = @"/intelligent-community/api/biz/gl/v1/glRecommends/list.htm";

/*门禁卡开门记录*/
NSString* const KDoorLockHistoryURL = @"/intelligent-community/api/biz/gl/v1/glStatisticss/add.htm";

/*动态提醒列表*/
NSString* const KNotifiesURL = @"/intelligent-community/api/ec/bbs/v1/bbsPostss/listNotification.htm";
