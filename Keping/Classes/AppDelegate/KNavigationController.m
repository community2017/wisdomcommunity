//
//  KNavigationController.m
//  NeiHan
//
//  Created by 柯平 on 2017/3/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNavigationController.h"
#import "SettingsViewController.h"

@interface KNavigationController ()

@end

@implementation KNavigationController

+(void)initialize{
    //设置不透明
    [[UINavigationBar appearance] setTranslucent:NO];
    //设置导航栏颜色
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navi_bar_back"]]];
    //[[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navi_bar_back"] forBarMetrics:UIBarMetricsCompact];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    //导航栏返回按钮颜色
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //去掉返回安妮文字
    //[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-80, -80) forBarMetrics:UIBarMetricsDefault];
    //设置导航栏文字颜色字体大小
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:kBoldFont(20),NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIBarButtonItem* item = [UIBarButtonItem appearance];
    [item setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
        //[[UINavigationBar appearance] setShadowImage:[UIImage new]];
        //[[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.childViewControllers.count) {
        //自动隐藏tabBar
        if ([viewController isKindOfClass:[SettingsViewController class]]) {
            [viewController setHidesBottomBarWhenPushed:NO];
        }else{
            [viewController setHidesBottomBarWhenPushed:YES];
        }
        /**
         *  如果在堆栈控制器数量大于1 加返回按钮
         */
        if (self.viewControllers.count > 0) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 7, 30, 30)];
            [btn setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
            
            btn.imageEdgeInsets = UIEdgeInsetsMake(5, 5,5, 5);
            btn.tintColor = [UIColor colorWithRed:0.42f green:0.33f blue:0.27f alpha:1.00f];
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
            [btn addTarget:self action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
            viewController.navigationItem.leftBarButtonItem = leftItem;
        } else {
            //viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTitle:@"" tintColor:[UIColor colorWithRed:0.42f green:0.33f blue:0.27f alpha:1.00f] touchBlock:nil];
        }
    }
    [super pushViewController:viewController animated:animated];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
