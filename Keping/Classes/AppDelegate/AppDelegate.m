//
//  AppDelegate.m
//  Keping
//
//  Created by 柯平 on 2017/6/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "KLocateCommand.h"
#import "KCommandManage.h"
#import "KDatabase.h"
#import "KLoginViewController.h"
#import "KClearNavigationController.h"
#import "KLaunchViewController.h"
#import "KTabBarController.h"
#import "KNavigationController.h"
#import <IQKeyboardManager.h>
#import <Hyphenate/Hyphenate.h>

@interface AppDelegate ()
@property(nonatomic,strong)YYReachability* reachability;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //定位
    [self locateAction];
    
    //进度条/提示框配置
    [[KAppConfig sharedConfig] setHudType:HudShowTypeMB];
    
    //建表
    //[KDatabase createTablesNeeded];
    
    //keyboard
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Done"];
    
    //网络
    self.reachability = [YYReachability reachability];
    KLog(@"当前网络状态：%@",self.reachability.status == YYReachabilityStatusNone ? @"无网络":self.reachability.status == YYReachabilityStatusWiFi ? @"WiFi网络":@"移动网络");
    [KAppConfig sharedConfig].networkStatus = self.reachability.status;
    @weakify(self);
    self.reachability.notifyBlock = ^(YYReachability *reachability){
        [weak_self networkChanged:reachability];
    };
    
    //服务器配置
    [[KNetworkConfig sharedConfig] setBaseUrl:KBaseURL];
    [[KAppConfig sharedConfig] setRequestTimeInterval:30.0];
    
    //环信配置
    EMOptions* option = [EMOptions optionsWithAppkey:KHyApiKey];
    option.apnsCertName = KHyPushCerName;
    [option setIsAutoAcceptGroupInvitation:NO];
    [[EMClient sharedClient] initializeSDKWithOptions:option];
    
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor grayColor];
    
    /*
     if (![[KAppConfig sharedConfig] isNoneFirstOpen]) {
     self.window.rootViewController = [[KLoginViewController alloc]init];
     [[KAppConfig sharedConfig] setIsNoneFirstOpen:YES];
     }else{
     self.window.rootViewController = [[KTabBarController alloc]init];
     }
     */
    if ([self isUserLogin]) {
        [self appAutoLogin];
    }else{
        self.window.rootViewController = [[KNavigationController alloc] initWithRootViewController:[[KLoginViewController alloc]init]];
    }
    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void)locateAction
{
    KLocateCommand * locateCmd = [[KLocateCommand alloc]init];
    locateCmd.timeOutDefault = 60.0f;
    [KCommandManage executeCommand:locateCmd completeBlock:^(id<Command> cmd) {
        if (locateCmd.responseStatus == KLocateStatusSuccess) {
            KLog(@"City:%@",locateCmd.cityName);
        }
    }];
}

-(void)networkChanged:(YYReachability*)reachability{
    YYReachabilityStatus status = reachability.status;
    switch (status) {
        case YYReachabilityStatusNone:
        {
            KLog(@"网络服务已断开");
        }
            break;
        case YYReachabilityStatusWiFi:
        {
            [self isNetworkResume];
        }
            break;
        case YYReachabilityStatusWWAN:
        {
            KLog(@"移动网络");
            [self isNetworkResume];
            YYReachabilityWWANStatus wwStatus = reachability.wwanStatus;
            switch (wwStatus) {
                case YYReachabilityWWANStatus2G:
                    KLog(@"2G网络");
                    break;
                case YYReachabilityWWANStatus3G:
                    KLog(@"4G网络");
                    break;
                case YYReachabilityWWANStatus4G:
                    KLog(@"4G网络");
                    break;
                case YYReachabilityWWANStatusNone:
                    KLog(@"无移动网络");
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    
    //更新网络状态
    [KAppConfig sharedConfig].networkStatus = status;
}

-(void)isNetworkResume
{
    if ([[KAppConfig sharedConfig] networkStatus] == YYReachabilityStatusNone) {
        KLog(@"网络已恢复：WIFI网络");
        //1.刷新首页数据
        
#warning NetworkResume
        [[NSNotificationCenter defaultCenter] postNotificationName:@"KNetworkResumeKey" object:nil];
        //2.重新定位
        [self locateAction];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    CLAuthorizationStatus locationStatus = [CLLocationManager authorizationStatus];
    if (locationStatus != [KAppConfig sharedConfig].locationStatus) {
        [[KAppConfig sharedConfig] setLocationStatus:locationStatus];
        [[NSNotificationCenter defaultCenter] postNotificationName:KLocationAuthorizedStatusChangedNotification object:nil];
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
