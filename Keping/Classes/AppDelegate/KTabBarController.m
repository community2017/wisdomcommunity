//
//  KTabBarController.m
//  NeiHan
//
//  Created by 柯平 on 2017/3/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTabBarController.h"
#import "HomeViewController.h"
#import "KNavigationController.h"
#import "KDirectoryViewController.h"
#import "KDoorAccessoryViewController.h"
#import "KCCTVViewController.h"
#import "KDoorAccessoryViewController.h"

@interface KTabBarController ()

@end

@implementation KTabBarController

+(void)initialize{
    
    //设置为不透明
    [[UITabBar appearance] setTranslucent:NO];
    
    //设置背景颜色
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:0.97f green:0.97f blue:0.97f alpha:1.00f]];
    
    //拿到整个导航控制器的外观
    UITabBarItem* item = [UITabBarItem appearance];
    item.titlePositionAdjustment = UIOffsetMake(0, 1.5);
    
    //Normal状态
    [item setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:[UIFont systemFontSize]],NSForegroundColorAttributeName:[UIColor colorWithRed:0.62f green:0.62f blue:0.63f alpha:1.00f]} forState:UIControlStateNormal];
    
    //选中状态
    [item setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:[UIFont systemFontSize]],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"714FFA"]} forState:UIControlStateSelected];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addChildViewController:[HomeViewController new] imageName:@"tabBar_Home" title:@"Home"];
    [self addChildViewController:[KDirectoryViewController new] imageName:@"tabBar_Directory" title:@"Directory"];
    [self addChildViewController:[KCCTVViewController new] imageName:@"tabBar_CCTV" title:@"CCTV"];
    [self addChildViewController:[KDoorAccessoryViewController new] imageName:@"tabBar_DoorAccess" title:@"Door Access"];
}

-(void)addChildViewController:(UIViewController *)childController imageName:(NSString*)imageName title:(NSString*)title{
    KNavigationController* navi = [[KNavigationController alloc]initWithRootViewController:childController];
    navi.tabBarItem.title = title;
    navi.tabBarItem.image = [UIImage imageNamed:imageName];
    navi.tabBarItem.selectedImage = [[UIImage imageNamed:[NSString stringWithFormat:@"%@_sel",imageName]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self addChildViewController:navi];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
