//
//  AppContast.h
//  Keping
//
//  Created by 柯平 on 2017/6/17.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - const

/*占位图*/
extern NSString* const KImagePlaceholder;
//图片路径
extern NSString* const KImageURL;
/*获取图片*/
extern NSString* const KGetImageURL;



#pragma mark - 环信

/*API Key*/
extern NSString* const KHyApiKey;

/*Client ID*/
extern NSString* const KHyClientID;

/*Client Secret*/
extern NSString* const KHyClientSecret;

/*push name*/
extern NSString* const KHyPushCerName;



#pragma mark  - 服务器地址
/*服务器地址*/
UIKIT_EXTERN NSString* const KBaseURL;

#pragma mark - 用户

/*验证码获取*/
UIKIT_EXTERN NSString* const KGetSMSCodeURL;

/*登录*/
UIKIT_EXTERN NSString* const KLoginURL;

/*注册*/
UIKIT_EXTERN NSString* const KRegisterURL;

/*找回密码*/
UIKIT_EXTERN NSString* const KForgetPwdURL;

/*更新密码*/
extern NSString* const KUpdatePwdURL;//api/biz/ou/v1/partys/updatePwd.htm

/*更新用户信息*/
extern NSString* const KUpdateprofileURL;


/*实名认证*/
UIKIT_EXTERN NSString* const KUserVeritifyURL;

/*更新用户信息成功*/
extern NSString* const KUpdateUserInfoSuccessNotification;

/*定位权限发生变化*/
extern NSString* const KLocationAuthorizedStatusChangedNotification;


#pragma mark - 关于我们

/*用户协议*/
UIKIT_EXTERN NSString* const KUserProtocolURL;

/*关于我们*/
UIKIT_EXTERN NSString* const KAboutUsURL;

/*获取当前版本信息*/
extern NSString* const KVersionURL;

/*T&C / Privacy 和 FAQ*/
extern NSString* const KGetSomeInfoURL;

/*检查新版本*/
extern NSString* const KCheckVersionURL;

/*T&C / Privacy & FAQ*/
extern NSString* const KTCPrivacyFAQURL;


#pragma mark - CCTV

/*修改用户cctv*/
UIKIT_EXTERN NSString* const KUpdateUserCCTVURL;

/*用户cctv列表*/
UIKIT_EXTERN NSString* const KUserCCTVsURL;


#pragma mark - Coounity
/*公告详情*/
UIKIT_EXTERN NSString* const KAnnouncementDetailURL;

/*添加动态评论*/
UIKIT_EXTERN NSString* const KAddCommunityStatusCommentURL;

/*根据条件获取社区*/
UIKIT_EXTERN NSString* const KGetCommunityURL;

/*添加动态*/
UIKIT_EXTERN NSString* const KAddCommunityStatusURL;

/*添加社区反馈*/
UIKIT_EXTERN NSString* const KAddCommunityFeedbackURL;

/*点赞*/
UIKIT_EXTERN NSString* const KPraiseURL;

/*社区公告*/
UIKIT_EXTERN NSString* const KAnnouncementsURL;

/*社区认证*/
UIKIT_EXTERN NSString* const KCommunityVeritifyURL;

/*获取社区用户推荐信*/
UIKIT_EXTERN NSString* const KCommunityRecommandLetterURL;

/*场馆预定*/
extern NSString* const KPCategoriesURL;
/*订场*/
extern NSString* const KPSuperstarBookURL;
/*预定列表*/
extern NSString* const KPSuperstarBookListURL;

/*上传图片*/
extern NSString* const KUploadImageURL;//api/biz/gl/upload/upLoadImages.htm

/*预定详情*/
extern NSString* const KFacilitiesBookDetailURL;// /api/biz/ou/v1//shopBooks/{get/bookId}.htm

/*商家详情*/
extern NSString* const KShopDetailURL;

/*认证社区列表*/
extern NSString* const KVertifiedCommunitiesURL;

#pragma mark - Directory

/*directory列表*/
extern NSString* const KDirectoryDataURL;

/*Directory详情*/
extern NSString* const KDirectoryDetailURL;

/*Directory轮播图*/
extern NSString* const KDirectoryPhotosURL;

#pragma mark - chat

/*contacts*/
extern NSString* const KChatContactsURL;

/*history*/
extern NSString* const KChatHistoryURL;

/*上传手机联系人*/
extern NSString* const KUploadContactsURL;

/*sos列表*/
extern NSString* const KSOSsURL;

/*添加SOS联系人*/
extern NSString* const KAddSOSURL;

/*删除sos联系人*/
extern NSString* const KDeleteSOSURL;

/*请求添加好友*/
extern NSString* const KAddFriendURL;

/*同意添加好友*/
extern NSString* const KAgreeAddURL;

/*搜索好友*/
extern NSString* const KSearchFriendURL;

/*点击聊天回调*/
extern NSString* const KChatClickBlockURL;

/*获取好友*/
extern NSString* const KGetFriendsURL;

/*获取好友详情*/
extern NSString* const KGetFriendInfoURL;

#pragma mark - 社区

/*公告、动态点赞*/
extern NSString* const KPraiseURL;

/*公告或动态评论*/
extern NSString* const KAddCommentURL;

/*公告或动态评论列表*/
extern NSString* const KCommentsURL;

/*公告或动态评论的评论*/
extern NSString* const KCommentOfCommentObjURL;

/*公告或动态评论的评论列表*/
extern NSString* const KCommentsOfCommentObjURL;

/*公告或动态评论的评论回复*/
extern NSString* const KReplayOfCommentObjURL;

/*公告或动态评论详情*/
extern NSString* const KCommentsDetailURL;

/*公告详情*/
extern NSString* const KNoticeDetailURL;

/*发布动态*/
extern NSString* const KPublishStatusURL;

/*开锁与解锁*/
extern NSString* const KLockOrNotURL;

/*我的门禁卡列表*/
extern NSString* const KDoorCardsURL;

/*根据条件获取社区*/
extern NSString* const KGetIfCommunitiesURL;

/*添加社区动态*/
extern NSString* const KAddCommunityStatusURL;

/*添加社区反馈*/
extern NSString* const KAddCommunityFeedbackURL;

/*获取社区动态列表*/
extern NSString* const KCommunityStatusURL;

/*获取社区用户私信*/
extern NSString* const KCommunityPrivateLetterURL;

/*门禁卡开门记录*/
extern NSString* const KDoorLockHistoryURL;

/*动态提醒列表*/
extern NSString* const KNotifiesURL;//























