//
//  NSObject+BoardManage.h
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (BoardManage)

//应用程序代理
-(AppDelegate*)appDelegate;

//切换到跟视图到登录界面
-(void)changeRootToLoginViewController;

//切换到主界面
-(void)changeRootToHomeViewController;

//自动登录
-(void)appAutoLogin;

//退出登录
-(void)userChooseLogout;

//用户是否已经登录
-(BOOL)isUserLogin;

//网络状态

//当前用户


//几个根视图控制器跳转
-(void)changeToRootOfIndex:(NSInteger)index;

//常用跳转




@end
