/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */


#import "EaseBubbleView+Location.h"

@implementation EaseBubbleView (Location)

- (void)layoutSubviews {
    [super layoutSubviews];
}

#pragma mark - private

- (void)_setupLocationBubbleConstraints
{
    [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(self.margin.left);
        make.top.equalTo(self).offset(7);
        make.right.equalTo(self).offset(-10);
        make.height.mas_equalTo(16);
    }];
    
    [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.locationLabel);
        make.top.equalTo(self.locationLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(12);
    }];
    
    [self.locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.top.equalTo(self.addressLbl.mas_bottom).offset(7);
    }];
}

#pragma mark - public

- (void)setupLocationBubbleView
{
    self.locationLabel = [[UILabel alloc] init];
    [self.backgroundImageView addSubview:self.locationLabel];
    
    self.addressLbl = [[UILabel alloc] init];
    self.addressLbl.textColor = kColor_word_gray_2;
    self.addressLbl.font = [UIFont systemFontOfSize:11];
    [self.backgroundImageView addSubview:self.addressLbl];
    
    self.locationImageView = [[UIImageView alloc] init];
    self.locationImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.locationImageView.clipsToBounds = YES;
    [self.backgroundImageView addSubview:self.locationImageView];
    
    [self _setupLocationBubbleConstraints];
}

- (void)updateLocationMargin:(UIEdgeInsets)margin
{
    _margin = margin;
    
    [self.locationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(self.margin.left);
        make.top.equalTo(self).offset(7);
        make.right.equalTo(self).offset(-10);
        make.height.mas_equalTo(16);
    }];
}

@end
