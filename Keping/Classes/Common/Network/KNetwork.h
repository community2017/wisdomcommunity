//
//  KNetwork.h
//
//  Copyright (c) 2012-2016 YTKNetwork https://github.com/yuantiku
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


//ReadMe：
//1.copy自猿题库大神;
//2.Copyright (c) 2012-2016 YTKNetwork https://github.com/yuantiku
//3.新增参数集签名处理
//4.新增GET、POST两个最常用的请求子类，如有其它请求，如HEAD、PUT、DELETE等也可以，仿照着添加。使用时只需要设置URL和参数集即可

#import <Foundation/Foundation.h>

#ifndef _KNETWORK_
    #define _KNETWORK_

#if __has_include(<KNetwork/KNetwork.h>)

    FOUNDATION_EXPORT double KNetworkVersionNumber;
    FOUNDATION_EXPORT const unsigned char KNetworkVersionString[];

    #import <KNetwork/KRequest.h>
    #import <KNetwork/KBaseRequest.h>
    #import <KNetwork/KNetworkAgent.h>
    #import <KNetwork/KBatchRequest.h>
    #import <KNetwork/KBatchRequestAgent.h>
    #import <KNetwork/KChainRequest.h>
    #import <KNetwork/KChainRequestAgent.h>
    #import <KNetwork/KNetworkConfig.h>

#else

    #import "KRequest.h"
    #import "KBaseRequest.h"
    #import "KNetworkAgent.h"
    #import "KBatchRequest.h"
    #import "KBatchRequestAgent.h"
    #import "KChainRequest.h"
    #import "KChainRequestAgent.h"
    #import "KNetworkConfig.h"

#endif /* __has_include */

#endif /* _KNETWORK_ */
