//
//  KPostRequest.h
//  ChiKe
//
//  Created by Kevin on 2017/4/18.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KRequest.h"

@interface KPostRequest : KRequest

@property(nonatomic,copy)NSString* url;

@end
