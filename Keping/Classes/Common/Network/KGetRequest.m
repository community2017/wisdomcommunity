//
//  KGetRequest.m
//  ChiKe
//
//  Created by Kevin on 2017/4/18.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KGetRequest.h"

@implementation KGetRequest

-(id)requestArgument{
    return self.params;
}

-(KRequestMethod)requestMethod{
    return KRequestMethodGET;
}

-(NSTimeInterval)requestTimeoutInterval{
    return [[KAppConfig sharedConfig] requestTimeInterval];
}

-(NSString *)requestUrl{
    return self.url;
}

-(void)setUrl:(NSString *)url{
    if (!url || ![url isNotBlank]) return;
    _url = url;
}

@end
