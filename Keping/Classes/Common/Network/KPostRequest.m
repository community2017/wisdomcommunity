//
//  KPostRequest.m
//  ChiKe
//
//  Created by Kevin on 2017/4/18.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KPostRequest.h"

@implementation KPostRequest

-(id)requestArgument{
    return self.params;
}

-(KRequestMethod)requestMethod{
    return KRequestMethodPOST;
}

-(NSTimeInterval)requestTimeoutInterval{
    return [[KAppConfig sharedConfig] requestTimeInterval];
}

-(void)setUrl:(NSString *)url{
    if (!url || ![url isNotBlank]) return;
    _url = url;
}

@end
