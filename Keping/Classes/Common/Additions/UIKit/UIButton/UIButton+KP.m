//
//  UIButton+KP.m
//  Keping
//
//  Created by 柯平 on 2017/6/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "UIButton+KP.h"

@implementation UIButton (KP)

+(UIButton *)commonButtonWithTitle:(NSString *)title
{
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateNormal];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"button_back_disable"] forState:UIControlStateDisabled];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateHighlighted];
    btn.clipsToBounds = YES;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return btn;
}
- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIButton imageWithColor:backgroundColor] forState:state];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (void)setupImageWithImageName:(NSString *)imageName title:(NSString *)title{
    [self setupImageWithImageName:imageName title:title offset:10.0f];
}


- (void)setupImageWithImageName:(NSString *)imageName title:(NSString *)title offset:(CGFloat)offset{
    [self setTitle:title forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    self.titleEdgeInsets = UIEdgeInsetsZero;
    self.imageEdgeInsets = UIEdgeInsetsZero;
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0,
                                            -self.imageView.frame.size.width,
                                            -self.imageView.frame.size.height - offset,
                                            0);
    // 由于iOS8中titleLabel的size为0，使用intrinsicContentSize
    self.imageEdgeInsets = UIEdgeInsetsMake(-self.titleLabel.intrinsicContentSize.height - offset,
                                            0,
                                            0,
                                            -self.titleLabel.intrinsicContentSize.width);
}

- (void)setupRightImageWithImageName:(NSString *)imageName leftTitle:(NSString *)title offset:(CGFloat)offset state:(UIControlState)state
{
    [self setTitle:title forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:imageName] forState:state];
    
    self.titleEdgeInsets = UIEdgeInsetsZero;
    self.imageEdgeInsets = UIEdgeInsetsZero;
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0,
                                            -self.imageView.frame.size.width - offset,
                                            0,
                                            self.imageView.frame.size.width);
    self.imageEdgeInsets = UIEdgeInsetsMake(0,
                                            self.titleLabel.intrinsicContentSize.width,
                                            0,
                                            -self.titleLabel.intrinsicContentSize.width - offset);
    
}

- (void)showBadgeWithValue:(NSInteger)value textColor:(UIColor *)color badgeColor:(UIColor *)bgColor
{
    [self clearBadge];
    
    if (value == 0) return;
    UILabel *badgeView = [[UILabel alloc]init];
    badgeView.textAlignment = NSTextAlignmentCenter;
    badgeView.layer.cornerRadius = 6;
    badgeView.layer.masksToBounds = YES;
    badgeView.backgroundColor = bgColor;
    badgeView.textColor = color;
    badgeView.font = [UIFont systemFontOfSize:9];
    badgeView.tag = 1000;
    if (value > 99) {
        badgeView.text = [NSString stringWithFormat:@"99+"];
    }
    badgeView.text = [NSString stringWithFormat:@"%ld",(long)value];
    [self addSubview:badgeView];
    
    [badgeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageView.mas_right).offset(-7);
        make.top.equalTo(self.imageView.mas_top).offset(-5);
        make.height.mas_equalTo(12);
        make.width.mas_greaterThanOrEqualTo(12);
    }];
    
    if (value >= 10 && value <= 99) {
        [badgeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(16);
        }];
    }else if (value > 99) {
        [badgeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(21);
        }];
    }
}

- (void)showBadgeWithValue:(NSInteger)value
{
    [self showBadgeWithValue:value textColor:[UIColor whiteColor] badgeColor:[UIColor redColor]];
}

- (void)clearBadge
{
    for (UIView *subView in self.subviews) {
        
        if (subView.tag == 1000) {
            
            [subView removeFromSuperview];
            
        }
    }
}


@end
