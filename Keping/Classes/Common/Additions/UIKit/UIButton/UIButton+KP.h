//
//  UIButton+KP.h
//  Keping
//
//  Created by 柯平 on 2017/6/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (KP)

+(UIButton*)commonButtonWithTitle:(NSString*)title;

/**
 设置按钮不同状态下的背景色

 @param backgroundColor 背景颜色
 @param state 按钮状态
 */
- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;

/** 默认图片与文字上下间隙为10.0f */
- (void )setupImageWithImageName:(NSString *)imageName title:(NSString *)title;

/** 根据自己button的大小，设置图片与文字上下间隙 */
- (void)setupImageWithImageName:(NSString *)imageName title:(NSString *)title offset:(CGFloat)offset;

/** 根据自己button的大小，设置图片与文字左右间隙 */
- (void)setupRightImageWithImageName:(NSString *)imageName leftTitle:(NSString *)title offset:(CGFloat)offset state:(UIControlState)state;

- (void)showBadgeWithValue:(NSInteger)value textColor:(UIColor *)color badgeColor:(UIColor *)bgColor;
- (void)showBadgeWithValue:(NSInteger)value;
- (void)clearBadge;

@end
