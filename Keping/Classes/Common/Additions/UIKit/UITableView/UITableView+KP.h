//
//  UITableView+KP.h
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MJRefreshNormalHeader;
@protocol RefreshProtocol;

@interface UITableView (KP)

/**/
@property(nonatomic,assign)NSInteger pageNo;
/**/
@property(nonatomic,assign)NSInteger pageSize;
/**/
@property(nonatomic,strong)MJRefreshNormalHeader* refreshHeader;
/**/
@property(nonatomic,strong)NSArray* dataList;

-(void)addHeaderWithTarget:(id<RefreshProtocol>)target beginRefre:(BOOL)beginRefresh;
-(void)addFooterWithTarget:(id<RefreshProtocol>)target;
-(void)setRefresDataSource:(NSArray*)dataSource;


@end

@protocol RefreshProtocol <NSObject>

@optional
-(void)tableViewRefreshHandle:(UITableView*)tableView;

@end
