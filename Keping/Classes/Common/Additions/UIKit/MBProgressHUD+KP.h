//
//  MBProgressHUD+KP.h
//  Keping
//
//  Created by 柯平 on 2017/8/21.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (KP)

+(void)showError:(NSError*)error;
+(void)showError:(NSError*)error inView:(UIView*)view;
+(void)showErrorText:(NSString*)text;
+(void)showErrorText:(NSString*)text inView:(UIView*)view;

+(void)showSuccessText:(NSString*)text;
+(void)showSuccessText:(NSString*)text inView:(UIView*)view;

+(void)showLoading;
+(void)showLoadingInView:(UIView*)view;
+(void)showLoading:(NSString*)text;
+(void)showLoading:(NSString *)text timeout:(NSTimeInterval)timeout;
+(void)showLoading:(NSString*)text inView:(UIView*)view;
+(void)showLoading:(NSString*)text inView:(UIView*)view timeout:(NSTimeInterval)timeout;

+(void)showMsg:(NSString*)msg;
+(void)showMsg:(NSString *)msg inView:(UIView*)view;
+(void)showMsg:(NSString*)msg icon:(NSString*)icon inView:(UIView*)view;

+(void)hideHud;
+(void)hideHudInView:(UIView*)view;

@end
