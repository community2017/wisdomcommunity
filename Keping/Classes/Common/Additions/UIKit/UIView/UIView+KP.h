//
//  UIView+KP.h
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (KP)


/**
 custom appear aniamtion
 */
-(void)appear;

/**
 Custom set view disappear animation

 @param selector custom selector
 */
-(void)disappearWithSelector:(SEL)selector;

/**
 custom set view corner

 @param corners corner
 @param radius corner size
 */
-(void)setRoundedCorners:(UIRectCorner)corners radius:(CGFloat)radius;


/**
 find the first responder

 @return first responder
 */
-(UIView*)findFirstResponser;

/*添加点击事件*/
- (void)setTapActionWithBlock:(void (^)(void))block;


@end
