//
//  UIBarButtonItem+KP.h
//  Keping
//
//  Created by 柯平 on 2017/6/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (KP)

+ (UIBarButtonItem *)initWithImage:(NSString *)imageName;// wihtSel:(SEL)sel;

+ (UIBarButtonItem *)initWithImage:(NSString *)imageName withName:(NSString *)name;// wihtSel:(SEL)sel;

+ (UIBarButtonItem *)initWithImage:(NSString *)imageName
                          withName:(NSString *)name
                         customSet:(void(^)(UIButton *btn))setBlock;

+ (UIBarButtonItem *)itemWithImage:(NSString *)imageName
                          withName:(NSString *)name
                            target:(id)target
                            action:(SEL)action;

@end
