//
//  MBProgressHUD+KP.m
//  Keping
//
//  Created by 柯平 on 2017/8/21.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "MBProgressHUD+KP.h"

@implementation MBProgressHUD (KP)

+(void)showError:(NSError*)error
{
    [self showError:error inView:nil];
}
+(void)showError:(NSError*)error inView:(UIView*)view
{
    [self showMsg:error.localizedDescription?:@"Unknown Error" icon:@"error_hud" inView:view];
}

+(void)showErrorText:(NSString*)text
{
    [self showErrorText:text inView:nil];
}
+(void)showErrorText:(NSString*)text inView:(UIView*)view
{
    [self showMsg:text icon:@"error_hud" inView:view];
}

+(void)showSuccessText:(NSString*)text
{
    [self showSuccessText:text inView:nil];
}
+(void)showSuccessText:(NSString*)text inView:(UIView*)view
{
    [self showMsg:text icon:@"success_hud" inView:view];
}

+(void)showLoading
{
    [self showLoadingInView:nil];
}
+(void)showLoadingInView:(UIView*)view
{
    [self showLoading:nil inView:view];
}
+(void)showLoading:(NSString*)text
{
    [self showLoading:text inView:nil];
}
+(void)showLoading:(NSString *)text timeout:(NSTimeInterval)timeout
{
    [self showLoading:text inView:nil timeout:timeout];
}
+(void)showLoading:(NSString*)text inView:(UIView*)view
{
    [self showLoading:text inView:view timeout:45];
}

+(void)showLoading:(NSString *)text inView:(UIView*)view timeout:(NSTimeInterval)timeout
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view?:[UIApplication sharedApplication].keyWindow animated:YES];
    if ([text isNotBlank]) {
        hud.label.numberOfLines = 0;
        hud.label.text = text;
    }
    hud.mode = MBProgressHUDModeIndeterminate;
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    [hud showAnimated:YES];
    [hud hideAnimated:YES afterDelay:timeout];
}

+(void)showMsg:(NSString *)msg
{
    [self showMsg:msg inView:nil];
}

+(void)showMsg:(NSString *)msg inView:(UIView *)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view?:[UIApplication sharedApplication].keyWindow animated:YES];
    if ([msg isNotBlank]) {
        hud.label.numberOfLines = 0;
        hud.detailsLabel.text = msg;
    }
    hud.mode = MBProgressHUDModeText;
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    [hud showAnimated:YES];
    [hud hideAnimated:YES afterDelay:2.5];
}

+(void)showMsg:(NSString *)msg icon:(NSString *)icon inView:(UIView *)view
{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.bezelView.color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    hud.label.text = msg;    // 设置图片
    hud.contentColor = [UIColor whiteColor];
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icon]];
    hud.backgroundView.color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1.25秒之后再消失
    [hud hideAnimated:YES afterDelay:1.75];
}

+(void)hideHud
{
    [self hideHudInView:nil];
}
+(void)hideHudInView:(UIView *)view
{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    NSMutableArray *huds = [NSMutableArray array];
    NSArray *subviews = view.subviews;
    for (UIView *aView in subviews) {
        if ([aView isKindOfClass:[MBProgressHUD class]]) {
            [huds addObject:aView];
        }
    }
    for (MBProgressHUD *hud in huds) {
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES];
    }
}

@end
