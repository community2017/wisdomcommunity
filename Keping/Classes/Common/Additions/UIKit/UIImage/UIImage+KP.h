//
//  UIImage+KP.h
//  Keping
//
//  Created by 柯平 on 2017/6/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (KP)
/*!
 @abstract      获取一个可拉伸的UIImage
 @param         imageName  图片名称
 @result        UIImage autorelease的对象
 */
+ (UIImage *)streImageNamed:(NSString *)imageName;
+ (UIImage *)streImageNamed:(NSString *)imageName capX:(CGFloat)x capY:(CGFloat)y;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

/**
 根据字符串生成二维码图片

 @param string 字符串
 @param size 图片大小
 @return 二维码图片
 */
+ (UIImage *)generateCodeImageWithString:(NSString*)string withSize:(CGSize)size;

+ (UIImage *)generateUIImageWithCIImage:(CIImage*)ciImage withSize:(CGSize)size;

/**
 *  @brief 取路径下的视频一帧做缩略图
 *
 *  @param videoURL <#videoURL description#>
 *
 *  @return <#return value description#>
 */
+(UIImage *)VideoThumbImage:(NSURL *)videoURL;



/**
 *  @brief 图片原比例缩放处理
 *
 *  @param image <#image description#>
 *  @param scale <#scale description#>
 *
 *  @return <#return value description#>
 */
+ (UIImage *)ThumbnailWithImage:(UIImage *)image
                          scale:(CGFloat)scale;


/**
 *  @brief  截图指定view成图片
 *
 *  @param view 一个view
 *
 *  @return 图片
 */
+ (UIImage *)captureWithView:(UIView *)view;



//可定制截图区域
+ (UIImage *)getImageWithSize:(CGRect)myImageRect FromImage:(UIImage *)bigImage;


/**
 *  @brief 添加水印
 *
 *  @param image     <#image description#>
 *  @param maskImage <#maskImage description#>
 *  @param rect      <#rect description#>
 *
 *  @return <#return value description#>
 */
+ (UIImage *)addImage:(UIImage *)image
         addMsakImage:(UIImage *)maskImage
             maskRect:(CGRect)rect;





/**
 *  @brief KMargin参数很重要，不能乱调
 *
 *  @param void <#void description#>
 *
 *  @return <#return value description#>
 */
#define KMargin 5
#define KCornRadius 7
#define KTriangleWidth 10
#define KTriangleSpace 15
/**
 *  @brief 绘制聊天气泡：可绘制图片
 *
 *  @param image    <#image description#>
 *  @param rect     <#rect description#>
 *  @param isSender <#isSender description#>
 */
+ (void)drawImage:(UIImage *)image atFrame:(CGRect)rect isSender:(BOOL)isSender;




+ (UIImage *)imageWithColor:(UIColor *)color;

@end
