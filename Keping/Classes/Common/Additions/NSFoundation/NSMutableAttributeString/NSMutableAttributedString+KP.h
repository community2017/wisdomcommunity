//
//  NSMutableAttributedString+KP.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString (KP)

/**
 根据约束的宽度来求高度
 */
-(CGFloat)heightWithConstrainedWidth:(CGFloat)width;

+(NSMutableAttributedString*)imageStringWithFontSize:(CGFloat)fontSize image:(UIImage*)image;

@end
