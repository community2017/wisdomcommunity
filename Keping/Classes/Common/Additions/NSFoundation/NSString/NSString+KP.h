//
//  NSString+KP.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (KP)

#pragma mark - Date&Time&Constellation

/**
 根据日期计算星座
 */
-(NSString*)constellationOfDate;


#pragma mark - App

/**
 应用名称
 */
+(NSString*)appName;
/**
 用用版本号
 */
+(NSString*)appShortVersionString;

/**
 用用版本号
 */
+(NSString*)appVersion;

#pragma mark - Size/Rect

/**
 计算文本高度
 
 @param font 字体
 @param width 约束宽度
 */
-(CGFloat)heightWithFont:(UIFont*)font constrainedToWidth:(CGFloat)width;

/**
 计算文本宽度
 
 @param font 字体
 @param height 约束高度
 */
-(CGFloat)widthWithFont:(UIFont*)font constrainedToHeight:(CGFloat)height;

/**
 计算文本大小
 
 @param font 字体
 @param width 约束高度
 */
-(CGSize)sizeWithFont:(UIFont*)font constrainedToWidth:(CGFloat)width;

/**
 计算文本大小
 
 @param font 字体
 @param height 约束高度
 */
-(CGSize)sizeWithFont:(UIFont*)font constrainedToHeight:(CGFloat)height;

/**
 计算文本大小
 
 @param font 字体
 @param size 约束区域
 */
-(CGRect)rectWithFont:(UIFont*)font constrainedToSize:(CGSize)size;

#pragma mark - StringOpeartion

/**
 反转字符串
 
 @param string 需要反转的字符串
 @return 反转后的字符串
 */
-(NSString*)reverseString:(NSString*)string;

#pragma mark - Veritify
/** 邮箱验证 */
- (BOOL)isValidEmail;

/** 手机号码验证 */
- (BOOL)isValidPhoneNum;

/** 车牌号验证 */
- (BOOL)isValidCarNo;

/** 网址验证 */
- (BOOL)isValidUrl;

/** 邮政编码 */
- (BOOL)isValidPostalcode;

/** 纯汉字 */
- (BOOL)isValidChinese;

/**
 @brief     是否符合IP格式，xxx.xxx.xxx.xxx
 */
- (BOOL)isValidIP;

/** 身份证验证 refer to http://blog.csdn.net/afyzgh/article/details/16965107*/
- (BOOL)isValidIdCardNum;

/**
 @brief     是否符合最小长度、最长长度，是否包含中文,首字母是否可以为数字
 @param     minLenth 账号最小长度
 @param     maxLenth 账号最长长度
 @param     containChinese 是否包含中文
 @param     firstCannotBeDigtal 首字母不能为数字
 @return    正则验证成功返回YES, 否则返回NO
 */
- (BOOL)isValidWithMinLenth:(NSInteger)minLenth
                   maxLenth:(NSInteger)maxLenth
             containChinese:(BOOL)containChinese
        firstCannotBeDigtal:(BOOL)firstCannotBeDigtal;

/**
 @brief     是否符合最小长度、最长长度，是否包含中文,数字，字母，其他字符，首字母是否可以为数字
 @param     minLenth 账号最小长度
 @param     maxLenth 账号最长长度
 @param     containChinese 是否包含中文
 @param     containDigtal   包含数字
 @param     containLetter   包含字母
 @param     containOtherCharacter   其他字符
 @param     firstCannotBeDigtal 首字母不能为数字
 @return    正则验证成功返回YES, 否则返回NO
 */
- (BOOL)isValidWithMinLenth:(NSInteger)minLenth
                   maxLenth:(NSInteger)maxLenth
             containChinese:(BOOL)containChinese
              containDigtal:(BOOL)containDigtal
              containLetter:(BOOL)containLetter
      containOtherCharacter:(NSString *)containOtherCharacter
        firstCannotBeDigtal:(BOOL)firstCannotBeDigtal;

/** 去掉两端空格和换行符 */
- (NSString *)stringByTrimmingBlank;

/** 去掉html格式 */
- (NSString *)removeHtmlFormat;

/** 工商税号 */
- (BOOL)isValidTaxNo;

/**
 汉字转化为拼音
 
 @param isSpace 每个汉字的拼音之间加上空格
 @return 拼音
 */
-(NSString*)hanziConvertToPinYinIsSpaceInclude:(BOOL)isSpace;


/**
 获取每组汉字的首字母（一般用于tableview的section）
 
 @return 大写首字母
 */
-(NSString*)getFirstLetterOfHanzi;


/**
 银行卡
 
 @return 银行卡发卡行
 */
-(NSString*)getBankCardName;

/**
 银行卡有效性校验
 */
-(BOOL)isValidBankCard;


/**
 url字符串处理，主要是对url中特殊字符进行转译
 */
-(NSString*)urlStringHandle;

/**
 获取IP地址
 */
+(NSString*)ipAddress;

/**
 获取随机数
 
 @param digit 随机数的位数
 @return 随机数
 */
+(NSString*)randomNumWithDigit:(NSInteger)digit;


+(NSString*)timeStringWithFormat:(NSString*)format;

-(NSString*)stringEncodeByGBK;

-(NSString*)stringDecodeByGBK;

@end
