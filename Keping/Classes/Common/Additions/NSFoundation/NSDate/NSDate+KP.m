//
//  NSDate+KP.m
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "NSDate+KP.h"

@implementation NSDate (KP)

+(NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)formatString
{
    NSDateFormatter* format = [[NSDateFormatter alloc] init];
    [format setDateFormat:formatString];
    NSDate* date = [format dateFromString:string];
    return date;
}

+(NSString *)stringFormDate:(NSDate *)date withFormat:(NSString *)formatString
{
    NSDateFormatter* format = [[NSDateFormatter alloc] init];
    [format setDateFormat:formatString];
    NSString* dateString = [format stringFromDate:date];
    return dateString;
}

+(NSInteger)daysFormDate:(NSDate *)formDate toDate:(NSDate *)toDate
{
    NSCalendar* chineseClendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSUInteger unitFlags = NSCalendarUnitDay;
    
    NSDateComponents *gaps = [chineseClendar components:unitFlags fromDate:formDate toDate:toDate  options:0];
    return [gaps day];
}

@end
