//
//  NSDate+KP.h
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (KP)

/**
 字符串转日期

 @param string 日期字符串
 @param formatString 日期格式
 @return 日期
 */
+(NSDate*)dateFromString:(NSString*)string withFormat:(NSString*)formatString;


/**
 日期格式转字符串

 @param date 日期
 @param formatString 日期格式
 @return 字符串日期
 */
+(NSString*)stringFormDate:(NSDate*)date withFormat:(NSString*)formatString;

/**
 计算两个日期之间的天数

 @param formDate 起始日期
 @param toDate 终止日期
 @return 天数
 */
+(NSInteger)daysFormDate:(NSDate*)formDate toDate:(NSDate*)toDate;


@end
