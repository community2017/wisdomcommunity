//
//  NSObject+KP.h
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KUserInfo;

@interface NSObject (KP)

//应用程序代理
-(AppDelegate*)appDelegate;

//当前用户
-(KUserInfo*)user;

@end
