//
//  NSFileManager+KP.m
//  SunningEBuy
//
//  Created by 柯平 on 2017/6/7.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "NSFileManager+KP.h"

@implementation NSFileManager (KP)

+(NSURL*)URLsDriectory:(NSSearchPathDirectory)directory
{
    return [self.defaultManager URLsForDirectory:directory inDomains:NSUserDomainMask].lastObject;
}
+(NSString*)pathsDirectory:(NSSearchPathDirectory)directory
{
    return NSSearchPathForDirectoriesInDomains(directory, NSUserDomainMask, YES).firstObject;
}

+(NSURL *)documentsURL
{
    return [self URLsDriectory:NSDocumentDirectory];
}
+(NSURL *)libraryURL
{
    return [self URLsDriectory:NSLibraryDirectory];
}
+(NSURL *)cachesURL
{
    return [self URLsDriectory:NSCachesDirectory];
}

+(NSString *)documentsPath
{
    return [self pathsDirectory:NSDocumentDirectory];
}
+(NSString *)libraryPath
{
    return [self pathsDirectory:NSLibraryDirectory];
}
+(NSString *)cachesPath
{
    return [self pathsDirectory:NSCachesDirectory];
}

+(NSString *)customPathWithDirectory:(NSSearchPathDirectory)directory isProcessInfo:(BOOL)isProcessInfo customPath:(NSString *)customPath
{
    NSString* path = [self pathsDirectory:directory];
    NSString* filePath = @"";
    if (isProcessInfo) {
        filePath = [[[path stringByAppendingPathComponent:[[NSProcessInfo processInfo] processName]] stringByAppendingPathComponent:customPath] copy];
    }else{
        filePath = [[path stringByAppendingPathComponent:customPath] copy];
    }
    
    BOOL isDir = YES;
    BOOL isExist = [self.defaultManager fileExistsAtPath:filePath isDirectory:&isDir];
    if (!isExist) {
        [self.defaultManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:NULL error:NULL];
    }
    return filePath;
}
+(NSString *)customPathWithDirectory:(NSSearchPathDirectory)directory isProcessInfo:(BOOL)isProcessInfo customPath:(NSString *)customPath fileName:(NSString *)fileName fileType:(NSString *)fileType
{
    NSString* filePath = [self customPathWithDirectory:directory isProcessInfo:isProcessInfo customPath:customPath];
    return [filePath stringByAppendingString:[NSString stringWithFormat:@"%@.%@",fileName?:@"",fileType?:@""]];
}

+(BOOL)saveObject:(id)object withName:(NSString *)fileName
{
    NSString* filePath = [self customPathWithDirectory:NSCachesDirectory isProcessInfo:NO customPath:fileName fileName:nil fileType:@".archive"];
    return [NSKeyedArchiver archiveRootObject:object toFile:filePath];
}
+(id)getObjectByFileName:(NSString *)fileName
{
    NSString* filePath = [self customPathWithDirectory:NSCachesDirectory isProcessInfo:NO customPath:fileName fileName:nil fileType:@".archive"];
    return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}
+(BOOL)removeObjectByFileName:(NSString *)fileName
{
    NSString* filePath = [self customPathWithDirectory:NSCachesDirectory isProcessInfo:NO customPath:fileName fileName:nil fileType:@".archive"];
    NSError* error;
    BOOL success = [self.defaultManager removeItemAtPath:filePath error:&error];
    if (!success) {
        NSLog(@"Error：%@",error.localizedDescription);
    }
    return success;
}

@end
