//
//  NSFileManager+KP.h
//  SunningEBuy
//
//  Created by 柯平 on 2017/6/7.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (KP)

+(NSURL*)documentsURL;
+(NSURL*)libraryURL;
+(NSURL*)cachesURL;

+(NSString*)documentsPath;
+(NSString*)libraryPath;
+(NSString*)cachesPath;


/**
 create custom file path
 
 @param directory NSSearchPathDirectory
 @param isProcessInfo if add processInfo
 @param customPath custom path
 @return custom file path
 */
+(NSString*)customPathWithDirectory:(NSSearchPathDirectory)directory isProcessInfo:(BOOL)isProcessInfo customPath:(NSString*)customPath;

/**
 create custom file path with file name and file type
 
 @param directory NSSearchPathDirectory
 @param isProcessInfo if add processInfo
 @param customPath custom path
 @param fileName file name
 @param fileType file type
 @return file path
 */
+(NSString*)customPathWithDirectory:(NSSearchPathDirectory)directory isProcessInfo:(BOOL)isProcessInfo customPath:(NSString*)customPath fileName:(NSString*)fileName fileType:(NSString*)fileType;


/**
 archive object
 
 @param object the object to be archived
 @param fileName file name
 @return is archive success
 */
+(BOOL)saveObject:(id)object withName:(NSString*)fileName;

/**
 unarchive object by file name
 
 @param fileName file name
 @return object
 */
+(id)getObjectByFileName:(NSString*)fileName;

/**
 delete object with file name
 
 @param fileName file name
 @return is remove object success
 */
+(BOOL)removeObjectByFileName:(NSString*)fileName;


@end
