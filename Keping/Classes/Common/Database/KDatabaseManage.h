//
//  KDatabaseManage.h
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabaseQueue;

@interface KDatabaseManage : NSObject
{
    BOOL _isDatabaseOpened;
    NSString* _writablePath;
    FMDatabaseQueue* _databaseQueue;
}

/**/
@property(nonatomic,copy)NSString* writablePath;
/**/
@property(nonatomic,strong)FMDatabaseQueue* databaseQueue;

+(KDatabaseManage*)sharedManage;

+(void)releaseManage;

-(BOOL)isDatabaseOpen;

-(void)openDatabase;

-(void)closeDatabase;

@end
