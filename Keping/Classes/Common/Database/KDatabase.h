//
//  KDatabase.h
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB.h>

@interface KDatabase : NSObject
{
    @protected
    FMDatabaseQueue* _databaseQueue;
}

@end

@interface KDatabase ()

/**/
@property(nonatomic,strong)FMDatabaseQueue* databaseQueue;

+(void)createTablesNeeded;

@end
