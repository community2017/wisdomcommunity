//
//  KDatabase.m
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDatabase.h"
#import "KDatabaseManage.h"

@implementation KDatabase

@synthesize databaseQueue = _databaseQueue;

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.databaseQueue = [KDatabaseManage sharedManage].databaseQueue;
    }
    return self;
}

-(FMDatabaseQueue *)databaseQueue
{
    if (![[KDatabaseManage sharedManage] isDatabaseOpen]) {
        [[KDatabaseManage sharedManage] openDatabase];
        _databaseQueue = [KDatabaseManage sharedManage].databaseQueue;
        if (_databaseQueue) {
            [KDatabase createTablesNeeded];
        }
    }
    return _databaseQueue;
}

+(void)createTablesNeeded
{
    @autoreleasepool {
        FMDatabaseQueue* databaseQueue = [KDatabaseManage sharedManage].databaseQueue;
        [databaseQueue inTransaction:^(FMDatabase * _Nonnull db, BOOL * _Nonnull rollback) {
            //创建数据表
            /*info_user*/
            NSString* sql1 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS info_users (userId INTEGER PRIMARY KEY NOT NULL,account TEXT NOT NULL,password TEXT,isRemeberPwd INTEGER,phone TEXT,photo TEXT,birthday TEXT,name TEXT,nickName TEXT,email TEXT,sex INTEGER)"];

            /*info_system*/
            NSString* sql2 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS info_system (systemId INTEGER PRIMARY KEY NOT NULL,appVersion TEXT NOT NULL,osVersion TEXT NOT NULL,deviceIp TEXT NOT NULL,openUDID TEXT,isLogin INTEGER,field1 TEXT,field2 TEXT)"];
            /*info_search*/
            NSString* sql3 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS info_search (searchId INTEGER PRIMARY KEY AUTOINCREMENT,isLogin INTEGER,loginAccount TEXT,searchText TEXT NOT NULL)"];
            /*info_crash*/
            NSString* sql4 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS info_crash (crashId INTEGER PRIMARY KEY NOT NULL,isLogin INTEGER,loginAccount TEXT,crashInfo TEXT NOT NULL)"];
            
            /*browsing_history*/
            NSString* sql5 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS browse_history (historyId INTEGER PRIMARY KEY NOT NULL,productId TEXT UNIQUE,productCode TEXT,shopCode TEXT,productName TEXT,price TEXT,evaluation TEXT,cityCode TEXT,productImageUrl TEXT)"];
            /*scan_history*/
            NSString* sql6 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS scan_history (historyId INTEGER PRIMARY KEY NOT NULL,productId TEXT UNIQUE,productCode TEXT,productName TEXT,price TEXT,evaluation TEXT,productImageUrl TEXT)"];
            /*search_history*/
            NSString* sql7 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS search_history (keywordId INTEGER PRIMARY KEY NOT NULL,keyword TEXT UNIQUE)"];
            /*address_info*/
            NSString* sql8 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS address_info (addressId INTEGER PRIMARY KEY NOT NULL,countryCode TEXT,countryName TEXT,provinceCode TEXT,provinceName TEXT,cityCode TEXT,cityName TEXT,blockCode TEXT,blockName TEXT,streetCode TEXT,streetName TEXT,detailAddress TEXT)"];
            
            [db executeUpdate:sql1];
            [db executeUpdate:sql2];
            [db executeUpdate:sql3];
            [db executeUpdate:sql4];
            [db executeUpdate:sql5];
            [db executeUpdate:sql6];
            [db executeUpdate:sql7];
            [db executeUpdate:sql8];
            
        }];
    }
}

@end
