//
//  DefineConstant.h
//  Keping
//
//  Created by 柯平 on 2017/6/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef DEFINE_CONSTANT_H
#define DEFINE_CONSTANT_H 1

UIKIT_EXTERN NSString* const KButtonTextColor;

//block声明
#ifdef NS_BLOCKS_AVAILABLE
typedef void (^KBaseBlock)(void);
typedef void(^KOperationCallBackBlock)(BOOL isSuccess,NSString* errorMsg);
typedef void(^KBaseCallBackBlock)(NSError* error,NSDictionary* responseDic);
#endif

//单例创建
#undef	AS_SINGLETON
#define AS_SINGLETON( __class ) \
+ (__class *)sharedInstance;

#undef	DEF_SINGLETON
#define DEF_SINGLETON( __class ) \
+ (__class *)sharedInstance \
{ \
static dispatch_once_t once; \
static __class * __singleton__; \
dispatch_once( &once, ^{ __singleton__ = [[__class alloc] init]; } ); \
return __singleton__; \
}

#endif
