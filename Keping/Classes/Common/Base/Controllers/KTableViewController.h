//
//  KTableViewController.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KViewController.h"
#import "KTableView.h"

@interface KTableViewController : KViewController<UITableViewDataSource,UITableViewDelegate,RefreshDelegate>

@property(nonatomic,weak)KTableView* tableView;
@property(nonatomic,weak)KTableView* groupView;

@end
