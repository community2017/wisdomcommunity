//
//  KViewController.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNetwork.h"
#import "KWhiteNaviBar.h"
#import "AppDelegate.h"
#import "KAlertView.h"
#import "KActionSheet.h"

typedef NS_ENUM(NSInteger,KGetImageChannel) {
    KGetImageByCamera = 1,
    KGetImageByPhotoLibrary
};

@protocol KGetImageDelegate;

@interface KViewController : UIViewController<KRequestDelegate>

/**/
@property(nonatomic,strong)AppDelegate* app;

/**/
@property(nonatomic,strong)KWhiteNaviBar* whiteNaviBar;

/* 网络监控 */
@property(nonatomic,assign)BOOL isNetworkAvailable;

/* 滚动式图 */
@property(nonatomic,weak)UIScrollView* scrollView;

/* 列表数据 */
@property(nonatomic,assign)NSInteger page;
/**/
@property(nonatomic,assign)NSInteger totalPage;
@property(nonatomic,assign)NSInteger pageSize;
@property(nonatomic,strong)NSMutableArray* data;

@property(nonatomic,weak)id<KGetImageDelegate>imageDelegate;

/* 子控制器管理 */
-(void)addChildVc:(UIViewController*)childVc;
-(void)removeChildVc:(UIViewController*)childVc;

/* 数据请求 */
-(void)getData;


/* MB-HUD */
-(void)showLoading;
-(void)showText:(NSString*)text;
-(void)showLoading:(NSString*)loading;
-(void)showLoading:(NSString *)loading timeout:(NSTimeInterval)time;

-(void)hideHUD;

/* SVHUD */
-(void)showSuccess:(NSString*)text;
-(void)showErrorText:(NSString*)text;
-(void)showError:(NSError*)error;
-(void)showErrorText:(NSString *)text
             errCode:(NSInteger)errCode;

/* Alert */
-(void)showAlertInfo:(NSString*)info;
- (void)checkCameraAvailability:(void (^)(BOOL auth))block;
/*获取图片*/
-(void)getImageWithType:(UIImagePickerControllerSourceType)sourceType;

-(BOOL)checkUserLoginOrNot;

@end

@protocol KGetImageDelegate <NSObject>

-(void)didGetImage:(UIImage*)image;


@end
