//
//  KTableViewController.m
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewController.h"
#import "KTableViewCell.h"

@interface KTableViewController ()

@end

@implementation KTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = false;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 懒加载
-(KTableView *)tableView{
    if (!_tableView) {
        KTableView* tableView = [[KTableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectZero];
        tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.rowHeight = 44.0;
        tableView.sectionFooterHeight = 0.0;
        tableView.sectionHeaderHeight = 0.0;
        tableView.refreshProtocol = self;
        tableView.refreshEnable = YES;
        tableView.loadmoreEnable = YES;
        [self.view addSubview:tableView];
        _tableView = tableView;
    }
    return _tableView;
}

-(KTableView *)groupView{
    if (!_groupView) {
        KTableView* groupView = [[KTableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        groupView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectZero];
        groupView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
            //groupView.contentInset = UIEdgeInsetsMake(-34, 0, 0, 0);
        groupView.separatorInset = UIEdgeInsetsZero;
        groupView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
        groupView.rowHeight = 44.0;
        groupView.sectionFooterHeight = 0.0;
        groupView.sectionHeaderHeight = 0.0;
        groupView.delegate = self;
        groupView.dataSource = self;
        groupView.refreshProtocol = self;
        groupView.refreshEnable = YES;
        groupView.loadmoreEnable = YES;
        [self.view addSubview:groupView];
        _groupView = groupView;
    }
    return _groupView;
}


#pragma mark - UITableViewDelegate/UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
#pragma mark - RefreshProtocol  //留给子类实现
-(void)refresh{
    self.page = 1;
    
}
-(void)loadmore{
    self.page ++;
    
}

-(void)getData{
    [super getData];
}



@end
