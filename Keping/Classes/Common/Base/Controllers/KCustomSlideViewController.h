//
//  KCustomSlideViewController.h
//  NeiHan
//
//  Created by Kevin on 2017/3/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol KCustomSlideDelegate,KCustomSlideDataSource;

@interface KCustomSlideViewController : UIViewController
@property(nonatomic,weak)id<KCustomSlideDelegate>delegate;
@property(nonatomic,weak)id<KCustomSlideDataSource>dataSource;

@property(nonatomic,assign)NSInteger selectedIndex;

-(void)reloadData;

@end

@protocol KCustomSlideDelegate <NSObject>

@optional

/** 滚动偏移量*/
-(void)customSlideViewController:(KCustomSlideViewController*)slideViewController slideOffset:(CGPoint)slideOffset;
-(void)customSlideViewController:(KCustomSlideViewController *)slideViewController slideIndex:(NSInteger)slideIndex;

@end
@protocol KCustomSlideDataSource <NSObject>
/** 子控制器数量*/
-(NSInteger)numberOfChildViewControllersInSlideViewController:(KCustomSlideViewController*)slideViewController;
/** 子控制器*/
-(UIViewController*)slideViewController:(KCustomSlideViewController*)slideViewController viewControllerAtIndex:(NSInteger)index;

@end
