//
//  KClearNavigationController.m
//  Keping
//
//  Created by 柯平 on 2017/6/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KClearNavigationController.h"

@interface KClearNavigationController ()

@end

@implementation KClearNavigationController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.translucent = YES;
    //compact：及时更新背景样式,让导航条彻底透明，不显示底部那条线。 maskToBounds：不让透明图片影响状态栏
    
    self.navigationBar.layer.masksToBounds = YES;
}

#pragma mark -推出另外一个控制器的逻辑控制
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    //设置所有界面的背景图片
    //UIImageView * backImageView = [UIImageView ff_imageViewWithImageName:@"Rectangle 363"];
    
    //[viewController.view insertSubview:backImageView atIndex:0];
    
    //[backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
    //    make.edges.equalTo(viewController.view);
        
    //}];
    
    if (self.childViewControllers.count > 0) {
        // 设置返回按钮
        //viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem ff_barButtonWithTitle:@"" imageName:@"Chevron" target:self action:@selector(goBack)];
        viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:@"black_back" withName:@"" target:self action:@selector(cmdBack)];
    }
    
    [super pushViewController:viewController animated:animated];
}

-(void)cmdBack
{
    [self popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
