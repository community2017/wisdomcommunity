//
//  KWebViewController.h
//  NeiHan
//
//  Created by Kevin on 2017/3/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KViewController.h"

@interface KWebViewController : KViewController

@property(nonatomic,copy)NSString* html;
@property(nonatomic,copy)NSString* requestUrl;
@property(nonatomic,copy)NSURL*    url;
@property(nonatomic,strong)NSURLRequest* request;



@end
