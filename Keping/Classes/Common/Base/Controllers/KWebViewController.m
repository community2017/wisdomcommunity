//
//  KWebViewController.m
//  NeiHan
//
//  Created by Kevin on 2017/3/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KWebViewController.h"
#import <WebKit/WebKit.h>

@interface KWebViewController ()<UIWebViewDelegate,WKUIDelegate,WKNavigationDelegate>

@property(nonatomic,strong)WKWebView* webView;
@property(nonatomic,strong)UIView* progrssView;


@end

@implementation KWebViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_webView) {
        [_webView reload];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    WKWebViewConfiguration *webConfig = [[WKWebViewConfiguration alloc] init];
    webConfig.preferences = [[WKPreferences alloc] init];
    webConfig.preferences.minimumFontSize = 10;
    webConfig.preferences.javaScriptEnabled = YES;
    webConfig.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    webConfig.processPool = [[WKProcessPool alloc] init];
    _webView = [[WKWebView alloc]initWithFrame:self.view.bounds configuration:webConfig];
    _webView.backgroundColor = [UIColor clearColor];
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.height -= 64;
    [self.view addSubview:_webView];
    
    [_webView addObserver:self forKeyPath:@"loading" options:NSKeyValueObservingOptionNew context:nil];
    [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"loading"]) {
    }else if ([keyPath isEqualToString:@"title"]){
        self.title = _webView.title;
    }else if ([keyPath isEqualToString:@"estimatedProgress"]){
        double progress = _webView.estimatedProgress;
        [UIView animateWithDuration:0.01 animations:^{
            self.progrssView.alpha = 1.0;
        } completion:nil];
        self.progrssView.width = kScreenWidth*progress;
        if (_webView.estimatedProgress == 1.0) {
            [self hideProgress];
        }
    }
    if (!_webView.loading) {
        [self hideProgress];
    }
}

-(void)hideProgress
{
    [UIView animateWithDuration:0.2 animations:^{
        self.progrssView.alpha = 0.0;
    } completion:nil];
}

-(void)dealloc
{
    _webView.UIDelegate = nil;
    _webView.navigationDelegate = nil;
    _webView.scrollView.delegate = nil;
    [_webView removeObserver:self forKeyPath:@"loading" context:nil];
    [_webView removeObserver:self forKeyPath:@"title" context:nil];
    [_webView removeObserver:self forKeyPath:@"estimatedProgress" context:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setRequest:(NSURLRequest *)request{
    if (!request) return;
    _request = request;
    
    [self.webView loadRequest:request];
}
-(void)setUrl:(NSURL *)url{
    if (!url) return;
    _url = url;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}
-(void)setRequestUrl:(NSString *)requestUrl{
    if (![requestUrl isNotBlank] || ![requestUrl isValidUrl]) return;
    _requestUrl = requestUrl;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]]];
}
-(void)setHtml:(NSString *)html{
    if (![html isNotBlank]) return;
    _html = html;
    
    [self.webView loadHTMLString:html baseURL:[NSURL URLWithString:@""]];
}

-(UIView *)progrssView
{
    if (!_progrssView) {
        _progrssView = [UIView new];
        [_progrssView setFrame:CGRectMake(0, 0, CGFLOAT_MIN, 3)];
        [self.view addSubview:_progrssView];
        _progrssView.backgroundColor = kGreenColor;
        [self.view bringSubviewToFront:_progrssView];
    }
    return _progrssView;
}


#pragma UIWebViewDelegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

#pragma mark - WKDelegate
-(void)webViewDidClose:(WKWebView *)webView{
    
}
-(void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
}
-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
}
-(void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    
}
-(void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    
}
-(void)webView:(WKWebView *)webView commitPreviewingViewController:(UIViewController *)previewingViewController{
    
}

@end
