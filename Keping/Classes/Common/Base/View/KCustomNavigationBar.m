//
//  KCustomNavigationBar.m
//  ChiKe
//
//  Created by Kevin on 2017/4/8.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KCustomNavigationBar.h"

@interface KCustomNavigationBar ()

@property(nonatomic,weak)UIImageView* shadowImageView;
@property(nonatomic,weak)UIView* line;



@end

@implementation KCustomNavigationBar

-(instancetype)init{
    return [self initWithFrame:CGRectMake(0, 0,kScreenWidth,64)];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
    }
    return self;
}


-(void)config{
    [self.shadowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-0.2);
        make.height.mas_equalTo(0.1);
    }];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    if (self.leftButton) {
        [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(5);
            make.width.mas_equalTo(44);
            make.height.mas_equalTo(34);
            make.top.mas_equalTo(25);
        }];
    }
    if (self.titleView) {
        [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(54);
            make.right.mas_equalTo(-54);
            make.height.mas_equalTo(34);
            make.top.mas_equalTo(25);
        }];
    }
    
}

-(UIView *)line{
    if (!_line) {
        UIView* li = [UIView new];
        li.backgroundColor = kGrayColor;
        [self addSubview:li];
        _line = li;
    }
    return _line;
}
-(UIImageView *)shadowImageView{
    if (!_shadowImageView) {
        UIImageView* imv = [[UIImageView alloc]initWithImage:[UIImage imageWithColor:kWhiteColor]];
        [self addSubview:imv];
        _shadowImageView = imv;
    }
    return _shadowImageView;
}


-(void)setTitleView:(UIView *)titleView{
    if (!titleView) return;
    _titleView = titleView;
    [self addSubview:titleView];
}

-(void)setLeftButton:(UIButton *)leftButton{
    if (!leftButton) return;
    _leftButton = leftButton;
    [self addSubview:leftButton];
    __weak typeof(self) weak_self = self;
    [leftButton addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        if ([weak_self.delegate respondsToSelector:@selector(didClickleftButton)]) {
            [weak_self.delegate didClickleftButton];
        }
    }];
//    [leftButton setTapActionWithBlock:^{
//        if ([weak_self.delegate respondsToSelector:@selector(didClickleftButton)]) {
//            [weak_self.delegate didClickleftButton];
//        }
//    }];
}



@end
