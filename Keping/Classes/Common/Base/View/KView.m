//
//  KView.m
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KView.h"

@implementation KView

-(instancetype)init{
    if (self == [super init]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

-(UIView *)contentView{
    if (!_contentView) {
        UIView* contentView = [[UIView alloc]init];
        contentView.backgroundColor = [UIColor clearColor];
        [self addSubview:contentView];
        _contentView = contentView;
    }
    return _contentView;
}

-(UIView *)backgroundView{
    if (!_backgroundView) {
        UIView* backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor clearColor];
        [self addSubview:backgroundView];
        _backgroundView = backgroundView;
    }
    return _backgroundView;
}

@end
