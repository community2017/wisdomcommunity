//
//  KWhiteNaviBar.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KWhiteNaviBar.h"



@implementation KWhiteNaviBar


-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = kWhiteColor;
        [self.backBtn addTarget:self action:@selector(popBackAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

-(UIButton *)backBtn
{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _backBtn.frame = CGRectMake(7, 27, 30, 30);
        [_backBtn setImage:[UIImage imageNamed:@"black_back"] forState:UIControlStateNormal];
        _backBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 5,5, 5);
        _backBtn.tintColor = [UIColor colorWithRed:0.42f green:0.33f blue:0.27f alpha:1.00f];
        
        [self addSubview:_backBtn];
    }
    return _backBtn;
}

-(void)popBackAction
{
    if (self.KClickBack) {
        self.KClickBack(YES);
    }
}

@end
