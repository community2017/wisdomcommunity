//
//  KSelectTitleView.h
//  Keping
//
//  Created by 柯平 on 2017/10/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSelectTitleView : UIView

//-(instancetype)initWithItems:(NSArray*)items;
/**/
@property(nonatomic,strong)UIColor* itemTextColor;
/**/
@property(nonatomic,strong)UIColor* itemSelectTextColor;
/**/
@property(nonatomic,strong)UIColor* itemBackColor;
/**/
@property(nonatomic,strong)UIColor* itemSelectBackColor;



@end
