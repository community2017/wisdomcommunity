//
//  KActionSheet.h
//  NeiHan
//
//  Created by Kevin on 2017/3/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

#define KActionSheetTag 111111
@class KActionSheet;

typedef void (^KActionSheetClickBlock)(KActionSheet* sheet,NSInteger clickIndex,NSString* title);
@interface KActionSheet : UIView

+(instancetype)actionSheetWithTitle:(NSString*)title titleMessage:(NSString*)titleMessage cancelTitle:(NSString*)cancelTitle otherTitles:(NSString*)otherTitle,... NS_REQUIRES_NIL_TERMINATION;

-(void)show;

@end
