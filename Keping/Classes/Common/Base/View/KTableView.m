//
//  KTableView.m
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableView.h"
#import "MJRefresh.h"

@implementation KTableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        [self config];
    }
    return self;
}

-(void)config{
    self.delaysContentTouches = NO;
    self.canCancelContentTouches = YES;
    self.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    // Remove touch delay (since iOS 8)
    UIView *wrapView = self.subviews.firstObject;
    // UITableViewWrapperView
    if (wrapView && [NSStringFromClass(wrapView.class) hasSuffix:@"WrapperView"]) {
        for (UIGestureRecognizer *gesture in wrapView.gestureRecognizers) {
            // UIScrollViewDelayedTouchesBeganGestureRecognizer
            if ([NSStringFromClass(gesture.class) containsString:@"DelayedTouchesBegan"] ) {
                gesture.enabled = NO;
                break;
            }
        }
    }
}

-(BOOL)touchesShouldCancelInContentView:(UIView *)view{
    if ([view isKindOfClass:[UIControl class]]) {
        return YES;
    }
    return [super touchesShouldCancelInContentView:view];
}

-(void)setRefreshEnable:(BOOL)refreshEnable{
    _refreshEnable = refreshEnable;
    if (refreshEnable) {
        self.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            if ([self.refreshProtocol respondsToSelector:@selector(refresh)]) {
                [self.refreshProtocol refresh];
            }
        }];
    }else{
        if (self.mj_header) {
            self.mj_header = nil;
            [self.mj_header removeFromSuperview];
        }
    }
}
-(void)setLoadmoreEnable:(BOOL)loadmoreEnable{
    _loadmoreEnable = loadmoreEnable;
    if (loadmoreEnable) {
        self.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            if ([self.refreshProtocol respondsToSelector:@selector(loadmore)]) {
                [self.refreshProtocol loadmore];
            }
        }];
    }else{
        if (self.mj_footer) {
            self.mj_footer = nil;
            [self.mj_footer removeFromSuperview];
        }
        
    }
}

-(void)endRefresh{
    if (self.mj_header) {
        if (self.mj_header.isRefreshing) {
            [self.mj_header endRefreshing];
        }
    }
    if (self.mj_footer) {
        if (self.mj_footer.isRefreshing) {
            [self.mj_footer endRefreshing];
        }
    }
    [self reloadData];
}
-(void)beginRefresh{
    if (self.mj_header) {
        [self.mj_header beginRefreshing];
    }
}

@end
