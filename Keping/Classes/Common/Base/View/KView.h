//
//  KView.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KView : UIView

@property(nonatomic,weak)UIView* contentView;
@property(nonatomic,weak)UIView* backgroundView;

@end
