//
//  KTitleItem.m
//  NeiHan
//
//  Created by Kevin on 2017/3/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTitleItem.h"

@interface KTitleItem ()

@property(nonatomic,weak)UIButton* item;
@property(nonatomic,weak)UIView*   line;

@end

@implementation KTitleItem

-(instancetype)init{
    self = [super init];
    
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.item mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.line.mas_top);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(2);
    }];
}

-(UIView *)line{
    if (!_line) {
        UIView* line = [[UIView alloc]init];
        [self addSubview:line];
        _line = line;
    }
    return _line;
}
-(UIButton *)item{
    if (!_item) {
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:button];
        [button addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
        _item = button;
    }
    return _item;
}

-(void)clickButton{
    if (self.ClickTitle) {
        self.ClickTitle();
    }
}

-(void)setSelected:(BOOL)selected{
    _selected = selected;
    if (selected) {
        self.item.selected = YES;
        [self.item setTitleColor:self.selectColor forState:UIControlStateSelected];
        self.item.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.line.backgroundColor = self.selectColor;
        self.line.hidden = NO;
    }else{
        self.item.selected = NO;
        [self.item setTitleColor:self.normalColor forState:UIControlStateNormal];
        self.item.titleLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        self.line.backgroundColor = self.normalColor;
        self.line.hidden = YES;
    }
}

-(void)setSelectColor:(UIColor *)selectColor{
    _selectColor = selectColor;
}

-(void)setTitle:(NSString *)title{
    [self.item setTitle:title forState:UIControlStateNormal];
}

@end
