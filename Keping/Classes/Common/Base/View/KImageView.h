//
//  KImageView.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. 
//  copy from YYKit Copyright (c) 2015 ibireme. All rights reserved.
//  自定义ImageView基类，可以方便快速的在不同框架之间切换

#import <UIKit/UIKit.h>

typedef void (^ImageCompletionBlock) (UIImage * image,NSURL *url,NSError * error,NSInteger receivedSize, NSInteger expectedSize);

@interface KImageView : UIView

@property (nonatomic, weak) UILabel *label;
//@property (nonatomic, weak) UIView * contentView;
@property (nonatomic, weak) UIImageView* imageView;
@property (nonatomic, assign) UIViewContentMode contentMode;

@property(nonatomic,assign)BOOL showProgress;//default YES
@property(nonatomic,assign)BOOL showIndicator;  //default NO
@property(nonatomic,assign)BOOL showTip;    //default YES

-(void)setImageURL:(NSURL*)imageURL;
-(void)setImageURL:(NSURL *)imageURL placeholder:(UIImage*)placeholder;
-(void)setImageURL:(NSURL *)imageURL placeholder:(UIImage *)placeholder completion:(ImageCompletionBlock)completion;

@end

