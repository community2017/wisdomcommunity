//
//  KTitleItem.h
//  NeiHan
//
//  Created by Kevin on 2017/3/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTitleItem : UIView

@property(nonatomic,strong)UIColor* selectColor;
@property(nonatomic,strong)UIColor* normalColor;
@property(nonatomic,assign)BOOL     selected;
@property(nonatomic, copy)NSString* title;

@property(nonatomic,copy)void (^ClickTitle)();

@end
