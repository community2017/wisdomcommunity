//
//  KPickerView.h
//  ChiKe
//
//  Created by Kevin on 2017/5/2.
//  Copyright © 2017年 keping. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KCityModel;

typedef NS_ENUM(NSInteger,KPickerViewStyle) {
    KPickerViewStyleNormal = 0,
    KPickerViewStyleDate,
    KPickerViewStyleArea
};

@interface KPickerView : UIView

/**/
@property(nonatomic,copy)void (^addressBlock)(KCityModel* province,KCityModel* city,KCityModel* block);
-(void)showInView:(UIView*)view;



@end
