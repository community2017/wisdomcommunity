//
//  KActionSheet.m
//  NeiHan
//
//  Created by Kevin on 2017/3/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KActionSheet.h"

#define RowHeight 44.0f

@interface KActionSheetTitleView : KView
@property(nonatomic,copy)NSString* title;
@property(nonatomic,copy)NSString* message;
@end
@interface KActionSheetTitleView ()
@property(nonatomic,weak)UILabel* titleLb;
@property(nonatomic,weak)UILabel* messageLb;
@end
@implementation KActionSheetTitleView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.title = self.title ?:@"";
    self.message = self.message ?:@"";
    
    //title
    CGFloat titleX = 15;
    CGFloat titleY = self.title ? 10 : 0;
    CGFloat titleW = kScreenWidth - 30;
    CGFloat titleH = self.title ? 44 : 0;
    self.titleLb.frame = CGRectMake(titleX, titleY, titleW, titleH);
    
    //message
    CGFloat msgX = 15;
    CGFloat msgY = CGRectGetMaxY(self.titleLb.frame)+10;
    CGFloat msgW = kScreenWidth - 30;
    CGFloat msgH = self.message ? [self.message heightForFont:[UIFont systemFontOfSize:12] width:kScreenWidth-30] : 0;
    self.messageLb.frame = CGRectMake(msgX, msgY, msgW, msgH);
    
}

-(UILabel *)titleLb{
    if (!_titleLb) {
        UILabel* lb = [[UILabel alloc]init];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:lb];
        _titleLb = lb;
    }
    return _titleLb;
}
-(UILabel *)messageLb{
    if (!_messageLb) {
        UILabel* lb = [[UILabel alloc]init];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:lb];
        _messageLb = lb;
    }
    return _messageLb;
}
-(void)setTitle:(NSString *)title{
    _title = title ? : @"";
}
-(void)setMessage:(NSString *)message{
    _message = message ? : @"";
}

@end

@interface KActionSheetFootView : KView
@property(nonatomic,copy)NSString* title;
@end
@interface KActionSheetFootView ()
@property(nonatomic,weak)UILabel* titleLb;
@end
@implementation KActionSheetFootView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        
    }
    return self;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.top.mas_equalTo(10);
    }];
}
-(UILabel *)titleLb{
    if (!_titleLb) {
        UILabel* lb = [[UILabel alloc]init];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:lb];
        _titleLb = lb;
    }
    return _titleLb;
}


@end

@interface KActionSheet ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSMutableArray* titles;
@property(nonatomic,weak)UIView* backgroundView;
@property(nonatomic,weak)KTableView* tableView;
@property(nonatomic,weak)KActionSheetTitleView* titleView;
@property(nonatomic,weak)KActionSheetFootView* footView;

@property(nonatomic,copy)NSString* actionTitle;
@property(nonatomic,copy)NSString* actionTitleMessage;
@property(nonatomic,copy)NSString* footTitle;

@end

@implementation KActionSheet
+(instancetype)actionSheetWithTitle:(NSString *)title titleMessage:(NSString *)titleMessage cancelTitle:(NSString *)cancelTitle otherTitles:(NSString *)otherTitle, ... NS_REQUIRES_NIL_TERMINATION{
    KActionSheet* sheet = [[KActionSheet alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    sheet.actionTitle = title?:@"";
    sheet.actionTitleMessage = titleMessage?:@"";
    sheet.footTitle = cancelTitle;
    
    NSString* subTitle;
    va_list argumentList;
    if(otherTitle){
        [sheet.titles addObject:title];
        va_start(argumentList, otherTitle);
        while((subTitle = va_arg(argumentList, id))){
            NSString* tit = [subTitle copy];
            [sheet.titles addObject:tit];
        }
        va_end(argumentList);
    }
    return sheet;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        [self backgroundView];
    }
    return self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titles.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    KTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KActionSheet className]];
    if (!cell) {
        cell = [[KTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KActionSheet className]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.text = self.titles[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(![touch.view isEqual:self.tableView]) {
        [self dismiss];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self dismissWithIndex:indexPath];
}

#pragma mark - Actions
-(void)show{
    if ([[[UIApplication sharedApplication] keyWindow] viewWithTag:KActionSheetTag]) {
        return;
    }
    
    self.backgroundView.alpha = 0.0;
    self.tableView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, [self heightOfTableView]);
    UIWindow* keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    self.tag = KActionSheetTag;
    
    [UIView animateKeyframesWithDuration:0.22 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        self.backgroundView.alpha = 1.0;
        self.tableView.transform = CGAffineTransformMakeTranslation(0, -[self heightOfTableView]);
    } completion:^(BOOL finished) {
        //
    }];
}

-(CGFloat)heightOfTableView{
    CGFloat rowHeight = 44;
    
    return rowHeight * self.titles.count + CGRectGetHeight(self.tableView.tableHeaderView.frame)+CGRectGetHeight(self.tableView.tableFooterView.frame);
}

-(void)dismiss{
    [UIView animateWithDuration:0.22f animations:^{
        self.backgroundView.alpha = 0.0;
        self.tableView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        if(finished) {
            [self removeFromSuperview];
        }
    }];
}

-(void)dismissWithIndex:(NSIndexPath*)indexPath{
    
}

#pragma mark - LazyLoad
-(NSMutableArray *)titles{
    if (!_titles) {
        _titles = [NSMutableArray array];
    }
    return _titles;
}

-(KTableView *)tableView{
    if (!_tableView) {
        KTableView* table = [[KTableView alloc]init];
        table.delegate = self;
        table.dataSource = self;
        table.scrollEnabled = NO;
        [self addSubview:table];
        _tableView = table;
    }
    return _tableView;
}
-(KActionSheetFootView *)footView{
    if (!_footView) {
        KActionSheetFootView* foot = [[KActionSheetFootView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, RowHeight)];
        _footView = foot;
    }
    return _footView;
}
-(KActionSheetTitleView *)titleView{
    if (!_titleView) {
        KActionSheetTitleView* header = [[KActionSheetTitleView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, 5)];
        _titleView = header;
    }
    return _titleView;
}
-(UIView *)backgroundView{
    if (!_backgroundView) {
        UIView* back = [[UIView alloc]initWithFrame:self.bounds];
        back.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2f];
        [self addSubview:back];
        _backgroundView = back;
    }
    return _backgroundView;
}

@end
