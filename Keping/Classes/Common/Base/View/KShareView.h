//
//  KShareView.h
//  ChiKe
//
//  Created by Kevin on 2017/4/22.
//  Copyright © 2017年 keping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KShareViewItem : NSObject

/**/
@property(nonatomic,copy)NSString* itemName;
/**/
@property(nonatomic,copy)NSString* itemImageName;

@end

@interface KShareView : UIView

@property(nonatomic,strong)NSArray* shareItems;

-(instancetype)initWithItems:(NSArray*)shareItems;
-(void)showInView:(UIView*)view;

@end
