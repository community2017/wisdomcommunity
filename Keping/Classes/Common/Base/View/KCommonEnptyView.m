//
//  KCommonEnptyView.m
//  NeiHan
//
//  Created by 柯平 on 2017/3/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCommonEnptyView.h"

@interface KCommonEnptyView ()

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *detailText;

@property (nonatomic, copy) NSMutableAttributedString *attributedText;
@property (nonatomic, copy) NSMutableAttributedString *attributedDetailText;
@property (nonatomic, copy) NSString *imageName;

@end


@implementation KCommonEnptyView

-(instancetype)initWithText:(NSString *)text detailText:(NSString *)detailText imageName:(NSString *)imageName{
    if (self = [super init]) {
        self.text = text;
        self.detailText = detailText;
        self.imageName = imageName;
    }
    return self;
}

-(instancetype)initWithAttributeText:(NSMutableAttributedString *)attributeText detailAttributeText:(NSMutableAttributedString *)detailAttributeText imageName:(NSString *)imageName{
    if (self = [super init]) {
        self.attributedText = attributeText;
        self.attributedDetailText = detailAttributeText;
        self.imageName = imageName;
    }
    return self;
}

-(void)showInView:(UIView *)view{
    if (view) {
        CGFloat selfH = 0;
        if (self.detailText || self.attributedDetailText) {
            selfH = 170;
        }else{
            selfH = 140;
        }
        [view addSubview:self];
        self.frame = CGRectMake(0, 44, view.width, selfH);
    }
}

-(void)setText:(NSString *)text{
    _text = text;
    if ([text isKindOfClass:[NSString class]]) {
        self.textLabel.text = text;
    }
}
-(void)setAttributedText:(NSMutableAttributedString *)attributedText{
    _attributedText = attributedText;
    if ([attributedText isKindOfClass:[NSAttributedString class]]) {
        self.textLabel.attributedText = attributedText;
    }
}

-(void)setDetailText:(NSString *)detailText{
    _detailText = detailText;
    if ([detailText isKindOfClass:[NSString class]]) {
        self.detailTextLabel.text = detailText;
    }
}

-(void)setAttributedDetailText:(NSMutableAttributedString *)attributedDetailText{
    _attributedDetailText = attributedDetailText;
    if ([attributedDetailText isKindOfClass:[NSAttributedString class]]) {
        self.detailTextLabel.attributedText = attributedDetailText;
    }
}

-(void)setImageName:(NSString *)imageName{
    _imageName = imageName;
    if ([imageName isKindOfClass:[NSString class]]) {
        self.topTipImageView.image = [UIImage imageNamed:imageName];
    }
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat topTipW = self.topTipImageView.image.size.width > kScreenWidth/3 ? kScreenWidth/3 : self.topTipImageView.image.size.width;
    CGFloat topTipX = kScreenWidth / 2.0 - topTipW / 2.0;
    CGFloat topTipY = 30;
    CGFloat topTipH = self.topTipImageView.image.size.height;
    self.topTipImageView.frame = CGRectMake(topTipX, topTipY, topTipW, topTipH);
    
    if (self.text) {
        CGFloat firstW = kScreenWidth - 80 * 2;
        CGFloat firstX = (kScreenWidth - firstW) / 2.0;
        CGFloat firstY = self.topTipImageView.bottom;
        CGFloat firstH = [self.text heightWithFont:self.textLabel.font constrainedToWidth:firstW];
        self.textLabel.frame = CGRectMake(firstX, firstY, firstW, firstH);
    } else if (self.attributedText) {
        CGFloat firstW = kScreenWidth - 80 * 2;
        CGFloat firstX = (kScreenWidth - firstW) / 2.0;
        CGFloat firstY = self.topTipImageView.bottom + 8;
        CGFloat firstH = [self.attributedText heightWithConstrainedWidth:firstW];
        self.textLabel.frame = CGRectMake(firstX, firstY, firstW, firstH);
    } else {
        CGFloat firstW = kScreenWidth - 80 * 2;
        CGFloat firstX = (kScreenWidth - firstW) / 2.0;
        CGFloat firstY = self.topTipImageView.bottom + 0;
        CGFloat firstH = 0;
        self.textLabel.frame = CGRectMake(firstX, firstY, firstW, firstH);
    }
    
    if (self.detailText) {
        CGFloat secondX = self.textLabel.left;
        CGFloat secondY = self.textLabel.bottom + 8;
        CGFloat secondW = self.textLabel.width;
        CGFloat secondH = [self.detailText heightWithFont:self.detailTextLabel.font constrainedToWidth:secondW];
        self.detailTextLabel.frame = CGRectMake(secondX, secondY, secondW, secondH);
    } else if (self.attributedDetailText) {
        CGFloat secondX = self.textLabel.left;
        CGFloat secondY = self.textLabel.bottom + 8;
        CGFloat secondW = self.textLabel.width;
        CGFloat secondH = [self.attributedDetailText heightWithConstrainedWidth:secondW];
        self.detailTextLabel.frame = CGRectMake(secondX, secondY, secondW, secondH);
    } else {
        CGFloat secondX = self.textLabel.left;
        CGFloat secondY = self.textLabel.bottom + 8;
        CGFloat secondW = self.textLabel.width;
        CGFloat secondH = 0;
        self.detailTextLabel.frame = CGRectMake(secondX, secondY, secondW, secondH);
    }
    
    
}


- (UILabel *)textLabel {
    if (!_textLabel) {
        UILabel *label = [[UILabel alloc] init];
        label.font = kFont(16);
        label.textColor = [UIColor darkTextColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        [self addSubview:label];
        _textLabel = label;
    }
    return _textLabel;
}

-(UILabel *)detailTextLabel{
    if (!_detailTextLabel) {
        UILabel *label = [[UILabel alloc] init];
        [self addSubview:label];
        label.font = kFont(14);
        label.textColor = kDarkGrayColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        _detailTextLabel = label;
    }
    return _detailTextLabel;
}

-(UIImageView *)topTipImageView{
    if (!_topTipImageView) {
        UIImageView *img = [[UIImageView alloc] init];
        img.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:img];
        _topTipImageView = img;
        img.layer.masksToBounds = YES;
    }
    return _topTipImageView;
}

@end
