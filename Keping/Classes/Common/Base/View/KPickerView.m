//
//  KPickerView.m
//  ChiKe
//
//  Created by Kevin on 2017/5/2.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KPickerView.h"
#import "KLocationDataModel.h"
#import "KCityModel.h"

@interface KPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic,weak)UIPickerView* pickerView;
@property(nonatomic,weak)UIButton* confirmBtn;
@property(nonatomic,weak)UIButton* cancelBtn;
@property(nonatomic,weak)UIView* contentView;
@property(nonatomic,weak)UIView* toolBar;
@property(nonatomic,assign)CGFloat contentTop;
@property(nonatomic,strong)KLocationDataModel* addressData;
@property(nonatomic,strong)NSArray* provinces;
@property(nonatomic,strong)NSMutableArray* cities;
@property(nonatomic,strong)NSMutableArray* blocks;
@property(nonatomic,strong)KCityModel* curProvince;
@property(nonatomic,strong)KCityModel* curCity;
@property(nonatomic,strong)KCityModel* curBlock;

@end

@implementation KPickerView
-(instancetype)init{
    if (self == [super init]) {
        self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        self.contentTop = self.height;
        self.toolBar.backgroundColor = COLOR(89, 89, 89);
        self.contentView.backgroundColor = [UIColor whiteColor];
            //self.backgroundColor = COLOR(187, 187, 187);
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.toolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(60);
    }];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(60);
    }];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(self.toolBar.mas_bottom);
    }];
}

#pragma mark - Actions
-(void)confirmAction{
    self.contentView.alpha = 1.0;
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.contentView.alpha = 0.0;
        self.contentView.frame = CGRectMake(0, self.height, self.width, 240);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (self.addressBlock) {
            self.addressBlock(self.curProvince,self.curCity,self.curBlock);
        }
    }];
    
}
-(void)cancelAction{
    self.contentView.alpha =1.0;
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.contentView.alpha = 0.0;
        self.contentView.frame = CGRectMake(0, self.height, self.width, 240);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}

+(BOOL)isBeingShowed:(UIView*)view{
    UIView* picker = [view viewWithTag:90808];
    return picker != nil;
}
-(void)showInView:(UIView *)view{
    UIView* content = view ? : [UIApplication sharedApplication].keyWindow;
    [content addSubview:self];
    self.tag = 90808;
    self.frame = CGRectMake(0, 0, content.width, content.height);
    self.contentView.alpha = 0.0;
    
    [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.contentView.alpha = 1.0;
        self.contentView.frame = CGRectMake(0, self.height-240, self.width, 240);
    } completion:^(BOOL finished) {
        
    }];
    
}

#pragma mark - Delegate/DataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (0 == component) {
        return self.provinces.count;
    }else if (1 == component){
        return self.cities.count;
    }else if (2 == component){
        return self.blocks.count;
    }
    return 0;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return self.width/3;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 44;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (0 == component) {
        KCityModel* province = self.provinces[row];
        return province.areaName;
    }else if (1 == component){
        KCityModel* city = self.cities[row];
        return city.areaName;
    }else if (2 == component){
        KCityModel* block = self.blocks[row];
        return block.areaName;
    }
    return @"";
}

/*  *CUSTOM*
 -(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
 return nil;
 }
 
 -(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
 return nil;
 }
 */

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    KLog(@"row:%ld\ncomponent:%ld",(long)row,(long)component);
    if (0 == component) {
        [self.cities removeAllObjects];
        [self.blocks removeAllObjects];
        self.curProvince = nil;
        self.curCity = nil;
        self.curBlock = nil;
        self.curProvince = self.provinces[row];
        [self.cities addObjectsFromArray:[self.addressData getCitiesDataWithProvince:self.curProvince]];
        if (self.cities.count > 0) {
            [self.blocks addObjectsFromArray:[self.addressData getAreasDataWithCity:self.cities[0]]];
        }
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
    }else if (1 == component){
        self.curCity = nil;
        self.curBlock = nil;
        [self.blocks removeAllObjects];
        self.curCity = self.cities[row];
        [self.blocks addObjectsFromArray:[self.addressData getAreasDataWithCity:self.curCity]];
        [pickerView reloadComponent:2];
    }else if (2 == component){
        self.curBlock = nil;
        self.curBlock = self.blocks[row];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - LazyLoad
-(KLocationDataModel *)addressData{
    if (!_addressData) {
        _addressData = [[KLocationDataModel alloc]init];
    }
    return _addressData;
}
-(NSArray *)provinces{
    if (!_provinces) {
        _provinces = [self.addressData getProviceData];
    }
    return _provinces;
}
-(NSMutableArray *)cities{
    if (!_cities) {
        _cities = [NSMutableArray array];
        if (self.provinces.count > 0) {
            self.curProvince = self.provinces[0];
            [_cities addObjectsFromArray:[self.addressData getCitiesDataWithProvince:self.curProvince]];
        }
    }
    return _cities;
}
-(NSMutableArray *)blocks{
    if (!_blocks) {
        _blocks = [NSMutableArray array];
        if (self.cities.count > 0) {
            self.curCity = self.cities[0];
            [_blocks addObjectsFromArray:[self.addressData getAreasDataWithCity:self.curCity]];
        }
        if (_blocks.count > 0) {
            self.curBlock = _blocks[0];
        }
    }
    return _blocks;
}

-(UIPickerView *)pickerView{
    if (!_pickerView) {
        UIPickerView* picker = [[UIPickerView alloc]init];
        picker.delegate = self;
        picker.dataSource = self;
        [self.contentView addSubview:picker];
        _pickerView = picker;
    }
    return _pickerView;
}
-(UIView *)contentView{
    if (!_contentView) {
        UIView* content = [[UIView alloc]initWithFrame:CGRectMake(0, self.height, self.width, 240)];
        [self addSubview:content];
        _contentView = content;
    }
    return _contentView;
}

-(UIView *)toolBar{
    if (!_toolBar) {
        UIView* tool = [[UIView alloc]init];
        [self.contentView addSubview:tool];
        _toolBar = tool;
    }
    return _toolBar;
}

-(UIButton *)confirmBtn{
    if (!_confirmBtn) {
        UIButton* confirm = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirm setTitle:@"确定" forState:UIControlStateNormal];
        [confirm setTitleColor:COLOR(0, 107, 254) forState:UIControlStateNormal];
        confirm.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [confirm addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
        [self.toolBar addSubview:confirm];
        _confirmBtn = confirm;
    }
    return _confirmBtn;
}

-(UIButton *)cancelBtn{
    if (!_cancelBtn) {
        UIButton* cancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancel setTitle:@"取消" forState:UIControlStateNormal];
        [cancel setTitleColor:COLOR(0, 107, 254) forState:UIControlStateNormal];
        cancel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [cancel addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [self.toolBar addSubview:cancel];
        _cancelBtn = cancel;
    }
    return _cancelBtn;
}

@end
