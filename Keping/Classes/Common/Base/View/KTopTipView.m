//
//  KTopTipView.m
//  NeiHan
//
//  Created by Kevin on 2017/3/27.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTopTipView.h"

@interface KTopTipView ()
@property(nonatomic,weak)UILabel* tipLabel;

@end

@implementation KTopTipView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

-(UILabel *)tipLabel{
    if (!_tipLabel) {
        UILabel* lb = [[UILabel alloc]initWithFrame:self.bounds];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:[KAppConfig sharedConfig].fontSize];
        lb.textColor = [UIColor whiteColor];
        lb.backgroundColor = [UIColor colorWithRed:1.00f green:0.49f blue:0.65f alpha:1.00f];
        [self addSubview:lb];
        _tipLabel = lb;
    }
    return _tipLabel;
}

-(void)setText:(NSString *)text{
    if (![text isNotBlank]) return;
    _text = text;
    self.tipLabel.text = text;
}

-(void)showInView:(UIView *)view{
    NSInteger time = self.tipLabel.text.length/5;
    [UIView animateWithDuration:0.4 animations:^{
        self.frame = CGRectMake(0, 0, view.width, 40);
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.4 animations:^{
                self.frame = CGRectMake(0, -40, view.width, 40);
            }];
        });
    }];
}

@end
