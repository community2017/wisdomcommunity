//
//  KAgreeButton.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KAgreeButton.h"

@implementation KAgreeButton

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.selected = NO;
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(2);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.width.height.mas_equalTo(15);
    }];
    [self.textLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imageView.mas_right).offset(2);
        make.right.mas_equalTo(-2);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
}

-(UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        _imageView.clipsToBounds = YES;
        [self addSubview:_imageView];
    }
    return _imageView;
}

-(YYLabel *)textLb
{
    if (!_textLb) {
        _textLb = [[YYLabel alloc]init];
        _textLb.textVerticalAlignment = YYTextVerticalAlignmentCenter;
        _textLb.font = [UIFont systemFontOfSize:[UIFont smallSystemFontSize]];
        [self addSubview:_textLb];
    }
    return _textLb;
}

@end
