//
//  KDataPickerView.m
//  ChiKe
//
//  Created by Kevin on 2017/4/28.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KDatePickerView.h"

@interface KDatePickerView ()
@property(nonatomic,weak)UIDatePicker* datePicker;
@property(nonatomic,weak)UIView* toolBar;
@property(nonatomic,weak)UIButton* cancelBtn;
@property(nonatomic,weak)UIButton* confirmBtn;



@end

@implementation KDatePickerView

-(instancetype)init{
    if (self == [super init]) {
        self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    }
    return self;
}

#pragma mark - Actions
-(void)popUpInView:(UIView *)view{
    UIView* content = view ?:[UIApplication sharedApplication].keyWindow;
    [content addSubview:self];
    self.frame = CGRectMake(0, 0, content.width, content.height);
    self.tag = 10808;
    
    self.contentView.alpha = 0.0;
        //self.contentView.frame = CGRectMake(0, self.height, self.width, 240);
    self.datePicker.alpha = 0.0;
    [self.cancelBtn setTitleColor:COLOR(19, 130, 255) forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:COLOR(19, 130, 255) forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.datePicker.alpha = 1.0;
        self.contentView.alpha = 1.0;
        self.contentView.frame = CGRectMake(30, self.height/2-120,self.width-60, 240);
    } completion:^(BOOL finished) {
        
    }];
}

+(BOOL)isShowing:(UIView*)view{
    UIView* picker = [view viewWithTag:10808];
    return picker != nil;
}

-(void)showInView:(UIView *)view{
    UIView* content = view ?:[UIApplication sharedApplication].keyWindow;
    [content addSubview:self];
    self.frame = CGRectMake(0, 0, content.width, content.height);
    self.tag = 10808;
    
    self.contentView.alpha = 0.0;
    self.contentView.frame = CGRectMake(0, self.height, self.width, 240);
    self.datePicker.alpha = 0.0;
    [self.cancelBtn setTitleColor:COLOR(19, 130, 255) forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:COLOR(19, 130, 255) forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.datePicker.alpha = 1.0;
        self.contentView.alpha = 1.0;
        self.contentView.frame = CGRectMake(0, self.height-240,self.width, 240);
    } completion:^(BOOL finished) {
        
    }];
}

//-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
//    
//}

-(void)dismissInView:(UIView*)superView{
    [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(0, superView.height, self.width, self.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)cancelDate{
    [UIView animateWithDuration:0.30 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(0, kScreenHeight, self.width, self.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)confirmDate{
    NSString* dateText = [self.datePicker.date stringWithFormat:@"yyyy-MM-dd"];
    [UIView animateWithDuration:0.30 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(0, kScreenHeight, self.width, self.height);
    } completion:^(BOOL finished) {
        if (self.KDatePickerBlock) {
            self.KDatePickerBlock(dateText);
        }
        [self removeFromSuperview];
    }];
}

#pragma mark - LazyLoad
-(UIView *)contentView{
    if (!_contentView) {
        UIView* content = [[UIView alloc]init];
        content.backgroundColor = COLOR(255, 255, 255);
        content.clipsToBounds = YES;
        [self addSubview:content];
        _contentView = content;
    }
    return _contentView;
}
-(UIView *)toolBar{
    if (!_toolBar) {
        UIView* bar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width, 44)];
        bar.backgroundColor = COLOR(244, 244, 246);
        [self.contentView addSubview:bar];
        _toolBar = bar;
    }
    return _toolBar;
}

-(UIButton *)cancelBtn{
    if (!_cancelBtn) {
        UIButton* cancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancel setTitle:@"取消" forState:UIControlStateNormal];
        cancel.frame = CGRectMake(15, 0, 60, self.toolBar.height);
        cancel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [cancel addTarget:self action:@selector(cancelDate) forControlEvents:UIControlEventTouchUpInside];
        [self.toolBar addSubview:cancel];
        _cancelBtn = cancel;
    }
    return _cancelBtn;
}

-(UIButton *)confirmBtn{
    if (!_confirmBtn) {
        UIButton* confirm = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirm setTitle:@"确定" forState:UIControlStateNormal];
        confirm.frame = CGRectMake(self.toolBar.width-75, 0, 60, self.toolBar.height);
        confirm.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [confirm addTarget:self action:@selector(confirmDate) forControlEvents:UIControlEventTouchUpInside];
        [self.toolBar addSubview:confirm];
        _confirmBtn = confirm;
    }
    return _confirmBtn;
}

-(UIDatePicker *)datePicker{
    if (!_datePicker) {
        UIDatePicker* picker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, self.toolBar.height, self.contentView.width, self.contentView.height-self.toolBar.height)];
        picker.datePickerMode = UIDatePickerModeDate;
        picker.locale = [NSLocale localeWithLocaleIdentifier:@"zh"];
        picker.date = [NSDate date];
        [self.contentView addSubview:picker];
        _datePicker = picker;
    }
    return _datePicker;
}

@end
