//
//  KTableView.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RefreshDelegate;

@interface KTableView : UITableView

@property(nonatomic,assign)BOOL refreshEnable;
@property(nonatomic,assign)BOOL loadmoreEnable;
@property(nonatomic,weak)id<RefreshDelegate>refreshProtocol;

-(void)beginRefresh;
-(void)endRefresh;

@end

@protocol RefreshDelegate <NSObject>

@optional

-(void)refresh;
-(void)loadmore;


@end
