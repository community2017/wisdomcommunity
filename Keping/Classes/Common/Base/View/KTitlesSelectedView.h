//
//  KTitlesSelectedView.h
//  NeiHan
//
//  Created by Kevin on 2017/3/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTitlesSelectedView : UIView

@property(nonatomic,strong)NSArray* titles;
@property(nonatomic,assign)NSInteger selectedIndex;
@property(nonatomic,assign)CGPoint contentOffset;
@property(nonatomic,copy)void (^itemClickBlock)(NSInteger index);



@end
