//
//  KWhiteNaviBar.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KView.h"

@protocol KWhiteNaviDelegate <NSObject>

-(void)popBack;

@end

@interface KWhiteNaviBar : UIView

/**/
@property(nonatomic,copy)void (^KClickBack)(BOOL isBack);
/**/
@property(nonatomic,strong)UIButton* backBtn;

/**/
@property(nonatomic,weak)id<KRequestDelegate> delegate;

@end

