//
//  KAgreeButton.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAgreeButton : UIView

/**/
@property(nonatomic,strong)UIImageView* imageView;
/**/
@property(nonatomic,strong)YYLabel* textLb;
/**/
@property(nonatomic,assign)BOOL selected;
/**/
@property(nonatomic,copy)void (^agreeButtonBlock) (BOOL clickText,NSString* text);

@end
