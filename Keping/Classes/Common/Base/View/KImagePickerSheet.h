//
//  KImagePickerSheet.h
//  ChiKe
//
//  Created by Kevin on 2017/4/29.
//  Copyright © 2017年 keping. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,KImageOperationType) {
    KImageOperationTypeSend = 1,
    KImageOperationTypeCancel,
    KImageOperationTypeCamera,
    KImageOperationTypePhotoLib
};

@interface KImagePickerSheet : NSObject

@end
