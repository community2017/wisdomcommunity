//
//  KShareView.m
//  ChiKe
//
//  Created by Kevin on 2017/4/22.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KShareView.h"
#import "FXBlurView.h"

@implementation KShareViewItem

@end

@interface KShareItemCell : UICollectionViewCell

@property(nonatomic,strong)KShareViewItem* shareItem;

@end

@interface KShareItemCell ()
@property(nonatomic,weak)UIImageView* imageView;
@property(nonatomic,weak)UILabel* titleLabel;


@end

@implementation KShareItemCell
-(instancetype)init{
    return [self initWithFrame:CGRectMake(0, 0, 60, 80)];
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.top.mas_equalTo(5);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(self.imageView.mas_bottom);
    }];
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        UILabel* lb = [[UILabel alloc]init];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = SystemSmallFont;
        lb.backgroundColor = kWhiteColor;
        [self.contentView addSubview:lb];
        _titleLabel = lb;
    }
    return _titleLabel;
}
-(UIImageView *)imageView{
    if (!_imageView) {
        UIImageView* imv = [[UIImageView alloc]init];
        imv.clipsToBounds = YES;
        imv.layer.cornerRadius = 3.0f;
        imv.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:imv];
        _imageView = imv;
    }
    return _imageView;
}

@end

@interface KShareView ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,weak)FXBlurView* blurView;
@property(nonatomic,weak)UIView* contentView;

@property(nonatomic,weak)UICollectionView* collectionView;

@end

@implementation KShareView



#pragma mark - UICollectionViewDelegate/DataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.shareItems.count;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.shareItems[section] count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    KShareItemCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[KShareItemCell className] forIndexPath:indexPath];
    cell.shareItem = self.shareItems[indexPath.section][indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(60, 80);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(0, 0);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(0, 0);
}

#pragma mark - LazyLoad
-(UIView *)contentView{
    if (!_contentView) {
        UIView* content = [[UIView alloc]init];
        content.backgroundColor = [UIColor whiteColor];
        [self addSubview:content];
        _contentView = content;
    }
    return _contentView;;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionView* collect = [[UICollectionView alloc]init];
        collect.delegate = self;
        collect.dataSource = self;
        
    }
    return _collectionView;
}


+(BOOL)isShowingInView:(UIView*)view{
    UIView* share = [view viewWithTag:910909];
    return share != nil;
}

-(void)showInView:(UIView *)view{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    if ([KShareView isShowingInView:view]) return;
    
    
}



@end
