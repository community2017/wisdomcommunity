//
//  KCommonEnptyView.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KView.h"

@interface KCommonEnptyView : KView

@property(nonatomic,weak)UIImageView* topTipImageView;
@property(nonatomic,weak)UILabel*     textLabel;
@property(nonatomic,weak)UILabel*     detailTextLabel;

-(instancetype)initWithText:(NSString*)text
                 detailText:(NSString*)detailText
                  imageName:(NSString*)imageName;

-(instancetype)initWithAttributeText:(NSMutableAttributedString*)attributeText
                 detailAttributeText:(NSMutableAttributedString*)detailAttributeText
                           imageName:(NSString*)imageName;

-(void)showInView:(UIView*)view;

@end
