//
//  KDataPickerView.h
//  ChiKe
//
//  Created by Kevin on 2017/4/28.
//  Copyright © 2017年 keping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KDatePickerView : UIView

@property(nonatomic,weak)UIView* contentView;

@property(nonatomic,copy)void (^KDatePickerBlock)(NSString* date);

-(void)showInView:(UIView*)view;    //show in the center
-(void)popUpInView:(UIView*)view;   //show in the bottom

@end
