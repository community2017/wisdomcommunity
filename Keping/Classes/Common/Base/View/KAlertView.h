//
//  KAlertView.h
//  NeiHan
//
//  Created by Kevin on 2017/4/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,KAlertViewButtonAlignment) {
    KAlertViewButtonAlignmentHorizontal = 0,
    KAlertViewButtonAlignmentVertical
};

typedef NS_ENUM(NSInteger,KAlertViewStyle) {
    KAlertViewAlert = 0,
    KAlertViewTextInput,
    KAlertViewTip
};

@interface KAlertView : UIView

-(instancetype)initWithTitle:(NSString*)title content:(NSString*)content placeholder:(NSString*)placeholder style:(KAlertViewStyle)style;
/*自定义设置确认按钮和取消按钮的标题*/
-(instancetype)initWithTitle:(NSString*)title content:(NSString*)content placeholder:(NSString*)placeholder style:(KAlertViewStyle)style confirmTitle:(NSString*)confirmTitle cancelTitle:(NSString*)cancelTitle;
-(instancetype)initWithTitle:(NSString*)title content:(NSString*)content placeholder:(NSString*)placeholder style:(KAlertViewStyle)style alignment:(KAlertViewButtonAlignment)alignment confirmTitle:(NSString*)confirmTitle cancelTitle:(NSString*)cancelTitle;

/*点击按钮回调*/
@property(nonatomic,copy)void(^ClickBlock) (BOOL isConfirm,NSString* inputText);

-(void)showInView:(UIView*)view;

@end
