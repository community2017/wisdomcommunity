//
//  KCustomNavigationBar.h
//  ChiKe
//
//  Created by Kevin on 2017/4/8.
//  Copyright © 2017年 keping. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol KCustomNavigationBarDelegate;

@interface KCustomNavigationBar : UIView

@property(nonatomic,weak)UIView* titleView;
@property(nonatomic,weak)UIButton* leftButton;
@property(nonatomic,weak)UIButton* rightButton;

@property(nonatomic,copy)void(^LeftButtonBlock)();
@property(nonatomic,weak)id<KCustomNavigationBarDelegate> delegate;
@property(nonatomic,weak)UIButton* closeBtn;

@end

@protocol KCustomNavigationBarDelegate <NSObject>

-(void)didClickleftButton;

@end
