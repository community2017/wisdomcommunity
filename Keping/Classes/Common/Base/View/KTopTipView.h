//
//  KTopTipView.h
//  NeiHan
//
//  Created by Kevin on 2017/3/27.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTopTipView : UIView

@property(nonatomic,copy)NSString* text;

-(void)showInView:(UIView*)view;

@end
