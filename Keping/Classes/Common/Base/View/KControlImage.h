//
//  KControlImage.h
//  NeiHan
//
//  Created by 柯平 on 2017/3/11.
//  Copyright © 2017年 柯平. All rights reserved.
//  自定义ImageView,可以探测用户浏览图片的手势类型

#import <UIKit/UIKit.h>


@interface KControlImage : UIView

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) void (^touchBlock)(KControlImage *view, YYGestureRecognizerState state, NSSet *touches, UIEvent *event);
@property (nonatomic, copy) void (^longPressBlock)(KControlImage *view, CGPoint point);

@end
