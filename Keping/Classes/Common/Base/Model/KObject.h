//
//  KObject.h
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//  This class is built for archive/unarchive/remove object

#import <Foundation/Foundation.h>

@interface KObject : NSObject

-(BOOL)archive;

-(BOOL)archiveWithKey:(NSString*)key;

+(instancetype)unarchive;

+(instancetype)unarchiveWithKey:(NSString*)key;

+(BOOL)remove;

+(BOOL)removeWithKey:(NSString*)key;

@end
