//
//  KObject.m
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KObject.h"
#import "NSFileManager+KP.h"

@interface KObject ()<NSCopying,NSCoding>

@end

@implementation KObject
-(BOOL)archive
{
    return [self archiveWithKey:nil];
}

-(BOOL)archiveWithKey:(NSString *)key
{
    return [NSFileManager saveObject:self withName:[NSString stringWithFormat:@"%@%@",key ?: @"",[self className]]];
}

+(instancetype)unarchive
{
    return [self unarchiveWithKey:nil];
}

+(instancetype)unarchiveWithKey:(NSString *)key
{
    return [NSFileManager getObjectByFileName:[NSString stringWithFormat:@"%@%@",key?:@"",[self className]]];
}

+(BOOL)remove
{
    return [self removeWithKey:nil];
}

+(BOOL)removeWithKey:(NSString *)key
{
    return [NSFileManager removeObjectByFileName:[NSString stringWithFormat:@"%@%@",key?:@"",[self className]]];
}

#pragma mark - NSCoding/NSCoping
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self modelEncodeWithCoder:aCoder];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    return [self modelInitWithCoder:aDecoder];
}

- (id)copyWithZone:(NSZone *)zone {
    return [self modelCopy];
}

- (NSUInteger)hash {
    return [self modelHash];
}

- (BOOL)isEqual:(id)object {
    return [self modelIsEqual:object];
}

- (NSString *)description {
    return [self modelDescription];
}

@end
