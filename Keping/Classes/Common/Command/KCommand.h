//
//  KCommand.h
//  Keping
//
//  Created by 柯平 on 2017/6/18.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Command,CommandDelegate;

#ifdef NS_BLOCKS_AVAILABLE
typedef void (^CommandCallBackBlock)(id<Command>cmd);
#endif

@protocol Command <NSObject>

/**/
@property(nonatomic,strong)NSDictionary* userInfo;
/**/
@property(nonatomic,weak)id<CommandDelegate> delegate;
/**/
@property(nonatomic,copy)CommandCallBackBlock callbackBlock;

-(void)execute;
-(void)cancel;
-(void)done;

@end

@protocol CommandDelegate <NSObject>

@optional
-(void)commandDidFinished:(id<Command>)cmd;
-(void)commandDidFailed:(id<Command>)cmd;

@end

@interface KCommand : NSObject<Command>

/**/
@property(nonatomic,strong)NSDictionary* userInfo;
/**/
@property(nonatomic,weak)id<CommandDelegate> delegate;
/**/
@property(nonatomic,copy)CommandCallBackBlock callbackBlock;

+(id)command;

-(void)execute;
-(void)cancel;
-(void)done;

@end


