//
//  KCommand.m
//  Keping
//
//  Created by 柯平 on 2017/6/18.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCommand.h"
#import "KCommandManage.h"

@implementation KCommand

-(void)dealloc
{
    [self cancel];
}

+(id)command
{
    return [[self alloc] init];
}

-(void)execute
{
    
}

-(void)cancel
{
    self.delegate = nil;
    self.callbackBlock = nil;
}

-(void)done
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([_delegate respondsToSelector:@selector(commandDidFinished:)]) {
            [_delegate commandDidFinished:self];
        }
        if (_callbackBlock) {
            _callbackBlock(self);
        }
        
        self.callbackBlock = nil;
    });
}

@end
