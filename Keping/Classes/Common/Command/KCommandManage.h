//
//  KCommandManage.h
//  Keping
//
//  Created by 柯平 on 2017/6/18.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KCommand.h"

@interface KCommandManage : NSObject

+(instancetype)sharedInstance;

/**/
@property(strong)NSMutableArray* commandQueue;

+(void)executeCommand:(id<Command>)cmd observer:(id<CommandDelegate>)observer;
+(void)executeCommand:(id<Command>)cmd completeBlock:(CommandCallBackBlock)completeBlock;

+(void)cancelCommand:(id<Command>)cmd;
+(void)cancelCommandByClass:(Class)cls;
+(void)cancelCommandByObserver:(id)observer;

+(id)isExecutingCommand:(Class)cls;

@end
