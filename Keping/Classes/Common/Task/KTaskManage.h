//
//  KTaskManage.h
//  Keping
//
//  Created by 柯平 on 2017/6/19.
//  Copyright © 2017年 柯平. All rights reserved.
//  托管Task的类，保证Task能够完全执行

#import <Foundation/Foundation.h>
#import "KTask.h"


@interface KTaskManage : NSObject

@property (readonly, strong) NSOperationQueue *taskOperationQueue;

+ (instancetype)sharedQueue;

- (void)addTask:(KTask *)task;
- (void)removeTask:(KTask *)task;
- (KTask *)containTask:(Class)taskName;
- (KTask *)containTask:(Class)taskName observer:(id)observer;
- (NSSet *)tasksForObserver:(id)observer;

@end
