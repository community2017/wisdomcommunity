//
//  KLogger.m
//  Keping
//
//  Created by 柯平 on 2017/6/17.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KLogger.h"
#import "SystemInfo.h"


@interface KLogger ()

@end

@implementation KLogger

+ (void)load
{
#ifdef DEBUG
    fprintf( stderr, "****************************************************************************************\n" );
    fprintf( stderr, "    											   \n" );
    fprintf( stderr, "    	Copyright © 2017 柯平               \n" );
    fprintf( stderr, "    	Email:xtxh@outlook.com               \n" );
    fprintf( stderr, "    	https://xtxh@bitbucket.org/xtxh                         \n" );
    fprintf( stderr, "    										       \n" );
    
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    
    fprintf( stderr, "    	Device:%s || iOS %s	\n", [SystemInfo platformString].UTF8String, [SystemInfo osVersion].UTF8String );
    fprintf( stderr, "    	ip: %s	\n", [SystemInfo localIPAddress].UTF8String );
    fprintf( stderr, "    	Home: %s	\n", [NSBundle mainBundle].bundlePath.UTF8String );
    fprintf( stderr, "    												\n" );
    fprintf( stderr, "****************************************************************************************\n" );
#endif
    
#endif
}


@end
