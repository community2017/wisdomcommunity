//
//  KLogger.h
//  Keping
//
//  Created by 柯平 on 2017/6/17.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <CocoaLumberjack.h>
//
//#ifdef DEBUG
//#define KLog(FORMAT, ...) fprintf(stderr,"%s:[Line %d]\t%s\n",[[NSString stringWithUTF8String:__PRETTY_FUNCTION__] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
//#else
//#define KLog(...)
//#endif


@interface KLogger : NSObject



@end
