//
//  IPAddress.h
//  Keping
//
//  Created by 柯平 on 2017/6/17.
//  Copyright © 2017年 柯平. All rights reserved.
//

#ifndef IPAddress_h
#define IPAddress_h

#define MAXADDRS     32

extern char *if_names[MAXADDRS];

extern char *ip_names[MAXADDRS];

extern char *hw_addrs[MAXADDRS];

extern unsigned long ip_addrs[MAXADDRS];


void InitAddresses(void);

void FreeAddresses(void);

void GetIPAddresses(void);

void GetHWAddresses(void);


#endif /* IPAddress_h */
