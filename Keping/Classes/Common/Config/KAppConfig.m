//
//  KAppConfig.m
//  NeiHan
//
//  Created by Kevin on 2017/3/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KAppConfig.h"
#import "KP_UserDefault.h"

@implementation KAppConfig

+(KAppConfig *)sharedConfig{
    static KAppConfig* config = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        config = [[self alloc]init];
    });
    return config;
}

-(void)useDefaultConfig{
    self.fontSize = [UIFont systemFontSize];
    self.fontColor = @"#333333";
    self.defaultCity = @"北京";
    self.isSavePassword = YES;
    self.isShackOn = YES;
    self.isSoundOn = YES;
    self.isNoticeOn = YES;
    self.isUseDefaultConfig = KUserSetingTypeDefault;
    self.hudType = 1;
}
/*是否启用默认设置*/
-(void)setIsUseDefaultConfig:(KUserSetingType)isUseDefaultConfig{
    [KP_UserDefault saveNSInteger:isUseDefaultConfig withKey:@"KIsUseDefaultConfigKey"];
}
-(KUserSetingType)isUseDefaultConfig{
    return [KP_UserDefault getInteger:@"KIsUseDefaultConfigKey"];
}

/*是否非第一次打开App*/
-(void)setIsNoneFirstOpen:(BOOL)isNoneFirstOpen{
    [KP_UserDefault saveBool:isNoneFirstOpen withKey:@"KIsNoneFirstOpenKey"];
}
-(BOOL)isNoneFirstOpen{
    return [KP_UserDefault getBool:@"KIsNoneFirstOpenKey"];
}

/*accessToken*/
-(void)setAccessToken:(NSString *)accessToken{
    [KP_UserDefault saveNSObject:accessToken withKey:@"KAccessTokenKey"];
}
-(NSString *)accessToken{
    return (NSString*)[KP_UserDefault getNSObject:@"KAccessTokenKey"];
}
/*openid*/
-(void)setOpenid:(NSString *)openid{
    [KP_UserDefault saveNSObject:openid withKey:@"KOpenidKey"];
}
-(NSString *)openid{
    return (NSString*)[KP_UserDefault getNSObject:@"KOpenidKey"];
}
/*refreshToken*/
-(void)setRefreshToken:(NSString *)refreshToken{
    [KP_UserDefault saveNSObject:refreshToken withKey:@"KRefreshTokenKey"];
}
-(NSString *)refreshToken{
    return (NSString*)[KP_UserDefault getNSObject:@"KRefreshTokenKey"];
}
/*expiration*/
-(void)setExpiration:(NSString *)expiration{
    [KP_UserDefault saveNSObject:expiration withKey:@"KExpirationKey"];
}
-(NSString *)expiration{
    return (NSString*)[KP_UserDefault getNSObject:@"KExpirationKey"];
}
///*expires_in*/
-(void)setExpires_in:(NSInteger)expires_in{
    [KP_UserDefault saveNSInteger:expires_in withKey:@"KExpires_inKey"];
}
-(NSInteger)expires_in{
    return [KP_UserDefault getInteger:@"KExpires_inKey"];
}
/*uid*/
-(void)setUid:(NSString *)uid{
    [KP_UserDefault saveNSObject:uid withKey:@"KUidKey"];
}
-(NSString *)uid{
    return (NSString*)[KP_UserDefault getNSObject:@"KUidKey"];
}
-(void)setReceiveMsgType:(NSUInteger)receiveMsgType{
    [KP_UserDefault saveNSInteger:receiveMsgType withKey:@"KReceiveMsgTypeKey"];
}
-(NSUInteger)receiveMsgType{
    return [KP_UserDefault getInteger:@"KReceiveMsgTypeKey"];
}

//@property(nonatomic,readwrite,copy)NSString* uid;

/*登录方式*/
-(void)setLoginType:(KLoginType)loginType{
    [KP_UserDefault saveNSInteger:loginType withKey:@"KLoginTypeKey"];
}
-(KLoginType)loginType{
    return [KP_UserDefault getInteger:@"KLoginTypeKey"];
}

-(void)setIsInfoSaved:(BOOL)isInfoSaved{
    [KP_UserDefault saveBool:isInfoSaved withKey:@"KIsUserInfoSavedKey"];
}
-(BOOL)isInfoSaved{
    return [KP_UserDefault getBool:@"KIsUserInfoSavedKey"];
}

/*网络请求超时时间*/
-(void)setRequestTimeInterval:(NSTimeInterval)requestTimeInterval{
    [KP_UserDefault saveDouble:requestTimeInterval withKey:@"KRequestTimeIntervalKey"];
}
-(NSTimeInterval)requestTimeInterval{
    return [KP_UserDefault getDouble:@"KRequestTimeIntervalKey"] > 0 ? [KP_UserDefault getDouble:@"KRequestTimeIntervalKey"] : 30.0;
}

/*sessionID*/
-(void)setSessionId:(NSString *)sessionId{
    [KP_UserDefault saveNSObject:sessionId withKey:@"KSessionIdKey"];
}
-(NSString *)sessionId{
    return (NSString*)[KP_UserDefault getNSObject:@"KSessionIdKey"];
}

/*字体大小*/
-(void)setFontSize:(CGFloat)fontSize{
    if (!fontSize) {
        fontSize = [UIFont systemFontSize];
    }
    [KP_UserDefault saveFloat:fontSize withkey:@"KFontSizeKey"];
}
-(CGFloat)fontSize{
    return [KP_UserDefault getFloat:@"KFontSizeKey"];
}

/*字体颜色*/
-(void)setFontColor:(NSString *)fontColor{
    [KP_UserDefault saveNSObject:fontColor withKey:@"KFontColorKey"];
}
-(NSString *)fontColor{
    return (NSString*)[KP_UserDefault getNSObject:@"KFontColorKey"];
}

/*是否登录*/
-(void)setLogined:(BOOL)logined{
    [KP_UserDefault saveBool:logined withKey:@"KIsLoginKey"];
}
-(BOOL)isLogined{
    return [KP_UserDefault getBool:@"KIsLoginKey"];
}

/*是否实名认证*/
-(void)setVeritify:(BOOL)veritify
{
    [KP_UserDefault saveBool:veritify withKey:@"KIsUserVeritifyKey"];
}
-(BOOL)isVeritify
{
    return [KP_UserDefault getBool:@"KIsUserVeritifyKey"];
}

/*是否是快速登录*/
-(void)setIsFast:(BOOL)isFast{
    [KP_UserDefault saveBool:isFast withKey:@"KIsFastLoginKey"];
}
-(BOOL)isFastLogin{
    return [KP_UserDefault getBool:@"KIsFastLoginKey"];
}

/*是否记住用户密码*/
-(BOOL)isRememberPassword
{
    return [KP_UserDefault getBool:@"KIsRememberUserPasswordKey"];
}
-(void)setRememberPassword:(BOOL)rememberPassword
{
    [KP_UserDefault saveBool:rememberPassword withKey:@"KIsRememberUserPasswordKey"];
}

/*用户名*/
-(void)setUsername:(NSString *)username{
    [KP_UserDefault saveNSObject:username withKey:@"KUserNameKey"];
}
-(NSString *)username{
    return (NSString*)[KP_UserDefault getNSObject:@"KUserNameKey"];
}

/*密码*/
-(void)setPassword:(NSString *)password{
    [KP_UserDefault saveNSObject:password withKey:@"KPasswordKey"];
}
-(NSString *)password{
    return (NSString*)[KP_UserDefault getNSObject:@"KPasswordKey"];
}

/*邮箱*/
-(void)setEmail:(NSString *)email{
    [KP_UserDefault saveNSObject:email withKey:@"KEmailKey"];
}
-(NSString *)email{
    return (NSString*)[KP_UserDefault getNSObject:@"KEmailKey"];
}

/*手机号*/
-(void)setPhone:(NSString *)phone{
    [KP_UserDefault saveNSObject:phone withKey:@"KPhoneKey"];
}
-(NSString *)phone{
    return (NSString*)[KP_UserDefault getNSObject:@"KPhoneKey"];
}

/*用户id*/
-(void)setUserId:(NSString *)userId{
    [KP_UserDefault saveNSObject:userId withKey:@"KUserIdKey"];
}
-(NSString *)userId{
    return (NSString*)[KP_UserDefault getNSObject:@"KUserIdKey"];
}

/*是否保存密码*/
-(void)setIsSavePassword:(BOOL)isSavePassword{
    [KP_UserDefault saveBool:isSavePassword withKey:@"KIsSavePasswordKey"];
}
-(BOOL)isSavePassword{
    return [KP_UserDefault getBool:@"KIsSavePasswordKey"];
}

/*手机是否绑定*/
-(void)setIsBindMobile:(BOOL)isBindMobile{
    [KP_UserDefault saveBool:isBindMobile withKey:@"KIsBindMobileKey"];
}
-(BOOL)isBindMobile{
    return [KP_UserDefault getBool:@"KIsBindMobileKey"];
}

/*邮箱是否激活*/
-(void)setIsEmailActivated:(BOOL)isEmailActivated{
    [KP_UserDefault saveBool:isEmailActivated withKey:@"KIsEmailActivitedKey"];
}
-(BOOL)isEmailActivated{
    return [KP_UserDefault getBool:@"KIsEmailActivitedKey"];
}

/*声音开关*/
-(void)setIsSoundOn:(BOOL)isSoundOn{
    [KP_UserDefault saveBool:isSoundOn withKey:@"KIsSoundOnKey"];
}
-(BOOL)isSoundOn{
    return [KP_UserDefault getBool:@"KIsSoundOnKey"];
}

/*震动开关*/
-(void)setIsShackOn:(BOOL)isShackOn{
    [KP_UserDefault saveBool:isShackOn withKey:@"KIsShackOnKey"];
}
-(BOOL)isShackOn{
    return [KP_UserDefault getBool:@"KIsShackOnKey"];
}

/*通知开关*/
-(void)setIsNoticeOn:(BOOL)isNoticeOn{
    [KP_UserDefault saveBool:isNoticeOn withKey:@"KIsNoticeOnKey"];
}
-(BOOL)isNoticeOn{
    return [KP_UserDefault getBool:@"KIsNoticeOnKey"];
}

/*自动登录*/
-(void)setIsAutoLoginOn:(BOOL)isAutoLoginOn{
    [KP_UserDefault saveBool:isAutoLoginOn withKey:@"KIsNoticeOnKey"];
}
-(BOOL)isAutoLoginOn{
    return [KP_UserDefault getBool:@"KIsNoticeOnKey"];
}

/*图片质量：省流量模式  0,自动 1，高清，2低清*/
-(void)setImageQuailty:(NSInteger )imageQuailty{
    [KP_UserDefault saveNSInteger:imageQuailty withKey:@"KImageQualityKey"];
}
-(NSInteger )imageQuailty{
    return [KP_UserDefault getInteger:@"KImageQualityKey"];
}

/*定位是否成功*/
-(void)setIsLocateSuccess:(BOOL)isLocateSuccess{
    [KP_UserDefault saveBool:isLocateSuccess withKey:@"KIsLocateSuccessfulKey"];
}
-(BOOL)isLocateSuccess{
    return [KP_UserDefault getBool:@"KIsLocateSuccessfulKey"];
}

/*经度*/
-(void)setLatitude:(double)latitude{
    [KP_UserDefault saveDouble:latitude withKey:@"KLatitudeKey"];
}
-(double)latitude{
    return [KP_UserDefault getDouble:@"KLatitudeKey"];
}

/*纬度*/
-(void)setLongitude:(double)longitude{
    [KP_UserDefault saveDouble:longitude withKey:@"KLongitudeKey"];
}
-(double)longitude{
    return [KP_UserDefault getDouble:@"KLongitudeKey"];
}

/*ip*/
-(void)setIpAddress:(NSString *)ipAddress{
    [KP_UserDefault saveNSObject:ipAddress withKey:@"KipAddressKey"];
}
-(NSString *)ipAddress{
    return (NSString*)[KP_UserDefault getNSObject:@"KipAddressKey"];
}

/*默认省份*/
-(void)setDefaultProvince:(NSString *)defaultProvince{
    [KP_UserDefault saveNSObject:defaultProvince withKey:@"KDefaultProvinceKey"];
}
-(NSString *)defaultProvince{
    return (NSString*)[KP_UserDefault getNSObject:@"KDefaultProvinceKey"];
}

/*收货地址*/
-(void)setShippingAddress:(NSString *)shippingAddress{
    [KP_UserDefault saveNSObject:shippingAddress withKey:@"KShippingAddressKey"];
}
-(NSString *)shippingAddress{
    return (NSString*)[KP_UserDefault getNSObject:@"KShippingAddressKey"];
}
/*家乡省份*/
-(void)setHometownProvince:(NSString *)hometownProvince{
    [KP_UserDefault saveNSObject:hometownProvince withKey:@"KHometownProvinceKey"];
}
-(NSString *)hometownProvince{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownProvinceKey"];
}
/*家乡省编码*/
-(void)setHometownProvinceCode:(NSString *)hometownProvinceCode{
    [KP_UserDefault saveNSObject:hometownProvinceCode withKey:@"KHometownProvinceCodeKey"];
}
-(NSString *)hometownProvinceCode{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownProvinceCodeKey"];
}
/*家乡城市*/
-(void)setHometownCity:(NSString *)hometownCity{
    [KP_UserDefault saveNSObject:hometownCity withKey:@"KHometownCityKey"];
}
-(NSString *)hometownCity{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownCityKey"];
}
/*家乡城市编码*/
-(void)setHometownCityCode:(NSString *)hometownCityCode{
    [KP_UserDefault saveNSObject:hometownCityCode withKey:@"KHometownCityCodeKey"];
}
-(NSString *)hometownCityCode{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownCityCodeKey"];
}
/*家乡地区*/
-(void)setHometownDistrict:(NSString *)hometownDistrict{
    [KP_UserDefault saveNSObject:hometownDistrict withKey:@"KHometownDistrictKey"];
}
-(NSString *)hometownDistrict{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownDistrictKey"];
}
/*家乡地区编码*/
-(void)setHometownDistrictCode:(NSString *)hometownDistrictCode{
    [KP_UserDefault saveNSObject:hometownDistrictCode withKey:@"KHometownDistrictCodeKey"];
}
-(NSString *)hometownDistrictCode{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownDistrictCodeKey"];
}
/*家乡地址*/
-(void)setHometownAddress:(NSString *)hometownAddress{
    [KP_UserDefault saveNSObject:hometownAddress withKey:@"KHometownAddressKey"];
}
-(NSString *)hometownAddress{
    return (NSString*)[KP_UserDefault getNSObject:@"KHometownAddressKey"];
}

/*默认城市*/
-(void)setDefaultCity:(NSString *)defaultCity{
    [KP_UserDefault saveNSObject:defaultCity withKey:@"KDefaultCityKey"];
}
-(NSString *)defaultCity{
    return (NSString*)[KP_UserDefault getNSObject:@"KDefaultCityKey"];
}

/*默认城市编码*/
-(void)setDefaultCityCode:(NSString *)defaultCityCode{
    [KP_UserDefault saveNSObject:defaultCityCode withKey:@"KDefaultCityCodeKey"];
}
-(NSString *)defaultCityCode{
    return (NSString*)[KP_UserDefault getNSObject:@"KDefaultCityCodeKey"];
}

/*默认地区*/
-(void)setDefaultBlock:(NSString *)defaultBlock{
    [KP_UserDefault saveNSObject:defaultBlock withKey:@"KDefaultBlockKey"];
}
-(NSString *)defaultBlock{
    return (NSString*)[KP_UserDefault getNSObject:@"KDefaultBlockKey"];
}

/*默认详细地址*/
-(void)setDefaultAddress:(NSString *)defaultAddress{
    [KP_UserDefault saveNSObject:defaultAddress withKey:@"KDefaultAddressKey"];
}
-(NSString *)defaultAddress{
    return (NSString*)[KP_UserDefault getNSObject:@"KDefaultAddressKey"];
}

/*用户是否登录*/
-(void)setUserLoginOrNot:(BOOL)userLoginOrNot{
    [KP_UserDefault saveBool:userLoginOrNot withKey:@"KUserLoginOrNotKey"];
}
-(BOOL)userLoginOrNot{
    return [KP_UserDefault getBool:@"KUserLoginOrNotKey"];
}

-(void)setGpsDistrict:(NSString *)gpsDistrict{
    [KP_UserDefault saveNSObject:gpsDistrict withKey:@"KGPSDistrictKey"];
}
-(NSString *)gpsDistrict{
    return (NSString*)[KP_UserDefault getNSObject:@"KGPSDistrictKey"];
}

-(void)setGpsDistrictCode:(NSString *)gpsDistrictCode{
    [KP_UserDefault saveNSObject:gpsDistrictCode withKey:@"KGPSDistrictCodeKey"];
}
-(NSString *)gpsDistrictCode{
    return (NSString*)[KP_UserDefault getNSObject:@"KGPSDistrictCodeKey"];
}

/*定位城市*/
-(void)setGpsCity:(NSString *)gpsCity{
    [KP_UserDefault saveNSObject:gpsCity withKey:@"KGPSCityKey"];
}
-(NSString *)gpsCity{
    return (NSString*)[KP_UserDefault getNSObject:@"KGPSCityKey"];
}

/*定位城市编码*/
-(void)setGpsCityCode:(NSString *)gpsCityCode{
    [KP_UserDefault saveNSObject:gpsCityCode withKey:@"KGPSCityCodeKey"];
}
-(NSString *)gpsCityCode{
    return (NSString*)[KP_UserDefault getNSObject:@"KGPSCityCodeKey"];
}

/*签到状态*/
-(void)setIsRegist:(BOOL)isRegist{
    [KP_UserDefault saveBool:isRegist withKey:@"KIsRegisteKey"];
}
-(BOOL)isRegist{
    return [KP_UserDefault getBool:@"KIsRegisteKey"];
}

/*提示框显示方式*/
-(void)setHudType:(HudShowType)hudType{
    [KP_UserDefault saveNSInteger:hudType withKey:@"KHudShowTypeKey"];
}
-(HudShowType)hudType{
    return [KP_UserDefault getInteger:@"KHudShowTypeKey"];
}

-(void)setNetworkStatus:(YYReachabilityStatus)networkStatus{
    [KP_UserDefault saveNSInteger:networkStatus withKey:@"KNetworkStatusKey"];
}
-(YYReachabilityStatus)networkStatus{
    return [KP_UserDefault getInteger:@"KNetworkStatusKey"];
}

/*定位权限*/
-(void)setLocationAuthorized:(BOOL)locationAuthorized
{
    [KP_UserDefault saveBool:locationAuthorized withKey:@"KLocationAuthorizedKey"];
}

-(BOOL)locationAuthorized
{
    return [KP_UserDefault getBool:@"KLocationAuthorizedKey"];
}

-(void)setLocationStatus:(CLAuthorizationStatus)locationStatus
{
    [KP_UserDefault saveNSInteger:locationStatus withKey:@"KCLAuthorizationStatusKey"];
}
-(CLAuthorizationStatus)locationStatus
{
    return (int)[KP_UserDefault getInteger:@"KCLAuthorizationStatusKey"];
}

/*通知权限*/
-(void)setNotificationAuthorized:(BOOL)notificationAuthorized
{
    [KP_UserDefault saveBool:notificationAuthorized withKey:@"KNotificationAuthorizedKey"];
}
-(BOOL)notificationAuthorized
{
    return [KP_UserDefault getBool:@"KNotificationAuthorizedKey"];
}
/*通讯录权限*/
-(void)setContactAuthorized:(BOOL)contactAuthorized
{
    [KP_UserDefault saveBool:contactAuthorized withKey:@"KContactsAuthorizedKey"];
}
-(BOOL)contactAuthorized
{
    return [KP_UserDefault getBool:@"KContactsAuthorizedKey"];
}

@end
