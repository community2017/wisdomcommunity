//
//  KP_UserDefault.h
//  CarManagerBao
//
//  Created by dkm on 16/11/2.
//  Copyright © 2016年 DKM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KP_UserDefault : NSObject

#pragma mark-Class UserDefaults 初始化
+(NSUserDefaults*)shareUserDefaults;

#pragma mark-Class UserDefaults 保存数据---(仅支持六种类型：Bool、Float、NSInteger、Object、Double、Url)

+(void)saveBool:(BOOL)obj withKey:(NSString*)key;

+(void)saveFloat:(float)obj withkey:(NSString*)key;

+(void)saveNSInteger:(NSInteger)obj withKey:(NSString*)key;

+(void)saveNSObject:(NSObject*)obj withKey:(NSString*)key;

+(void)saveDouble:(double)obj withKey:(NSString*)key;

+(void)saveNSURL:(NSURL*)obj withKey:(NSString*)key;


#pragma mark-Class UserDefaults 读取数据---(仅支持六中类型：Bool、Float、NSInteger、Object(id)、Double、Url)

+(BOOL)getBool:(NSString*)key;

+(float)getFloat:(NSString*)key;

+(NSInteger)getInteger:(NSString*)key;

+(NSObject*)getNSObject:(NSString*)key;

+(double)getDouble:(NSString*)key;

+(NSURL*)getNSUrl:(NSString*)key;

#pragma mark - DELETE
+(void)removeObjectForKey:(NSString*)key;

@end
