//
//  KP_UserDefault.m
//  CarManagerBao
//
//  Created by dkm on 16/11/2.
//  Copyright © 2016年 DKM. All rights reserved.
//

#import "KP_UserDefault.h"

@implementation KP_UserDefault

static NSUserDefaults* userDefault;
+(NSUserDefaults *)shareUserDefaults{
    if (!userDefault) {
        userDefault = [NSUserDefaults standardUserDefaults];
    }
    return userDefault;
}

#pragma mark-Class UserDefaults 保存数据
+(void)saveBool:(BOOL)obj withKey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] setBool:obj forKey:key];
    [[KP_UserDefault shareUserDefaults] synchronize];
}

+(void)saveFloat:(float)obj withkey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] setFloat:obj forKey:key];
    [[KP_UserDefault shareUserDefaults]synchronize];
}

+(void)saveDouble:(double)obj withKey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] setDouble:obj forKey:key];
    [[KP_UserDefault shareUserDefaults]synchronize];
}

+(void)saveNSInteger:(NSInteger)obj withKey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] setInteger:obj forKey:key];
    [[KP_UserDefault shareUserDefaults] synchronize];
}

+(void)saveNSURL:(NSURL *)obj withKey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] setURL:obj forKey:key];
    [[KP_UserDefault shareUserDefaults] synchronize];
}

+(void)saveNSObject:(NSObject *)obj withKey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] setObject:obj forKey:key];
    [[KP_UserDefault shareUserDefaults] synchronize];
}

#pragma mark-Class  UserDefaults 读取数据

+(BOOL)getBool:(NSString *)key{
    return [[KP_UserDefault shareUserDefaults] boolForKey:key];
}

+(float)getFloat:(NSString *)key{
    return [[KP_UserDefault shareUserDefaults] floatForKey:key];
}

+(double)getDouble:(NSString *)key{
    return [[KP_UserDefault shareUserDefaults] doubleForKey:key];
}

+(NSInteger)getInteger:(NSString *)key{
    return [[KP_UserDefault shareUserDefaults] integerForKey:key];
}

+(NSObject *)getNSObject:(NSString *)key{
    return [[KP_UserDefault shareUserDefaults] objectForKey:key];
}

+(NSURL *)getNSUrl:(NSString *)key{
    return [[KP_UserDefault shareUserDefaults] URLForKey:key];
}

#pragma mark - DELETE
+(void)removeObjectForKey:(NSString *)key{
    [[KP_UserDefault shareUserDefaults] removeObjectForKey:key];
    [[KP_UserDefault shareUserDefaults] synchronize];
}

@end
