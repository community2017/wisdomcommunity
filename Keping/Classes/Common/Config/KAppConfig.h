//
//  KAppConfig.h
//  NeiHan
//
//  Created by Kevin on 2017/3/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,HudShowType) {
    HudShowTypeNone     = 0,
    HudShowTypeSV       = 1,
    HudShowTypeMB       = 2,
    HudShowTypeCustom   = 3
};

typedef NS_ENUM(NSInteger,KUserSetingType) {
    KUserSetingTypeNone = 0,
    KUserSetingTypeDefault = 1,
    KUserSetingTypeCustom = 2
};

typedef NS_ENUM(NSInteger,KLoginType) {
    KLoginTypeNone = 0,             //未登录
    KLoginTypeGeneral,              //账号密码登录
    KLoginTypeFastPhone,            //使用手机号快速登录
    KLoginTypeFastEmail,            //使用邮箱账号快速登录
    KLoginTypeOAuthQQ,              //使用QQ授权登录
    KLoginTypeOAuthWX,              //使用微信授权登录
    KLoginTypeOAuthSina,            //使用新浪微博授权登录
};

@interface KAppConfig : NSObject


+(KAppConfig*) sharedConfig;

-(void) useDefaultConfig;

/*是否启用默认设置:
    0-用户未设置是否启用默认设置
    1-用户启用默认设置
    2-用户使用自定义设置
 */
@property (nonatomic, readwrite, assign)KUserSetingType isUseDefaultConfig;
/**/
@property(nonatomic,readwrite,assign)BOOL isNoneFirstOpen;

#pragma mark - SSO
/*accessToken*/
@property(nonatomic,readwrite,copy)NSString* accessToken;
/*openid*/
@property(nonatomic,readwrite,copy)NSString* openid;
/*refreshToken*/
@property(nonatomic,readwrite,copy)NSString* refreshToken;
/*expiration*/
@property(nonatomic,readwrite,copy)NSString* expiration;
/*expires_in*/
@property(nonatomic,readwrite,assign)NSInteger expires_in;
/*uid*/
@property(nonatomic,readwrite,copy)NSString* uid;

/**/
@property(nonatomic,readwrite,assign)NSUInteger receiveMsgType;
/*登录方式*/
@property(nonatomic,readwrite,assign)KLoginType loginType;
/**/
@property(nonatomic,readwrite,assign)BOOL isInfoSaved;
/*sessionID*/
@property(nonatomic,readwrite,copy)NSString* sessionId;

/*字体大小*/
@property (nonatomic, readwrite, assign) CGFloat    fontSize;
/*字体颜色*/
@property (nonatomic, readwrite, copy) NSString    *fontColor;
/*是否登录*/
@property (nonatomic, readwrite, assign,getter=isLogined) BOOL        logined;
/*是否实名认证*/
@property (nonatomic, readwrite, assign,getter=isLogined) BOOL        veritify;
/*是否是快速登录*/
@property(nonatomic,readwrite,assign,getter=isFastLogin)BOOL isFast;
/**/
@property(nonatomic,readwrite,assign,getter=isRememberPassword)BOOL rememberPassword;
/*用户名*/
@property (nonatomic, readwrite, copy) NSString    *username;
/*密码*/
@property (nonatomic, readwrite, copy) NSString    *password;
/*邮箱*/
@property (nonatomic, readwrite, copy) NSString    *email;
/*手机号*/
@property (nonatomic, readwrite, copy) NSString    *phone;
/*用户id*/
@property (nonatomic, readwrite, copy) NSString    *userId;
/*是否保存密码*/
@property (nonatomic, readwrite, assign) BOOL        isSavePassword;
/*手机是否绑定*/
@property (nonatomic, readwrite, assign) BOOL        isBindMobile;
/*邮箱是否激活*/
@property (nonatomic, readwrite, assign) BOOL    isEmailActivated;
/*声音开关*/
@property (nonatomic, readwrite, assign) BOOL    isSoundOn;
/*震动开关*/
@property (nonatomic, readwrite, assign) BOOL    isShackOn;
/*通知开关*/
@property (nonatomic, readwrite, assign) BOOL    isNoticeOn;

#pragma mark - LogIn/Out
/*自动登录*/
@property (nonatomic, readwrite, assign) BOOL    isAutoLoginOn;
/*用户是否登录*/
@property (nonatomic, readwrite, assign) BOOL    userLoginOrNot;
/*图片质量：省流量模式  0,自动 1，高清，2低清*/
@property (nonatomic, readwrite, assign) NSInteger imageQuailty;

#pragma mark - 定位&地址
/*定位是否成功*/
@property(nonatomic, readwrite, assign)BOOL isLocateSuccess;
/*经度*/
@property (nonatomic, readwrite, assign) double    latitude;
/*纬度*/
@property (nonatomic, readwrite, assign) double    longitude;
/*ip*/
@property (nonatomic, readwrite, copy) NSString    *ipAddress;
/*默认省份*/
@property (nonatomic, readwrite, copy) NSString    *defaultProvince;
/*收货地址*/
@property(nonatomic,readwrite,copy)NSString* shippingAddress;

/*家乡省份*/
@property(nonatomic,readwrite,copy)NSString* hometownProvince;
/*家乡省编码*/
@property(nonatomic,readwrite,copy)NSString* hometownProvinceCode;
/*家乡城市*/
@property(nonatomic,readwrite,copy)NSString* hometownCity;
/*家乡城市编码*/
@property(nonatomic,readwrite,copy)NSString* hometownCityCode;
/*家乡地区*/
@property(nonatomic,readwrite,copy)NSString* hometownDistrict;
/*家乡地区编码*/
@property(nonatomic,readwrite,copy)NSString* hometownDistrictCode;
/*家乡地址*/
@property(nonatomic,readwrite,copy)NSString* hometownAddress;
/*默认城市*/
@property (nonatomic, readwrite, copy) NSString    *defaultCity;
/*默认城市编码*/
@property (nonatomic, readwrite, copy) NSString    *defaultCityCode;
/*默认地区*/
@property (nonatomic, readwrite, copy) NSString    *defaultBlock;
/*默认详细地址*/
@property (nonatomic, readwrite, copy) NSString    *defaultAddress;


/*定位地区*/
@property(nonatomic,readwrite, copy)NSString* gpsDistrict;
/*定位地区编码*/
@property(nonatomic,readwrite,copy)NSString* gpsDistrictCode;
/*定位城市*/
@property (nonatomic, readwrite, copy) NSString        *gpsCity;
/*定位城市编码*/
@property (nonatomic, readwrite, copy) NSString        *gpsCityCode;

/*签到状态*/
@property (nonatomic, readwrite, assign) BOOL       isRegist;

/*HUD类型*/
@property (nonatomic, readwrite, assign) HudShowType hudType;

#pragma mark - Network
/*网络请求超时时间*/
@property(nonatomic,readwrite,assign)NSTimeInterval requestTimeInterval;
/*网络状态实时监控*/
@property(nonatomic,assign)YYReachabilityStatus networkStatus;

/*定位权限*/
@property(nonatomic,assign)BOOL locationAuthorized;
/**/
@property(nonatomic,assign)CLAuthorizationStatus locationStatus;
/*通知权限*/
@property(nonatomic,assign)BOOL notificationAuthorized;
/*通讯录权限*/
@property(nonatomic,assign)BOOL contactAuthorized;

@end
