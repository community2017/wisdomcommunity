//
//  KMacro.h
//  Keping
//
//  Created by 柯平 on 2017/8/16.
//  Copyright © 2017年 柯平. All rights reserved.
//  常用宏定义

#ifndef KMacro_h
#define KMacro_h


#pragma mark - Switch

#define Is_Enterprise 1



#pragma mark --------------- block -----------------
#ifdef NS_BLOCKS_AVAILABLE
typedef void (^KBaseBlock)(void);
typedef void(^KOperationCallBackBlock)(BOOL isSuccess,NSString* errorMsg);
typedef void(^KBaseCallBackBlock)(NSError* error,NSDictionary* responseDic);
#endif

#pragma mark --------------- 单例创建 -----------------
#undef	AS_SINGLETON
#define AS_SINGLETON( __class ) \
+ (__class *)sharedInstance;

#undef	DEF_SINGLETON
#define DEF_SINGLETON( __class ) \
+ (__class *)sharedInstance \
{ \
static dispatch_once_t once; \
static __class * __singleton__; \
dispatch_once( &once, ^{ __singleton__ = [[__class alloc] init]; } ); \
return __singleton__; \
}


#pragma mark --------------- Color -----------------
//带有RGBA的颜色设置
#define COLORA(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
#define COLOR(R, G, B)     COLORA(R, G, B, 1.0)

#define kWhiteColor [UIColor whiteColor]
#define kBlackColor [UIColor blackColor]
#define kDarkGrayColor [UIColor darkGrayColor]
#define kLightGrayColor [UIColor lightGrayColor]
#define kGrayColor [UIColor grayColor]
#define kRedColor [UIColor redColor]
#define kGreenColor [UIColor greenColor]
#define kBlueColor [UIColor blueColor]
#define kCyanColor [UIColor cyanColor]
#define kYellowColor [UIColor yellowColor]
#define kMagentaColor [UIColor magentaColor]
#define kOrangeColor [UIColor orangeColor]
#define kPurpleColor [UIColor purpleColor]
#define kBrownColor [UIColor brownColor]
#define kClearColor [UIColor clearColor]

#define KBackColor UIColorHex(EEEFEF)

#define KNaviBarColor [UIColor colorWithRed:23/255.0 green:27/255.0 blue:78/255.0 alpha:1.0]
#define KDarkTextColor [UIColor colorWithRed:39/255.0 green:49/255.0 blue:102/255.0 alpha:1.0]
#define KLightTextColor [UIColor colorWithRed:180/255.0 green:180/255.0 blue:180/255.0 alpha:1.0]
#define KGrayBackColor  [UIColor colorWithRed:244/255.0 green:245/255.0 blue:246/255.0 alpha:1.0]
#define KConfirmBackColor [UIColor colorWithRed:0/255.0 green:194/255.0 blue:255/255.0 alpha:1.0]

#define kThemeColor UIColorHex(1300A0)
#define kTextThemeColor UIColorHex(1300A0)


// 字体颜色
#define kColor_word_black         HEX_RGB(0x333333)
#define kColor_word_gray_1        HEX_RGB(0x666666)
#define kColor_word_gray_2        HEX_RGB(0x999999)
#define kColor_word_gray_3        HEX_RGB(0xcccccc)
#define kColor_word_gray_4        RGB(230, 230, 230)
#define kColor_under_line         HEX_RGB(0xDDDDDD)
#define kColor_background         HEX_RGB(0xf5f5f5)
#define kColor_yellow             HEX_RGB(0xf4c034)

#pragma mark - -----------------Device ----------------

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
/**
 屏幕适配系数
 */
#define K_FactorW(A) ([UIScreen mainScreen].bounds.size.width/375.0 * (A))
#define K_FactorH(A) ([UIScreen mainScreen].bounds.size.height/667.0 * (A))

//各种判断手机类型
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define APPDELEGATE (((AppDelegate*)([UIApplication sharedApplication].delegate)))

#pragma mark ----------------Font-----------------
/***  普通字体 */
#define kFont(size) [UIFont systemFontOfSize:size]
/***  粗体 */
#define kBoldFont(size) [UIFont boldSystemFontOfSize:size]

/***  斜体 */
#define KItalicFont(size) [UIFont italicSystemFontOfSize:size]
#define KItalicSystemFont [UIFont italicSystemFontOfSize:[UIFont systemFontSize]]
#define KItalicSmallSystemFont [UIFont italicSystemFontOfSize:[UIFont smallSystemFontSize]]
#define KItalicLabelFont [UIFont italicSystemFontOfSize:[UIFont labelFontSize]]
#define KItalicButtonFont [UIFont italicSystemFontOfSize:[UIFont buttonFontSize]]

//SystemFont
#define SystemFont     [UIFont systemFontOfSize:[UIFont systemFontSize]]
#define SystemBoldFont    [UIFont boldSystemFontOfSize:[UIFont systemFontSize]]
#define SystemSmallFont     [UIFont systemFontOfSize:[UIFont smallSystemFontSize]]
#define SystemSmallBoldFont [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]]

//LabelFont
#define LabelFont [UIFont systemFontOfSize:[UIFont labelFontSize]]
#define LabelBoldFont [UIFont boldSystemFontOfSize:[UIFont labelFontSize]]

//ButtonFont
#define ButtonFont [UIFont systemFontOfSize:[UIFont buttonFontSize]]
#define ButtonBoldFont [UIFont boldSystemFontOfSize:[UIFont buttonFontSize]]
#define CommonFont [UIFont systemFontOfSize:16]
#define NloginTextFont [UIFont systemFontOfSize:K_FactorH(18)]

#pragma mark ---------------------国际化-----------------------
#undef L
#define L(key) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

#pragma mark ---------------------Log------------------------
#ifdef DEBUG
#define KLog(FORMAT, ...) fprintf(stderr,"%s:[Line %d]\t%s\n",[[NSString stringWithUTF8String:__PRETTY_FUNCTION__] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define KLog(...)
#endif


// 默认头像
#define kAvatar_person            [UIImage imageNamed:@"personal_icon"]
#define kAvatar_group             [UIImage imageNamed:@"group-chat_icon-1"]

#pragma mark - GlobalDefine

typedef NS_ENUM(NSInteger,KCCTVShowMode) {
    KCCTVShowModeOne,
    KCCTVShowModeFour
};

typedef NS_ENUM(NSInteger,KUserType) {
    KUserTypeUnknow = 0,
    KUserTypeOwner,
    KUserTypeTourist
};

typedef NS_ENUM(NSInteger,KInputOperateType) {
    KInputOperateTypeNone = 0,
    KInputOperateTypeUpdateName,
    KInputOperateTypeUpdateAddr
};

#endif /* KMacro_h */
