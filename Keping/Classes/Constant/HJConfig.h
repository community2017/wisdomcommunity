//
//  HJConfig.h
//  Keping
//
//  Created by a on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#ifndef HJConfig_h
#define HJConfig_h



#pragma mark -- 单例
//----------------------单例----------------------------
// .h文件
#define HJSingletonH + (instancetype)sharedInstance;
// .m文件
#define HJSingletonM \
static id _instace; \
\
+ (instancetype)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instace = [super allocWithZone:zone]; \
}); \
return _instace; \
} \
\
+ (instancetype)sharedInstance \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instace = [[self alloc] init]; \
}); \
return _instace; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instace; \
}
//----------------------单例----------------------------

//----------------------自定义cell-----------------------


//----------------------自定义cell-----------------------
//----------------------属性设置-------------------------
#define keyPath(objc,keyPath) @(((void)objc.keyPath, #keyPath))

#define APP_Frame_Height   [[UIScreen mainScreen] bounds].size.height

#define App_Frame_Width    [[UIScreen mainScreen] bounds].size.width

//比列
#define HJScaleWidth(__VA_ARGS__) (App_Frame_Width/375)*(__VA_ARGS__)
#define HJScaleHeight(__VA_ARGS__) (APP_Frame_Height/667)*(__VA_ARGS__)
#define HJScaleFont(__VA_ARGS__) (App_Frame_Width/375)*(__VA_ARGS__)

#define ALERT(msg)  [[[UIAlertView alloc]initWithTitle:@"提示" message:msg delegate:nil \
cancelButtonTitle:@"确定" otherButtonTitles:nil,nil] show]

#define App_Delegate ((AppDelegate*)[[UIApplication sharedApplication]delegate])

#define App_RootCtr  [self getCurrentViewController].navigationController.view

#define WEAKSELF __weak typeof(self) weakSelf = self



#define HJFont(FONTSIZE)  [UIFont systemFontOfSize:(FONTSIZE)]
//----------------------属性设置-------------------------





#endif 
