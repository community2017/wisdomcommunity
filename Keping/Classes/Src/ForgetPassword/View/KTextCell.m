//
//  KTextCell.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTextCell.h"

@implementation KTextCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

-(UITextField *)textTf
{
    if (!_textTf) {
        _textTf = [UITextField new];
        _textTf.delegate = self;
        [self.contentView addSubview:_textTf];
    }
    return _textTf;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.textTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.top.mas_equalTo(5);
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isNotBlank]) {
        if (self.textBlock) {
            self.textBlock(textField.text?:@"");
        }
    }
}


@end
