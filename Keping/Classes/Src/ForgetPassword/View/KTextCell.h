//
//  KTextCell.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KTextCell : KTableViewCell<UITextFieldDelegate>

/**/
@property(nonatomic,copy)void(^textBlock) (NSString* text);
/**/
@property(nonatomic,strong)UITextField* textTf;
/**/
@property(nonatomic,strong)UIColor* placeholerColor;

@end
