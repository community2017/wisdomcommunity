//
//  KVeritifyCodeCell.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KVeritifyCodeCell : KTableViewCell<UITextFieldDelegate>
/**/
@property(nonatomic,copy)void(^buttonBlock) (void);
/**/
@property(nonatomic,copy)void(^codeBlock) (NSString* text);
/**/
@property(nonatomic,strong)UITextField* textTf;
/**/
@property(nonatomic,strong)UIButton* codeBtn;

@end
