//
//  KVeritifyCodeCell.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KVeritifyCodeCell.h"

@implementation KVeritifyCodeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

-(UITextField *)textTf
{
    if (!_textTf) {
        _textTf = [UITextField new];
        _textTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _textTf.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        _textTf.delegate = self;
        [self.contentView addSubview:_textTf];
    }
    return _textTf;
}

-(UIButton *)codeBtn
{
    if (!_codeBtn) {
        _codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];;
        [_codeBtn setBackgroundImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateNormal];
        _codeBtn.titleLabel.numberOfLines = 0;
        _codeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _codeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _codeBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _codeBtn.titleLabel.font = [UIFont systemFontOfSize:[UIFont smallSystemFontSize]];
        _codeBtn.clipsToBounds = YES;
        [_codeBtn setTitleColor:kWhiteColor forState:UIControlStateNormal];
        [_codeBtn setBackgroundImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateHighlighted];
        [_codeBtn addTarget:self action:@selector(getCode) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_codeBtn];
    }
    return _codeBtn;
}

-(void)getCode{
    if (self.buttonBlock) {
        self.buttonBlock();
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-5);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(kScreenWidth/3);
    }];
    
    [self.textTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(self.codeBtn.mas_left);
        make.bottom.mas_equalTo(0);
        make.top.mas_equalTo(5);
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isNotBlank]) {
        if (self.codeBlock) {
            self.codeBlock(textField.text);
        }
    }
}

@end
