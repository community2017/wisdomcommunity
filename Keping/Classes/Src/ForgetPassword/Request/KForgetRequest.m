//
//  KForgetRequest.m
//  Keping
//
//  Created by 柯平 on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KForgetRequest.h"

@implementation KForgetRequest

-(NSString *)requestUrl
{
    return KForgetPwdURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(id)requestArgument
{
    return self.params;
}

@end

@implementation KChangePwdRequest

-(NSString *)requestUrl
{
    return KUpdatePwdURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(id)requestArgument
{
    return self.params;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

@end
