//
//  KStatusRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KStatusRequest.h"

@implementation KStatusRequest

-(NSString *)requestUrl
{
    return KCommunityStatusURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId};
}


@end
