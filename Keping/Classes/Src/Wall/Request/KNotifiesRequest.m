//
//  KNotifiesRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/25.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNotifiesRequest.h"

@implementation KNotifiesRequest

-(NSString *)requestUrl
{
    return KNotifiesURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return nil;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId};
}

@end
