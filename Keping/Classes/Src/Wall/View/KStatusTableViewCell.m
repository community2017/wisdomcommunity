//
//  KStatusTableViewCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/22.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KStatusTableViewCell.h"
#import "KStatusData.h"
#import "UIView+KP.h"




@implementation KStatusView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kScreenWidth;
        frame.size.height = 1.0f;
    }
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor clearColor];
    self.exclusiveTouch = YES;
    
//    @weakify(self);
    
    //容器
    _contentView = [UIView new];
    _contentView.width = kScreenWidth;
    _contentView.height = 1.0;
    _contentView.backgroundColor = kWhiteColor;
    [self addSubview:_contentView];
    
    //头像
    _avatarView = [UIImageView new];
    _avatarView.clipsToBounds = YES;
    _avatarView.layer.cornerRadius = 5.;
    _avatarView.contentMode = UIViewContentModeScaleAspectFill;
    _avatarView.size = CGSizeMake(KStatusCellAvatarWH, KStatusCellAvatarWH);
    _avatarView.origin = CGPointMake(KStatusCellPadding, KStatusCellPadding+3);
    [_contentView addSubview:_avatarView];
    [_avatarView setTapActionWithBlock:^{
        //点击图像
    }];
    
    //名字
    _nameLabel = [YYLabel new];
    _nameLabel.size = CGSizeMake(KStatusCellContentWidth-KStatusCellAvatarWH-KStatusCellPadding*3, 24);
    _nameLabel.font = [UIFont boldSystemFontOfSize:15];
    _nameLabel.left = _avatarView.right+KStatusCellPadding;
    _nameLabel.top = KStatusCellPadding;
    _nameLabel.displaysAsynchronously = YES;
    _nameLabel.ignoreCommonProperties = YES;
    _nameLabel.fadeOnAsynchronouslyDisplay = NO;
    _nameLabel.fadeOnHighlight = NO;
    _nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _nameLabel.highlightTapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        //点击名字
    };
    [_contentView addSubview:_nameLabel];
    
    //时间
    _timeLabel = [YYLabel new];
    _timeLabel.size = _nameLabel.size;
    _timeLabel.font = _nameLabel.font;
    _timeLabel.left = _nameLabel.left;
    _timeLabel.top = _nameLabel.bottom;
    _timeLabel.displaysAsynchronously = YES;
    _timeLabel.ignoreCommonProperties = YES;
    _timeLabel.fadeOnAsynchronouslyDisplay = NO;
    _timeLabel.fadeOnHighlight = NO;
    _timeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_contentView addSubview:_timeLabel];
    
    //文本
    _contentLabel = [YYLabel new];
    _contentLabel.font = [UIFont boldSystemFontOfSize:15];
    _contentLabel.left = _avatarView.right+KStatusCellPadding;
    _contentLabel.top = KStatusCellPadding;
    _contentLabel.displaysAsynchronously = YES;
    _contentLabel.ignoreCommonProperties = YES;
    _contentLabel.fadeOnAsynchronouslyDisplay = NO;
    _contentLabel.fadeOnHighlight = NO;
    _contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_contentView addSubview:_contentLabel];
    
    //图片
    NSMutableArray *picViews = [NSMutableArray new];
    for (int i = 0; i < 9; i++) {
        UIImageView *imageView = [UIImageView new];
        imageView.size = CGSizeMake(100, 100);
        imageView.hidden = YES;
        imageView.clipsToBounds = YES;
        imageView.backgroundColor = UIColorHex(f0f0ff0);
        imageView.exclusiveTouch = YES;
        [imageView setTapActionWithBlock:^{
            //点击图片
        }];
        [picViews addObject:imageView];
        [_contentView addSubview:imageView];
    }
    _picViews = picViews;
    
    //喜欢
    _likesLabel = [YYLabel new];
    _likesLabel.font = [UIFont boldSystemFontOfSize:15];
    _likesLabel.left = _avatarView.right+KStatusCellPadding;
    _likesLabel.top = KStatusCellPadding;
    _likesLabel.displaysAsynchronously = YES;
    _likesLabel.ignoreCommonProperties = YES;
    _likesLabel.fadeOnAsynchronouslyDisplay = NO;
    _likesLabel.fadeOnHighlight = NO;
    _likesLabel.text = @"0 People likes this";
    _likesLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_contentView addSubview:_likesLabel];
    
    //喜欢
    _commentsLabel = [YYLabel new];
    _commentsLabel.font = [UIFont boldSystemFontOfSize:15];
    _commentsLabel.left = _avatarView.right+KStatusCellPadding;
    _commentsLabel.top = KStatusCellPadding;
    _commentsLabel.displaysAsynchronously = YES;
    _commentsLabel.ignoreCommonProperties = YES;
    _commentsLabel.fadeOnAsynchronouslyDisplay = NO;
    _commentsLabel.fadeOnHighlight = NO;
    _commentsLabel.text = @"0 Comments";
    _commentsLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_contentView addSubview:_commentsLabel];
    
    _seperateLine = [UIView new];
    _seperateLine.backgroundColor = kGrayColor;
    _seperateLine.size = CGSizeMake(kScreenWidth, 1);
    [_contentView addSubview:_seperateLine];
    
    _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _commentButton.size = CGSizeMake(84, 34);
    [_contentView addSubview:_commentButton];
    _commentImageView = [UIImageView new];
    _commentImageView.image = [[UIImage imageNamed:@"comment"] imageByResizeToSize:CGSizeMake(24, 24)];
    [_commentImageView setFrame:CGRectMake(0, 5, _commentImageView.width, _commentImageView.height)];
    [_commentButton addSubview:_commentImageView];
    _commentLabel = [UILabel new];
    _commentLabel.text = @"Comment";
    _commentLabel.textColor = kGrayColor;
    _commentLabel.font = SystemSmallFont;
    _commentLabel.textAlignment = NSTextAlignmentCenter;
    [_commentLabel setFrame:CGRectMake(_commentImageView.left, _commentImageView.top, _commentButton.width-_commentImageView.width, _commentImageView.height)];
    [_commentButton addSubview:_commentLabel];
    
    _likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _likeButton.size = CGSizeMake(80, 34);
    [_contentView addSubview:_likeButton];
    _likeImageView = [UIImageView new];
    _likeImageView.image = [[UIImage imageNamed:@"dynamic_like"] imageByResizeToSize:CGSizeMake(24, 24)];
    [_likeImageView setFrame:CGRectMake(0, 5, _commentImageView.width, _commentImageView.height)];
    [_commentButton addSubview:_likeImageView];
    _likeLabel = [UILabel new];
    _likeLabel.text = @"Comment";
    _likeLabel.textColor = kGrayColor;
    _likeLabel.font = SystemSmallFont;
    _likeLabel.textAlignment = NSTextAlignmentCenter;
    [_likeLabel setFrame:CGRectMake(_commentImageView.left, _commentImageView.top, _commentButton.width-_commentImageView.width, _commentImageView.height)];
    [_likeButton addSubview:_likeLabel];
    
    return self;
}

-(void)setLayout:(KStatusLayout *)layout
{
    _layout = layout;
    self.height = layout.height;
    _contentView.height = 0;
    _contentView.height = layout.height;
    _contentView.origin = CGPointMake(0, 0);
    _contentView.width = kScreenWidth;
    
    CGFloat top = 0;
    @weakify(_avatarView);
    if (layout.status.isAd) {
        _nameLabel.textLayout = layout.titleLayout;
        
    }else{
        [_avatarView setImageWithURL:[NSURL URLWithString:layout.status.img?:@""] placeholder:[UIImage imageNamed:KImagePlaceholder] options:kNilOptions progress:nil transform:nil completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            if (!image) {
                weak__avatarView.image = [[UIImage imageNamed:KImagePlaceholder] imageByResizeToSize:CGSizeMake(KStatusCellAvatarWH, KStatusCellAvatarWH)];
            }else{
                weak__avatarView.image = [image imageByResizeToSize:weak__avatarView.size contentMode:UIViewContentModeScaleAspectFill];
            }
        }];
        
        _nameLabel.textLayout = layout.nameLayout;
        _timeLabel.textLayout = layout.timeLayout;
        
        
        
    }
}

@end

@implementation KStatusTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _statusView = [KStatusView new];
        _statusView.cell = self;
        [self.contentView addSubview:_statusView];
    }
    return self;
}

-(void)setLayout:(KStatusLayout *)layout
{
    self.height = layout.height;
    self.contentView.height = layout.height;
    _statusView.layout = layout;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
