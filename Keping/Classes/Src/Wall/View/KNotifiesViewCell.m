//
//  KNotifiesViewCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/25.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNotifiesViewCell.h"

@implementation KNotifiesViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _avatar = [UIImageView new];
        _avatar.clipsToBounds = YES;
        [self.contentView addSubview:_avatar];
        
        _nameLabel = [YYLabel new];
        [self.contentView addSubview:_nameLabel];
        
        _likeIcon = [UIImageView new];
        _likeIcon.image = [UIImage imageNamed:@"dynamic_liked"];
        [self.contentView addSubview:_likeIcon];
        
        _commentLabel = [UILabel new];
        _commentLabel.numberOfLines = 2;
        _commentLabel.font = kFont(12);
        _commentLabel.textColor = kDarkGrayColor;
        [self.contentView addSubview:_commentLabel];
        
        _timeLabel = [UILabel new];
        _timeLabel.textColor = kGrayColor;
        _timeLabel.font = kFont(12);
        [self.contentView addSubview:_timeLabel];
        
        _imageIcon = [UIImageView new];
        _imageIcon.clipsToBounds = YES;
        [self.contentView addSubview:_imageIcon];
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(10);
        make.width.height.mas_equalTo(48);
        
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end



