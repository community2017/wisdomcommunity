//
//  KStatusTableViewCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/22.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@class KStatusLayout,KStatusTableViewCell;



@interface KStatusView : UIView

/**/
@property(nonatomic,strong)UIView* contentView;
/**/
@property(nonatomic,strong)UIImageView* avatarView;
/**/
@property(nonatomic,strong)YYLabel* nameLabel;
/**/
@property(nonatomic,strong)YYLabel* timeLabel;
/**/
@property(nonatomic,strong)YYLabel* contentLabel;
/**/
@property(nonatomic,strong)NSArray* picViews;
/**/
@property(nonatomic,strong)YYLabel* likesLabel;
/**/
@property(nonatomic,strong)YYLabel* commentsLabel;
/**/
@property(nonatomic,strong)UIView* seperateLine;
/*Comment*/
@property(nonatomic,strong)UIButton* commentButton;
@property(nonatomic,strong)UIImageView* commentImageView;
@property(nonatomic,strong)UILabel* commentLabel;
/*Like*/
@property(nonatomic,strong)UIButton* likeButton;
@property(nonatomic,strong)UIImageView* likeImageView;
@property(nonatomic,strong)UILabel* likeLabel;

/**/
@property(nonatomic,weak)KStatusTableViewCell* cell;
/**/
@property(nonatomic,strong)KStatusLayout* layout;

@end

@interface KStatusTableViewCell : KTableViewCell

/**/
@property(nonatomic,strong)KStatusView* statusView;

-(void)setLayout:(KStatusLayout*)layout;

@end
