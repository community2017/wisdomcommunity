//
//  KNotifiesViewCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/25.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KNotifiesViewCell : KTableViewCell

/**/
@property(nonatomic,assign)BOOL isLike;
/**/
@property(nonatomic,strong)UIImageView* avatar;
/**/
@property(nonatomic,strong)YYLabel* nameLabel;
/**/
@property(nonatomic,strong)UILabel* commentLabel;
/**/
@property(nonatomic,strong)UIImageView* likeIcon;
/**/
@property(nonatomic,strong)UIImageView* imageIcon;
/**/
@property(nonatomic,strong)UILabel* timeLabel;

@end



