//
//  KWallViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KWallViewController.h"
#import "KStatusTableViewCell.h"
#import "KStatusRequest.h"
#import "KStatusData.h"
#import "KUserData.h"

@interface KWallViewController ()


@end

@implementation KWallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Wall";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.data addObject:@"What's happening?"];
    self.tableView.sectionFooterHeight = 10;
    self.tableView.sectionHeaderHeight = CGFLOAT_MIN;
    [self.tableView beginRefresh];
    
}

-(void)refresh
{
    [super refresh];
    [self getData];
}

-(void)loadmore
{
    [super loadmore];
    if (self.data.count <= 1) [self refresh];
    [self getData];
}

-(void)getData
{
    [super getData];
    KStatusRequest* request = [[KStatusRequest alloc] init];
    request.params = [NSDictionary dictionaryWithObjectsAndKeys:@(self.page),@"pageOn", nil];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KStatusData* data = [KStatusData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            if (self.page == 1) {
                [self.data removeAllObjects];
                [self.data addObject:@"What's happening?"];
            }
            if (data.bbsTopicViews.count > 0) {
                for (KStatus* status in data.bbsTopicViews) {
                    KStatusLayout* layout = [[KStatusLayout alloc] initWithStatus:status];
                    if (layout) [self.data addObject:layout];
                }
            }
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
        [self.tableView endRefresh];
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self.tableView endRefresh];
        [self showError:request.error];
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.data.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = self.data[indexPath.section];
    if ([obj isKindOfClass:[NSString class]]) {
        return 54;
    }else{
        return [(KStatusLayout*)obj height];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = self.data[indexPath.section];
    if ([obj isKindOfClass:[NSString class]]) {
        KTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTableViewCell className]];
        if (!cell) {
            cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTableViewCell className]];
        }
        cell.textLabel.text = @"What's happening?";
        cell.textLabel.font = SystemFont;
        cell.textLabel.textColor = kGrayColor;
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        KPartyView* user = [KPartyView unarchive];
        CGSize size = CGSizeMake(34, 34);
        @weakify(cell);
        [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KGetImageURL,user.profile]] placeholder:[[UIImage imageNamed:KImagePlaceholder] imageByResizeToSize:size] options:kNilOptions completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            if (image) {
                weak_cell.imageView.image = [image imageByResizeToSize:size];
            }
        }];
        return cell;
    }else{
        KStatusTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KStatusTableViewCell className]];
        if (!cell) {
            cell = [[KStatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KStatusTableViewCell className]];
        }
        [cell setLayout:obj];
        return cell;
    }
    return nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
