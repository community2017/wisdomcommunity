//
//  KNotifiesViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/25.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNotifiesViewController.h"
#import "KNotifiesViewCell.h"
#import "KNotifiesRequest.h"

@interface KNotifiesViewController ()

@end

@implementation KNotifiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Notifications";
    
    [self.tableView beginRefresh];
    
    
}
-(void)refresh
{
    [super refresh];
    [self getData];
}

-(void)loadmore
{
    [super loadmore];
    [self getData];
}

-(void)getData
{
    [super getData];
    KNotifiesRequest* request = [[KNotifiesRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KNotifiesViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KNotifiesViewCell className]];
    if (!cell) {
        cell = [[KNotifiesViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KNotifiesViewCell className]];
    }
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
