//
//  KTextLinePositionModifier.h
//  Keping
//
//  Created by 柯平 on 2017/10/22.
//  Copyright © 2017年 柯平. All rights reserved.
//

/**
 文本 Line 位置修改,将每行文本的高度和位置固定下来，不受中英文/Emoji字体的 ascent/descent 影响
 */

#import <Foundation/Foundation.h>

@interface KTextLinePositionModifier : NSObject<YYTextLinePositionModifier>

/*基准字体*/
@property(nonatomic,strong)UIFont* font;
/*文本顶部留白*/
@property(nonatomic,assign)CGFloat paddingTop;
/*文本底部留白*/
@property(nonatomic,assign)CGFloat paddingBottom;
/*行距倍数*/
@property(nonatomic,assign)CGFloat lineHeightMultiple;

/*计算高度*/
-(CGFloat)heightForLineCount:(NSUInteger)lineCount;

@end
