//
//  KStatusData.m
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KStatusData.h"
#import "KTextLinePositionModifier.h"

@implementation KStatus


-(BOOL)modelCustomTransformFromDictionary:(NSDictionary*)dic
{
    id isAd = [dic objectForKey:@"isAd"];
    if ([isAd isKindOfClass:[NSString class]]) {
        if ([isAd isEqualToString:@"Y"]) {
            self.isAd = YES;
        }
    }
    return YES;
}

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"imageUrls":[NSString class]};
}

@end

@implementation KStatusData

+(NSDictionary*)modelCustomPropertyMapper{
    return @{@"newnotificationsImge":@"newNotificationsImge",
             @"newnotificationsQty":@"newNotificationsQty"};
}

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"bbsTopicViews":[KStatus class]};
}




@end

@implementation KStatusLayout

-(instancetype)initWithStatus:(KStatus *)status
{
    if (!status) return nil;
    if (self == [super init]) {
        _status = status;
        _isAd = status.isAd;
        [self layout];
    }
    return self;
}

-(void)layout
{
    [self reset];
    if (!_status.isAd) {
        //用户信息
        _profileHeight = 68;
        NSMutableAttributedString *name = [[NSMutableAttributedString alloc] initWithString:_status.realName?:@""];
        name.font = SystemBoldFont;
        name.color = kBlackColor;
        name.lineBreakMode = NSLineBreakByCharWrapping;
        NSRange range = [name.string rangeOfString:_status.realName?:@"" options:kNilOptions range:name.rangeOfAll];
        YYTextBorder *highlightBorder = [YYTextBorder new];
        highlightBorder.insets = UIEdgeInsetsMake(-2, 0, -2, 0);
        highlightBorder.cornerRadius = 3;
        highlightBorder.fillColor = kGrayColor;
        YYTextHighlight *highlight = [YYTextHighlight new];
        [highlight setBackgroundBorder:highlightBorder];
        // 数据信息，用于稍后用户点击
        highlight.userInfo = @{@"username" : name,@"id":_status.createBy?:@""};
        [name setTextHighlight:highlight range:range];
        YYTextContainer* container = [YYTextContainer new];
        container.size = CGSizeMake(KStatusCellContentWidth-KStatusCellAvatarWH - KStatusCellPadding*3, HUGE);
        //container.linePositionModifier = modifier;
        container.maximumNumberOfRows = 1;
        _nameLayout = [YYTextLayout layoutWithContainer:container text:name];
        
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:_status.createTime];
        NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
        
        [fmt setDateFormat:@"dd MM yyyy HH:mm"];
        NSString* time = [NSString stringWithFormat:@"%@",[fmt stringFromDate:date]];
        
        NSMutableAttributedString *timeText = [[NSMutableAttributedString alloc] initWithString:time];
        timeText.font = SystemBoldFont;
        timeText.color = kBlackColor;
        timeText.lineBreakMode = NSLineBreakByCharWrapping;
        NSRange timeRange = [name.string rangeOfString:time?:@"" options:kNilOptions range:timeText.rangeOfAll];
        _timeLayout = [YYTextLayout layoutWithContainer:container text:name];
        
        //文本
        if (![_status.summary isNotBlank]) {
            _textHeight = 0;
        }else{
            NSMutableAttributedString* text = [[NSMutableAttributedString alloc] initWithString:_status.summary];
            text.font = SystemFont;
            text.color = kDarkGrayColor;
            
            KTextLinePositionModifier* modifier = [KTextLinePositionModifier new];
            modifier.font = SystemFont;
            modifier.paddingTop = 3.14;
            modifier.paddingBottom = 3.14;
            
            YYTextContainer* container = [YYTextContainer new];
            container.size = CGSizeMake(KStatusCellContentWidth, HUGE);
            container.linePositionModifier = modifier;
            
            _textLayout = [YYTextLayout layoutWithContainer:container text:text];
            if (!_textLayout) {
                _textHeight = 0;
            }else{
                _textHeight = [modifier heightForLineCount:_textLayout.rowCount];
            }
        }
        
        //图片
        CGSize picSize = CGSizeZero;
        CGFloat picHeight = 0;
        if (!_status.imageUrls || _status.imageUrls.count == 0) return;
        CGFloat len1_3 = (KStatusCellContentWidth - KStatusCellPadding*3)/3;
        len1_3 = CGFloatPixelRound(len1_3);
        switch (_status.imageUrls.count) {
            case 1:
            {
                CGFloat maxLen = KStatusCellContentWidth / 2.0;
                maxLen = CGFloatPixelRound(maxLen);
                picSize = CGSizeMake(maxLen, maxLen);
                picHeight = maxLen;
            }
                break;
            case 2:case 3:
            {
                picSize = CGSizeMake(len1_3, len1_3);
                picHeight = len1_3;
            }
                break;
            case 4: case 5: case 6:
            {
                picSize = CGSizeMake(len1_3, len1_3);
                picHeight = len1_3 * 2 + KStatusCellPadding/2;
            }
                break;
                
            default:
            {
                picSize = CGSizeMake(len1_3, len1_3);
                picHeight = len1_3 * 3 + KStatusCellPadding * 2;
            }
                break;
        }
        _picSize = picSize;
        _imagesHeight = picHeight;
        
        
        
        _infoHeight = 24;
        _operateHeight = 44;
        _bottomHeight = 0;
        
        _height += _profileHeight;
        _height += _textHeight;
        _height += _imagesHeight;
        _height += _infoHeight;
        _height += _operateHeight;
        _height += _bottomHeight;
        
    }else{
        _paddingTop = 10;
        _titleHeight = 24;
        _imagesTop = 5;
        _imagesHeight = 140;
        _bottomHeight = 10;
                
        _height += _paddingTop;
        _height += _titleHeight;
        _height += _imagesTop;
        _height += _imagesHeight;
        _height += _bottomHeight;
    }
}

-(void)reset{
    _height = 0;
    _paddingTop = 0;
    _titleHeight = 0;
    _textTop = 0;
    _textHeight = 0;
    _imagesTop = 0;
    _imagesHeight = 0;
    _profileHeight = 0;
    _picSize = CGSizeZero;
    _bottomHeight = 0;
    _infoHeight = 0;
    _operateHeight = 0;
    
    _nameLayout = nil;
    _titleLayout = nil;
    _textLayout = nil;
    _timeLayout = nil;
    _likeTextLayout = nil;
    _commentTextLayout = nil;
    
}







@end
