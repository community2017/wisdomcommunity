//
//  KStatusData.h
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KStatusCellPadding 10
#define KStatusCellAvatarWH 48
#define KStatusCellContentWidth (kScreenWidth -  KStatusCellPadding* 2)

@interface KStatus : NSObject

/**/
@property(nonatomic,copy)NSString* cateId;
/*评论数 */
@property(nonatomic,assign)NSInteger commontQty;
/*好友id*/
@property(nonatomic,copy)NSString* createBy;
/**/
@property(nonatomic,assign)NSTimeInterval createTime;
/*动态id*/
@property(nonatomic,copy)NSString* id;
/*图片数组*/
@property(nonatomic,strong)NSArray<NSString*>* imageUrls;
/**/
@property(nonatomic,copy)NSString* img;
/**/
@property(nonatomic,assign)BOOL isAd;
/**/
@property(nonatomic,assign)BOOL isLike;
/*点赞数*/
@property(nonatomic,assign)NSInteger likeQty;
/**/
@property(nonatomic,copy)NSString* likeNumber;
/**/
@property(nonatomic,copy)NSString* meId;
/*好友头像*/
@property(nonatomic,copy)NSString* profile;
/*好友名*/
@property(nonatomic,copy)NSString* realName;
/**/
@property(nonatomic,copy)NSString* subject;
/*标题*/
@property(nonatomic,copy)NSString* summary;

@end

@interface KStatusData : NSObject

/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,copy)NSString* newnotificationsImge;
/**/
@property(nonatomic,strong)NSArray<KStatus*>* bbsTopicViews;
/**/
@property(nonatomic,assign)NSInteger newnotificationsQty;

@end

@interface KStatusLayout : NSObject

-(instancetype)initWithStatus:(KStatus*)status;




/*动态*/
@property(nonatomic,strong)KStatus* status;

-(void)layout;

/*高度*/
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,assign)CGFloat profileHeight;
@property(nonatomic,assign)CGFloat titleHeight;
@property (nonatomic, assign) CGFloat paddingTop;
@property (nonatomic, assign) CGFloat textTop;
@property (nonatomic, assign) CGFloat textHeight;
@property (nonatomic, assign) CGFloat imagesTop;
@property (nonatomic, assign) CGFloat imagesHeight;
@property(nonatomic,assign)CGSize picSize;
@property(nonatomic,assign)CGFloat infoHeight;
@property (nonatomic, assign) CGFloat operateHeight;
@property(nonatomic,assign)CGFloat bottomHeight;

/**/
@property(nonatomic,assign)BOOL isAd;

/**/
@property(nonatomic,strong)YYTextLayout* nameLayout;
@property(nonatomic,strong)YYTextLayout* timeLayout;
@property(nonatomic,strong)YYTextLayout* titleLayout;
@property(nonatomic,strong)YYTextLayout* textLayout;
@property(nonatomic,strong)YYTextLayout* likeTextLayout;
@property(nonatomic,strong)YYTextLayout* commentTextLayout;

@end
