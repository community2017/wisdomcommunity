//
//  HomeViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/17.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HomeViewController.h"
#import "KHomeHeadView.h"
#import "HomeItemCell.h"
#import "KVeritificationViewController.h"
#import "SettingsViewController.h"
#import "HJFacilitieController.h"
#import "KInterconViewController.h"
#import "KNewsController.h"
#import "KSOSHomeController.h"
#import "KDirectoryViewController.h"
#import "KWallViewController.h"

@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray* items;
}
@property(nonatomic,strong)KHomeHeadView* headView;
@property(nonatomic,strong)UICollectionView* collectionView;

@end

@implementation HomeViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    items = @[@"News",@"Wall",@"SOS",@"Intercom",@"CCTV",@"Door Access",@"Facilities",@"Directory",@"Settings"];
    
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"LinkTime"]];
    self.collectionView.backgroundColor = kWhiteColor;
    [self.collectionView registerClass:[HomeItemCell class] forCellWithReuseIdentifier:[HomeItemCell className]];
    
    //提示是否需要验证
    if ([KAppConfig sharedConfig].veritify) {
        [self showVeritifyTip];
    }
}

-(void)showVeritifyTip
{
    KAlertView* alert = [[KAlertView alloc] initWithTitle:@"Registration Successful" content:@"You may be verified as owner using the verification code given by your community management." placeholder:nil style:KAlertViewAlert alignment:KAlertViewButtonAlignmentVertical confirmTitle:@"Veritify as owner" cancelTitle:@"Continue as guest"];
    @weakify(self);
    alert.ClickBlock = ^(BOOL isConfirm, NSString *inputText) {
        if (isConfirm) {
            KVeritificationViewController* vc = [KVeritificationViewController new];
            [weak_self.navigationController pushViewController:vc animated:YES];
        }
    };
    [alert showInView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITableViewDelegate/DataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return items.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.row];
    HomeItemCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[HomeItemCell className] forIndexPath:indexPath];
    cell.backView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_back",title]];
    cell.itemView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_item",title]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.row];
    if ([title isEqualToString:@"Facilities"]) {
        HJFacilitieController*vc=[HJFacilitieController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"Intercom"]){
        KInterconViewController* vc = [KInterconViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"News"]){
        KNewsController* vc = [KNewsController new];
        [self.navigationController pushViewController:vc animated:YES];
        //News
    }else if ([title isEqualToString:@"Settings"]){
        SettingsViewController* vc = [SettingsViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"SOS"]){
        KSOSHomeController* vc = [KSOSHomeController new];
        [self.navigationController pushViewController:vc animated:YES];
        //Newselse{
        
    }else if ([@"Directory" isEqualToString:title]){
//        KDirectoryViewController* vc = [KDirectoryViewController new];
//        vc.title = title;
//        [self.navigationController pushViewController:vc animated:YES];
        [self changeToRootOfIndex:1];
    }else if ([@"Door Access" isEqualToString:title]){
        [self changeToRootOfIndex:3];
    }else if ([@"CCTV" isEqualToString:title]){
        [self changeToRootOfIndex:2];
    }else if ([@"Wall" isEqualToString:title]){
        KWallViewController* vc = [KWallViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

#pragma mark - LazyLoad
-(KHomeHeadView *)headView
{
    if (!_headView) {
        _headView = [[KHomeHeadView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenWidth*0.6)];
        [self.view addSubview:_headView];
    }
    return _headView;
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat width = kScreenWidth/2-15;
        layout.itemSize = CGSizeMake(width, width*0.75);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headView.frame), kScreenWidth, kScreenHeight-CGRectGetHeight(self.headView.frame)-64-49) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}


@end
