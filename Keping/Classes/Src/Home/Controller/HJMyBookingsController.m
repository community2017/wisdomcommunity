//
//  HJMyBookingsController.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJMyBookingsController.h"
#import "HJSuperstarSoccerCell.h"
#import "HJShopBooksInfoRequest.h"

#import "HJShopBooksInfoModel.h"
#import "HJCellModel.h"

@interface HJMyBookingsController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据源
 */
@property(nonatomic,strong)NSMutableArray*dataAry;
/**
 详情数据
 */
@property(nonatomic,strong)HJShopBooksInfoModel*booksInfoModel;

@end

@implementation HJMyBookingsController
-(NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=[NSMutableArray array];
        NSArray*ary=@[@"Facility",@"Court Name",@"Date & Time",@"Phone"];
        for (int i =0; i<ary.count; i++) {
            HJCellModel*cellModel=[HJCellModel cellModelTitle:ary[i] content:@""];
            [_dataAry addObject:cellModel];
        }
    }
    return _dataAry;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self getData];
}

-(void)getData
{
    [super getData];
    
    HJShopBooksInfoRequest* request  = [[HJShopBooksInfoRequest alloc] initWithBookId:self.bookId];
    WEAKSELF;
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        weakSelf.booksInfoModel = [HJShopBooksInfoModel modelWithJSON:request.responseData];
        if (weakSelf.booksInfoModel.statusCode == 200) {
            for (int i =0; i<weakSelf.dataAry.count; i++) {
                HJCellModel*cellModel=weakSelf.dataAry[i];
                switch (i) {
                    case 0:
                        cellModel.content=weakSelf.booksInfoModel.shopBookView.itemName;
                        break;
                    case 1:
                        cellModel.content=weakSelf.booksInfoModel.shopBookView.storeName;
                        break;
                    case 2:
                        cellModel.content=[NSString stringWithFormat:@"%@ to %@",weakSelf.booksInfoModel.shopBookView.start,weakSelf.booksInfoModel.shopBookView.end];
                        break;
                    case 3:
                        cellModel.content=@"13802939201";
                        break;
                        
                    default:
                        break;
                }
            }
            [weakSelf.tableView reloadData];
        }else{
            [self showErrorText:weakSelf.booksInfoModel.msg errCode:weakSelf.booksInfoModel.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        KLog(@"%@",request.error);
    }];
    
}

-(void)setUI
{
    self.navigationItem.title=@"My Bookings";
    //self.navigationItem.rightBarButtonItem=[UIBarButtonItem itemWithImage:@"share" withName:nil target:self action:@selector(clickShare)];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    [btn setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 20, 20);
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.estimatedSectionFooterHeight=0;
    tableView.estimatedSectionHeaderHeight=0;
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    self.tableView=tableView;
    [self.view addSubview:tableView];

    UIView*headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, K_FactorH(40))];
    UILabel*contentLabel=[[UILabel alloc]initWithFrame:headerView.bounds];
    contentLabel.textAlignment=NSTextAlignmentCenter;
    contentLabel.text=@"Upcoming Booking";
    contentLabel.textColor=[UIColor colorWithHexString:@"#676767"];
    [headerView addSubview:contentLabel];
    headerView.backgroundColor=[UIColor clearColor];
    tableView.tableHeaderView=headerView;
    
}
-(void)clickShare
{
    
}
#pragma mark  --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataAry.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HJSuperstarSoccerCell*cell=[HJSuperstarSoccerCell cellWithTableView:tableView];
    cell.isShowArrow=NO;
    cell.cellModel=self.dataAry[indexPath.section];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(60);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return K_FactorH(10);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==3) {
        NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"186xxxx6979"];
        UIWebView*callWebview = [[UIWebView alloc] init];
        [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
