//
//  HJSuperstarSoccerController.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSuperstarSoccerController.h"

#import "HJSuperstarSoccerCell.h"

#import "HJHeaderImgView.h"

#import "HJCellModel.h"
#import "HJShopViewsData.h"

#import "KShopsDetailRequest.h"

@interface HJSuperstarSoccerController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableView
 */
@property(nonatomic,weak) UITableView*tableView;
/**
 预约按钮
 */
@property(nonatomic,weak)UIButton*bookBtn;
/**
 数据源
 */
@property(nonatomic,strong)NSArray*dataAry;
/**
 内容源
 */
@property(nonatomic,strong)NSMutableArray*contentAry;
/**
 商家详情
 */
@property(nonatomic,strong)KShopViewData*shopViewsData;
@end

@implementation HJSuperstarSoccerController

-(NSArray *)dataAry
{
    if (!_dataAry) {
        HJCellModel*cell1=[HJCellModel cellModelTitle:@"Court Name" content:@""];
        HJCellModel*cell2=[HJCellModel cellModelTitle:@"Date" content:@""];
        
        _dataAry=@[@[cell1],@[cell2]];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    [self getData];
}
-(void)setUI
{
    self.navigationItem.title=@"Superstar Soccer";
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.estimatedSectionHeaderHeight=0;
    tableView.estimatedSectionFooterHeight=0;
    self.tableView=tableView;
    [self.view addSubview:tableView];

    HJHeaderImgView*headerView=[HJHeaderImgView headerImgView];
    headerView.imgUrl=self.shopViewData.bannerImg;
    tableView.tableHeaderView=headerView;
    
    //添加table尾部
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, K_FactorH(80))];
    tableView.tableFooterView=footerView;
    footerView.backgroundColor=[UIColor clearColor];
    CGFloat margin =K_FactorW(20);
    UIButton*bookBtn=[[UIButton alloc]initWithFrame:CGRectMake(margin, K_FactorH(30), footerView.width-2*margin, footerView.height-K_FactorH(30))];
    self.bookBtn=bookBtn;
    [footerView addSubview:bookBtn];
    bookBtn.layer.cornerRadius=bookBtn.height/2;
    bookBtn.enabled=NO;
    bookBtn.layer.masksToBounds=YES;
    [bookBtn setBackgroundColor:[UIColor colorWithHexString:@"#6853fa"] forState:UIControlStateNormal];
    [bookBtn setBackgroundColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [bookBtn setTitle:@"Book" forState:UIControlStateNormal];
    bookBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [bookBtn addTarget:self action:@selector(clickBook) forControlEvents:UIControlEventTouchUpInside];

}
-(void)clickBook
{
    
}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    KShopsDetailRequest* facilitiesRequest = [[KShopsDetailRequest alloc]initWithShopId:self.shopViewData.id];
    WEAKSELF;
    [facilitiesRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        weakSelf.shopViewsData = [KShopViewData modelWithJSON:request.responseData];
        if (weakSelf.shopViewsData.statusCode == 200) {
            
        }else{
            [weakSelf showErrorText:weakSelf.shopViewsData.msg errCode:weakSelf.shopViewsData.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
    
}

#pragma mark  --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataAry.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataAry[section] count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HJSuperstarSoccerCell*cell=[HJSuperstarSoccerCell cellWithTableView:tableView];
    cell.cellModel=self.dataAry[indexPath.section][indexPath.row];
    cell.isShowArrow=NO;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return K_FactorH(10);
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(60);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
}

@end
