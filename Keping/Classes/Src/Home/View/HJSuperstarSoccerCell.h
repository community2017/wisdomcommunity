//
//  HJSuperstarSoccerCell.h
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HJCellModel;
@interface HJSuperstarSoccerCell : UITableViewCell
/**
 模型
 */
@property(nonatomic,strong)HJCellModel*cellModel;
/**
 是否显示箭头 默认显示
 */
@property(nonatomic,assign)BOOL isShowArrow;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
