
//
//  KHomeHeadView.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KHomeHeadView.h"

@implementation KHomeHeadView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    [self.itemView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-10);
    }];
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(210);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.mas_equalTo(self.itemView.mas_top).offset(10);
    }];
}

-(UIImageView *)backView
{
    if (!_backView) {
        _backView = [UIImageView new];
        _backView.clipsToBounds = YES;
        _backView.image = [UIImage imageNamed:@"home_top_back"];
        _backView.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:_backView];
    }
    return _backView;
}

-(UIImageView *)itemView
{
    if (!_itemView) {
        _itemView = [UIImageView new];
        _itemView.clipsToBounds = YES;
        _itemView.backgroundColor = kClearColor;
        _itemView.image = [UIImage imageNamed:@"home_setiapath_community"];
        _itemView.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:_itemView];
    }
    return _itemView;
}

-(UIImageView *)titleView
{
    if (!_titleView) {
        _titleView = [UIImageView new];
        _titleView.clipsToBounds = YES;
        _titleView.image = [UIImage imageNamed:@"home_search_bar"];
        _titleView.backgroundColor = kClearColor;
        _titleView.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:_titleView];
    }
    return _titleView;
}

@end
