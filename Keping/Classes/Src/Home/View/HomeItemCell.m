//
//  HomeItemCell.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HomeItemCell.h"

@implementation HomeItemCell


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    [self.itemView mas_makeConstraints:^(MASConstraintMaker *make) {
        //make.width.height.mas_equalTo(kScreenWidth/7);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(self.contentView.mas_height).multipliedBy(0.6);
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(self.itemView.mas_bottom).offset(5);
    }];
}

-(UIImageView *)backView
{
    if (!_backView) {
        _backView = [UIImageView new];
        _backView.layer.cornerRadius = 3.0;
        _backView.clipsToBounds = YES;
        _backView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_backView];
    }
    return _backView;
}

-(UIImageView *)itemView
{
    if (!_itemView) {
        _itemView = [UIImageView new];
        _itemView.clipsToBounds = YES;
        _itemView.backgroundColor = kClearColor;
        _itemView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_itemView];
    }
    return _itemView;
}

-(UILabel *)titleLb
{
    if (!_titleLb) {
        _titleLb.textAlignment = NSTextAlignmentCenter;
        _titleLb.textColor = kWhiteColor;
        _titleLb.font = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        _titleLb.backgroundColor = kClearColor;
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}

@end
