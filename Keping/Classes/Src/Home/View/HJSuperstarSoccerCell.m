//
//  HJSuperstarSoccerCell.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSuperstarSoccerCell.h"

#import "HJCellModel.h"

@interface HJSuperstarSoccerCell()
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;
/**
 箭头
 */
@property(nonatomic,weak)UIImageView*arrowImgView;

@end
@implementation HJSuperstarSoccerCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJSuperstarSoccerCell";
    HJSuperstarSoccerCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor=[UIColor colorWithHexString:@"#0f0f0f"];
    WEAKSELF;
    CGFloat margin =K_FactorW(10);
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf).offset(margin);
        make.top.bottom.mas_equalTo(weakSelf);
        make.width.mas_equalTo(K_FactorW(100));
    }];
    self.titleLabel=titleLabel;
    
    //箭头
    UIImageView*arrowImgView=[[UIImageView alloc]init];
    [self.contentView addSubview:arrowImgView];
    CGFloat imgWH=K_FactorW(25/2);
    arrowImgView.backgroundColor=[UIColor redColor];
    [arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf).offset(-margin);
        make.size.mas_equalTo(CGSizeMake(imgWH, imgWH));
        make.centerY.mas_equalTo(weakSelf);
    }];
    self.arrowImgView=arrowImgView;
    
    //内容
    UILabel*contentLael=[[UILabel alloc]init];
    [self.contentView addSubview:contentLael];
    contentLael.textColor=[UIColor colorWithHexString:@"#0f0f0f"];
    contentLael.textAlignment=NSTextAlignmentCenter;
    [contentLael mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel.mas_right).offset(margin);
        make.top.bottom.mas_equalTo(weakSelf);
        make.right.mas_equalTo(arrowImgView.mas_left).offset(-margin);
    }];
    self.contentLabel=contentLael;
}
-(void)setCellModel:(HJCellModel *)cellModel
{
    _cellModel=cellModel;
    self.titleLabel.text=cellModel.title;
    self.contentLabel.text=cellModel.content;
}
-(void)setIsShowArrow:(BOOL)isShowArrow
{
    _isShowArrow=isShowArrow;
    self.arrowImgView.hidden=!isShowArrow;
}
@end
