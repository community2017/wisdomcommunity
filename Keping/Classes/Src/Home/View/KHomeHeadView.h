//
//  KHomeHeadView.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KHomeHeadView : UIView

/**/
@property(nonatomic,strong)UIImageView* backView;
/**/
@property(nonatomic,strong)UIImageView* itemView;
/**/
@property(nonatomic,strong)UIImageView* titleView;


@end
