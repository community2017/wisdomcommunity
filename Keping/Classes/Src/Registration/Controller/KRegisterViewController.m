//
//  KRegisterViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/22.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KRegisterViewController.h"
#import "KTextCell.h"
#import "KVeritifyCodeCell.h"
#import "KAgreementViewController.h"
#import "KUserData.h"
#import "KLoginRequest.h"
#import "KCodeRequest.h"
#import "KRequestData.h"

@interface KRegisterViewController ()
{
    NSArray* items;
    
}
@property(nonatomic,strong)UILabel* registerLabel;
@property(nonatomic,strong)UIButton* registerBtn;
@property(nonatomic,strong)UIButton* agreeBtn;
@property(nonatomic,strong)YYLabel* textLabel;
@property(nonatomic,assign)CGFloat tableTop;

@property(nonatomic,copy)NSString* phone;
@property(nonatomic,copy)NSString* email;
@property(nonatomic,copy)NSString* nickName;
@property(nonatomic,copy)NSString* passWord;
@property(nonatomic,copy)NSString* confirmPwd;
@property(nonatomic,copy)NSString* smsCode;

@end

@implementation KRegisterViewController

-(void)viewWillAppear:(BOOL)animated
{    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.whiteNaviBar.hidden = NO;
    [self.view bringSubviewToFront:self.whiteNaviBar];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    items = @[@"UserName",@"Email",@"Phone",@"Enter Verification Code",@"Password",@"Confirm Password"];
    
    self.tableView.refreshEnable = NO;
    self.tableView.loadmoreEnable = NO;
    self.tableView.rowHeight = 54;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableTop = self.registerLabel.bottom;
    [self layout];
}

#pragma mark - Network
-(void)requestCode{
    KCodeRequest* codeRequest = [[KCodeRequest alloc] initWithPhone:self.phone];
    codeRequest.params = @{@"phone":self.phone};
    [codeRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KRequestData* data = [KRequestData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            [self showSuccess:@"验证码已发送！"];
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self showErrorText:@"发送失败！"];
    }];
}

-(void)registerAction{
    if (![self.nickName isNotBlank]) {
        [self showErrorText:@"用户名不能为空"];
        return;
    }
    if (![self.email isNotBlank]) {
        [self showErrorText:@"邮箱不能为空"];
        return;
    }
    if (![self.phone isNotBlank]) {
        [self showErrorText:@"手机号不能为空"];
        return;
    }
    if (![self.smsCode isNotBlank]) {
        [self showErrorText:@"验证码不能为空"];
        return;
    }
    
    if (![self.passWord isNotBlank]) {
        [self showErrorText:@"密码不能为空"];
        return;
    }
    if (![self.confirmPwd isNotBlank]) {
        [self showErrorText:@"确认密码不能为空"];
        return;
    }
    if (![self.passWord isEqualToString:self.confirmPwd]) {
        [self showErrorText:@"两次输入密码不一致！"];
        return;
    }
    
    [self showLoading:@"正在注册..."];
    KRegisterRequest* request = [[KRegisterRequest alloc] init];
    request.params = [NSDictionary dictionaryWithObjectsAndKeys:self.phone,@"phone",self.email,@"email",self.nickName,@"nickName",self.smsCode,@"smsCode",[self.passWord md5String],@"passWord",[self.confirmPwd md5String],@"confirmPwd", nil];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KUserData* data = [KUserData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            [self showSuccess:@"注册成功！"];
            if ([data archiveWithKey:@"KTempUserData"]) {
                
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self showErrorText:@"注册失败!"];
    }];
    
}

-(void)layout{
    [self.tableView setFrame:CGRectMake(20, self.tableTop, kScreenWidth-40, items.count*55)];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.registerBtn setFrame:CGRectMake(10, self.tableView.bottom+20, kScreenWidth-20, 39)];
    [self.agreeBtn setFrame:CGRectMake(self.registerBtn.left, self.registerBtn.bottom+5, 20, 20)];
    [self.textLabel setFrame:CGRectMake(self.agreeBtn.right+10, self.agreeBtn.top, kScreenWidth-self.agreeBtn.width-40, 20)];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return items.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @weakify(self);
    NSString* placeholder = items[indexPath.row];
    if ([placeholder isEqualToString:@"Enter Verification Code"]) {
        KVeritifyCodeCell* cell = [tableView dequeueReusableCellWithIdentifier:[KVeritifyCodeCell className]];
        if (!cell) {
            cell = [[KVeritifyCodeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KVeritifyCodeCell className]];
        }
        cell.textTf.placeholder = items[indexPath.row];
        [cell.codeBtn setTitle:@"Get Veritification Code" forState:UIControlStateNormal];
        cell.codeBtn.layer.cornerRadius = 19.5;
        cell.buttonBlock = ^{
            if (![weak_self.phone isNotBlank]) {
                [weak_self showErrorText:@"手机号码不能为空！"];
                return;
            }else{
                [weak_self requestCode];
            }
        };
        cell.codeBlock = ^(NSString *text) {
            if ([text isNotBlank]) {
                weak_self.smsCode = text;
            }
        };
        return cell;
    }else{
        KTextCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTextCell className]];
        if (!cell) {
            cell = [[KTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTextCell className]];
        }
        cell.textTf.placeholder = items[indexPath.row];
        cell.textBlock = ^(NSString *text) {
            if ([@"UserName" isEqualToString:placeholder]) {
                weak_self.nickName = text;
            }else if ([@"Email" isEqualToString:placeholder]){
                weak_self.email = text;
            }else if ([@"Phone" isEqualToString:placeholder]){
                weak_self.phone = text;
            }else if ([@"Password" isEqualToString:placeholder]){
                weak_self.passWord = text;
            }else if ([@"Confirm Password" isEqualToString:placeholder]){
                weak_self.confirmPwd = text;
            }
        };
        return cell;
    }
    return nil;
}



-(UIButton *)agreeBtn
{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateNormal];
        [_agreeBtn setImage:[UIImage imageNamed:@"disagree"] forState:UIControlStateSelected];
        @weakify(self);
        [_agreeBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            weak_self.agreeBtn.selected = !weak_self.agreeBtn.selected;
        }];
        [self.view addSubview:_agreeBtn];
    }
    return _agreeBtn;
}

-(YYLabel *)textLabel
{
    if (!_textLabel) {
        _textLabel = [[YYLabel alloc]init];
        _textLabel.textAlignment = NSTextAlignmentLeft;
        _textLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
        __weak typeof(self) weak_self = self;
        NSMutableAttributedString* tt = [[NSMutableAttributedString alloc]initWithString:@"I agree the terms of the User Agreement"];
        tt.font = SystemFont;
        tt.alignment = NSTextAlignmentCenter;
        [tt addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:tt.rangeOfAll];
        [tt setTextHighlightRange:NSMakeRange(0, 25) color:[UIColor darkGrayColor] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            weak_self.agreeBtn.selected = !weak_self.agreeBtn.selected;
        }];
        [tt setTextHighlightRange:NSMakeRange(25, tt.length-25) color:[UIColor colorWithHexString:@"714FFA"] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            KAgreementViewController* vc = [KAgreementViewController new];
            vc.title = @"User Agreement";
            [weak_self.navigationController pushViewController:vc animated:YES];
        }];
        tt.alignment = NSTextAlignmentLeft;
        _textLabel.attributedText = tt;
        [self.view addSubview:_textLabel];
    }
    return _textLabel;
}

-(UILabel *)registerLabel
{
    if (!_registerLabel) {
        _registerLabel = [UILabel new];
        _registerLabel = [[UILabel alloc]init];
        _registerLabel.text = @"Register";
        [_registerLabel sizeToFit];
        _registerLabel.textAlignment = NSTextAlignmentCenter;
        _registerLabel.textColor = [UIColor colorWithHexString:@"714FFA"];
        _registerLabel.font = [UIFont boldSystemFontOfSize:32];
        [_registerLabel setFrame:CGRectMake(0, 74, kScreenWidth, 54)];
        [self.view addSubview:_registerLabel];
    }
    return _registerLabel;
}

-(UIButton *)registerBtn
{
    if (!_registerBtn) {
        _registerBtn = [UIButton commonButtonWithTitle:@"Register"];
        _registerBtn.layer.cornerRadius = 19.5;
        [_registerBtn addTarget:self action:@selector(registerAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_registerBtn];
    }
    return _registerBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
