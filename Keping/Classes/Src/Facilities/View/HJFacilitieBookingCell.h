//
//  HJFacilitieBookingCell.h
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class shopBookIngViewsModel,shopBookOtherViewsModel;
@interface HJFacilitieBookingCell : UITableViewCell

/**
 正在预约中模型
 */
@property(nonatomic,strong)shopBookIngViewsModel*shopBookIngModel;
/**
 已处理模型
 */
@property(nonatomic,strong)shopBookOtherViewsModel*shopBookOtherModel;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
