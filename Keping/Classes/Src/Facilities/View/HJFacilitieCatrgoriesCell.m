//
//  HJFacilitieCatrgoriesCell.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitieCatrgoriesCell.h"

#import "HJShopViewsData.h"

@interface HJFacilitieCatrgoriesCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*imgView;
/**
 商家名称
 */
@property(nonatomic,weak)UILabel*titleLabel;

@end
@implementation HJFacilitieCatrgoriesCell
-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    //logo
    UIImageView*imgView=[[UIImageView alloc]init];
    [self.contentView addSubview:imgView];
    self.imgView=imgView;
    
    //商家名称
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor = kWhiteColor;
    titleLabel.font = kBoldFont(18);
    titleLabel.textAlignment=NSTextAlignmentCenter;
    self.titleLabel=titleLabel;
}

-(void)setShopViewData:(KShopView *)shopViewData
{
    if (!shopViewData) return;
    _shopViewData=shopViewData;
    @weakify(self);
    [self.imgView setImageWithURL:[NSURL URLWithString:shopViewData.logo] placeholder:[UIImage imageNamed:@"placeholder"] options:kNilOptions completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if (!image) {
            weak_self.imgView.image = [UIImage imageNamed:@"placeholder"];
        }else{
            weak_self.imgView.image = image;
        }
    }];
    NSString* title = shopViewData.storeName;
    if ([title isNotBlank]) {
        self.titleLabel.text = [title stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[title substringToIndex:1] uppercaseString]];
    }
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    self.imgView.frame=self.bounds;
    
    CGFloat H= K_FactorH(30);
    self.titleLabel.frame=(CGRect){{0,self.height-H},{self.width,H}};
}
@end
