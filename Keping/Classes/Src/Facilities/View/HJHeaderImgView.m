//
//  HJHeaderImgView.m
//  Keping
//
//  Created by a on 2017/9/6.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJHeaderImgView.h"

@interface HJHeaderImgView()
/**
 内部图片
 */
@property(nonatomic,weak)UIImageView*contentView;

@end
@implementation HJHeaderImgView

+(instancetype)headerImgView
{
    
    HJHeaderImgView*headerView=[[HJHeaderImgView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width,K_FactorH(180) )];
    
    return headerView;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        self.image=[UIImage imageNamed:@"bg_bar"];
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    UIImage*contentImg=[UIImage imageNamed:@"visit_friends2"];
    CGFloat contentW=self.width-2*K_FactorW(10);
    CGFloat contentH=contentW/contentImg.size.width*contentImg.size.height;
    if (contentH>self.height) {
        contentH=self.height-2*K_FactorH(10);
        contentW=contentH/contentImg.size.height*contentImg.size.width;
    }
    UIImageView*contentView=[[UIImageView alloc]initWithFrame:CGRectMake((self.width-contentW)/2,(self.height-contentH)/2,contentW, contentH)];
    [self addSubview:contentView];
    contentView.image=contentImg;
    self.contentView=contentView;

}
-(void)setImgUrl:(NSString *)imgUrl
{
    _imgUrl=imgUrl;
    [self.contentView setImageWithURL:[NSURL URLWithString:imgUrl] placeholder:[UIImage imageNamed:@"visit_friends2"]];
//    [self.contentView setImageURL:];
    
}
@end
