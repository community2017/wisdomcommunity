//
//  HJFacilitieBookingCell.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitieBookingCell.h"
#import "HJShopBooksModel.h"

@interface HJFacilitieBookingCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 年月
 */
@property(nonatomic,weak)UILabel*yearsLabel;
/**
 时间
 */
@property(nonatomic,weak)UILabel*timeLabel;

@end
@implementation HJFacilitieBookingCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJFacilitieBookingCell";
    HJFacilitieBookingCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.font = SystemBoldFont;
    titleLabel.textColor = kBlackColor;
    self.titleLabel=titleLabel;
    
    
    //年月
    UILabel*yearsLabel=[[UILabel alloc]init];
    [self.contentView addSubview:yearsLabel];
    yearsLabel.font = SystemFont;
    yearsLabel.textColor = kDarkGrayColor;
    self.yearsLabel=yearsLabel;
    
    //时间
    UILabel*timeLabel=[[UILabel alloc]init];
    [self.contentView addSubview:timeLabel];
    timeLabel.textColor=[UIColor colorWithHexString:@"#6a6a6a"];
    timeLabel.font = SystemFont;
    self.timeLabel=timeLabel;
}
-(void)setShopBookIngModel:(shopBookIngViewsModel *)shopBookIngModel
{
    _shopBookIngModel=shopBookIngModel;
    [self.iconView setImageWithURL:[NSURL URLWithString:shopBookIngModel.logo] placeholder:[UIImage imageNamed:KImagePlaceholder]];
    
    self.titleLabel.text=shopBookIngModel.storeName;
    
    self.yearsLabel.text=shopBookIngModel.bookDay;
    
    self.timeLabel.text=[NSString stringWithFormat:@"%@-%@",shopBookIngModel.start,shopBookIngModel.end];
}
-(void)setShopBookOtherModel:(shopBookOtherViewsModel *)shopBookOtherModel
{
    _shopBookOtherModel=shopBookOtherModel;
    [self.iconView setImageURL:[NSURL URLWithString:shopBookOtherModel.logo]];
    
    self.titleLabel.text=shopBookOtherModel.storeName;
    
    self.yearsLabel.text=shopBookOtherModel.bookDay;
    
    self.timeLabel.text=[NSString stringWithFormat:@"%@-%@",shopBookOtherModel.start,shopBookOtherModel.end];

}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat imgWH= K_FactorW(75);
    self.iconView.frame=(CGRect){{K_FactorW(10),(self.height-imgWH)/2},{imgWH,imgWH}};
    
    CGFloat labelH=imgWH/3;
    self.titleLabel.frame=(CGRect){{CGRectGetMaxX(self.iconView.frame)+K_FactorW(10),self.iconView.y},{self.width-CGRectGetMaxX(self.iconView.frame)-2*K_FactorW(10),labelH}};
    
    self.yearsLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)},{self.titleLabel.width,self.titleLabel.height}};
    
    self.timeLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.yearsLabel.frame)},{self.titleLabel.width,self.titleLabel.height}};
}
@end
