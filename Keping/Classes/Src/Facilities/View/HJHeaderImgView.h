//
//  HJHeaderImgView.h
//  Keping
//
//  Created by a on 2017/9/6.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJHeaderImgView : UIImageView

/**
 封面图片
 */
@property(nonatomic,copy)NSString*imgUrl;
+(instancetype)headerImgView;
@end
