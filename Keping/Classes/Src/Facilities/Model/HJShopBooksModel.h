//
//  HJShopBooksModel.h
//  Keping
//
//  Created by a on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@class shopBookIngViewsModel,shopBookOtherViewsModel;
@interface HJShopBooksModel : NSObject
/**
 状态描述
 */
@property(nonatomic,copy)NSString*msg;
/**
 正在预约中
 */
@property(nonatomic,strong)NSArray<shopBookIngViewsModel*>*shopBookIngViews;
/**
 已经处理
 */
@property(nonatomic,strong)NSArray<shopBookIngViewsModel*>*shopBookOtherViews;
/**
 状态 200成功
 */
@property(nonatomic,assign)NSInteger statusCode;
@end
@interface shopBookIngViewsModel : NSObject
/**
 预定日期
 */
@property(nonatomic,copy)NSString*bookDay;
/**
 社区名字
 */
@property(nonatomic,copy)NSString* communityName;
/**
 结束时间段
 */
@property(nonatomic,copy)NSString* end;
/**
 预定id
 */
@property(nonatomic,copy)NSString*id;
/**
 场地id
 */
@property(nonatomic,copy)NSString* itemId;
/**
 场地名称
 */
@property(nonatomic,copy)NSString* itemName;
/**
 logo
 */
@property(nonatomic,copy)NSString* logo;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString* shopId;
/**
 开始时间段
 */
@property(nonatomic,copy)NSString* start;
/**
 ing=预约中 ，success=成功，fail=预约失败，del=已删除,consume=消费
 */
@property(nonatomic,copy)NSString* status;
/**
 场馆名
 */
@property(nonatomic,copy)NSString*storeName;

@end

@interface shopBookOtherViewsModel : NSObject
/**
 预定日期
 */
@property(nonatomic,copy)NSString*bookDay;
/**
 社区名字
 */
@property(nonatomic,copy)NSString* communityName;
/**
 结束时间段
 */
@property(nonatomic,copy)NSString* end;
/**
 预定id
 */
@property(nonatomic,copy)NSString*id;
/**
 场地id
 */
@property(nonatomic,copy)NSString* itemId;
/**
 场地名称
 */
@property(nonatomic,copy)NSString* itemName;
/**
 logo
 */
@property(nonatomic,copy)NSString* logo;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString* shopId;
/**
 开始时间段
 */
@property(nonatomic,copy)NSString* start;
/**
 ing=预约中 ，success=成功，fail=预约失败，del=已删除,consume=消费
 */
@property(nonatomic,copy)NSString* status;
/**
 场馆名
 */
@property(nonatomic,copy)NSString*storeName;

@end
