//
//  HJCellModel.m
//  Keping
//
//  Created by a on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJCellModel.h"

@implementation HJCellModel
+(instancetype)cellModelTitle:(NSString *)title content:(NSString *)content
{
    HJCellModel*model=[[self alloc]init];
    model.title=title;
    model.content=content;
    return model;
}
@end
