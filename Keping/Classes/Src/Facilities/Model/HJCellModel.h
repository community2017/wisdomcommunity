//
//  HJCellModel.h
//  Keping
//
//  Created by a on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HJCellModel : NSObject
/**
 标题
 */
@property(nonatomic,copy)NSString*title;
/**
 内容
 */
@property(nonatomic,copy)NSString*content;

+(instancetype)cellModelTitle:(NSString*)title content:(NSString*)content;

@end
