//
//  HJShopViewsData.h
//  Keping
//
//  Created by 柯平 on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KShopItemTimeView : NSObject

@property (nonatomic, strong) NSString * businessHoursEnd;
@property (nonatomic, strong) NSString * businessHoursStart;
@property (nonatomic, strong) NSString * stauts;

@end

@interface KShopItemView : NSObject
/**/
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString * idField;
@property (nonatomic, strong) NSArray<KShopItemTimeView*>* shopItemTimeViews;
@property (nonatomic, strong) NSString * stauts;
@property (nonatomic, strong) NSString * name;

@end

@interface KShopView : NSObject

@property (nonatomic, strong) NSString * addr;
@property (nonatomic, strong) NSString * bannerImg;
@property (nonatomic, assign) NSInteger businessHours;
@property (nonatomic, strong) NSString * cateId;
@property (nonatomic, strong) NSString * contact;
@property (nonatomic, assign) NSTimeInterval createTime;
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * idField;
@property (nonatomic, assign) double latitude;
@property (nonatomic, strong) NSString * logo;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) NSInteger nowDate;
@property (nonatomic, assign) double openTime;
@property (nonatomic, strong) NSString * partyId;
@property (nonatomic, strong) NSArray<KShopItemView*>* shopItemViews;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * storeName;
@property (nonatomic, strong) NSString * summary;

@end

@interface KShopViewData : NSObject

@property (nonatomic, strong) NSString * msg;
@property (nonatomic, strong) KShopView * shopView;
@property (nonatomic, assign) NSInteger statusCode;

@end

@interface HJShopViewsData : NSObject


/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,strong)NSArray<KShopView*>* shopViews;

@end
