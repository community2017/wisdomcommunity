//
//  HJShopViewsData.m
//  Keping
//
//  Created by 柯平 on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJShopViewsData.h"

@implementation KShopItemTimeView

@end

@implementation KShopItemView

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"shopItemTimeViews":[KShopItemTimeView class]};
}
@end


@implementation KShopView

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"shopItemViews":[KShopItemView class]};
}
@end


@implementation KShopViewData

@end


@implementation HJShopViewsData

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"shopViews":[KShopView class]};
}
@end

