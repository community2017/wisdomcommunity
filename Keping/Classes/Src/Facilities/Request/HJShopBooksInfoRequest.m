//
//  HJShopBooksInfoRequest.m
//  Keping
//
//  Created by a on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJShopBooksInfoRequest.h"

@implementation HJShopBooksInfoRequest
{
    NSString* _requestUrl;
}

-(instancetype)initWithBookId:(NSString *)bookId
{
    if (![bookId isNotBlank]) return nil;
    if (self == [super init]) {
        NSString* url = [NSString stringWithFormat:@"%@",KFacilitiesBookDetailURL];
        if ([url containsString:@"bookId"]) {
            url = [url stringByReplacingOccurrencesOfString:@"bookId" withString:bookId];
        }
        _requestUrl = url;
    }
    
    return self;
}


-(NSString *)requestUrl
{
    return _requestUrl;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}


@end
