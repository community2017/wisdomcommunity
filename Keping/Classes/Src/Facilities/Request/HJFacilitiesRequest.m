//
//  HJFacilitiesRequest.m
//  Keping
//
//  Created by 柯平 on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitiesRequest.h"

@implementation HJFacilitiesRequest
-(NSString *)requestUrl
{
    return KPCategoriesURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}

@end
