//
//  HJShopBooksInfoRequest.h
//  Keping
//
//  Created by a on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HJShopBooksInfoRequest : KRequest

-(instancetype)initWithBookId:(NSString*)bookId;

@end
