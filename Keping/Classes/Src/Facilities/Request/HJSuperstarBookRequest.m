//
//  HJSuperstarBookRequest.m
//  Keping
//
//  Created by a on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSuperstarBookRequest.h"

@implementation HJSuperstarBookRequest
-(NSString *)requestUrl
{
    return KPSuperstarBookURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(id)requestArgument
{
    return self.params;
}
@end
