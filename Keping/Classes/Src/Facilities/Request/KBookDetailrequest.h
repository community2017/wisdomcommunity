//
//  KBookDetailrequest.h
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KRequest.h"

@interface KBookDetailrequest : KRequest

-(instancetype)initWithBookId:(NSString*)bookId;

@end
