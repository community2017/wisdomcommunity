//
//  KBookDetailrequest.m
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KBookDetailrequest.h"

@implementation KBookDetailrequest
{
    NSString* _requestUrl;
}

-(instancetype)initWithBookId:(NSString *)bookId
{
    if (![bookId isNotBlank]) return nil;
    
    self = [super init];
    if (self) {
        NSString* url = [NSString stringWithFormat:@"%@",KFacilitiesBookDetailURL];
        _requestUrl = [url stringByReplacingOccurrencesOfString:@"bookId" withString:bookId];
    }
    return self;
}

-(NSString *)requestUrl
{
    return _requestUrl;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}



@end
