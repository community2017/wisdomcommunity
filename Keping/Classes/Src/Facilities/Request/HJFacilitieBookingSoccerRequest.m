//
//  HJFacilitieBookingSoccerRequest.m
//  Keping
//
//  Created by a on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitieBookingSoccerRequest.h"

@implementation HJFacilitieBookingSoccerRequest
-(NSString *)requestUrl
{
    return KPSuperstarBookListURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

@end
