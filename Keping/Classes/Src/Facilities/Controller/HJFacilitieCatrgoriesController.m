//
//  HJFacilitieCatrgoriesController.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitieCatrgoriesController.h"
#import "HJSuperstarSoccerController.h"


#import "HJFacilitieCatrgoriesCell.h"

#import "HJFacilitiesRequest.h"

#import "HJShopViewsData.h"

@interface HJFacilitieCatrgoriesController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
/**
 数据源
 */
//@property(nonatomic,strong)NSArray*dataAry;

@property(nonatomic,strong)HJShopViewsData*shopData;
@property(nonatomic,weak)UICollectionView *collectionView;
@end

@implementation HJFacilitieCatrgoriesController
//-(NSArray *)dataAry
//{
//    if (!_dataAry) {
//        _dataAry=@[@"Gym",@"BBQArea",@"Tennis",@"TableTennis",@"Badminton",@"Futsa",@"SwimmingPool"];
//    }
//    return _dataAry;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self getData];
}
-(void)setUI
{
    // 这个是系统提供的布局类，可以布局一些比较规则的布局。
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    // 设置每个item的大小，
    CGFloat margin =K_FactorW(12);
    CGFloat itemW = (App_Frame_Width-3*margin)/2;
    UIImage*img=[UIImage imageNamed:@"Badminton"];
    CGFloat itemH =itemW/img.size.width*img.size.height;
    flowLayout.itemSize = CGSizeMake(itemW, itemH);

    // 设置列的最小间距
    flowLayout.minimumInteritemSpacing =margin;
    // 设置最小行间距
    flowLayout.minimumLineSpacing =margin;
    // 设置布局的内边距
    flowLayout.sectionInset = UIEdgeInsetsMake(margin,margin,margin,margin);
    // 滚动方向
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    //    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.collectionView=collectionView;
    // 置背景颜色
    collectionView.backgroundColor = [UIColor whiteColor];
    // 设置代理
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView registerClass:[HJFacilitieCatrgoriesCell class] forCellWithReuseIdentifier:[HJFacilitieCatrgoriesCell className]];
    
    [self.view addSubview:collectionView];
}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    HJFacilitiesRequest* facilitiesRequest = [HJFacilitiesRequest new];
    WEAKSELF;
    [facilitiesRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        weakSelf.shopData = [HJShopViewsData modelWithJSON:request.responseData];
        if (weakSelf.shopData.statusCode == 200) {
            [weakSelf.collectionView reloadData];
        }else{
            [self showErrorText:self.shopData.msg errCode:self.shopData.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];

}
// 返回分区数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

// 每个分区多少个item
- (NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.shopData.shopViews.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HJFacilitieCatrgoriesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[HJFacilitieCatrgoriesCell className] forIndexPath:indexPath];
    cell.shopViewData=self.shopData.shopViews[indexPath.row];
    
    return cell;
}

// 点击
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   
    HJSuperstarSoccerController*vc=[HJSuperstarSoccerController new];
    vc.shopViewData=self.shopData.shopViews[indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
