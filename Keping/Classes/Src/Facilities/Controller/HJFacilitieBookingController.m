//
//  HJFacilitieBookingController.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitieBookingController.h"
#import "HJMyBookingsController.h"


#import "HJFacilitieBookingCell.h"

#import "HJFacilitieBookingSoccerRequest.h"

#import "HJShopBooksModel.h"

@interface HJFacilitieBookingController ()<UITableViewDelegate,UITableViewDataSource>
/**
 数据模型
 */
@property(nonatomic,strong)HJShopBooksModel*bookModel;
/**
 分组标题
 */
@property(nonatomic,strong)NSArray*sectionAry;
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;

@end

@implementation HJFacilitieBookingController
-(NSArray *)sectionAry
{
    if (!_sectionAry) {
        _sectionAry=@[@"Upcoming Bookings",@"Past Bookings"];
    }
    return _sectionAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self getData];
}
-(void)setUI
{
    UITableView*tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.tableFooterView=[UIView new];
    self.tableView=tableView;
    [self.view addSubview:tableView];
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.tableView.frame=self.view.bounds;
}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    HJFacilitieBookingSoccerRequest* listRequest = [HJFacilitieBookingSoccerRequest new];
    [listRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        self.bookModel = [HJShopBooksModel modelWithJSON:request.responseData];
        if (self.bookModel.statusCode == 200) {
            [self.tableView reloadData];
        }else{
            [self showErrorText:self.bookModel.msg errCode:self.bookModel.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
    

}
#pragma mark  --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionAry.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return  self.bookModel.shopBookIngViews.count;
        
    }else
    {
        return self.bookModel.shopBookOtherViews.count;
    }
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HJFacilitieBookingCell*cell=[HJFacilitieBookingCell cellWithTableView:tableView];
    if (indexPath.section==0) {
        cell.shopBookIngModel=self.bookModel.shopBookIngViews[indexPath.row];
    }else
    {
        cell.shopBookIngModel=self.bookModel.shopBookOtherViews[indexPath.row];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return K_FactorH(40);
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return K_FactorH(100);
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [UIView new];
    view.backgroundColor = UIColorHex(F1F2F5);
    UILabel* label = [[UILabel alloc] init];
    label.size = CGSizeMake(kScreenWidth-30, 20);
    label.top = 10;
    label.left = 15;
    label.text = self.sectionAry[section];
    label.textColor = kDarkGrayColor;
    label.font = SystemFont;
    [view addSubview:label];
    return view;
}

//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return self.sectionAry[section];
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    shopBookIngViewsModel* model;
    if (0 == indexPath.section) {
        model = self.bookModel.shopBookIngViews[indexPath.row];
    }else{
        model = self.bookModel.shopBookOtherViews[indexPath.row];
    }
    HJMyBookingsController*vc=[HJMyBookingsController new];
    vc.bookId = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
