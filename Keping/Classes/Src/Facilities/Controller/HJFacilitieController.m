//
//  HJFacilitieController.m
//  Keping
//
//  Created by a on 2017/9/3.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJFacilitieController.h"
#import "HJFacilitieCatrgoriesController.h"
#import "HJFacilitieBookingController.h"

@interface HJFacilitieController ()<UIScrollViewDelegate>
/**
 头部试图
 */
@property(nonatomic,weak)UIView*headerView;
/**
内容
 */
@property(nonatomic,weak)UIScrollView*contentScrollView;
@end

@implementation HJFacilitieController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Facilities";
    //头部滚动视图
    UIView*headerView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.width, K_FactorH(45))];
     [self.view addSubview:headerView];
    self.headerView=headerView;
    

    //centerScroll
    UIScrollView*contentScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(headerView.frame), self.view.width,self.view.height-CGRectGetMaxY(headerView.frame))];
    //    contentScrollView.backgroundColor=[UIColor redColor];
    contentScrollView.pagingEnabled=YES;
    contentScrollView.showsVerticalScrollIndicator=NO;
    contentScrollView.showsHorizontalScrollIndicator=NO;
    contentScrollView.delegate=self;
    [self.view addSubview:contentScrollView];
    self.contentScrollView=contentScrollView;
    
    // 添加子控制器
    [self setupChildVc];
    
    // 添加标题
    [self setupTitle];
    
    // 默认显示第0个子控制器
    [self scrollViewDidEndScrollingAnimation:self.contentScrollView];
}

- (void)setupChildVc
{
    
    HJFacilitieCatrgoriesController *CategoriesVC = [[HJFacilitieCatrgoriesController alloc] init];
    CategoriesVC.title=@"Categories";
    [self addChildViewController:CategoriesVC];
    
    HJFacilitieBookingController *bookingVC = [[HJFacilitieBookingController alloc] init];
    bookingVC.title=@"MyBookings";
    [self addChildViewController:bookingVC];
}
/**
 * 添加标题
 */
- (void)setupTitle
{
    // 定义临时变量
    CGFloat btnW = self.view.width/self.childViewControllers.count;
    CGFloat btnY = 0;
    CGFloat btnH = self.headerView.frame.size.height;
    
    // 添加按钮
    for (NSInteger i = 0; i<self.childViewControllers.count; i++) {
        UIButton *btn = [[UIButton alloc] init];
        [btn setTitle:[self.childViewControllers[i] title] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"#4c5cf7"] forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.backgroundColor=[UIColor clearColor];
        [btn setBackgroundColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn setBackgroundColor:[UIColor colorWithHexString:@"#4c5cf7"] forState:UIControlStateNormal];
        CGFloat btnX = i * btnW;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        [btn addTarget:self action:@selector(clickHeadBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
        [self.headerView addSubview:btn];
        }
    
    
    
    // 设置contentSize
    self.contentScrollView.contentSize = CGSizeMake(self.childViewControllers.count * App_Frame_Width, 0);
}
/**
 * 监听顶部label点击
 */
- (void)clickHeadBtn:(UIButton *)btn
{
    // 取出被点击label的索引
    NSInteger index = btn.tag;
    
    // 让底部的内容scrollView滚动到对应位置
    CGPoint offset = self.contentScrollView.contentOffset;
    offset.x = index * self.contentScrollView.frame.size.width;
    [self.contentScrollView setContentOffset:offset animated:YES];
}
#pragma mark - <UIScrollViewDelegate>
/**
 * scrollView结束了滚动动画以后就会调用这个方法（比如- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated;方法执行的动画完毕后）
 */
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    // 一些临时变量
    CGFloat width = scrollView.frame.size.width;
    CGFloat height = scrollView.frame.size.height;
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 当前位置需要显示的控制器的索引
    NSInteger index = offsetX / width;
    
    // 让对应的顶部标题选中
    UIButton *btn = self.headerView.subviews[index];
    btn.selected=YES;
    //    CGPoint titleOffset = self.titleScrollView.contentOffset;
    // 让其他按钮回到最初的状态
    for (UIButton *otherBtn in self.headerView.subviews) {
        if ([otherBtn isKindOfClass:[UIButton class]]) {
            if (otherBtn != btn) otherBtn.selected=NO;
        }
        
    }
    // 取出需要显示的控制器
    UIViewController *willShowVc = self.childViewControllers[index];
    
    // 如果当前位置的位置已经显示过了，就直接返回
    if ([willShowVc isViewLoaded]) return;
    
    // 添加控制器的view到contentScrollView中;
    willShowVc.view.frame = CGRectMake(offsetX,0, width, height);
    [scrollView addSubview:willShowVc.view];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
  }
/**
 * 手指松开scrollView后，scrollView停止减速完毕就会调用这个
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

@end
