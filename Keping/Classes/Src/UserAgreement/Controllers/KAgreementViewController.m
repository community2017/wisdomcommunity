
//
//  KAgreementViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KAgreementViewController.h"
#import <WebKit/WebKit.h>
#import "KUserProtocolRequest.h"
#import "KProtocolData.h"

@interface KAgreementViewController ()<WKUIDelegate>
@property(nonatomic,strong)WKWebView* webView;


@end

@implementation KAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    KUserProtocolRequest* request = [[KUserProtocolRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingMutableContainers error:nil];
        KProtocolData* data = [[KProtocolData alloc] initWithDictionary:dic];
        if ([data.statusCode isEqualToString:@"200"]) {
            [self.webView loadHTMLString:data.glConfigViews.content baseURL:[NSURL URLWithString:KBaseURL]];
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self showError:request.error];
    }];
    
}

-(WKWebView *)webView
{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
        _webView.UIDelegate = self;
        [self.view addSubview:_webView];
    }
    return _webView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
