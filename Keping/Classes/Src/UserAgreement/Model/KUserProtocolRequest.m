//
//  KUserProtocolRequest.m
//  Keping
//
//  Created by 柯平 on 2017/8/22.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KUserProtocolRequest.h"

@implementation KUserProtocolRequest

-(NSString *)requestUrl{
    return KUserProtocolURL;
}
-(KRequestMethod)requestMethod{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return @{@"key":@"leaderProtocol"};
}

@end
