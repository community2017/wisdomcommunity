//
//  KLoginRequest.m
//  Keping
//
//  Created by 柯平 on 2017/8/21.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KLoginRequest.h"

@implementation KLoginRequest

-(NSString *)requestUrl
{
    return KLoginURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(id)requestArgument
{
    return self.params;
}

@end


@implementation KRegisterRequest

-(NSString *)requestUrl
{
    return KRegisterURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(id)requestArgument
{
    return self.params;
}

@end
