//
//  KUpdateProfileRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KUpdateProfileRequest.h"

@implementation KUpdateProfileRequest

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(NSString *)requestUrl
{
    return KUpdateprofileURL;
}

-(id)requestArgument
{
    return self.params;
}

@end
