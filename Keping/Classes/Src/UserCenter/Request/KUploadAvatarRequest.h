//
//  KUploadAvatarRequest.h
//  Keping
//
//  Created by 柯平 on 2017/10/14.
//  Copyright © 2017年 柯平. All rights reserved.
//  表单上传图片

#import "KRequest.h"

@interface KRequestImgData : NSObject

/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,copy)NSString* imageUrl;

@end

@interface KUploadAvatarRequest : KRequest

-(instancetype)initWithImage:(UIImage*)image;

@end
