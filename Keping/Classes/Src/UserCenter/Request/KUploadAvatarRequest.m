//
//  KUploadAvatarRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KUploadAvatarRequest.h"
#import <AFNetworking.h>

@implementation KRequestImgData

@end

@implementation KUploadAvatarRequest
{
    UIImage* _image;
}

-(instancetype)initWithImage:(UIImage *)image
{
    if (self == [super init]) {
        _image = image;
    }
    return self;
}

-(NSString *)requestUrl
{
    return KUploadImageURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

- (AFConstructingBlock)constructingBodyBlock {
    return ^(id<AFMultipartFormData> formData) {
        NSData *data = UIImageJPEGRepresentation(_image, 0.5);
        NSString *name = @"profile";
        NSString *formKey = @"profile.jpg";
        NSString *type = @"image/jpeg";
        [formData appendPartWithFileData:data name:name fileName:formKey mimeType:type];
    };
}

@end
