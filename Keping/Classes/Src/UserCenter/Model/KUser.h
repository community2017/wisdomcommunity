//
//  KUser.h
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KObject.h"

@interface KUser : KObject

/**/
@property(nonatomic,copy)NSString* avatar;
/**/
@property(nonatomic,copy)NSString* nickname;


@end
