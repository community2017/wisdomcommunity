//
//  KInputViewController.h
//  Keping
//
//  Created by 柯平 on 2017/10/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KViewController.h"

@interface KInputViewController : KViewController

-(instancetype)initWithTitle:(NSString*)title placeholder:(NSString *)placeholder key:(NSString*)key operate:(KInputOperateType)operateType;

@end
