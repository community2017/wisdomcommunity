//
//  KInputViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KInputViewController.h"
#import "KUpdateProfileRequest.h"
#import "KUserData.h"

@interface KInputViewController ()<YYTextViewDelegate>
/**/
@property(nonatomic,copy)NSString* placeholder;
/**/
@property(nonatomic,copy)NSString* key;
/**/
@property(nonatomic,copy)NSString* valueText;
/**/
@property(nonatomic,assign)KInputOperateType type;
/**/
@property(nonatomic,strong)YYTextView* textView;
/**/
@property(nonatomic,strong)UIButton* doneBtn;

@end

@implementation KInputViewController

-(instancetype)initWithTitle:(NSString *)title placeholder:(NSString *)placeholder key:(NSString *)key operate:(KInputOperateType)operateType
{
    self = [super init];
    self.placeholder = placeholder;
    self.key = key;
    self.type = operateType;
    self.title = title;
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_textView) {
        [_textView becomeFirstResponder];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _textView = [YYTextView new];
    [_textView setFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    _textView.backgroundColor = kWhiteColor;
    _textView.contentInset = UIEdgeInsetsMake(5, 10, 5, 10);
    _textView.placeholderFont = SystemFont;
    _textView.delegate = self;
    _textView.placeholderTextColor = kGrayColor;
    _textView.placeholderText = self.placeholder?:@"";
    [self.view addSubview:_textView];
    
    _doneBtn = [UIButton commonButtonWithTitle:@"Submit"];
    [_doneBtn setFrame:CGRectMake(15, _textView.bottom+20, kScreenWidth-30, 44)];
    [_doneBtn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_doneBtn];
    
}

-(void)doneAction{
    [self.view endEditing:YES];
    
    if (![self.valueText isNotBlank]) {
        [self showErrorText:self.placeholder?:@"The input can not be empty!"];
        return;
    }
    
    KUpdateProfileRequest* request = [[KUpdateProfileRequest alloc] init];
    request.params = [NSDictionary dictionaryWithObjectsAndKeys:self.valueText,self.key, nil];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KRequestData* data = [KRequestData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            KPartyView* user = [KPartyView unarchive];
            if ([self.key isEqualToString:@"nickName"]) {
                user.nickName = self.valueText;
            }else if ([self.key isEqualToString:@"addr"]){
                user.addr = self.valueText;
            }
            if ([user archive]) {
                KLog(@"保存用户信息成功");
                [[NSNotificationCenter defaultCenter] postNotificationName:KUpdateUserInfoSuccessNotification object:nil];
            }
            [self showSuccess:@"Successfully modified!"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self showErrorText:@"Network not available"];
    }];
    
}

-(void)textViewDidEndEditing:(YYTextView *)textView
{
    if ([_textView.text isNotBlank]) {
        self.valueText = textView.text;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
