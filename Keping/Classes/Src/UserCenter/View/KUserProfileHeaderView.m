//
//  KUserProfileHeaderView.m
//  Keping
//
//  Created by 柯平 on 2017/10/8.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KUserProfileHeaderView.h"
#import "KUserData.h"
#import "UIView+KP.h"

@implementation KUserProfileHeaderView

-(instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.height == 0 && frame.size.width == 0) {
        frame.size.width = kScreenWidth;
        frame.size.height = kScreenWidth;
    }
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorHex(f1f2f4);
        @weakify(self);
        
        //backView
        _backView = [UIImageView new];
        _backView.image = [UIImage imageNamed:@"user_info_back"];
        _backView.clipsToBounds = YES;
        _backView.frame = self.bounds;
        _backView.height = self.height/2.0;
        [self addSubview:_backView];
        
        //avatar
        _avatar = [UIImageView new];
        _avatar.clipsToBounds = YES;
        _avatar.size = CGSizeMake(60, 60);
        _avatar.center = self.center;
        [_avatar setTapActionWithBlock:^{
            [weak_self editAvatarAction:NO];
        }];
        [self bringSubviewToFront:_avatar];
        [self addSubview:_avatar];
        CALayer *avatarBorder = [CALayer layer];
        avatarBorder.frame = _avatar.bounds;
        avatarBorder.borderWidth = CGFloatFromPixel(2);
        avatarBorder.borderColor = kWhiteColor.CGColor;
        avatarBorder.cornerRadius = _avatar.height / 2;
        avatarBorder.shouldRasterize = YES;
        avatarBorder.rasterizationScale = kScreenScale;
        [_avatar.layer addSublayer:avatarBorder];
        
        //nameLabel
        _nameLabel = [UILabel new];
        _nameLabel.textColor = kDarkGrayColor;
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.top = _avatar.bottom+10;
        _nameLabel.left = 0;
        _nameLabel.width = self.width;
        _nameLabel.size = CGSizeMake(kScreenWidth, 20);
        [_nameLabel setTapActionWithBlock:^{
            [weak_self editAvatarAction:YES];
        }];
        [self addSubview:_nameLabel];
    }
    return self;
}

-(void)editAvatarAction:(BOOL)isName{
    if (self.editBlock) {
        self.editBlock(isName);
    }
}

-(void)setUser:(KPartyView *)user
{
    _user = user;
    @weakify(_avatar);
    [_avatar setImageWithURL:
     [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KGetImageURL,user.profile?:@""]] placeholder:[[[UIImage imageNamed:KImagePlaceholder] imageByResizeToSize:CGSizeMake(60, 60)] imageByRoundCornerRadius:100] options:kNilOptions completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if (image) {
            weak__avatar.image = [[image imageByResizeToSize:CGSizeMake(60, 60)] imageByRoundCornerRadius:100];
        }else{
            weak__avatar.image = [[[UIImage imageNamed:KImagePlaceholder] imageByResizeToSize:CGSizeMake(60, 60)] imageByRoundCornerRadius:100];
        }
    }];
    self.nameLabel.text = user.nickName;
}

@end
