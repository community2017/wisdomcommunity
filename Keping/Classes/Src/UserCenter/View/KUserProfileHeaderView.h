//
//  KUserProfileHeaderView.h
//  Keping
//
//  Created by 柯平 on 2017/10/8.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPartyView;

@interface KUserProfileHeaderView : UIView

/**/
@property(nonatomic,copy)void(^editBlock) (BOOL isName);
/**/
@property(nonatomic,strong)UIImageView* backView;
/**/
@property(nonatomic,strong)UIImageView* avatar;
/**/
@property(nonatomic,strong)UILabel* nameLabel;

/**/
@property(nonatomic,strong)KPartyView* user;

@end
