//
//	KRecommendViewsData.h
//
//	Create by 平 柯 on 20/8/2017
//	Copyright © 2017. All rights reserved.
//  推荐信

//	Model file generated by 平 柯: xtxh@outlook.com

#import <UIKit/UIKit.h>

@interface KGlRecommendView : NSObject

@property (nonatomic, strong) NSString * content;
@property (nonatomic, assign) NSInteger createTime;
@property (nonatomic, strong) NSString * enittyId;
@property (nonatomic, strong) NSString * idField;
@property (nonatomic, strong) NSString * imageUrl;
@property (nonatomic, strong) NSString * isSee;
@property (nonatomic, strong) NSString * partyId;
@property (nonatomic, strong) NSString * subject;


@end

@interface KRecommendViewsData : NSObject

@property (nonatomic, strong) NSArray * glRecommendViews;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, assign) NSInteger statusCode;

@end
