//
//  KBannersData.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KBannersData.h"

@implementation KBanerModel


@end

@implementation KBannersData

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"bannerViews":[KBanerModel class]};
}

@end
