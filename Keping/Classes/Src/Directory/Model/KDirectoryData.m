//
//  KDirectoryData.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDirectoryData.h"

@implementation KDirectory

@end

@implementation KDirectoryData

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"shopDirectView":[KDirectory class]};
}

@end

@implementation KDirectoryDetail

@end
