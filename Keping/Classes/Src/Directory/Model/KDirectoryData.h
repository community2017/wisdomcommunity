//
//  KDirectoryData.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KDirectory : NSObject

/*地址*/
@property(nonatomic,copy)NSString* addr;
/*联系方式*/
@property(nonatomic,copy)NSString* contact;
/*距离*/
@property(nonatomic,assign)CGFloat distance;
/*商品id*/
@property(nonatomic,copy)NSString*   id;
/*图片集*/
@property(nonatomic,copy)NSString* imageUrls;
/*是否为广告*/
@property(nonatomic,assign)BOOL isAd;
/*分类标签*/
@property(nonatomic,copy)NSString* isTop;
/**/
@property(nonatomic,assign)double latitude;
/**/
@property(nonatomic,assign)double longitude;
/*图片*/
@property(nonatomic,copy)NSString* logo;
/*店铺名*/
@property(nonatomic,copy)NSString* storeName;
/*简介*/
@property(nonatomic,copy)NSString* summary;
/**/
@property(nonatomic,copy)NSString* url;

@end

@interface KDirectoryData : NSObject

/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,strong)NSArray<KDirectory*>* shopDirectView;
/**/
@property(nonatomic,assign)NSInteger statusCode;

@end

@interface KDirectoryDetail : NSObject

/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,strong)KDirectory* shopView;

@end

