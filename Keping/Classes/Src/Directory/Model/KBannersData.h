//
//  KBannersData.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KBanerModel : NSObject

/**/
@property(nonatomic,copy)NSString* imageUrl;
/*店铺id*/
@property(nonatomic,copy)NSString* saleEntityId;
/*url:外链，id:店铺id*/
@property(nonatomic,copy)NSString* type;
/**/
@property(nonatomic,copy)NSString* url;

@end

@interface KBannersData : NSObject

/**/
@property(nonatomic,strong)NSArray<KBanerModel*>* bannerViews;
/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,copy)NSString* msg;

@end
