//
//  HJWebViewController.m
//  Keping
//
//  Created by a on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJWebViewController.h"
#import <WebKit/WebKit.h>

@interface HJWebViewController ()
/**
 浏览器
 */
@property(nonatomic,weak)WKWebView*webView;
@end

@implementation HJWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置UI
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=self.navBarTitle;
    
    WKWebView*webView=[[WKWebView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:webView];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    self.webView=webView;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.webView.frame=self.view.bounds;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
