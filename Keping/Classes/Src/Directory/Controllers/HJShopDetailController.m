//
//  HJShopDetailController.m
//  Keping
//
//  Created by a on 2017/8/24.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJShopDetailController.h"


#import "HJShopDetailContentView.h"

@interface HJShopDetailController ()
/**
 内容视图
 */
@property(nonatomic,weak)HJShopDetailContentView*contentView;
@end

@implementation HJShopDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Details";
    self.navigationItem.rightBarButtonItem=[UIBarButtonItem itemWithImage:@"" withName:@"缺少分享图片" target:self action:@selector(clickShare)];
    
    //内容视图
    HJShopDetailContentView*contentView=[[HJShopDetailContentView alloc]init];
    [self.view addSubview:contentView];
    self.contentView=contentView;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.contentView.frame=self.view.bounds;
}
-(void)clickShare
{
    ALERT(@"点击了分享");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
