//
//  KDirectoryViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDirectoryViewController.h"
#import "HJWebViewController.h"
#import "HJDirectorySubViewController.h"
#import "KDirectoryRequest.h"
#import <SDCycleScrollView.h>
#import "HomeItemCell.h"
#import "KBannersData.h"
#import "KWebViewController.h"

@interface KDirectoryViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,SDCycleScrollViewDelegate>
{
    NSArray* items;
}
@property(nonatomic,strong)SDCycleScrollView* headView;
@property(nonatomic,strong)UICollectionView* collectionView;

@end

@implementation KDirectoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Directory";
    
    items = @[
  @{@"title":@"Foods",@"key":@"100000000001706"},
  @{@"title":@"Healthcare",@"key":@"100000000001707"},
  @{@"title":@"Recreations",@"key":@"100000000001708"},
  @{@"title":@"Maintenance",@"key":@"100000000001709"},
  @{@"title":@"Supermarkets",@"key":@"100000000001710"},
  @{@"title":@"Laundry",@"key":@"100000000001705"}];
    
    self.collectionView.backgroundColor = kWhiteColor;
    [self.collectionView registerClass:[HomeItemCell class] forCellWithReuseIdentifier:[HomeItemCell className]];
    [self getData];
}

-(void)getData
{
    [super getData];
    KDirectoryPhotosRequest* request = [[KDirectoryPhotosRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KBannersData* data = [KBannersData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            NSMutableArray* urls = [NSMutableArray array];
            if (data.bannerViews.count > 0) {
                for (KBanerModel* banner in data.bannerViews) {
                    if ([banner.imageUrl isNotBlank]) {
                        [urls addObject:banner.imageUrl];
                        [self.data addObject:banner];
                    }
                }
                [self.headView setImageURLStringsGroup:urls];
            }
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        //
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return items.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = [items[indexPath.row] objectForKey:@"title"];
    HomeItemCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[HomeItemCell className] forIndexPath:indexPath];
    cell.backView.image = [UIImage imageNamed:[NSString stringWithFormat:@"directory_%@_back",title]];
    cell.itemView.image = [UIImage imageNamed:[NSString stringWithFormat:@"directory_%@_item",title]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HJDirectorySubViewController * vc = [HJDirectorySubViewController new];
    vc.title = [items[indexPath.row] objectForKey:@"title"];
    vc.key = [items[indexPath.row] objectForKey:@"key"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - LazyLoad
-(SDCycleScrollView *)headView
{
    if (!_headView) {
        _headView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenWidth*0.5) shouldInfiniteLoop:YES imageNamesGroup:@[@"directory_top",@"directory_top"]];
        _headView.pageControlDotSize = CGSizeMake(10, 10);
        _headView.backgroundColor = kWhiteColor;
        _headView.pageDotColor = kWhiteColor;
        _headView.currentPageDotColor = kYellowColor;
        _headView.autoScrollTimeInterval = 3.5;
        _headView.delegate = self;
        [self.view addSubview:_headView];
    }
    return _headView;
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat width = kScreenWidth/2-15;
        layout.itemSize = CGSizeMake(width, width/1.8);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headView.frame), kScreenWidth, kScreenHeight-CGRectGetHeight(self.headView.frame)-64-49) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}
#pragma mark -- SDCycleScrollViewDelegate
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (self.data.count == 0) return;
    KBanerModel* model = self.data[index];
    KWebViewController * vc = [KWebViewController new];
    vc.requestUrl = model.url;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
