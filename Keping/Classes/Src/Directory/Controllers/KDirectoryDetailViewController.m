//
//  KDirectoryDetailViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDirectoryDetailViewController.h"
#import "KDirectoryData.h"
#import <SDCycleScrollView.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface KDirectoryDetailViewController ()<SDCycleScrollViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate>
{
    MKMapView* _mapView;
    MKPointAnnotation *_annotation;
    CLLocationManager *_locationManager;
    CLLocationCoordinate2D _currentLocationCoordinate;
}

/**/
@property(nonatomic,strong)SDCycleScrollView* adView;
/**/
@property(nonatomic,strong)UILabel* countLabel;
/**/
@property(nonatomic,strong)YYLabel* nameLabel;
/**/
@property(nonatomic,strong)YYLabel* addressLabel;
/**/
@property(nonatomic,strong)UIButton* callButton;
@end

@implementation KDirectoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Details";
    
    //广告栏
    _adView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero shouldInfiniteLoop:YES imageNamesGroup:@[@"directory_top",@"directory_top"]];
    _adView.pageControlDotSize = CGSizeMake(10, 10);
    //    adView.pageDotImage=[UIImage imageNamed:@"pageCtrl"];
    //    adView.currentPageDotImage=[UIImage imageNamed:@""];
    _adView.pageDotColor = kWhiteColor;
    _adView.currentPageDotColor = kYellowColor;
    _adView.autoScrollTimeInterval = 3.5;
    _adView.delegate = self;
    [_adView setFrame:CGRectMake(0, 0, kScreenWidth, CGFloatPixelRound(200))];
    if ([self.model.imageUrls isNotBlank]) {
        NSMutableArray* imgs = [NSMutableArray array];
        if ([self.model.imageUrls containsString:@","]) {
            NSArray* urls = [self.model.imageUrls componentsSeparatedByString:@","];
            for (NSString* url in urls) {
                NSString* img = [NSString stringWithFormat:@"%@%@",KGetImageURL,url];
                [imgs addObject:img];
            }
            if (urls.count > 0) {
                [_adView setImageURLStringsGroup:urls];
            }
        }else{
            [_adView setImageURLStringsGroup:@[[NSString stringWithFormat:@"%@%@",KGetImageURL,self.model.imageUrls]]];
        }
        
    }
    [self.view addSubview:_adView];
    
    CGFloat WH = CGFloatPixelRound(45);
    CGFloat margin = CGFloatPixelRound(15);
    //图片计算视图
    _countLabel = [[UILabel alloc]init];
    _countLabel.backgroundColor = [UIColor colorWithHexString:@"#393a3e"];
    _countLabel.layer.borderColor = [UIColor colorWithHexString:@"#67686c"].CGColor;
    _countLabel.layer.masksToBounds = YES;
    _countLabel.layer.cornerRadius = WH/2;
    _countLabel.layer.borderWidth = CGFloatPixelRound(2);
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.textColor = [UIColor whiteColor];
    _countLabel.text = [NSString stringWithFormat:@"1/%zd",_adView.localizationImageNamesGroup.count];
    [_countLabel setFrame:CGRectMake(_adView.width - WH - margin, _adView.height - WH - margin, WH, WH)];
    [_adView addSubview:_countLabel];
    
    _nameLabel = [YYLabel new];
    _nameLabel.font = SystemBoldFont;
    _nameLabel.textColor = kBlackColor;
    [_nameLabel setFrame:CGRectMake(margin, _adView.bottom+10, kScreenWidth - margin*2, 30)];
    _nameLabel.attributedText = [self nameString:self.model.storeName];
    [self.view addSubview:_nameLabel];
    
    _addressLabel = [YYLabel new];
    _addressLabel.font = SystemSmallFont;
    _addressLabel.textColor = kDarkGrayColor;
    [_addressLabel setFrame:CGRectMake(_nameLabel.left, _nameLabel.bottom, _nameLabel.width, _nameLabel.height)];
    _addressLabel.attributedText = [self addressString:self.model.addr];
    [self.view addSubview:_addressLabel];
    
    _callButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_callButton setFrame:CGRectMake(10, _addressLabel.bottom+margin, kScreenWidth-20, 44)];
    [_callButton setBackgroundImage:[UIImage imageNamed:@"bg_bar"] forState:UIControlStateNormal];
    [_callButton setTitleColor:kWhiteColor forState:UIControlStateNormal];
    _callButton.titleLabel.font = SystemBoldFont;
    [_callButton setTitle:self.model.contact?:@"Contact" forState:UIControlStateNormal];
    _callButton.clipsToBounds = YES;
    _callButton.layer.cornerRadius = 3.0;
    [self.view addSubview:_callButton];
    
    _mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, _callButton.bottom+margin*2, kScreenWidth, kScreenHeight - CGRectGetMaxY(_callButton.frame)-margin*2)];
    _mapView.delegate = self;
    _mapView.mapType = MKMapTypeStandard;
    _mapView.zoomEnabled = YES;
    //_mapView.showsScale = YES;
    _mapView.showsUserLocation = YES;
    [self.view addSubview:_mapView];
    
    if (self.model.longitude > 0 || self.model.latitude > 0) {
        [self removerToLocation:
         CLLocationCoordinate2DMake(self.model.latitude, self.model.longitude)];
    }else{
        [self startLocation];
    }
}

-(void)dealloc
{
    _mapView.delegate = nil;
    _mapView = nil;
}

-(NSMutableAttributedString*)nameString:(NSString*)name{
    UIImage* nameImg = [UIImage imageNamed:@"store_name_icon"];
    NSMutableAttributedString* nameImgText = [NSMutableAttributedString imageStringWithFontSize:16 image:nameImg];
    NSMutableAttributedString* text = [[NSMutableAttributedString alloc] init];
    [text appendAttributedString:nameImgText];
    [text appendString:@"  "];
    [text appendString:name?:@" "];
    return text;
}

-(NSMutableAttributedString*)addressString:(NSString*)address{
    UIImage* addImg = [UIImage imageNamed:@"address_icon"];
    NSMutableAttributedString* addImgText = [NSMutableAttributedString imageStringWithFontSize:16 image:addImg];
    NSMutableAttributedString* text = [[NSMutableAttributedString alloc] init];
    [text appendAttributedString:addImgText];
    [text appendString:address?:@"  "];
    return text;
}

#pragma mark - SDDelegate
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    if (cycleScrollView.imageURLStringsGroup.count) {
        _countLabel.text = [NSString stringWithFormat:@"%ld/%ld",index+1,cycleScrollView.imageURLStringsGroup.count];
    }else{
        _countLabel.text = [NSString stringWithFormat:@"%ld/%ld",index+1,cycleScrollView.localizationImageNamesGroup.count];
    }
}
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    
}

#pragma mark - MapDelegate
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    @weakify(self);
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:userLocation.location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (!error && placemarks.count > 0) {
            //CLPlacemark* placemark = [placemarks firstObject];
            [weak_self removerToLocation:userLocation.coordinate];
        }
    }];
}

-(void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    if (error.code == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[error.userInfo objectForKey:NSLocalizedRecoverySuggestionErrorKey] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (void)startLocation
{
    if([CLLocationManager locationServicesEnabled]){
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = 5;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;//kCLLocationAccuracyBest;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
            [_locationManager requestWhenInUseAuthorization];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
            break;
        case kCLAuthorizationStatusDenied:
        {
        }
            
        default:
            break;
    }
}

//地图添加大头针
-(void)createAnnotationWithCoords:(CLLocationCoordinate2D)coords
{
    if (_annotation == nil) {
        _annotation = [[MKPointAnnotation alloc] init];
    }
    else{
        [_mapView removeAnnotation:_annotation];
    }
    _annotation.coordinate = coords;
    [_mapView addAnnotation:_annotation];
}

-(void)removerToLocation:(CLLocationCoordinate2D)locationCoordinate
{
    _currentLocationCoordinate = locationCoordinate;
    CGFloat zoomLevel = 0.01;
    MKCoordinateRegion region = MKCoordinateRegionMake(_currentLocationCoordinate, MKCoordinateSpanMake(zoomLevel, zoomLevel));
    [_mapView setRegion:[_mapView regionThatFits:region]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
