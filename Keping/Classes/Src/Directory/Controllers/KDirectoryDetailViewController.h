//
//  KDirectoryDetailViewController.h
//  Keping
//
//  Created by 柯平 on 2017/10/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KViewController.h"

@class KDirectory;

@interface KDirectoryDetailViewController : KViewController

/**/
@property(nonatomic,copy)NSString* key;
/**/
@property(nonatomic,strong)KDirectory* model;

@end
