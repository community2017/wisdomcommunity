//
//  HJWebViewController.h
//  Keping
//
//  Created by a on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KViewController.h"

@interface HJWebViewController : KViewController
/**
 url
 */
@property(nonatomic,copy)NSString*url;
/**
 导航栏名称
 */
@property(nonatomic,copy)NSString*navBarTitle;
@end
