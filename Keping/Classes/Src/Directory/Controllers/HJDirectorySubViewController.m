//
//  HJDirectorySubViewController.m
//  Keping
//
//  Created by a on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJDirectorySubViewController.h"
//#import "HJWebViewController.h"
//#import "HJShopDetailController.h"
#import "KDirectoryDetailViewController.h"
#import "KDirectoryData.h"
//#import "HJSubDirectoryCell.h"
//#import "HJAdViewCell.h"
#import "KDirectoryAdCell.h"
#import "KDirectoryShopCell.h"
#import "KDirectoryRequest.h"


@interface HJDirectorySubViewController ()

@end

@implementation HJDirectorySubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.groupView beginRefresh];
}

-(void)refresh
{
    [super refresh];
    [self getData];
}

-(void)loadmore
{
    [super loadmore];
    [self getData];
}

-(void)getData
{
    [super getData];
    KDirectoryRequest* request = [[KDirectoryRequest alloc] init];
    request.params = [NSDictionary dictionaryWithObjectsAndKeys:@([KAppConfig sharedConfig].latitude),@"latitude",@([KAppConfig sharedConfig].longitude),@"longitude",self.key,@"cateId",@(self.page),@"pageOn", nil];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KDirectoryData* data = [KDirectoryData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            if (self.page == 1) {
                [self.data removeAllObjects];
            }
            if (data.shopDirectView.count > 0) {
                [self.data addObjectsFromArray:
                 data.shopDirectView];
            }
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
        [self.groupView endRefresh];
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self.groupView endRefresh];
    }];
    
}

#pragma mark --UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KDirectory* model = self.data[indexPath.row];
    if (model.isAd) {
        KDirectoryAdCell * cell = [tableView dequeueReusableCellWithIdentifier:[KDirectoryAdCell className]];
        if (!cell) {
            cell = [[KDirectoryAdCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KDirectoryAdCell className]];
        }
        cell.model = model;
        return cell;
    }else{
        KDirectoryShopCell * cell = [tableView dequeueReusableCellWithIdentifier:[KDirectoryShopCell className]];
        if (!cell) {
            cell = [[KDirectoryShopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KDirectoryShopCell className]];
        }
        cell.model = model;
        return cell;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KDirectory* model = self.data[indexPath.row];
    if (!model.isAd) {
        return CGFloatPixelRound(195/2);
    }else{
         return CGFloatPixelRound(495/2);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    KDirectory* model = self.data[indexPath.row];
    if (!model.isAd) {
        KDirectoryDetailViewController * vc = [KDirectoryDetailViewController new];
        vc.key = model.id;
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        KWebViewController * vc = [KWebViewController new];
        vc.requestUrl = model.url;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
