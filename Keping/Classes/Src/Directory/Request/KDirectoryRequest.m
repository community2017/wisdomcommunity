//
//  KDirectoryRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDirectoryRequest.h"

@implementation KDirectoryRequest

-(id)requestArgument
{
    return self.params;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(NSString *)requestUrl
{
    return KDirectoryDataURL;
}

@end

@implementation KDirectoryDetailRequest
{
    NSString* _url;
}

-(instancetype)initWithKey:(NSString *)key
{
    self = [super init];
    if (self) {
        _url = [NSString stringWithFormat:@"%@",KDirectoryDetailURL];
        _url = [_url stringByReplacingOccurrencesOfString:
                @"{key}" withString:key];
    }
    return self;
}

-(NSString *)requestUrl
{
    return _url;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return nil;
}

@end

@implementation KDirectoryPhotosRequest

-(NSString *)requestUrl
{
    return KDirectoryPhotosURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

-(id)requestArgument
{
    return nil;
}

@end
