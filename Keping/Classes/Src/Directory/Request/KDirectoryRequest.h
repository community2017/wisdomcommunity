//
//  KDirectoryRequest.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KRequest.h"

@interface KDirectoryRequest : KRequest

@end

@interface KDirectoryDetailRequest : KRequest

-(instancetype)initWithKey:(NSString*)key;

@end

@interface KDirectoryPhotosRequest : KRequest

@end
