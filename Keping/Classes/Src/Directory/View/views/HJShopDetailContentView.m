//
//  HJShopDetailContentView.m
//  Keping
//
//  Created by a on 2017/8/24.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJShopDetailContentView.h"
#import <SDCycleScrollView.h>



@interface HJShopDetailContentView()<SDCycleScrollViewDelegate>
/**
 图片展示栏
 */
@property(nonatomic,weak)SDCycleScrollView*adView;
/**
 图片计算视图
 */
@property(nonatomic,weak)UILabel*iconCountView;
/**
 滚屏
 */
@property(nonatomic,weak)UIScrollView*scrollView;
/**
 店名图标
 */
@property(nonatomic,weak)UIImageView*houseImgView;
/**
 地址图标
 */
@property(nonatomic,weak)UIImageView*addressView;

/**
 店名
 */
@property(nonatomic,weak)UILabel*houseLabel;

/**
 地址
 */
@property(nonatomic,weak)UILabel*addressLabel;
/**
 地图
 */
@property(nonatomic,weak)UIView*mapView;
/**
 电话按钮
 */
@property(nonatomic,weak)UIButton*phoneBtn;

@end
@implementation HJShopDetailContentView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    UIScrollView*scrollView=[[UIScrollView alloc]init];
    [self addSubview:scrollView];
    self.scrollView=scrollView;

    //广告栏
    SDCycleScrollView*adView= [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero shouldInfiniteLoop:YES imageNamesGroup:@[@"directory_top",@"directory_top",@"directory_top",@"directory_top",@"directory_top"]];
    adView.pageControlDotSize = CGSizeMake(10, 10);
//    adView.pageDotImage=[UIImage imageNamed:@"pageCtrl"];
//    adView.currentPageDotImage=[UIImage imageNamed:@""];
    adView.pageDotColor = kWhiteColor;
    adView.currentPageDotColor = kYellowColor;
    adView.autoScrollTimeInterval = 3.5;
    adView.delegate = self;
    [scrollView addSubview:adView];
    self.adView=adView;
    [adView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(scrollView);
        make.width.mas_equalTo(App_Frame_Width);
        make.height.mas_equalTo(K_FactorH(200));
    }];
    
    
    CGFloat WH=K_FactorW(45);
    CGFloat margin=K_FactorW(15);
    //图片计算视图
    UILabel*iconCountView=[[UILabel alloc]init];
    iconCountView.backgroundColor=[UIColor colorWithHexString:@"#393a3e"];
    iconCountView.layer.borderColor=[UIColor colorWithHexString:@"#67686c"].CGColor;
    iconCountView.layer.masksToBounds=YES;
    iconCountView.layer.cornerRadius=WH/2;
    iconCountView.layer.borderWidth=K_FactorW(2);
    iconCountView.textAlignment=NSTextAlignmentCenter;
    iconCountView.textColor=[UIColor whiteColor];
    [adView addSubview:iconCountView];
    iconCountView.text=[NSString stringWithFormat:@"1/%zd",adView.localizationImageNamesGroup.count];
    self.iconCountView=iconCountView;
    [iconCountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(adView).offset(-margin);
        make.size.mas_equalTo(CGSizeMake(WH, WH));
    }];

    //店名图标
    UIImageView*houseImgView=[[UIImageView alloc]init];
    houseImgView.backgroundColor=[UIColor redColor];
    [scrollView addSubview:houseImgView];
    self.houseImgView=houseImgView;
    CGFloat imgWH=K_FactorW(22);
    [houseImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(scrollView).offset(margin);
        make.top.mas_equalTo(adView.mas_bottom).offset(margin);
        make.size.mas_equalTo(CGSizeMake(imgWH, imgWH));
    }];
    
    //店名
    UILabel*houseLabel=[[UILabel alloc]init];
    [scrollView addSubview:houseLabel];
    houseLabel.text=@"The SteakHouse";
    houseLabel.numberOfLines=0;
    houseLabel.font=[UIFont boldSystemFontOfSize:K_FactorW(20)];
    self.houseLabel=houseLabel;
    [houseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(houseImgView.mas_right).offset(margin);
        make.top.mas_equalTo(houseImgView);
        make.right.mas_equalTo(adView).offset(-margin);
    }];
   
    //地址图标
    UIImageView*addressView=[[UIImageView alloc]init];
    [scrollView addSubview:addressView];
    addressView.backgroundColor=[UIColor redColor];
    self.addressView=addressView;
    [addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(houseImgView);
        make.top.mas_equalTo(houseLabel.mas_bottom).offset(margin);
        make.size.mas_equalTo(houseImgView);
    }];

    
    //地址
    UILabel*addressLabel=[[UILabel alloc]init];
    [scrollView addSubview:addressLabel];
    addressLabel.textColor=[UIColor colorWithHexString:@"#656565"];
    addressLabel.text=@"No.123 Jalan Persiaran 456000 Puchong,Selangor";
    self.addressLabel=addressLabel;
    addressLabel.numberOfLines=0;
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(houseLabel);
        make.top.mas_equalTo(addressView);
    }];
    
    //电话按钮
    UIButton*phoneBtn=[[UIButton alloc]init];
    [scrollView addSubview:phoneBtn];
    phoneBtn.backgroundColor=[UIColor colorWithHexString:@"#545af8"];
    [phoneBtn setTitle:@"+603 78595993" forState:UIControlStateNormal];
    phoneBtn.layer.cornerRadius=K_FactorH(5);
    phoneBtn.layer.masksToBounds=YES;
    [phoneBtn addTarget:self action:@selector(clickPhoneBtn) forControlEvents:UIControlEventTouchUpInside];
    self.phoneBtn=phoneBtn;
    
    [phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(addressView);
        make.right.mas_equalTo(addressLabel);
        make.top.mas_equalTo(addressLabel.mas_bottom).offset(K_FactorH(15));
        make.height.mas_equalTo(K_FactorH(45));
    }];
    
    //地图
    UIView*mapView=[[UIView alloc]init];
    [scrollView addSubview:mapView];
    mapView.backgroundColor=[UIColor redColor];
    [mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(adView);
        make.top.mas_equalTo(phoneBtn.mas_bottom).offset(K_FactorH(33));
        make.height.mas_equalTo(K_FactorH(300));
    }];
    self.mapView=mapView;
    
    [self layoutIfNeeded];
//
    scrollView.contentSize=CGSizeMake(0, CGRectGetMaxY(mapView.frame));
}
-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGPoint newPoint=[self convertPoint:point toView:self.mapView];
    if ([self.mapView pointInside:newPoint withEvent:event]) {
        
        return self.mapView;
    }else
    {
       return [super hitTest:point withEvent:event];
    }
}
-(void)clickPhoneBtn
{
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"17612081683"];
    UIWebView *callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
    [self addSubview:callWebview];
    
}
#pragma mark  -- SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    self.iconCountView.text=[NSString stringWithFormat:@"%zd/%zd",index+1,cycleScrollView.localizationImageNamesGroup.count];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    self.scrollView.frame=self.bounds;
//    self.adView.frame=(CGRect){{0,0},CGSizeMake(self.width, K_FactorH(200))};
//    
//    CGFloat WH=K_FactorW(45);
//    CGFloat margin=K_FactorW(15);
//    self.iconCountView.frame=(CGRect){{self.width-margin-WH,self.adView.height-margin-WH},CGSizeMake(WH, WH)};
//    self.iconCountView.layer.cornerRadius=WH/2;
//    self.iconCountView.layer.borderWidth=K_FactorW(2);
//    
//    self.houseImgView.frame=(CGRect){{margin,margin},CGSizeMake(margin,margin)};
//    
//    self.houseLabel.frame=(CGRect){{CGRectGetMaxX(self.houseImgView.frame)+margin,self.houseImgView.y},CGSizeMake(self.width-CGRectGetMaxX(self.houseImgView.frame)-2*margin, 0)};
}
@end
