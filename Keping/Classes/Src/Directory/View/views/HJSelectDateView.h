//
//  HJSelectDateView.h
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HJSelectDateView;
@protocol HJSelectDateViewDelegate <NSObject>

-(void)selectDateView:(HJSelectDateView*)selectDateView clickSelectDate:(NSString*)dateStr indexPath:(NSIndexPath*)indexPath;


@end
@interface HJSelectDateView : UIView
/**
 代理
 */
@property(nonatomic,weak)id<HJSelectDateViewDelegate>delegate;
/**
 时间选择模式
 */
@property(nonatomic,assign)UIDatePickerMode DateModel;
/**
 数据index
 */
@property(nonatomic,assign)NSIndexPath*indexPath;
@end
