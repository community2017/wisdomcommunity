//
//  HJSelectCardView.h
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HJSelectCardView;
@class entranceCardViewsModel;
@protocol HJSelectCardViewDelegate <NSObject>

-(void)selectCardView:(HJSelectCardView*)selectCardView clickSelectCard:(entranceCardViewsModel*)entranceCardViews;

@end
@interface HJSelectCardView : UIView
/**
 代理
 */
@property(nonatomic,weak)id<HJSelectCardViewDelegate>delegate;
/**
 数据源
 */
@property(nonatomic,strong)NSArray*dataAry;
@end
