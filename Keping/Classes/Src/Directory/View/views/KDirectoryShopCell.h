//
//  KDirectoryShopCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@class KDirectory;

@interface KDirectoryShopCell : KTableViewCell

/**/
@property(nonatomic,strong)UIImageView* photoView;
/**/
@property(nonatomic,strong)UILabel* titleLabel;
/**/
@property(nonatomic,strong)UILabel* subTitleLabel;
/**/
@property(nonatomic,strong)UILabel* distanceLabel;

/**/
@property(nonatomic,strong)KDirectory* model;

@end
