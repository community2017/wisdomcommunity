//
//  KDirectoryAdCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDirectoryAdCell.h"
#import "KDirectoryData.h"

@implementation KDirectoryAdCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _topLine = [UIView new];
        _topLine.backgroundColor = UIColorHex(F1F2F4);
        [self.contentView addSubview:_topLine];
        
        _bottomLine = [UIView new];
        _bottomLine.backgroundColor = UIColorHex(F1F2F4);
        [self.contentView addSubview:_bottomLine];
        
        _adImageView = [UIImageView new];
        _adImageView.clipsToBounds = YES;
        [self.contentView addSubview:_adImageView];
        
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemBoldFont;
        _titleLabel.textColor = kBlackColor;
        [self.contentView addSubview:_titleLabel];
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat gap = CGFloatPixelRound(15);
    [_topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(gap);
    }];
    [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(gap);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(gap);
        make.right.mas_equalTo(-gap);
        make.height.mas_equalTo(CGFloatPixelRound(30));
        make.top.mas_equalTo(_topLine.mas_bottom).offset(10);
    }];
    [_adImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(gap);
        make.width.mas_equalTo(kScreenWidth-CGFloatPixelRound(30));
        make.bottom.mas_equalTo(_bottomLine.mas_top).offset(-gap);
    }];

}

-(void)setModel:(KDirectory *)model
{
    _model = model;
    @weakify(_adImageView);
    [_adImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KGetImageURL,model.logo?:@""]] placeholder:[[UIImage imageNamed:KImagePlaceholder] imageByRoundCornerRadius:5.0] options:kNilOptions completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if (image) {
            weak__adImageView.image = [image imageByRoundCornerRadius:5.0];
        }
    }];
    _titleLabel.text = model.storeName;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
