//
//  KDirectoryAdCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@class KDirectory;

@interface KDirectoryAdCell : KTableViewCell
/*topLine*/
@property(nonatomic,strong)UIView* topLine;
/*bottomLine*/
@property(nonatomic,strong)UIView* bottomLine;

/**/
@property(nonatomic,strong)UILabel* titleLabel;
/**/
@property(nonatomic,strong)UIImageView* adImageView;;

/**/
@property(nonatomic,strong)KDirectory* model;

@end
