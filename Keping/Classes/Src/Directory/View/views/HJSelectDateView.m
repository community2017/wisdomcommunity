//
//  HJSelectDateView.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSelectDateView.h"

@interface HJSelectDateView()
/**
 背景
 */
@property(nonatomic,weak)UIView*bgView;
/**
 分割线
 */
@property(nonatomic,weak)UIView*line;
/**
 取消按钮
 */
@property(nonatomic,weak)UIButton*cancelBtn;
/**
 确定按钮
 */
@property(nonatomic,weak)UIButton*sureBtn;
/**
 按钮间分割线
 */
@property(nonatomic,weak)UIView*btnLine;
/**
 时间选择picker
 */
@property(nonatomic,weak)UIDatePicker*datePicker;
/**
 高斯模糊效果
 */
@property(nonatomic,weak)UIVisualEffectView *effectview;
@end
@implementation HJSelectDateView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
        self.effectview=effectview;
        [self addSubview:effectview];
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    UIView*bgView=[[UIView alloc]init];
    bgView.backgroundColor=[UIColor whiteColor];
    [self addSubview:bgView];
    self.bgView=bgView;
    
    
    //时间选择
    UIDatePicker*datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [bgView addSubview:datePicker];
    self.datePicker=datePicker;
    
    //设置显示格式
    //默认根据手机本地设置显示为中文还是其他语言
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文显示
//    datePicker.locale = locale;
//    
    
//    //当前时间创建NSDate
//    NSDate *localDate = [NSDate date];
//    //在当前时间加上的时间：格里高利历
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    //设置时间
//    [offsetComponents setYear:0];
//    [offsetComponents setMonth:0];
//    [offsetComponents setDay:5];
//    [offsetComponents setHour:20];
//    [offsetComponents setMinute:0];
//    [offsetComponents setSecond:0];
//    //设置最大值时间
//    NSDate *maxDate = [gregorian dateByAddingComponents:offsetComponents toDate:localDate options:0];
    //设置属性
//    datePicker.minimumDate = localDate;
//    datePicker.maximumDate = maxDate;

    //分割线
    UIView*line=[[UIView alloc]init];
    line.backgroundColor=[UIColor colorWithHexString:@"#bdbdbd"];
    [bgView addSubview:line];
    self.line=line;
    
    //取消按钮
    UIButton*cancelBtn=[self creatBtn:@"Cancel" color:@"#191919"];
    [bgView addSubview:cancelBtn];
    self.cancelBtn=cancelBtn;
    
    //确定按钮
    UIButton*sureBtn=[self creatBtn:@"OK" color:@"#132dfe"];
    [bgView addSubview:sureBtn];
    self.sureBtn=sureBtn;
    
    //按钮间分割线
    UIView*btnLine=[[UIView alloc]init];
    [bgView addSubview:btnLine];
    btnLine.backgroundColor=[UIColor colorWithHexString:@"#bdbdbd"];
    self.btnLine=btnLine;
}
-(void)setDateModel:(UIDatePickerMode)DateModel
{
    _DateModel=DateModel;
    self.datePicker.datePickerMode=DateModel;
}
-(UIButton*)creatBtn:(NSString*)title color:(NSString*)color
{
    UIButton*btn=[[UIButton alloc]init];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:color] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
-(void)clickSelectBtn:(UIButton*)btn
{
    if([btn.currentTitle isEqualToString:@"OK"]){
        if ([_delegate respondsToSelector:@selector(selectDateView:clickSelectDate:indexPath:)]) {
            //NSDate格式转换为NSString格式
            NSDate *pickerDate = [self.datePicker date];// 获取用户通过UIDatePicker设置的日期和时间
            NSDateFormatter *pickerFormatter = [[NSDateFormatter alloc] init];// 创建一个日期格式器
            //YYYY-MM-dd HH-mm-ss
            if (self.DateModel == UIDatePickerModeDate) {
                [pickerFormatter setDateFormat:@"YYYY-MM-dd"];
            }else if (self.DateModel ==UIDatePickerModeTime)
            {
                 [pickerFormatter setDateFormat:@"HH:mm"];
            }
            
            NSString *dateString = [pickerFormatter stringFromDate:pickerDate];
            [_delegate selectDateView:self clickSelectDate:dateString indexPath:self.indexPath];
        }
    }
    [self removeFromSuperview];
}
-(void)dateChanged:(id)sender{
}
-(void)layoutSubviews
{
    [super layoutSubviews];
     self.effectview.frame=self.bounds;
    
    CGFloat bgH = K_FactorH(200);
    CGFloat bgW = K_FactorW(290);
    self.bgView.frame=(CGRect){{(self.width-bgW)/2,(self.height-bgH)/2},{bgW,bgH}};
    self.bgView.layer.cornerRadius=K_FactorH(8);
    self.bgView.layer.masksToBounds=YES;
    
    self.datePicker.frame=(CGRect){{0,0},{bgW,K_FactorH(150)}};
    
    self.line.frame=(CGRect){{0,CGRectGetMaxY(self.datePicker.frame)},{self.bgView.width,K_FactorW(1)}};
    CGFloat lineW=K_FactorW(1);
    CGFloat btnW=(self.bgView.width-lineW)/2;
    self.cancelBtn.frame=(CGRect){{0,CGRectGetMaxY(self.line.frame)},{btnW,self.bgView.height-CGRectGetMaxY(self.line.frame)}};
    
    self.btnLine.frame=(CGRect){{CGRectGetMaxX(self.cancelBtn.frame),self.cancelBtn.y},{lineW,self.cancelBtn.height}};
    
    self.sureBtn.frame=(CGRect){{CGRectGetMaxX(self.btnLine.frame),self.btnLine.y},self.cancelBtn.size};
}

@end
