//
//  HJSelectCardView.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSelectCardView.h"

#import "KEntranceCardsListModel.h"

@interface HJSelectCardView()<UIPickerViewDelegate,UIPickerViewDataSource>
/**
 背景
 */
@property(nonatomic,weak)UIView*bgView;
/**
 门号选择器
 */
@property(nonatomic,weak) UIPickerView*pickerView;
/**
 分割线
 */
@property(nonatomic,weak)UIView*line;
/**
 取消按钮
 */
@property(nonatomic,weak)UIButton*cancelBtn;
/**
 确定按钮
 */
@property(nonatomic,weak)UIButton*sureBtn;
/**
 按钮间分割线
 */
@property(nonatomic,weak)UIView*btnLine;
/**
 高斯模糊效果
 */
@property(nonatomic,weak)UIVisualEffectView *effectview;

@end
@implementation HJSelectCardView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
        self.effectview=effectview;
        [self addSubview:effectview];
//        self.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    UIView*bgView=[[UIView alloc]init];
    bgView.backgroundColor=[UIColor whiteColor];
    [self addSubview:bgView];
    self.bgView=bgView;
    
    
    //门号选择
    UIPickerView*pickerView=[[UIPickerView alloc]init];
    pickerView.delegate=self;
    pickerView.dataSource=self;
    [bgView addSubview:pickerView];
    self.pickerView=pickerView;
    
    //分割线
    UIView*line=[[UIView alloc]init];
    line.backgroundColor=[UIColor colorWithHexString:@"#bdbdbd"];
    [bgView addSubview:line];
    self.line=line;
    
    //取消按钮
    UIButton*cancelBtn=[self creatBtn:@"Cancel" color:@"#191919"];
    [bgView addSubview:cancelBtn];
    self.cancelBtn=cancelBtn;
    
    //确定按钮
    UIButton*sureBtn=[self creatBtn:@"OK" color:@"#132dfe"];
    [bgView addSubview:sureBtn];
    self.sureBtn=sureBtn;
    
    //按钮间分割线
    UIView*btnLine=[[UIView alloc]init];
    [bgView addSubview:btnLine];
    btnLine.backgroundColor=[UIColor colorWithHexString:@"#bdbdbd"];
    self.btnLine=btnLine;
}
-(UIButton*)creatBtn:(NSString*)title color:(NSString*)color
{
    UIButton*btn=[[UIButton alloc]init];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:color] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
-(void)clickSelectBtn:(UIButton*)btn
{
    if([btn.currentTitle isEqualToString:@"OK"]){
        if ([_delegate respondsToSelector:@selector(selectCardView:clickSelectCard:)]) {

            [_delegate selectCardView:self clickSelectCard:self.dataAry[[self.pickerView selectedRowInComponent:0]]];
        }
    }
    [self removeFromSuperview];
}
#pragma mark --UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 1;
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件指定列包含多少个列表项
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    return self.dataAry.count;
}

#pragma mark -- UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return [self.dataAry[row] cardNo];
}

// 当用户选中UIPickerViewDataSource中指定列和列表项时激发该方法
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{

}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    //设置分割线的颜色
    for(UIView *singleLine in pickerView.subviews)
    {
        if (singleLine.frame.size.height < 1)
        {
            singleLine.backgroundColor = [UIColor colorWithHexString:@"#bdbdbd"];
        }
    }
    
    //设置文字的属性
    UILabel *genderLabel = [UILabel new];
    genderLabel.textAlignment = NSTextAlignmentCenter;
    genderLabel.text = [self.dataAry[row] cardNo];
    genderLabel.textColor = [UIColor blackColor];
    
    return genderLabel;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return K_FactorH(42);
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    self.effectview.frame=self.bounds;
    CGFloat bgH = K_FactorH(200);
    CGFloat bgW = K_FactorW(290);
    self.bgView.frame=(CGRect){{(self.width-bgW)/2,(self.height-bgH)/2},{bgW,bgH}};
    self.bgView.layer.cornerRadius=K_FactorH(8);
    self.bgView.layer.masksToBounds=YES;
    
    self.pickerView.frame=(CGRect){{0,0},{bgW,K_FactorH(150)}};
    
    self.line.frame=(CGRect){{0,CGRectGetMaxY(self.pickerView.frame)},{self.bgView.width,K_FactorW(1)}};
    CGFloat lineW=K_FactorW(1);
    CGFloat btnW=(self.bgView.width-lineW)/2;
    self.cancelBtn.frame=(CGRect){{0,CGRectGetMaxY(self.line.frame)},{btnW,self.bgView.height-CGRectGetMaxY(self.line.frame)}};
    
    self.btnLine.frame=(CGRect){{CGRectGetMaxX(self.cancelBtn.frame),self.cancelBtn.y},{lineW,self.cancelBtn.height}};
    
    self.sureBtn.frame=(CGRect){{CGRectGetMaxX(self.btnLine.frame),self.btnLine.y},self.cancelBtn.size};
}
@end
