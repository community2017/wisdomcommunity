//
//  KDirectoryShopCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDirectoryShopCell.h"
#import "KDirectoryData.h"

@implementation KDirectoryShopCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _photoView = [UIImageView new];
        _photoView.clipsToBounds = YES;
        [self.contentView addSubview:_photoView];
        
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemBoldFont;
        _titleLabel.textColor = kBlackColor;
        [self.contentView addSubview:_titleLabel];
        
        _subTitleLabel = [UILabel new];
        _subTitleLabel.font = SystemFont;
        _subTitleLabel.textColor = kDarkGrayColor;
        [self.contentView addSubview:_subTitleLabel];
        
        _distanceLabel = [UILabel new];
        _distanceLabel.font = SystemFont;
        _distanceLabel.textColor = kBlackColor;
        [self.contentView addSubview:_distanceLabel];
        
    }
    return self;
}

-(void)setModel:(KDirectory *)model
{
    _model = model;
    @weakify(_photoView);
    [_photoView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KGetImageURL,model.logo?:@""]] placeholder:[[UIImage imageNamed:KImagePlaceholder] imageByRoundCornerRadius:5.0] options:kNilOptions completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if (image) {
            weak__photoView.image = [image imageByRoundCornerRadius:5.0];
        }
    }];
    _titleLabel.text = model.storeName;
    _subTitleLabel.text = model.addr?:model.summary?:@"";
    _distanceLabel.text = [NSString stringWithFormat:@"%.2fm",model.distance];
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat gap = CGFloatPixelRound(15);
    
    [_photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(gap);
        make.width.mas_equalTo(CGFloatPixelRound(105));
        make.height.mas_equalTo(CGFloatPixelRound(75));
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoView.mas_centerY);
        make.left.mas_equalTo(self.photoView.mas_right).offset(10);
        make.height.mas_equalTo(CGFloatPixelRound(20));
        make.right.mas_equalTo(-gap);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.subTitleLabel.mas_left);
        make.right.mas_equalTo(self.subTitleLabel.mas_right);
        make.height.mas_equalTo(CGFloatPixelRound(30));
        make.bottom.mas_equalTo(self.subTitleLabel.mas_top);
    }];
    [_distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.subTitleLabel.mas_left);
        make.right.mas_equalTo(self.subTitleLabel.mas_right);
        make.height.mas_equalTo(CGFloatPixelRound(30));
        make.top.mas_equalTo(self.subTitleLabel.mas_bottom);
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
