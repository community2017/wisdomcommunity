//
//  HJVisitFriendViewCell.m
//  Keping
//
//  Created by a on 2017/8/29.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJVisitFriendViewCell.h"

@interface HJVisitFriendViewCell()
///**
// *标记
// */
//@property(nonatomic,weak)UILabel*reuseIdLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;
/**
 选择图片
 */
@property(nonatomic,weak)UIImageView*selectImgView;
@end
@implementation HJVisitFriendViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJVisitFriendViewCell";
    HJVisitFriendViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
//    //标记
//    UILabel*reuseIdLabel=[[UILabel alloc]init];
//    [self.contentView addSubview:reuseIdLabel];
//    reuseIdLabel.text=@"*";
//    self.reuseIdLabel=reuseIdLabel;
    
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.contentView addSubview:contentLabel];
    contentLabel.textColor=[UIColor colorWithHexString:@"#0e0e0e"];
    self.contentLabel=contentLabel;
    
    UIImageView*selectImgView=[[UIImageView alloc]init];
    [self.contentView addSubview:selectImgView];
    self.selectImgView=selectImgView;

}
-(void)setIsSelect:(BOOL)isSelect
{
    _isSelect=isSelect;
    self.selectImgView.highlighted=isSelect;
}
-(void)setTitle:(NSString *)title
{
    _title=title;
    self.contentLabel.text=title;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin=K_FactorW(10);
//    self.reuseIdLabel.frame=(CGRect){{margin,0},{K_FactorW(20),self.height}};
    
    CGFloat imgWH=K_FactorW(15);
    self.selectImgView.frame=(CGRect){{self.width-imgWH-margin,(self.height-imgWH)/2},{imgWH,imgWH}};
    
    self.contentLabel.frame=(CGRect){{margin,0},{CGRectGetMinX(self.selectImgView.frame)-margin,self.height}};
}
@end
