//
//  HJInOutViewCell.h
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJInOutViewCell : UITableViewCell
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
