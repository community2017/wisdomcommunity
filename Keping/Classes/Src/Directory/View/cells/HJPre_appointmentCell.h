//
//  HJPre_appointmentCell.h
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJPre_appointmentCell : UITableViewCell
/**
 imgStr
 */
@property(nonatomic,copy)NSString*imgStr;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
