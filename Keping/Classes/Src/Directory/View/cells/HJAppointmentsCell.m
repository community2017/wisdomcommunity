//
//  HJAppointmentsCell.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJAppointmentsCell.h"

@interface HJAppointmentsCell()
/**
 头像
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 姓名
 */
@property(nonatomic,weak)UILabel*nameLabel;
/**
 时间
 */
@property(nonatomic,weak)UILabel*dateLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;
/**
 接受
 */
@property(nonatomic,weak)UIButton*acceptBtn;
/**
 拒绝
 */
@property(nonatomic,weak)UIButton*rejectBtn;

@end
@implementation HJAppointmentsCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJAppointmentsCell";
    HJAppointmentsCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    WEAKSELF;
    CGFloat margin=K_FactorW(10);
    //头像
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    iconView.backgroundColor=[UIColor redColor];
    CGFloat imgWH=K_FactorW(65);
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf).offset(margin);
        make.size.mas_equalTo(CGSizeMake(imgWH, imgWH));
        make.centerY.mas_equalTo(weakSelf);
    }];
    
    //接受按钮
    UIButton*acceptBtn=[[UIButton alloc]init];
    [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
    [self.contentView addSubview:acceptBtn];
    acceptBtn.backgroundColor=[UIColor redColor];
    self.acceptBtn=acceptBtn;
    CGFloat btnW=K_FactorW(55);
    [acceptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf).offset(-K_FactorW(12));
        make.top.mas_equalTo(iconView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(20);
    }];
    
    //拒绝按钮
    UIButton*rejectBtn=[[UIButton alloc]init];
    [rejectBtn setTitle:@"Reject" forState:UIControlStateNormal];
    self.rejectBtn=rejectBtn;
    rejectBtn.backgroundColor=[UIColor redColor];
    [self.contentView addSubview:rejectBtn];
    [rejectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(acceptBtn.mas_bottom).offset(margin);
        make.left.right.mas_equalTo(acceptBtn);
        make.size.mas_equalTo(acceptBtn);
    }];
    
    //姓名
    UILabel*nameLabel=[[UILabel alloc]init];
    [self.contentView addSubview:nameLabel];
    nameLabel.textColor=[UIColor colorWithHexString:@"#111111"];
    nameLabel.font=[UIFont boldSystemFontOfSize:18];
    self.nameLabel=nameLabel;
    nameLabel.text=@"Desdemona";
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(iconView.mas_right).offset(margin);
        make.right.mas_equalTo(acceptBtn.mas_left).offset(-margin);
        make.centerY.mas_equalTo(acceptBtn);
    }];
    
    //时间
    UILabel*dateLabel=[[UILabel alloc]init];
    [self.contentView addSubview:dateLabel];
    dateLabel.text=@"09 January 2017 9:00 pm";
    dateLabel.textColor=[UIColor colorWithHexString:@"#a0a0a0"];
    self.dateLabel=dateLabel;
    [dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(nameLabel);
        make.top.mas_equalTo(nameLabel.mas_bottom);
    }];
    
    //内容
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.contentView addSubview:contentLabel];
    contentLabel.textColor=[UIColor colorWithHexString:@"#1a1a1a"];
    self.contentLabel=contentLabel;
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dateLabel);
        make.top.mas_equalTo(dateLabel.mas_bottom);
    }];
    contentLabel.text=@"Pending";
}

@end
