//
//  HJPre_appointmentCell.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJPre_appointmentCell.h"

@interface HJPre_appointmentCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
@end
@implementation HJPre_appointmentCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJPre_appointmentCell";
    HJPre_appointmentCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    WEAKSELF;
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    CGFloat margin=K_FactorW(15);
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf).offset(margin);
        make.right.mas_equalTo(weakSelf).offset(-margin);
        make.bottom.mas_equalTo(weakSelf);
        make.top.mas_equalTo(K_FactorH(10));
    }];
}
-(void)setImgStr:(NSString *)imgStr
{
    _imgStr=imgStr;
    self.iconView.image=[UIImage imageNamed:imgStr];
}
@end
