//
//  HJVisitFriendViewCell.h
//  Keping
//
//  Created by a on 2017/8/29.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJVisitFriendViewCell : UITableViewCell
/**
 标题名
 */
@property(nonatomic,copy)NSString*title;
/**
 cell是否被选中，默认no(暂时没想到好方法)
 */
@property(nonatomic,assign)BOOL isSelect;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
