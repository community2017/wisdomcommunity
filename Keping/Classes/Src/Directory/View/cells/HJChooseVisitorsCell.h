//
//  HJChooseVisitorsCell.h
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJChooseVisitorsCell : UITableViewCell
/**
 标题名
 */
@property(nonatomic,copy)NSString*title;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
