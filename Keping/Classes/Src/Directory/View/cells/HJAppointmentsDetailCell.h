//
//  HJAppointmentsDetailCell.h
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJAppointmentsDetailCell : UITableViewCell
/**
 标题
 */
@property(nonatomic,copy)NSString*title;
/**
 内容
 */
@property(nonatomic,copy)NSString*content;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
