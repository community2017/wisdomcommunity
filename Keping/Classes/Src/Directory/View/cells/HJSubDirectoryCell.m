//
//  HJSubDirectoryCell.m
//  Keping
//
//  Created by a on 2017/8/24.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSubDirectoryCell.h"

@interface HJSubDirectoryCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 标题
 */
@property(nonatomic,weak)UILabel*distanceLabel;


@end
@implementation HJSubDirectoryCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJSubDirectoryCell";
    HJSubDirectoryCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    iconView.backgroundColor=[UIColor redColor];
    self.iconView=iconView;
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.text=@"Arizona Pancake House";
    titleLabel.textColor=[UIColor colorWithHexString:@"#0f0f0f"];
    self.titleLabel=titleLabel;
    
    
    //距离
    UILabel*distanceLabel=[[UILabel alloc]init];
    [self.contentView addSubview:distanceLabel];
    distanceLabel.textColor=[UIColor colorWithHexString:@"#484848"];
    distanceLabel.text=@"638m";
    self.distanceLabel=distanceLabel;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin = K_FactorW(10);
    CGFloat height = K_FactorH(150/2);
    CGFloat width = K_FactorW(210/2);
    self.iconView.frame=(CGRect){{K_FactorW(25/2),(self.height-height)/2},CGSizeMake(width, height)};
    
    CGFloat labelH=K_FactorH(20);
    self.titleLabel.frame=(CGRect){{CGRectGetMaxX(self.iconView.frame)+margin,self.iconView.y},CGSizeMake(self.width-CGRectGetMaxX(self.iconView.frame)-2*margin,labelH)};
    
    self.distanceLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.iconView.frame)-labelH},CGSizeMake(self.titleLabel.width, labelH)};
}
@end
