//
//  HJVisitFriendMessageCell.m
//  Keping
//
//  Created by a on 2017/8/29.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJVisitFriendMessageCell.h"

@interface HJVisitFriendMessageCell()
/**
 输入框
 */
@property(nonatomic,weak)UITextView*messageView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 尖头
 */
@property(nonatomic,weak)UIImageView*arrowImgView;

@end
@implementation HJVisitFriendMessageCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJVisitFriendMessageCell";
    HJVisitFriendMessageCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    self.titleLabel=titleLabel;
    
    //输入框
    UITextView*messageView=[[UITextView alloc]init];
    [self.contentView addSubview:messageView];
    self.messageView=messageView;
    messageView.layer.cornerRadius=K_FactorW(8);
    messageView.layer.masksToBounds=YES;
    messageView.layer.borderWidth=1;
    messageView.layer.borderColor=[UIColor colorWithHexString:@"#dfdfdf"].CGColor;
    
    //尖头
    UIImageView*arrowImgView=[[UIImageView alloc]init];
    [self.contentView addSubview:arrowImgView];
    self.arrowImgView=arrowImgView;
    arrowImgView.backgroundColor=[UIColor redColor];
}
-(void)setTitle:(NSString *)title
{
    _title=title;
    self.titleLabel.text=title;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin=K_FactorW(10);
    self.titleLabel.frame=(CGRect){{margin,0},{self.width,K_FactorH(60)}};
    
    CGFloat imgWH=K_FactorW(15);
    self.arrowImgView.frame=(CGRect){{self.width-margin-imgWH,(K_FactorH(60)-imgWH)/2},{imgWH,imgWH}};
    
    self.messageView.frame=(CGRect){{margin,CGRectGetMaxY(self.titleLabel.frame)},{self.width-2*margin,self.height-CGRectGetMaxY(self.titleLabel.frame)-K_FactorH(35/2)}};
}
@end
