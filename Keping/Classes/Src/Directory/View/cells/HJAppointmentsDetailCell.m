//
//  HJAppointmentsDetailCell.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJAppointmentsDetailCell.h"

@interface HJAppointmentsDetailCell()
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;

@end
@implementation HJAppointmentsDetailCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJAppointmentsDetailCell";
    HJAppointmentsDetailCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    WEAKSELF;
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    self.titleLabel=titleLabel;
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf).offset(K_FactorW(10));
        make.centerY.mas_equalTo(weakSelf);
       
    }];
    
    //内容
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.contentView addSubview:contentLabel];
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel.mas_right).offset(K_FactorW(15));
        make.centerY.mas_equalTo(weakSelf);
        make.right.mas_equalTo(weakSelf).offset(-K_FactorW(15));
    }];
    self.contentLabel=contentLabel;
    
}
-(void)setTitle:(NSString *)title
{
    _title=title;
    self.titleLabel.text=title;
}
-(void)setContent:(NSString *)content
{
    _content=content;
    self.contentLabel.text=content;
}
@end
