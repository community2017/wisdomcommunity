//
//  HJEnterGuestCell.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJEnterGuestCell.h"

@interface HJEnterGuestCell()
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 尖头
 */
@property(nonatomic,weak)UIImageView*arrowImgView;
/**
 详情
 */
@property(nonatomic,weak)UILabel*detailLabel;

@end
@implementation HJEnterGuestCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJEnterGuestCell";
    HJEnterGuestCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    self.titleLabel=titleLabel;
    
    //尖头
    UIImageView*arrowImgView=[[UIImageView alloc]init];
    [self.contentView addSubview:arrowImgView];
    self.arrowImgView=arrowImgView;
    arrowImgView.backgroundColor=[UIColor redColor];
    
    //详情
    UILabel*detailLabel=[[UILabel alloc]init];
    [self.contentView addSubview:detailLabel];
    self.detailLabel=detailLabel;
    detailLabel.numberOfLines=0;
    detailLabel.textColor=[UIColor colorWithHexString:@"#666666"];
    detailLabel.font=HJFont(15);
}
-(void)setTitle:(NSString *)title
{
    _title=title;
    self.titleLabel.text=title;
}
-(void)setDetail:(NSString *)detail
{
    _detail=detail;
    self.detailLabel.text=detail;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin=K_FactorW(10);
    CGFloat labelH =K_FactorH(40);
    self.titleLabel.frame=(CGRect){{margin,0},{self.width,labelH}};
    
    CGFloat imgWH=K_FactorW(15);
    self.arrowImgView.frame=(CGRect){{self.width-margin-imgWH,(labelH-imgWH)/2},{imgWH,imgWH}};

    self.detailLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)},{self.width-2*margin,self.height-CGRectGetMaxY(self.titleLabel.frame)}};
}
@end
