//
//  HJAdViewCell.m
//  Keping
//
//  Created by a on 2017/8/24.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJAdViewCell.h"

@interface HJAdViewCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;

@end
@implementation HJAdViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJAdViewCell";
    HJAdViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.text=@"广告标题";
    titleLabel.textColor=[UIColor colorWithHexString:@"#0f0f0f"];
    self.titleLabel=titleLabel;

    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    iconView.backgroundColor=[UIColor redColor];
    self.iconView=iconView;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.titleLabel.frame=(CGRect){{K_FactorW(25/2),K_FactorH(15/2)},CGSizeMake(self.width, K_FactorH(20))};
    
    self.iconView.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)+K_FactorH(15/2)},CGSizeMake(self.width-2*self.titleLabel.x, self.height-(CGRectGetMaxY(self.titleLabel.frame)+K_FactorH(15/2))-K_FactorH(25/2))};
}
@end
