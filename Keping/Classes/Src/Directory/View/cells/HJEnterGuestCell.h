//
//  HJEnterGuestCell.h
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJEnterGuestCell : UITableViewCell
+(instancetype)cellWithTableView:(UITableView*)tableView;
/**
 title
 */
@property(nonatomic,copy)NSString*title;
/**
 详情
 */
@property(nonatomic,copy)NSString*detail;
@end
