//
//  HJInOutViewCell.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJInOutViewCell.h"

@interface HJInOutViewCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 门锁号
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 时间
 */
@property(nonatomic,weak)UILabel*timeLabel;

@end
@implementation HJInOutViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJInOutViewCell";
    HJInOutViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    iconView.image=[UIImage imageNamed:@"in"];
    self.iconView=iconView;
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.text=@"000 56625";
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:20];
    self.titleLabel=titleLabel;
    
    
    //锁时间
    UILabel*timeLabel=[[UILabel alloc]init];
    [self.contentView addSubview:timeLabel];
    timeLabel.textColor=[UIColor colorWithHexString:@"#959595"];
    timeLabel.text=@"Setiapath Residence : Gate 1 Wed,30 Jan 2017,8.12 pm";
    timeLabel.numberOfLines=0;
    timeLabel.font=HJFont(K_FactorW(16));
    self.timeLabel=timeLabel;
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat imgW=K_FactorW(65);
    UIImage*img=[UIImage imageNamed:@"in"];
    CGFloat imgH =imgW/img.size.width*img.size.height;
    CGFloat margin=K_FactorW(20);
    if (imgH > (self.height-margin)) {
        imgH=self.height-margin;
        imgW=imgH/img.size.height*img.size.width;
    }
    
    self.iconView.frame=(CGRect){{self.width-K_FactorW(15)-imgW,(self.height-imgH)/2},{imgW,imgH}};
    
    self.titleLabel.frame=(CGRect){{margin,self.iconView.y},{CGRectGetMinX(self.iconView.frame)-2*margin,K_FactorH(25)}};
    
    self.timeLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)},{self.titleLabel.width,CGRectGetMaxY(self.iconView.frame)-CGRectGetMaxY(self.titleLabel.frame)}};
}

@end
