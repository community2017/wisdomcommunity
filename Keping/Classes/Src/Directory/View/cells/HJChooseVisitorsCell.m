//
//  HJChooseVisitorsCell.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJChooseVisitorsCell.h"


@interface HJChooseVisitorsCell()
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 选择按钮
 */
@property(nonatomic,weak)UIButton*chooseBtn;

@end
@implementation HJChooseVisitorsCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJChooseVisitorsCell";
    HJChooseVisitorsCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    self.titleLabel=titleLabel;
    
    //选择按钮
    UIButton*chooseBtn=[[UIButton alloc]init];
    [self.contentView addSubview:chooseBtn];
    self.chooseBtn=chooseBtn;
    chooseBtn.backgroundColor=[UIColor redColor];
}
-(void)setTitle:(NSString *)title
{
    _title=title;
    self.titleLabel.text=title;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin=K_FactorW(10);
    
    CGFloat btnWH=K_FactorW(20);
    self.chooseBtn.frame=(CGRect){{self.width-margin-btnWH,(self.height-btnWH)/2},{btnWH,btnWH}};
    
      self.titleLabel.frame=(CGRect){{margin,0},{CGRectGetMinX(self.chooseBtn.frame)-2*margin,self.height}};
    
}
@end
