//
//  KPersonalModel.h
//  Keping
//
//  Created by a on 2017/9/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPersonalModel : NSObject
/**
 @mock=成功
 */
@property(nonatomic,copy)NSString*msg;
/**
 @mock=200
 */
@property(nonatomic,assign)NSInteger statusCode;

@property(nonatomic,strong)NSArray*glRecommendViews;
@end

@interface glRecommendViews : NSObject
/**
  内容
  */
@property(nonatomic,copy)NSString*content;
/**
  时间
  */
@property(nonatomic,assign)NSInteger createTime;
/**
 社区id
 */
@property(nonatomic,copy)NSString*enittyId;
/**
 id
 */
@property(nonatomic,copy)NSString*id;
/**
 图片
 */
@property(nonatomic,copy)NSString*imageUrl;
/**
 是否查看过
 */
@property(nonatomic,copy)NSString*isSee;
/**
 用户id
 */
@property(nonatomic,copy)NSString*partyId;
/**
 简介
 */
@property(nonatomic,copy)NSString*subject;
@end
