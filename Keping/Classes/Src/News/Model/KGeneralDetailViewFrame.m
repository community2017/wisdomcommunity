//
//  KGeneralDetailViewFrame.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KGeneralDetailViewFrame.h"

@implementation KGeneralDetailViewFrame
-(void)setMoxing:(NSString *)moxing
{
    _moxing=moxing;
    CGFloat marginW =K_FactorW(12);
    CGFloat marginH= K_FactorH(15);
    CGFloat imgWH=K_FactorW(40);
    self.iconFrame=(CGRect){{marginW,marginH},{imgWH,imgWH}};
    
    CGSize nikeSize=[@"Heniry" sizeWithAttributes:@{NSFontAttributeName:HJFont(K_FactorW(20))}];
    self.nikeFrame=(CGRect){{CGRectGetMaxX(self.iconFrame)+marginW,self.iconFrame.origin.y},nikeSize};
    
    CGSize contentSize=[@"Life is like a colorfu; chidhood dream, make a person yearn to make people yearning" boundingRectWithSize:CGSizeMake(App_Frame_Width-self.nikeFrame.origin.x-marginW, MAXFLOAT) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:HJFont(K_FactorW(20))} context:nil].size;
    self.contentFrame=(CGRect){{self.nikeFrame.origin.x,CGRectGetMaxY(self.nikeFrame)+marginH},contentSize};
    
    CGSize timeSize=[@"3 hours ago" sizeWithAttributes:@{NSFontAttributeName:HJFont(K_FactorW(16))}];
    self.timeFrame=(CGRect){{self.nikeFrame.origin.x,CGRectGetMaxY(self.contentFrame)+marginH},timeSize};
    
    CGSize countSize=[@"Comment" sizeWithAttributes:@{NSFontAttributeName:HJFont(K_FactorW(20))}];
    self.commentCountFrame=(CGRect){{App_Frame_Width-marginW-countSize.width,CGRectGetMinY(self.timeFrame)-(countSize.height-timeSize.height)/2},countSize};
    
    CGFloat btnW=K_FactorW(20);
    CGFloat btnH=K_FactorH(15);
    self.commentBtnFrame=(CGRect){{CGRectGetMinX(self.commentCountFrame)-marginW-btnW,CGRectGetMinY(self.timeFrame)-(btnH-timeSize.height)/2},{btnW,btnH}};
    
    self.cellHeight=CGRectGetMaxY(self.commentBtnFrame)+marginH;
}
@end
