//
//  KGeneralDetailViewFrame.h
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KGeneralDetailViewFrame : NSObject
/**
 模型
 */
@property(nonatomic,strong)NSString*moxing;
/**
 头像frame
 */
@property(nonatomic,assign)CGRect iconFrame;
/**
 昵称frame
 */
@property(nonatomic,assign)CGRect nikeFrame;
/**
 内容frame
 */
@property(nonatomic,assign)CGRect contentFrame;
/**
 时间frame
 */
@property(nonatomic,assign)CGRect timeFrame;
/**
 评论数frame
 */
@property(nonatomic,assign)CGRect commentCountFrame;
/**
 评论按钮frame
 */
@property(nonatomic,assign)CGRect commentBtnFrame;
/**
 cellHeight
 */
@property(nonatomic,assign)CGFloat cellHeight;
@end
