//
//  KFeedbackModel.h
//  Keping
//
//  Created by a on 2017/9/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KFeedbackModel : NSObject
/**
 @mock=成功
 */
@property(nonatomic,copy)NSString*msg;
/**
 @mock=200
 */
@property(nonatomic,assign)NSInteger statusCode;
@end
