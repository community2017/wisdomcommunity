//
//  KbbsTopicsModel.h
//  Keping
//
//  Created by a on 2017/9/21.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
@class bbsTopicViews;
@interface KbbsTopicsModel : NSObject
/**
 <#Description#>
 */
@property(nonatomic,strong)NSArray<bbsTopicViews*>*bbsTopicViews;
/**
 @mock=成功
 */
@property(nonatomic,copy)NSString*msg;
/**
 @mock=200
 */
@property(nonatomic,assign)NSInteger statusCode;
@end
@interface bbsTopicViews : NSObject
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*cateId;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*createTime;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*id;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString* img;
/**
 是否是广告
 */
@property(nonatomic,copy)NSString*isAd;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*isLike;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*meId;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*subject;
/**
 <#Description#>
 */
@property(nonatomic,copy)NSString*summary;

@end
