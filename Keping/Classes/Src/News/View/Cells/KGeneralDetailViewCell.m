//
//  KGeneralDetailViewCell.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KGeneralDetailViewCell.h"


#import "KGeneralDetailViewFrame.h"
@interface KGeneralDetailViewCell()
/**
 头像
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 昵称
 */
@property(nonatomic,weak)UILabel*nikeLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;
/**
 时间
 */
@property(nonatomic,weak)UILabel*timeLabel;
/**
 评论按钮
 */
@property(nonatomic,weak)UIButton*commentBtn;
/**
 评论数
 */
@property(nonatomic,weak)UILabel*countLabel;
@end
@implementation KGeneralDetailViewCell


+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"KGeneralDetailViewCell";
    KGeneralDetailViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    iconView.backgroundColor=[UIColor redColor];
    self.iconView=iconView;
    
    //昵称
    UILabel*nikeLabel=[[UILabel alloc]init];
    [self.contentView addSubview:nikeLabel];
    nikeLabel.text=@"Heniry";
    nikeLabel.textColor=[UIColor colorWithHexString:@"#959595"];
    nikeLabel.font=HJFont(K_FactorW(20));
    self.nikeLabel=nikeLabel;
    
    
    //内容
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.contentView addSubview:contentLabel];
    contentLabel.textColor=[UIColor colorWithHexString:@"101010"];
    contentLabel.text=@"Life is like a colorfu; chidhood dream, make a person yearn to make people yearning";
    contentLabel.font=HJFont(K_FactorW(20));
    contentLabel.numberOfLines=0;
    self.contentLabel=contentLabel;
    
    //时间
    UILabel*timeLabel=[[UILabel alloc]init];
    [self.contentView addSubview:timeLabel];
    timeLabel.textColor=[UIColor colorWithHexString:@"#d6d6d6"];
    timeLabel.font=HJFont(K_FactorW(16));
    timeLabel.text=@"3 hours ago";
    self.timeLabel=timeLabel;
    
    //评论按钮
    UIButton*commentBtn=[[UIButton alloc]init];
    [self.contentView addSubview:commentBtn];
    [commentBtn setTitleColor:[UIColor colorWithHexString:@"#aaaaaa"] forState:UIControlStateNormal];
    commentBtn.backgroundColor=[UIColor redColor];
    [commentBtn addTarget:self action:@selector(clickCommentBtn) forControlEvents:UIControlEventTouchUpInside];
    self.commentBtn=commentBtn;
    
    //评论数
    UILabel*countLabel=[[UILabel alloc]init];
    [self.contentView addSubview:countLabel];
    countLabel.font=HJFont(K_FactorW(20));
    countLabel.textColor=[UIColor colorWithHexString:@"#aaaaaa"];
    countLabel.text=@"Comment";
    self.countLabel=countLabel;
}
-(void)clickCommentBtn
{
    if ([_delegate respondsToSelector:@selector(generalDetailViewCellClickComment:)]) {
        [_delegate generalDetailViewCellClickComment:self];
    }
}
-(void)setGeneralDetailViewFrame:(KGeneralDetailViewFrame *)generalDetailViewFrame
{
    _generalDetailViewFrame=generalDetailViewFrame;
    
    self.iconView.frame=generalDetailViewFrame.iconFrame;
    self.iconView.layer.cornerRadius=generalDetailViewFrame.iconFrame.size.height/2;
    self.iconView.layer.masksToBounds=YES;
    
    
    self.nikeLabel.frame=generalDetailViewFrame.nikeFrame;
    
    self.contentLabel.frame=generalDetailViewFrame.contentFrame;
    
    self.timeLabel.frame=generalDetailViewFrame.timeFrame;
    
    self.commentBtn.frame=generalDetailViewFrame.commentBtnFrame;
    
    self.countLabel.frame=generalDetailViewFrame.commentCountFrame;
}
@end
