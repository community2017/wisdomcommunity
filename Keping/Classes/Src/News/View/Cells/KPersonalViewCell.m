//
//  KPersonalViewCell.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KPersonalViewCell.h"
#import "KPersonalModel.h"

@interface KPersonalViewCell()

/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 时间
 */
@property(nonatomic,weak)UILabel*timeLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;
@end
@implementation KPersonalViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"KAdViewCell";
    KPersonalViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
       
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
         self.backgroundColor=[UIColor clearColor];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    self.contentView.layer.borderColor=[UIColor colorWithHexString:@"#e6e6e6"].CGColor;
    self.contentView.layer.borderWidth=1;
    self.contentView.layer.cornerRadius=8;
    self.contentView.backgroundColor=[UIColor whiteColor];
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor=[UIColor colorWithHexString:@"#191919"];
    self.titleLabel=titleLabel;

    //时间
    UILabel*timeLabel=[[UILabel alloc]init];
    timeLabel.textColor=[UIColor colorWithHexString:@"#cacaca"];
    timeLabel.font=HJFont(K_FactorW(15));
    [self.contentView addSubview:timeLabel];
    self.timeLabel=timeLabel;
    
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    
    //内容
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.contentView addSubview:contentLabel];
    contentLabel.textColor=[UIColor colorWithHexString:@"#191919"];
    contentLabel.font=HJFont(K_FactorW(16));
    contentLabel.numberOfLines=2;
    self.contentLabel=contentLabel;
}
-(void)setGlRecommendViews:(glRecommendViews *)glRecommendViews
{
    _glRecommendViews=glRecommendViews;
//    self.titleLabel.text=glRecommendViews.subject;
    
    NSTimeInterval second = glRecommendViews.createTime / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:second];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *dateStr = [fmt stringFromDate:date];
    self.timeLabel.text=dateStr;
    
    [self.iconView setImageWithURL:[NSURL URLWithString:[KImageURL stringByAppendingString:glRecommendViews.imageUrl]] placeholder:[UIImage imageNamed:KImagePlaceholder]];
    
    self.contentLabel.text=glRecommendViews.content;
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat marginW=K_FactorW(10);
    CGFloat marginH=K_FactorH(15);
    CGFloat labelH=K_FactorH(20);
    self.contentView.frame=(CGRect){{marginW,marginH},{self.width-2*marginW,self.height-marginH}};
    
    marginH=K_FactorH(10);
    self.titleLabel.frame=(CGRect){{marginW,marginH},{self.contentView.width-2*marginW,labelH}};
    
    self.timeLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)},{self.titleLabel.width,labelH}};
    
    self.iconView.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.timeLabel.frame)+marginH/2},{self.titleLabel.width,K_FactorH(120)}};
    
    self.contentLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.iconView.frame)+marginH},{self.titleLabel.width,2*labelH}};
}
@end
