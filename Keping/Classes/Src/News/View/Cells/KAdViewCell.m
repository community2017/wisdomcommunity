//
//  KAdViewCell.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KAdViewCell.h"
#import "KbbsTopicsModel.h"

@interface KAdViewCell ()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;

@end
@implementation KAdViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"KGeneralViewCell";
    KAdViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor=[UIColor colorWithHexString:@"#191919"];
    self.titleLabel=titleLabel;
}
-(void)setBbsTopicViews:(bbsTopicViews *)bbsTopicViews
{
    _bbsTopicViews=bbsTopicViews;
    [self.iconView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KImageURL,bbsTopicViews.img]] placeholder:[UIImage imageNamed:KImagePlaceholder]];
    
    self.titleLabel.text=bbsTopicViews.subject;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat marginW = K_FactorW(10);
    CGFloat marginH= K_FactorH(10);
    CGFloat height = K_FactorH(340/2);
    CGFloat labelH=K_FactorH(20);
    self.titleLabel.frame=(CGRect){{marginW,marginH},{self.width-2*marginW,labelH}};
    
    self.iconView.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)+marginH},CGSizeMake(self.titleLabel.width, height)};
    
}

@end
