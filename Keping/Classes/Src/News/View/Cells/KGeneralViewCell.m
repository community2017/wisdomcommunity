//
//  KGeneralViewCell.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KGeneralViewCell.h"

#import "KbbsTopicsModel.h"

@interface KGeneralViewCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 内容
 */
@property(nonatomic,weak)UILabel*contentLabel;
@end
@implementation KGeneralViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"KAdViewCell";
    KGeneralViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor=[UIColor colorWithHexString:@"#191919"];
    self.titleLabel=titleLabel;
    
    
    //内容
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.contentView addSubview:contentLabel];
    contentLabel.textColor=[UIColor colorWithHexString:@"#848484"];
    contentLabel.font=HJFont(K_FactorW(16));
    contentLabel.numberOfLines=2;
    self.contentLabel=contentLabel;
}
-(void)setBbsTopicViews:(bbsTopicViews *)bbsTopicViews
{
    _bbsTopicViews=bbsTopicViews;
    [self.iconView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KImageURL,bbsTopicViews.img]] placeholder:[UIImage imageNamed:KImagePlaceholder]];
    
    self.titleLabel.text=bbsTopicViews.subject;
    
    self.contentLabel.text=bbsTopicViews.summary;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin = K_FactorW(10);
    CGFloat height = K_FactorH(175/2);
    CGFloat width = K_FactorW(265/2);
    self.iconView.frame=(CGRect){{K_FactorW(25/2),(self.height-height)/2},CGSizeMake(width, height)};
    
    CGFloat labelH=K_FactorH(20);
    self.titleLabel.frame=(CGRect){{CGRectGetMaxX(self.iconView.frame)+margin,self.iconView.y},CGSizeMake(self.width-CGRectGetMaxX(self.iconView.frame)-2*margin,labelH)};
    
    self.contentLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)+margin},CGSizeMake(self.titleLabel.width,2*labelH)};
}

@end
