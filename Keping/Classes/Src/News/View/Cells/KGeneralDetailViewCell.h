//
//  KGeneralDetailViewCell.h
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KGeneralDetailViewFrame;
@class KGeneralDetailViewCell;
@protocol KGeneralDetailViewCellDelegate <NSObject>

/**
 点击评论

 @param generalDetailViewCell self
 */
-(void)generalDetailViewCellClickComment:(KGeneralDetailViewCell*)generalDetailViewCell;

@end
@interface KGeneralDetailViewCell : UITableViewCell
/**
 视图模型
 */
@property(nonatomic,strong)KGeneralDetailViewFrame*generalDetailViewFrame;
/**
 代理
 */
@property(nonatomic,weak)id<KGeneralDetailViewCellDelegate>delegate;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
