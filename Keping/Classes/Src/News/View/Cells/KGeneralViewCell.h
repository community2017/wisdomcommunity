//
//  KGeneralViewCell.h
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
@class bbsTopicViews;
@interface KGeneralViewCell : UITableViewCell

/**
  公告数据
  */
@property(nonatomic,strong)bbsTopicViews*bbsTopicViews;
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
