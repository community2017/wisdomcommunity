//
//  KFeedbackRequest.m
//  Keping
//
//  Created by a on 2017/9/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KFeedbackRequest.h"

@implementation KFeedbackRequest
-(NSString *)requestUrl
{
    return KAddCommunityFeedbackURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodPOST;
}

-(id)requestArgument
{
    return self.params;
}
@end
