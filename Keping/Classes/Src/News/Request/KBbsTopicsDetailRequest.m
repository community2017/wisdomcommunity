//
//  KBbsTopicsDetailRequest.m
//  Keping
//
//  Created by a on 2017/9/27.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KBbsTopicsDetailRequest.h"

@implementation KBbsTopicsDetailRequest
{
    NSString* _requestUrl;
}

-(instancetype)initWithTopicId:(NSString *)topicId
{
    if (![topicId isNotBlank]) return nil;
    if (self == [super init]) {
        NSString* url = [NSString stringWithFormat:@"%@",KAnnouncementDetailURL];
        if ([url containsString:@"topicId"]) {
            url = [url stringByReplacingOccurrencesOfString:@"topicId" withString:topicId];
        }
        _requestUrl = url;
    }
    
    return self;
}


-(NSString *)requestUrl
{
    return _requestUrl;
}
-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}
@end
