//
//  NewsPersonalRequest.m
//  Keping
//
//  Created by a on 2017/9/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "NewsPersonalRequest.h"

@implementation NewsPersonalRequest
-(NSString *)requestUrl
{
    return KCommunityRecommandLetterURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}
@end
