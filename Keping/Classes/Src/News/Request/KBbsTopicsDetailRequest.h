//
//  KBbsTopicsDetailRequest.h
//  Keping
//
//  Created by a on 2017/9/27.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KBbsTopicsDetailRequest : KRequest
-(instancetype)initWithTopicId:(NSString*)topicId;
@end
