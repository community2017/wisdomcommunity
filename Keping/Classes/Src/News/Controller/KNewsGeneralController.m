//
//  KNewsGeneralController.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNewsGeneralController.h"
#import "HJWebViewController.h"
#import "KNewsGeneralDetailController.h"

#import "SDCycleScrollView.h"
#import "KUserData.h"

#import "KAdViewCell.h"
#import "KGeneralViewCell.h"

#import "KbbsTopicsModel.h"

#import "KBbsTopicsRequest.h"
@interface KNewsGeneralController ()<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
/**
 广告视图
 */
@property(nonatomic,weak)SDCycleScrollView*adView;
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据模型
 */
@property(nonatomic,strong)KbbsTopicsModel*bbsTopicsModel;
/**
 轮播图模型
 */
@property(nonatomic,strong)KbbsTopicsModel*adModel;
/**
 页码
 */
@property(nonatomic,assign)NSInteger pageOn;
@end

@implementation KNewsGeneralController

- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化页面为1
    self.pageOn=1;
    
    [self setUI];
    
    //[self getData];
    //获取轮播图
    //[self getAd];
}
-(void)setUI
{
    UITableView*tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.tableFooterView=[UIView new];
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
    SDCycleScrollView*adView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:KImagePlaceholder]];
    adView.pageControlDotSize = CGSizeMake(10, 10);
    adView.pageDotColor = kWhiteColor;
    adView.currentPageDotColor = kYellowColor;
    adView.autoScrollTimeInterval = 2.5;
    adView.delegate = self;
    tableView.tableHeaderView=adView;
    self.adView=adView;
}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    KBbsTopicsRequest* bbsTopicsRequest = [KBbsTopicsRequest new];
    WEAKSELF;
    KPartyView* user = [KPartyView unarchive];
    bbsTopicsRequest.params = @{@"cateId":@"100000007000014",@"communityId":user.communityId?user.communityId:@"",@"pageOn":@(self.pageOn)};
    [bbsTopicsRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        weakSelf.bbsTopicsModel = [KbbsTopicsModel modelWithJSON:request.responseData];
        if (weakSelf.bbsTopicsModel.statusCode == 200) {
            if (weakSelf.bbsTopicsModel.bbsTopicViews.count) {
                weakSelf.pageOn++;
            }
            [weakSelf.tableView reloadData];
        }else{
            [self showErrorText:self.bbsTopicsModel.msg errCode:self.bbsTopicsModel.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
}
-(void)getAd{
    KBbsTopicsRequest* bbsTopicsRequest = [KBbsTopicsRequest new];
    WEAKSELF;
      KPartyView* user = [KPartyView unarchive];
    bbsTopicsRequest.params = @{@"cateId":@"100000007000005",@"communityId":user.communityId?user.communityId:@""};
    [bbsTopicsRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        
        KbbsTopicsModel*bbsTopicsModel = [KbbsTopicsModel modelWithJSON:request.responseData];
        weakSelf.adModel=bbsTopicsModel;
        if (bbsTopicsModel.statusCode==200) {
            NSMutableArray*ary=[NSMutableArray array];
            for (bbsTopicViews*bbs in bbsTopicsModel.bbsTopicViews) {
                [ary addObject:[KImageURL stringByAppendingString:bbs.img]];
            }
            weakSelf.adView.imageURLStringsGroup=ary;
        }else{
            [self showErrorText:bbsTopicsModel.msg errCode:bbsTopicsModel.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.adView.frame=CGRectMake(0, 0, kScreenWidth,K_FactorH(200));
    self.tableView.frame=self.view.bounds;
    self.tableView.tableHeaderView=self.adView;
}
#pragma mark -- SDCycleScrollViewDelegate
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    //查看公告详情
    bbsTopicViews*bbsTopicViewsModel=self.adModel.bbsTopicViews[index];
    KNewsGeneralDetailController*vc=[KNewsGeneralDetailController new];
    vc.bbsTopicViews=bbsTopicViewsModel;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark --UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bbsTopicsModel.bbsTopicViews.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    bbsTopicViews*bbsTopicViewsModel=self.bbsTopicsModel.bbsTopicViews[indexPath.row];
    if ([bbsTopicViewsModel.isAd isEqualToString:@"Y"]) {
        //广告
        KAdViewCell*cell=[KAdViewCell cellWithTableView:tableView];
        cell.bbsTopicViews=self.bbsTopicsModel.bbsTopicViews[indexPath.row];
       return cell;
    }else{
        KGeneralViewCell*cell=[KGeneralViewCell cellWithTableView:tableView];
        cell.bbsTopicViews=self.bbsTopicsModel.bbsTopicViews[indexPath.row];
        return cell;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    bbsTopicViews*bbsTopicViewsModel=self.bbsTopicsModel.bbsTopicViews[indexPath.row];
     if ([bbsTopicViewsModel.isAd isEqualToString:@"Y"]) {
         return K_FactorH(450/2);
     }
    return K_FactorH(115);
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    bbsTopicViews*bbsTopicViewsModel=self.bbsTopicsModel.bbsTopicViews[indexPath.row];
        //查看公告详情
        KNewsGeneralDetailController*vc=[KNewsGeneralDetailController new];
        vc.bbsTopicViews=bbsTopicViewsModel;
        [self.navigationController pushViewController:vc animated:YES];
}

@end
