//
//  KGeneralCommentsController.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KGeneralCommentsController.h"

@interface KGeneralCommentsController ()

@end

@implementation KGeneralCommentsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Comments";
    self.view.backgroundColor=[UIColor whiteColor];
    //头像
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.view addSubview:iconView];
    
    //昵称
    UILabel*nikeLabel=[[UILabel alloc]init];
    [self.view addSubview:nikeLabel];
    
    //时间
    UILabel*timeLabel=[[UILabel alloc]init];
    [self.view addSubview:timeLabel];
    
    //内容
    UILabel*contentLabel=[[UILabel alloc]init];
    [self.view addSubview:contentLabel];
    
    //评论区
    UIView*commentView=[[UIView alloc]init];
    [self.view addSubview:commentView];
}
@end
