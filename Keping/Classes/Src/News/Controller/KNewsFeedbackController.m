//
//  KNewsFeedbackController.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNewsFeedbackController.h"
#import "UITextView+KPlaceholder.h"

#import "KFeedbackModel.h"
#import "KFeedbackRequest.h"

#import "KUserData.h"
@interface KNewsFeedbackController ()
/**
 反馈输入框
 */
@property(nonatomic,weak)UITextView*feedBackView;
/**
 提交按钮
 */
@property(nonatomic,weak)UIButton*submitBtn;
@end

@implementation KNewsFeedbackController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    //输入框
    self.view.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    UITextView*feedBackView=[[UITextView alloc]initWithFrame:CGRectZero];
    feedBackView.placeholder=@"Please write down your feedback...";
    feedBackView.backgroundColor=[UIColor whiteColor];
    feedBackView.layer.cornerRadius=8;
    feedBackView.font=HJFont(K_FactorW(18));
    [self.view addSubview:feedBackView];
    self.feedBackView=feedBackView;
    
    //提交按钮
    UIButton*submitBtn=[[UIButton alloc]initWithFrame:CGRectZero];
    [self.view addSubview:submitBtn];
    self.submitBtn=submitBtn;
    submitBtn.layer.masksToBounds=YES;
    [submitBtn setBackgroundColor:[UIColor colorWithHexString:@"#6853fa"] forState:UIControlStateNormal];
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [submitBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];

 
}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    KFeedbackRequest* feedbackRequest = [KFeedbackRequest new];
    KPartyView* user = [KPartyView unarchive];
    feedbackRequest.params=@{@"entityId":user.communityId};
    WEAKSELF;
    [feedbackRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        KFeedbackModel*feedbackModel = [KFeedbackModel modelWithJSON:request.responseData];
        if (feedbackModel.statusCode == 200) {
            [weakSelf showSuccess:feedbackModel.msg];
            weakSelf.feedBackView.text=nil;
        }else{
            [weakSelf showErrorText:feedbackModel.msg errCode:feedbackModel.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
}
-(void)clickSubmit
{
    [self getData];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGFloat marginW=K_FactorW(10);
    CGFloat marginH=K_FactorH(10);
    self.feedBackView.frame=(CGRect){{marginW,marginH},{self.view.width-2*marginW,K_FactorH(485/2)}};
    
    self.submitBtn.frame=(CGRect){{K_FactorW(25),CGRectGetMaxY(self.feedBackView.frame)+K_FactorH(30)},{self.view.width-2*K_FactorW(25),K_FactorH(50)}};
    self.submitBtn.layer.cornerRadius=self.submitBtn.height/2;
}
@end
