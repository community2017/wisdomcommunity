//
//  KNewsGeneralDetailController.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNewsGeneralDetailController.h"
#import "KGeneralCommentsController.h"

#import <WebKit/WebKit.h>

#import "KBbsTopicsDetailRequest.h"


#import "KGeneralDetailViewCell.h"

#import "KBbsTopicView.h"
#import "KbbsTopicsModel.h"
#import "KGeneralDetailViewFrame.h"
@interface KNewsGeneralDetailController ()<UITableViewDelegate,UITableViewDataSource,KGeneralDetailViewCellDelegate>
/**
 web
 */
@property(nonatomic,weak)WKWebView*webView;
/**
 table
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 头部
 */
@property(nonatomic,weak)UIView*headerView;
/**
 底部评论框
 */
@property(nonatomic,weak)UITextField*commentField;
/**
 公告模型
 */
@property(nonatomic,strong)KBbsTopicView*topicViews;
/**
 视图模型
 */
@property(nonatomic,strong)KGeneralDetailViewFrame*generalDetailViewFrame;
@end

@implementation KNewsGeneralDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    //[self getData];
    self.generalDetailViewFrame=[[KGeneralDetailViewFrame alloc]init];
    self.generalDetailViewFrame.moxing=@"";
}
-(void)setUI
{
    self.view.backgroundColor=[UIColor colorWithHexString:@"#f2f2f2"];
    self.navigationItem.title=@"News";
    
    //底部评论table
    UITableView*tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f2f2"];
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
    //头部网页
    UIView*headerView=[[UIView alloc]init];
    WKWebView*webView=[[WKWebView alloc]init];
    tableView.tableHeaderView=headerView;
    webView.backgroundColor=[UIColor whiteColor];
    self.webView=webView;
    [headerView addSubview:webView];
    self.headerView=headerView;
    
    //地图评论输入框
    UITextField*commentField=[[UITextField alloc]init];
    [self.view addSubview:commentField];
    commentField.placeholder=@"write a comment";
    commentField.layer.masksToBounds=YES;
    commentField.layer.borderColor=[UIColor colorWithHexString:@"#c1c1c1"].CGColor;
    commentField.layer.borderWidth=1;
    commentField.leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 40)];
    commentField.leftViewMode=UITextFieldViewModeAlways;
    self.commentField=commentField;

}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    KBbsTopicsDetailRequest* bbsTopicsRequest =[[KBbsTopicsDetailRequest alloc]initWithTopicId:self.bbsTopicViews.id];
    WEAKSELF;
    [bbsTopicsRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        weakSelf.topicViews = [KBbsTopicView modelWithJSON:request.responseData];
        if (weakSelf.topicViews.statusCode == 200) {
           
        }else{
            [self showErrorText:weakSelf.topicViews.msg errCode:weakSelf.topicViews.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGFloat footerH=K_FactorH(60);
    
    self.headerView.frame=(CGRect){{0,0},{self.view.width,self.view.height/2}};
    
    self.webView.frame=(CGRect){{0,0},{self.headerView.width,self.headerView.height-K_FactorH(10)}};
    
    self.tableView.frame=CGRectMake(0, 0, self.view.width, self.view.height-footerH);
    self.tableView.tableHeaderView=self.headerView;
    
    CGFloat marginW= K_FactorW(15);
    CGFloat fieldH= K_FactorW(45);
    self.commentField.frame=(CGRect){{marginW,CGRectGetMaxY(self.tableView.frame)+(footerH-fieldH)/2},{self.view.width-2*marginW,fieldH}};
    self.commentField.layer.cornerRadius=self.commentField.height/2;
}
#pragma mark --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KGeneralDetailViewCell*cell=[KGeneralDetailViewCell cellWithTableView:tableView];cell.delegate=self;
    cell.generalDetailViewFrame=self.generalDetailViewFrame;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.generalDetailViewFrame.cellHeight;
};
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView*headerView=[[UIView alloc]init];
    UILabel*contentLabel=[[UILabel alloc]initWithFrame:CGRectMake(K_FactorW(15), 0, App_Frame_Width, K_FactorH(35))];
    contentLabel.text=@"Comments";
    contentLabel.textColor=[UIColor colorWithHexString:@"#9a9a9a"];
    headerView.backgroundColor=[UIColor whiteColor];
    [headerView addSubview:contentLabel];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return K_FactorH(35);
}
#pragma mark --KGeneralDetailViewCellDelegate
-(void)generalDetailViewCellClickComment:(KGeneralDetailViewCell *)generalDetailViewCell
{
    KGeneralCommentsController*vc=[KGeneralCommentsController new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
