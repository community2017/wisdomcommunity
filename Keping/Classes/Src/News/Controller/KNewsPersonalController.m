//
//  KNewsPersonalController.m
//  Keping
//
//  Created by a on 2017/9/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNewsPersonalController.h"

#import "KPersonalViewCell.h"

#import "NewsPersonalRequest.h"

#import "KPersonalModel.h"
#import "KUserData.h"

@interface KNewsPersonalController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
  数据模型
  */
@property(nonatomic,strong)KPersonalModel*personalModel;
@end

@implementation KNewsPersonalController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    //[self getData];
}
-(void)setUI{
    
    UITableView*tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    self.tableView=tableView;
}
-(void)getData
{
    [super getData];
    [self showLoading];
    
    NewsPersonalRequest* PersonalRequest = [NewsPersonalRequest new];
    KPartyView* user = [KPartyView unarchive];
    PersonalRequest.params=@{@"entityId":user.communityId};
    WEAKSELF;
    [PersonalRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        //type1
        weakSelf.personalModel = [KPersonalModel modelWithJSON:request.responseData];
        if (weakSelf.personalModel.statusCode == 200) {
            if (weakSelf.personalModel.glRecommendViews.count) {
            }
            [weakSelf.tableView reloadData];
        }else{
            [self showErrorText:self.personalModel.msg errCode:self.personalModel.statusCode];
        }
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self hideHUD];
        [self showError:request.error];
    }];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.tableView.frame=self.view.bounds;
}
#pragma mark -- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.personalModel.glRecommendViews.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KPersonalViewCell*cell=[KPersonalViewCell cellWithTableView:tableView];
    cell.glRecommendViews=self.personalModel.glRecommendViews[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return K_FactorH(510/2);
}
@end
