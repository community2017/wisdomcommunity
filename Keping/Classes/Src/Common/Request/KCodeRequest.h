//
//  KCodeRequest.h
//  Keping
//
//  Created by 柯平 on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KRequest.h"

@interface KCodeRequest : KRequest

-(instancetype)initWithPhone:(NSString*)phone;

@end
