//
//  KCodeRequest.m
//  Keping
//
//  Created by 柯平 on 2017/8/23.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCodeRequest.h"

@implementation KCodeRequest
{
    NSString* _phone;
}

-(instancetype)initWithPhone:(NSString *)phone
{
    self = [super init];
    if (self) {
        _phone = nil;
        _phone = phone;
    }
    return self;
}

-(NSString *)requestUrl
{
    return KGetSMSCodeURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    if ([_phone isNotBlank]) {
        return @{@"phone":_phone};
    }else{
        return self.params;
    }
}

@end
