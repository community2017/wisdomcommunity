//
//  CheckUpdateCommand.h
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCommand.h"

typedef enum{
    KUpdateModeAuto = 1,    //自动更新
    KUpdateModeManual       //手动更新
} KUpdateMode;

@interface CheckUpdateCommand : KCommand
{
    @private
    KUpdateMode _updateMode;
}

/**/
@property(nonatomic)BOOL needUpdate;

-(instancetype)initWithUpdateMode:(KUpdateMode)updateMode;

+(BOOL)autoChecked;

@end
