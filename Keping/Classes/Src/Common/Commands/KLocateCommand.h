//
//  KLocateCommand.h
//  Keping
//
//  Created by 柯平 on 2017/6/18.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCommand.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

typedef NS_ENUM(NSInteger,KLocateStatus) {
    KLocateStatusSuccess    = 0,
    KLocateStatusFail       = 1,
    KLocateStatusAddressFail        = 2,
    KLocateStatusServiceUnable      = 3,
    KLocateStatusUserDenied         = 4
};

@interface KLocateCommand : KCommand<CLLocationManagerDelegate>
{
    CLLocationManager* manager;
    CLGeocoder* geocoder;
}

//responses
@property(nonatomic,assign)KLocateStatus responseStatus;
/*地址信息dic*/
@property(nonatomic,strong)NSDictionary* addressInfoDic;

/*城市名称，如：南京市*/
@property(nonatomic,copy)NSString* cityName;

/*当前用户坐标*/
@property(nonatomic,assign)CLLocationCoordinate2D coordinate;

/*定位超时*/
@property(nonatomic,assign)CGFloat timeOutDefault;

//将定位到的城市保存到Config,需要手动调用
+(void)saveLocatedCity:(NSString*)cityName;

@end
