//
//  CheckUpdateCommand.m
//  Keping
//
//  Created by 柯平 on 2017/7/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "CheckUpdateCommand.h"

static BOOL authChecked = NO;

@interface CheckUpdateCommand ()

-(void)checkVersionUpdateWithMap:(NSDictionary*)map;

@end

@implementation CheckUpdateCommand

+(BOOL)autoChecked
{
    return authChecked;
}

-(instancetype)initWithUpdateMode:(KUpdateMode)updateMode
{
    self = [super init];
    if (self) {
        _updateMode = updateMode;
    }
    return self;
}

-(void)execute
{
    //自动检查
    if (_updateMode == KUpdateModeAuto) {
        //如果已经检查过了，直接结束
        if (authChecked) {
            [self done];
        }else{
            [self checkVersionUpdateWithMap:@{}];
        }
    }else{
        self.needUpdate = YES;
        [self checkVersionUpdateWithMap:@{}];
    }
}



@end
