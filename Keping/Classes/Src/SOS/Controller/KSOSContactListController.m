//
//  KSOSContactListController.m
//  Keping
//
//  Created by a on 2017/9/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KSOSContactListController.h"
#import "KSOSAddContactController.h"

#import "KSOSContactListCell.h"
@interface KSOSContactListController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableview
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 footview
 */
@property(nonatomic,weak)UIView*footerView;
/**
 addbtn
 */
@property(nonatomic,weak)UIButton*addContactBtn;

/**
 数据源
 */
@property(nonatomic,strong)NSMutableArray*dataAry;

@end

@implementation KSOSContactListController
-(NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=[NSMutableArray array];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"SOS";
    
    UITableView*tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
    //添加table尾部
    UIView*footerView=[[UIView alloc]init];
    tableView.tableFooterView=footerView;
    footerView.backgroundColor=[UIColor clearColor];
    tableView.tableFooterView=footerView;
    self.footerView=footerView;
    
    UIButton*addContactBtn=[[UIButton alloc]initWithFrame:CGRectZero];
    [footerView addSubview:addContactBtn];
    self.addContactBtn=addContactBtn;
    addContactBtn.layer.masksToBounds=YES;
    addContactBtn.layer.borderWidth=1;
    addContactBtn.layer.borderColor=[UIColor colorWithHexString:@"#7f4afc"].CGColor;
    [addContactBtn setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addContactBtn setTitleColor:[UIColor colorWithHexString:@"#4f5ef9"] forState:UIControlStateNormal];
    [addContactBtn setTitle:@"Add New Contact" forState:UIControlStateNormal];
    addContactBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [addContactBtn addTarget:self action:@selector(clickAddContact) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)clickAddContact
{
    KSOSAddContactController*vc=[KSOSAddContactController new];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.tableView.frame=self.view.bounds;
    
    self.footerView.frame=CGRectMake(0, 0, App_Frame_Width, K_FactorH(80));
    
    CGFloat margin =K_FactorW(20);
    CGFloat btnH=K_FactorH(50);
    self.addContactBtn.frame=CGRectMake(margin,self.footerView.height-btnH-margin,self.footerView.width-2*margin,btnH);
    self.addContactBtn.layer.cornerRadius=self.addContactBtn.height/2;
    
    self.tableView.tableFooterView=self.footerView;
}
#pragma mark -- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KSOSContactListCell*cell=[KSOSContactListCell cellWithTableView:tableView];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(75);
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.dataAry.count?K_FactorH(95/2):0.0f;
    
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.dataAry.count) {
        UIView*headerView=[[UIView alloc]init];
        headerView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
        UILabel*titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(K_FactorW(10), 0, App_Frame_Width-2*K_FactorW(10), K_FactorH(95/2))];
        titleLabel.text=[NSString stringWithFormat:@"Emergency Contacts (%zd / 5 )",self.dataAry.count];
        [headerView addSubview:titleLabel];
        return headerView;
    }else
    {
        return nil;
    }
   
}
@end
