//
//  KSOSAddContactController.m
//  Keping
//
//  Created by a on 2017/9/18.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KSOSAddContactController.h"

#import "KSOSAddContactCell.h"

@interface KSOSAddContactController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableview
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 头部搜索栏
 */
@property(nonatomic,weak)UIView*headerView;
/**
 搜索
 */
@property(nonatomic,weak)UITextField * searchView;

/**
 数据源
 */
@property(nonatomic,strong)NSMutableArray*dataAry;


@end

@implementation KSOSAddContactController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Add New Contact";
    
    //头部搜索栏
    UIView*headerView=[[UIView alloc]init];
    [self.view addSubview:headerView];
    self.headerView=headerView;
    
    UITextField * searchView = [[UITextField alloc]init];
    searchView.textAlignment=NSTextAlignmentCenter;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#6d52fb"];
    searchView.layer.masksToBounds=YES;
    
    searchView.textColor=[UIColor whiteColor];
    [headerView addSubview:searchView];
    searchView.placeholder=@"Search";
    self.searchView=searchView;
    
    
    UITableView*tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    [self.view addSubview:tableView];
    tableView.tableFooterView=[UIView new];
    self.tableView=tableView;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.headerView.frame=CGRectMake(0, 0, self.view.width, K_FactorH(65));
    
    CGFloat H= K_FactorH(45);
    self.searchView.frame=CGRectMake(K_FactorW(25),(self.headerView.height-H)/2, self.headerView.width-2*K_FactorW(25), H);
    self.searchView.layer.cornerRadius=self.searchView.height/2;
    
    
    self.tableView.frame=CGRectMake(0, CGRectGetMaxY(self.headerView.frame), self.view.width, self.view.height-CGRectGetMaxY(self.headerView.frame));
}
#pragma mark -- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KSOSAddContactCell*cell=[KSOSAddContactCell cellWithTableView:tableView];
    return cell;;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(75);
}
@end
