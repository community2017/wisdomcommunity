//
//  KSOSHomeController.m
//  Keping
//
//  Created by a on 2017/9/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KSOSHomeController.h"
#import "KSOSContactListController.h"


#import "KTitleBtnView.h"

@interface KSOSHomeController ()
/**
 背景图片
 */
@property(nonatomic,weak)UIImageView*bgImgView;
/**
 求助按钮
 */
@property(nonatomic,weak)UIButton*SeekHelpBtn;
@end

@implementation KSOSHomeController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.navBarBgAlpha = @"0.0";
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //self.navBarBgAlpha = @"1.0";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

-(void)setUI
{
    WEAKSELF;
    self.navigationItem.title=@"SOS";
    self.navigationItem.rightBarButtonItem=[UIBarButtonItem itemWithImage:@"cctv_enum" withName:nil target:self action:@selector(clickRight)];
    //背景图片
    UIImageView*bgImgView=[[UIImageView alloc]init];
    [self.view addSubview:bgImgView];
    self.bgImgView=bgImgView;
    bgImgView.image=[UIImage imageNamed:@"sos_warning_bg"];
    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(weakSelf.view);
        make.top.mas_equalTo(weakSelf.view).offset(-64);
    }];
    
    //求助按钮
    CGFloat seekWH=K_FactorW(200);
    UIButton*SeekHelpBtn=[[UIButton alloc]init];
    [self.view addSubview:SeekHelpBtn];
    self.SeekHelpBtn=SeekHelpBtn;
    [SeekHelpBtn setImage:[UIImage imageNamed:@"sos_warning"] forState:UIControlStateNormal];
    [SeekHelpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(seekWH, seekWH));
        make.centerX.mas_equalTo(weakSelf.view);
        make.bottom.mas_equalTo(weakSelf.view.mas_centerY);
    }];
    
    //提示
    UILabel*explainLabel=[[UILabel alloc]init];
    [self.view addSubview:explainLabel];
    explainLabel.text=@"Always Enable GPS";
    explainLabel.textColor=[UIColor whiteColor];
    [explainLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(SeekHelpBtn.mas_bottom).offset(K_FactorH(20));
        make.centerX.mas_equalTo(weakSelf.view);
    }];
    
    //开关
    UISwitch*sw=[[UISwitch alloc]init];
    [self.view addSubview:sw];
    [sw mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(explainLabel.mas_right).offset(K_FactorW(20));
        make.centerY.mas_equalTo(explainLabel);
    }];
    
    //照相按钮
    
    KTitleBtnView*photoBtn=[[KTitleBtnView alloc]init];
    photoBtn.label.text=@"snap Photo";
     [photoBtn.btn setImage:[UIImage imageNamed:@"sos_camera"] forState:UIControlStateNormal];
    [self.view addSubview:photoBtn];
    CGFloat W= K_FactorW(100);
    CGFloat H= K_FactorH(140);
    CGFloat marign=(App_Frame_Width-2*W)/2;
    [photoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(weakSelf.view).offset(-K_FactorH(60));
        make.right.mas_equalTo(weakSelf.view.mas_centerX).offset(-marign/2);
        make.size.mas_equalTo(CGSizeMake(W, H));
    }];
    
    
    //test按钮
    KTitleBtnView*testBtn=[[KTitleBtnView alloc]init];
    testBtn.label.text=@"Test Button";
     [testBtn.btn setImage:[UIImage imageNamed:@"sos_test"] forState:UIControlStateNormal];
    [self.view addSubview:testBtn];
    [testBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(photoBtn);
        make.left.mas_equalTo(weakSelf.view.mas_centerX).offset(marign/2);
        make.size.mas_equalTo(CGSizeMake(W, H));
    }];

    
}
-(void)clickRight
{
    KSOSContactListController*vc=[KSOSContactListController new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
