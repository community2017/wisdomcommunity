//
//  KSOSAddContactCell.h
//  Keping
//
//  Created by a on 2017/9/20.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSOSAddContactCell : UITableViewCell
+(instancetype)cellWithTableView:(UITableView*)tableView;
@end
