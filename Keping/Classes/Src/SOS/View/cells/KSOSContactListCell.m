//
//  KSOSContactListCell.m
//  Keping
//
//  Created by a on 2017/9/19.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KSOSContactListCell.h"


@interface KSOSContactListCell()
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 昵称
 */
@property(nonatomic,weak)UILabel*nikeLabel;

@end
@implementation KSOSContactListCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"KSOSContactListCell";
    KSOSContactListCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    self.iconView=iconView;
    WEAKSELF;
    CGFloat imgWH=K_FactorW(50);
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf).offset(K_FactorW(15));
        make.size.mas_equalTo(CGSizeMake(imgWH, imgWH));
        make.centerY.mas_equalTo(weakSelf);
    }];
    
    //标题
    UILabel*nikeLabel=[[UILabel alloc]init];
    [self.contentView addSubview:nikeLabel];
    nikeLabel.font = SystemBoldFont;
    nikeLabel.textColor = kBlackColor;
    self.nikeLabel=nikeLabel;
    [nikeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(iconView.mas_right).offset(K_FactorW(15));
        make.centerY.mas_equalTo(weakSelf);
    }];
    
    //按钮
    UIButton*btn=[[UIButton alloc]init];
    [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.contentView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf).offset(-K_FactorW(15));
        make.centerY.mas_equalTo(weakSelf);
    }];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}
@end
