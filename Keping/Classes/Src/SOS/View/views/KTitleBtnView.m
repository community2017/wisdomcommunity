//
//  KTitleBtnView.m
//  Keping
//
//  Created by a on 2017/9/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTitleBtnView.h"

@interface KTitleBtnView()



@end
@implementation KTitleBtnView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    UIButton*btn=[[UIButton alloc]init];
    [self addSubview:btn];
    self.btn=btn;
    
    UILabel*label=[[UILabel alloc]init];
    [self addSubview:label];
    label.textAlignment=NSTextAlignmentCenter;
    label.textColor=[UIColor whiteColor];
    self.label=label;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat btnWH = self.width;
    self.btn.frame=(CGRect){{0,0},{btnWH,btnWH}};
    self.label.frame=(CGRect){{0,CGRectGetMaxY(self.btn.frame)},{self.width,self.height-CGRectGetMaxY(self.btn.frame)}};
}
@end
