//
//  KTitleBtnView.h
//  Keping
//
//  Created by a on 2017/9/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTitleBtnView : UIView
/**
 
 */
@property(nonatomic,weak) UIButton*btn;
/**
 
 */
@property(nonatomic,weak)UILabel*label;
@end
