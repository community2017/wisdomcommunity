//
//  SettingsViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCollectionCell.h"
#import "KNavigationController.h"
#import "KLoginViewController.h"
#import "KUserCenterViewController.h"
#import "KSuggestionViewController.h"
#import "KVersionViewController.h"
#import "KTCPrivacyViewController.h"
#import "KPermissionViewController.h"
#import "KNotificationViewController.h"
#import "KCommunityVertifyViewController.h"
#import "KUserData.h"

@interface SettingsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray* items;
}
@property(nonatomic,strong)UICollectionView* collectionView;
@property(nonatomic,strong)UIButton* logoutBtn;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    items = @[@"Profile",@"Community",@"Notification",@"About Us",@"APP Permissions",@"T&C/Privacy",@"Version",@"Suggestion",@"Clear Cache"];
    self.collectionView.backgroundColor = kWhiteColor;
    [self.collectionView registerClass:[SettingsCollectionCell class] forCellWithReuseIdentifier:[SettingsCollectionCell className]];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return items.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.row];
    SettingsCollectionCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[SettingsCollectionCell className] forIndexPath:indexPath];
    //cell.backgroundColor = [UIColor whiteColor];
    
    if ([title isEqualToString:@"T&C/Privacy"]) {
        cell.itemView.image = [UIImage imageNamed:@"setting_Privacy"];
    }else{
        cell.itemView.image = [UIImage imageNamed:[NSString stringWithFormat:@"setting_%@",title]];
    }
    cell.titleLb.text = title;
    cell.titleLb.textColor = [UIColor darkGrayColor];
    return cell;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(kScreenWidth);
    }];
    [self.logoutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.collectionView.mas_bottom).offset(20);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(40);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat width = (kScreenWidth-2)/3;
        layout.itemSize = CGSizeMake(width, width);
        layout.minimumLineSpacing = 1;
        layout.minimumInteritemSpacing = 1;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenWidth) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        //_collectionView.backgroundColor = KBackColor;
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

-(UIButton *)logoutBtn
{
    if (!_logoutBtn) {
        _logoutBtn = [UIButton commonButtonWithTitle:@"Log Out"];
        _logoutBtn.layer.cornerRadius = 20;
        @weakify(self);
        [_logoutBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            KAlertView* alert = [[KAlertView alloc] initWithTitle:@"Log Out?" content:@"Are you sure you want to Log Out?" placeholder:nil style:KAlertViewAlert alignment:KAlertViewButtonAlignmentHorizontal confirmTitle:@"OK" cancelTitle:@"Cancel"];
            alert.ClickBlock = ^(BOOL isConfirm, NSString *inputText) {
                if (isConfirm) {
                    [weak_self userChooseLogout];
                }
            };
            [alert showInView:weak_self.view];
        }];
        [self.view addSubview:_logoutBtn];
    }
    return _logoutBtn;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.row];
    @weakify(self);
    if ([title isEqualToString:@"Clear Cache"]) {
        KAlertView* alert = [[KAlertView alloc] initWithTitle:title content:@"Are you sure you want to clear cache?" placeholder:nil style:KAlertViewAlert alignment:KAlertViewButtonAlignmentHorizontal confirmTitle:@"OK" cancelTitle:@"Cancel"];
        alert.ClickBlock = ^(BOOL isConfirm, NSString *inputText) {
            if (isConfirm) {
                [self showSuccess:@"Clear Success!"];
            }
        };
        [alert showInView:weak_self.view];
    }else if ([title isEqualToString:@"Suggestion"]){
        KSuggestionViewController* vc = [KSuggestionViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"Profile" isEqualToString:title]) {
        KUserCenterViewController* vc = [KUserCenterViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"Community" isEqualToString:title]){
        KCommunityVertifyViewController* vc = [KCommunityVertifyViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"Version" isEqualToString:title]){
        KVersionViewController* vc = [KVersionViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"T&C/Privacy" isEqualToString:title]){
        KTCPrivacyViewController* vc = [KTCPrivacyViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"APP Permissions" isEqualToString:title]){
        KPermissionViewController* vc = [KPermissionViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"About Us" isEqualToString:title]){
        KVersionViewController* vc = [KVersionViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([@"Notification" isEqualToString:title]){
        KNotificationViewController* vc = [KNotificationViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
