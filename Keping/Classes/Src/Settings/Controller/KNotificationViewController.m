//
//  KNotificationViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KNotificationViewController.h"
#import "KSwitchCell.h"

@interface KNotificationViewController ()
@property(nonatomic,strong)NSArray* notifications;
@property(nonatomic,strong)NSArray* sectionTitles;
@end

@implementation KNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Notification";
    self.sectionTitles = @[@"All Notifications",@"Message",@"Video Intercom",@"Specific Notification"];
    self.notifications =
  @[
  @[@"Enable All Notifications"],
  @[@"Enable Message",@"Vibrate"],
  @[@"Enable Video Intercom",@"Vibrate"],
  @[@"Access Card Use",@"Incoming Visiors"]
  ];
    
    self.groupView.refreshEnable = NO;
    self.groupView.loadmoreEnable = NO;
    self.groupView.sectionFooterHeight = CGFLOAT_MIN;
    self.groupView.sectionHeaderHeight = CGFLOAT_MIN;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.notifications.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray*)self.notifications[section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @weakify(self);
    NSString* title = self.notifications[indexPath.section][indexPath.row];
    KSwitchCell* cell = [tableView dequeueReusableCellWithIdentifier:[KSwitchCell className]];
    if (!cell) {
        cell = [[KSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KSwitchCell className]];
    }
    cell.operateBlock = ^(BOOL open) {
        [weak_self operateWithTitle:title open:open];
    };
    cell.titleLabel.text = title;
    return cell;
}

-(void)operateWithTitle:(NSString*)title open:(BOOL)open{
    if ([@"All Notifications" isEqualToString:title]) {
        // 获取通知中心 是否 允许程序通知消息的值。
        

    }else if ([@"" isEqualToString:title]){
        
    }else if ([@"" isEqualToString:title]){
        
    }else{
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [UIView new];
    UILabel* titleLabel = [UILabel new];
    titleLabel.font = SystemFont;
    [titleLabel setFrame:CGRectMake(10, 20, kScreenWidth-20, 20)];
    titleLabel.text = self.sectionTitles[section];
    [view addSubview:titleLabel];
    return view;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return self.sectionTitles[section];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
