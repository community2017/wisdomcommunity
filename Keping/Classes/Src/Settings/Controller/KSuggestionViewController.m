//
//  KSuggestionViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/9.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KSuggestionViewController.h"

@interface KSuggestionViewController ()<YYTextViewDelegate>
@property(nonatomic,strong)YYTextView* textView;
@property(nonatomic,strong)UIButton* submitBtn;

@end

@implementation KSuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Suggestion";
    
    _textView = [[YYTextView alloc] initWithFrame:CGRectMake(10, 10, kScreenWidth-20, kScreenWidth/2)];
    _textView.backgroundColor = kWhiteColor;
    _textView.delegate = self;
    _textView.layer.cornerRadius = 3.0f;
    _textView.layer.borderColor = kGrayColor.CGColor;
    _textView.layer.borderWidth = 0.1;
    _textView.placeholderFont = SystemFont;
    _textView.placeholderTextColor = kDarkGrayColor;
    _textView.placeholderText = @"Please write down your feedback...";
    [self.view addSubview:_textView];
    
    _submitBtn = [UIButton commonButtonWithTitle:@"Submit"];
    [_submitBtn setFrame:CGRectMake(15, _textView.bottom+30, kScreenWidth-30, 40)];
    [self.view addSubview:_submitBtn];
    
    
    
}

-(void)submitFeedback{
    if (![_textView.text isNotBlank]) {
        [self showErrorText:@"Please write down your feedback!"];
        return;
    }
    
    
    
}

-(void)textViewDidChange:(YYTextView *)textView
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
