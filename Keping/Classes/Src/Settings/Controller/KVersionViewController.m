//
//  KVersionViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/9.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KVersionViewController.h"
#import "NSString+KP.h"

@interface KVersionViewController ()

@property(nonatomic,strong)UIImageView* backView;
@property(nonatomic,strong)UIImageView* linkView;
@property(nonatomic,strong)UILabel* appLabel;
@property(nonatomic,strong)UILabel* versionLabel;
@property(nonatomic,strong)UILabel* crLabel1;
@property(nonatomic,strong)UILabel* crLabel2;
@property(nonatomic,strong)UIButton* updateBtn;

@end

@implementation KVersionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Version";
    self.view.backgroundColor = kWhiteColor;
    
    _linkView = [[UIImageView alloc] init];
    _linkView.image = [UIImage imageNamed:@"linkTime_icon"];
    _linkView.size = CGSizeMake(kScreenWidth/5*3, kScreenWidth/5);
    _linkView.top = 20;
    _linkView.centerX = self.view.centerX;
    [self.view addSubview:_linkView];
    
    _backView = [[UIImageView alloc] initWithFrame:CGRectMake(0, _linkView.bottom+10, kScreenWidth, kScreenHeight - 30 - _linkView.height)];
    _backView.image = [UIImage imageNamed:@"about_back"];
    [self.view addSubview:_backView];
    
    _appLabel = [UILabel new];
    _appLabel.textAlignment = NSTextAlignmentCenter;
    _appLabel.textColor = kWhiteColor;
    _appLabel.size = CGSizeMake(kScreenWidth, 30);
    _appLabel.font = kBoldFont(24);
    _appLabel.text = @"LinkTIME";
    _appLabel.top = self.view.centerY;
    _appLabel.centerX = self.view.centerX;
    [self.view addSubview:_appLabel];
    
    _versionLabel = [UILabel new];
    _versionLabel.textAlignment = NSTextAlignmentCenter;
    _versionLabel.textColor = kWhiteColor;
    _versionLabel.size = CGSizeMake(kScreenWidth, 20);
    _versionLabel.font = SystemSmallFont;
    _versionLabel.top = _appLabel.bottom + 10;
    _versionLabel.text = [NSString stringWithFormat:@"Version %@",[NSString appShortVersionString]];
    [self.view addSubview:_versionLabel];
    
    NSMutableAttributedString* text1 = [[NSMutableAttributedString alloc] initWithString:@"©2007 "];
    [text1 addAttributes:@{NSFontAttributeName:SystemSmallFont,NSForegroundColorAttributeName:kWhiteColor} range:text1.rangeOfAll];
    NSMutableAttributedString* text2 = [[NSMutableAttributedString alloc] initWithString:@"Linktime dotcom Sdn.Bhd."];
    [text2 addAttributes:@{NSFontAttributeName:SystemBoldFont,NSForegroundColorAttributeName:kWhiteColor} range:text2.rangeOfAll];
    NSMutableAttributedString* text3 = [[NSMutableAttributedString alloc] initWithString:@"(121147-A)"];
    [text3 addAttributes:@{NSFontAttributeName:SystemSmallFont,NSForegroundColorAttributeName:kWhiteColor} range:text3.rangeOfAll];
    NSMutableAttributedString* crText = [[NSMutableAttributedString alloc] init];
    [crText appendAttributedString:text1];
    [crText appendAttributedString:text2];
    [crText appendAttributedString:text3];
    crText.alignment = NSTextAlignmentCenter;
    _crLabel1 = [UILabel new];
    _crLabel1.textColor = kWhiteColor;
    _crLabel1.textAlignment = NSTextAlignmentCenter;
    _crLabel1.size = _versionLabel.size;
    _crLabel1.top = _versionLabel.bottom+10;
    _crLabel1.font = SystemFont;
    _crLabel1.attributedText = crText;
    [self.view addSubview:_crLabel1];
    
    _crLabel2 = [UILabel new];
    _crLabel2.textColor = kWhiteColor;
    _crLabel2.textAlignment = NSTextAlignmentCenter;
    _crLabel2.size = _versionLabel.size;
    _crLabel2.top = _crLabel1.bottom+10;
    _crLabel2.font = SystemFont;
    _crLabel2.text = @"All Rights Reserved.";
    [self.view addSubview:_crLabel2];
    
    NSMutableAttributedString* text = [[NSMutableAttributedString alloc] initWithString:@"Version update"];
    [text addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleThick) range:text.rangeOfAll];
    [text addAttribute:NSForegroundColorAttributeName value:kWhiteColor range:text.rangeOfAll];
    [text addAttribute:NSFontAttributeName value:kBoldFont(18) range:text.rangeOfAll];
    text.alignment = NSTextAlignmentCenter;
    
    _updateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _updateBtn.size = CGSizeMake(kScreenWidth, 40);
    _updateBtn.top = kScreenHeight - 100;
    _updateBtn.centerX = self.view.centerX;
    _updateBtn.backgroundColor = kClearColor;
    [_updateBtn setTitleColor:kWhiteColor forState:UIControlStateNormal];
    [_updateBtn setAttributedTitle:text forState:UIControlStateNormal];
    [_updateBtn addTarget:self action:@selector(checkUpdateAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_updateBtn];
    
    [self.view sendSubviewToBack:_backView];
}

-(void)checkUpdateAction{
    [self showText:@"No updates available"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
