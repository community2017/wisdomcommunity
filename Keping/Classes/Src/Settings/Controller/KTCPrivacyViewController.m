//
//  KTCPrivacyViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/9.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTCPrivacyViewController.h"
#import "KTCFAQRequest.h"
#import "KTCPFAQModel.h"
#import <UITableView+FDTemplateLayoutCell.h>
#import "KPrivacyCell.h"

@interface KTCPrivacyViewController ()

@end

@implementation KTCPrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"T&C/Privacy";
    
    self.tableView.loadmoreEnable = NO;
    [self.tableView registerClass:[KPrivacyCell class] forCellReuseIdentifier:[KPrivacyCell className]];
    [self.tableView beginRefresh];
}

-(void)refresh
{
    [super refresh];
    [self getData];
}

-(void)getData
{
    [super getData];
    KTCFAQRequest* request = [[KTCFAQRequest alloc] initWithType:@"privacy"];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KTCPFAQModel* data = [KTCPFAQModel modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            if (self.page == 1) {
                [self.data removeAllObjects];
            }
            [self.data addObjectsFromArray:data.privacyEntityViews];
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
        [self.tableView endRefresh];
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self.tableView endRefresh];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KPrivacyCell* cell = [tableView dequeueReusableCellWithIdentifier:[KPrivacyCell className]];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

-(void)configureCell:(KPrivacyCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    cell.model = self.data[indexPath.row];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:[KPrivacyCell className] configuration:^(id cell) {
        [self configureCell:cell atIndexPath:indexPath];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
