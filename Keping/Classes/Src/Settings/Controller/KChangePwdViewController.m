//
//  KChangePwdViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KChangePwdViewController.h"
#import "KFootView.h"
#import "KTextCell.h"
#import "KUserData.h"
#import "KForgetRequest.h"

@interface KChangePwdViewController ()

/**/
@property(nonatomic,strong)UILabel* headView;
/**/
@property(nonatomic,strong)KFootView* footView;
/**/
@property(nonatomic,copy)NSString* oldPwd;
/**/
@property(nonatomic,copy)NSString* xinPwd;
/**/
@property(nonatomic,copy)NSString* checkPwd;

@end

@implementation KChangePwdViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.whiteNaviBar.hidden = NO;
    [self.view bringSubviewToFront:self.whiteNaviBar];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.refreshEnable = NO;
    self.tableView.loadmoreEnable = NO;
    self.tableView.tableHeaderView = self.headView;
    self.tableView.tableFooterView = self.footView;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20);
    [self.tableView setFrame:
     CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64)];
    self.tableView.rowHeight = 54;
    self.data = @[@"Enter Old Password",@"Enter New Password",@"Confirm New Password"].mutableCopy;
}

-(void)confirmChanges{
    [self.view endEditing:YES];
    if (![self.oldPwd isNotBlank]) {
        [self showText:@"Old password can not be empty"];
        return;
    }
    if (![self.xinPwd isNotBlank]) {
        [self showText:@"New password can not be empty"];
        return;
    }
    if (![self.checkPwd isNotBlank]) {
        [self showText:@"Please enter your new password again"];
        return;
    }
    
    KPartyView* user = [KPartyView unarchive];
    if (user.phone) {
        KChangePwdRequest* request = [[KChangePwdRequest alloc] init];
        request.params = [NSDictionary dictionaryWithObjectsAndKeys:[self.checkPwd md5String],@"passWord",[self.oldPwd md5String],@"oldPwd", nil];
        [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
            KRequestData* data = [KRequestData modelWithJSON:request.responseData];
            if (data.statusCode == 200) {
                [self showSuccess:@"Password updates succeeded"];
                if ([KAppConfig sharedConfig].isRememberPassword) {
                    [[KAppConfig sharedConfig] setPassword:self.checkPwd];
                }
                [self userChooseLogout];
            }else{
                [self showErrorText:data.msg errCode:data.statusCode];
            }
        } failure:^(__kindof KBaseRequest * _Nonnull request) {
            //
        }];
        
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @weakify(self);
    NSString* placeholder = self.data[indexPath.row];
    KTextCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTextCell className]];
    if (!cell) {
        cell = [[KTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTextCell className]];
    }
    cell.textTf.secureTextEntry = YES;
    [cell.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.top.bottom.mas_equalTo(0);
    }];
    NSMutableAttributedString* attPlace = [[NSMutableAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:SystemFont,NSForegroundColorAttributeName:kDarkGrayColor}];
    cell.textTf.attributedPlaceholder = attPlace;
    cell.textBlock = ^(NSString *text) {
        if ([@"Enter Old Password" isEqualToString:placeholder]) {
            weak_self.oldPwd = text;
        }else if ([@"Enter New Password" isEqualToString:placeholder]){
            weak_self.xinPwd = text;
        }else if ([@"Confirm New Password" isEqualToString:placeholder]){
            if (![weak_self.xinPwd isEqualToString:text]) {
                [weak_self showErrorText:@"Incorrect password!"];
            }else{
                weak_self.checkPwd = text;
            }
        }
    };
    return cell;
}

-(UILabel *)headView
{
    if (!_headView) {
        _headView = [[UILabel alloc] init];
        [_headView setFrame:CGRectMake(0, 0, kScreenWidth, CGFloatPixelRound(160))];
        _headView.backgroundColor = kWhiteColor;
        _headView.text = @"Change Password";
        _headView.textAlignment = NSTextAlignmentCenter;
        _headView.textColor = UIColorHex(6F50FA);//714FFA
        _headView.font = [UIFont boldSystemFontOfSize:32];
    }
    return _headView;
}

-(KFootView *)footView
{
    if (!_footView) {
        @weakify(self);
        _footView = [[KFootView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 84)];
        [_footView.footButton setTitle:@"Confirm" forState:UIControlStateNormal];
        _footView.footClick = ^{
            [weak_self confirmChanges];
        };
    }
    return _footView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
