//
//  KCommunityVertifyViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCommunityVertifyViewController.h"
#import "KCommunityVerInfoCell.h"
#import "KVertifiedCommunitiesRequest.h"
#import "KCommunityEntityViewData.h"
#import "KVeritificationViewController.h"

@interface KCommunityVertifyViewController ()
/**/
@property(nonatomic,strong)UIView* footView;
/**/
@property(nonatomic,strong)UIButton* addButton;

@end

@implementation KCommunityVertifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Community";
    self.tableView.loadmoreEnable = NO;
    self.tableView.tableFooterView = self.footView;
    [self.tableView beginRefresh];
    
}

-(void)refresh
{
    [super refresh];
    
    [self getData];
}

-(void)getData
{
    [super getData];
    KVertifiedCommunitiesRequest* request = [[KVertifiedCommunitiesRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KCommunityVertifyData* data = [KCommunityVertifyData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            [self.data addObjectsFromArray:data.communityUserAuthenticationViews];
        }else{
            [self showErrorText:data.msg errCode:data.statusCode];
        }
        [self.tableView endRefresh];
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self.tableView endRefresh];
    }];
}

-(void)addNewCommunity{
    KVeritificationViewController* vc = [KVeritificationViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.data.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 64;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KCommunityVerInfoCell* cell = [tableView dequeueReusableCellWithIdentifier:[KCommunityVerInfoCell className]];
    if (!cell) {
        cell = [[KCommunityVerInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KCommunityVerInfoCell className]];
    }
    cell.community = self.data[indexPath.section];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [UIView new];
    UILabel* titleLabel = [UILabel new];
    titleLabel.text = @"Login as:";
    titleLabel.textColor = kBlackColor;
    titleLabel.font = SystemFont;
    [titleLabel setFrame:CGRectMake(10, 24, 120, 40)];
    [view addSubview:titleLabel];
    
    UIButton* selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectBtn setFrame:CGRectMake(kScreenWidth-44, 24, 34, 34)];
    selectBtn.centerY = titleLabel.centerY;
    [selectBtn setImage:[UIImage imageNamed:@"unSelect"] forState:UIControlStateNormal];
    [selectBtn setImage:[UIImage imageNamed:@"community_select"] forState:UIControlStateSelected];
    [view addSubview:selectBtn];
    
    return view;
}

-(UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 84)];
        _footView.backgroundColor = kClearColor;
        [self.addButton setFrame:CGRectMake(15, 30, kScreenWidth-30, 44)];
        [_footView addSubview:self.addButton];
    }
    return _footView;
}

-(UIButton *)addButton
{
    if (!_addButton) {
        _addButton = [UIButton commonButtonWithTitle:@"Add New Community"];
        [_addButton addTarget:self action:@selector(addNewCommunity) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
