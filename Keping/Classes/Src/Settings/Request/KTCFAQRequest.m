//
//  KTCFAQRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTCFAQRequest.h"

@implementation KTCFAQRequest
{
    NSString* _type;
}

-(instancetype)initWithType:(NSString *)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

-(NSString *)requestUrl
{
    return KTCPrivacyFAQURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

-(id)requestArgument
{
    return @{@"type":_type};
}

@end
