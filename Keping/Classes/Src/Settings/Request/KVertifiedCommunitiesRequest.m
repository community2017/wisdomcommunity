//
//  KVertifiedCommunitiesRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KVertifiedCommunitiesRequest.h"

@implementation KVertifiedCommunitiesRequest

-(NSString *)requestUrl
{
    return KVertifiedCommunitiesURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return nil;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

@end
