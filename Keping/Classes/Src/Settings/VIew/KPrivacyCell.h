//
//  KPrivacyCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@class KPrivacyModel;

@interface KPrivacyCell : KTableViewCell

@property(nonatomic,strong)UILabel* titleLabel;
@property(nonatomic,strong)UILabel* contentLabel;

@property(nonatomic,strong)KPrivacyModel* model;

@end
