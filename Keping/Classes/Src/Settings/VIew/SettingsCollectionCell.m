//
//  SettingsCollectionCell.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "SettingsCollectionCell.h"

@implementation SettingsCollectionCell


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.itemView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(kScreenWidth/9);
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.bottom.mas_equalTo(self.contentView.mas_centerY).offset(-5);
    }];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(self.itemView.mas_bottom).offset(5);
    }];
}

-(UILabel *)titleLb
{
    if (!_titleLb) {
        _titleLb = [UILabel new];
        _titleLb.textAlignment = NSTextAlignmentCenter;
        _titleLb.textColor = kDarkGrayColor;
        _titleLb.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        _titleLb.backgroundColor = kClearColor;
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}

-(UIImageView *)itemView
{
    if (!_itemView) {
        _itemView = [UIImageView new];
        _itemView.contentMode = UIViewContentModeScaleToFill;
        _itemView.backgroundColor = kClearColor;
        [self.contentView addSubview:_itemView];
    }
    return _itemView;
}

@end
