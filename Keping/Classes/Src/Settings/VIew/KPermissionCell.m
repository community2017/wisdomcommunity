//
//  KPermissionCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KPermissionCell.h"

@implementation KPermissionCell

-(void)operateSwitch:(UISwitch*)sender{
    if (self.operateBlock) {
        self.operateBlock(sender.isOn);
    }
}

-(void)setInfoDic:(NSDictionary *)infoDic
{
    _infoDic = infoDic;
    NSString* title = infoDic[@"title"];
    NSString* content = infoDic[@"content"];
    self.titleLabel.text = title;
    self.contentLabel.text = content;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemBoldFont;
        _titleLabel.textColor = kBlackColor;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UISwitch *)switchIB
{
    if (!_switchIB) {
        _switchIB = [UISwitch new];
        [_switchIB addTarget:self action:@selector(operateSwitch:) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:_switchIB];
    }
    return _switchIB;
}

-(UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = SystemFont;
        _contentLabel.textColor = kDarkGrayColor;
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.switchIB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(30);
        make.centerY.mas_equalTo(self.titleLabel.mas_centerY);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(20);
        make.right.mas_equalTo(self.switchIB.mas_left).offset(-10);
        make.height.mas_equalTo(30);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel.mas_left);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(10);
        make.bottom.mas_equalTo(-10);
    }];
}

-(CGSize)sizeThatFits:(CGSize)size
{
    CGFloat totalHeight = 0;
    totalHeight += 70;
    totalHeight += [self.contentLabel sizeThatFits:CGSizeMake(size.width-20, size.height)].height;
    
    return CGSizeMake(size.width, totalHeight);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
