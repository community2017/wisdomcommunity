//
//  KCommunityVerInfoCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@class KCommunityVertifyView;

@interface KCommunityVerInfoCell : KTableViewCell

/**/
@property(nonatomic,strong)UILabel* nameLabel;
/**/
@property(nonatomic,strong)UILabel* nameValueLabel;

/**/
@property(nonatomic,strong)UILabel* codeLabel;
/**/
@property(nonatomic,strong)UILabel* codeValueLabel;

/**/
@property(nonatomic,strong)UILabel* addressLabel;
/**/
@property(nonatomic,strong)UILabel* addressValueLabel;

/**/
@property(nonatomic,strong)KCommunityVertifyView* community;

@end
