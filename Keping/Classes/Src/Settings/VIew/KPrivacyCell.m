//
//  KPrivacyCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KPrivacyCell.h"
#import "KTCPFAQModel.h"

@implementation KPrivacyCell

-(void)setModel:(KPrivacyModel *)model
{
    _model = model;
    self.titleLabel.text = model.title;
    self.contentLabel.text = model.content;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemBoldFont;
        _titleLabel.textColor = kBlackColor;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = SystemFont;
        _contentLabel.textColor = kDarkGrayColor;
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(20);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(30);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel.mas_left);
        make.right.mas_equalTo(self.titleLabel.mas_right);
        make.top.mas_equalTo(self.titleLabel.mas_bottom);
        make.bottom.mas_equalTo(-10);
    }];
}

-(CGSize)sizeThatFits:(CGSize)size
{
    CGFloat totalHeight = 0;
    totalHeight += 60;
    totalHeight += [self.contentLabel sizeThatFits:CGSizeMake(size.width-20, size.height)].height;
    
    return CGSizeMake(size.width, totalHeight);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
