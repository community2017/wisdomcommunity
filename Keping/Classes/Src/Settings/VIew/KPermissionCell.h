//
//  KPermissionCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KPermissionCell : KTableViewCell

/**/
@property(nonatomic,copy)void(^operateBlock) (BOOL allow);

@property(nonatomic,strong)UILabel* titleLabel;
@property(nonatomic,strong)UILabel* contentLabel;
@property(nonatomic,strong)UISwitch* switchIB;

@property(nonatomic,strong)NSDictionary* infoDic;

@end
