//
//  KCommunityVerInfoCell.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCommunityVerInfoCell.h"
#import "KCommunityEntityViewData.h"

@implementation KCommunityVerInfoCell

-(void)setCommunity:(KCommunityVertifyView *)community
{
    _community = community;
    self.nameValueLabel.text = community.realyName;
    self.codeValueLabel.text = community.verificationCode;
    self.addressLabel.text = community.unitNo;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(5);
    }];
    [self.nameValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(self.nameLabel.mas_right).offset(5);
        make.top.mas_equalTo(self.nameLabel.mas_top);
        make.height.mas_equalTo(self.nameLabel.mas_height);
    }];
    
    [self.codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(5);
    }];
    [self.codeValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(self.codeLabel.mas_right).offset(5);
        make.top.mas_equalTo(self.codeLabel.mas_top);
        make.height.mas_equalTo(self.codeLabel.mas_height);
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(5);
    }];
    [self.addressValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(self.addressLabel.mas_right).offset(5);
        make.top.mas_equalTo(self.addressLabel.mas_top);
        make.height.mas_equalTo(self.addressLabel.mas_height);
    }];
    
}

-(UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [self getLabel];
        _nameLabel.text = @"Name";
        _nameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLabel;
}

-(UILabel *)nameValueLabel{
    if (!_nameValueLabel) {
        _nameValueLabel = [self getLabel];
        _nameValueLabel.textAlignment = NSTextAlignmentRight;
    }
    return _nameValueLabel;
}

-(UILabel *)codeLabel
{
    if (!_codeLabel) {
        _codeLabel = [self getLabel];
        _codeLabel.text = @"Veritification Code";
    }
    return _codeLabel;
}

-(UILabel *)codeValueLabel
{
    if (!_codeValueLabel) {
        _codeValueLabel = [self getLabel];
        _codeValueLabel.textAlignment = NSTextAlignmentRight;
    }
    return _codeValueLabel;
}

-(UILabel *)addressLabel
{
    if (!_addressLabel) {
        _addressLabel = [self getLabel];
        _addressLabel.text = @"Address";
    }
    return _addressLabel;
}

-(UILabel *)addressValueLabel
{
    if (!_addressValueLabel) {
        _addressValueLabel = [self getLabel];
        _addressValueLabel.textAlignment = NSTextAlignmentRight;
    }
    return _addressValueLabel;
}

-(UILabel* )getLabel{
    UILabel* lb = [UILabel new];
    lb.textColor = kDarkGrayColor;
    lb.font = SystemFont;
    return lb;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
