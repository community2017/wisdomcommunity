//
//  KTCPFAQModel.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPrivacyModel : NSObject

@property(nonatomic,copy)NSString* content;
@property(nonatomic,assign)NSTimeInterval createTime;
@property(nonatomic,copy)NSString* id;
@property(nonatomic,copy)NSString* title;

@end

@interface KTCPFAQModel : NSObject

@property(nonatomic,copy)NSString* msg;
@property(nonatomic,assign)NSInteger statusCode;
@property(nonatomic,strong)NSArray<KPrivacyModel*>* privacyEntityViews;

@end
