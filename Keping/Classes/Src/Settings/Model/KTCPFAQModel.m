//
//  KTCPFAQModel.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTCPFAQModel.h"

@implementation KPrivacyModel

@end

@implementation KTCPFAQModel

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"privacyEntityViews":[KPrivacyModel class]};
}

@end
