//
//  KCityModel.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KCityModel : NSObject

/**/
@property(nonatomic,copy)NSString* stat;
/*zip*/
@property(nonatomic,copy)NSString* zip;
/*区域编码*/
@property(nonatomic,copy)NSString* areaCode;
/*区域名*/
@property(nonatomic,copy)NSString* areaName;
/*区域类型 1.省级 2.地市 3.县区*/
@property(nonatomic,assign)NSInteger areaType;
/*父类areaCode*/
@property(nonatomic,copy)NSString* parentArea;
/*区域首写字母*/
@property(nonatomic,copy)NSString* areaDesc;

@end
