//
//  KLocationDataModel.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDatabase.h"

@class KCityModel;

@interface KLocationDataModel : KDatabase

-(BOOL)insertObject:(KCityModel*)object;
-(NSArray*)getProviceData;
-(NSArray*)getCitiesDataWithProvince:(KCityModel*)province;
-(NSArray*)getAreasDataWithCity:(KCityModel*)city;
-(BOOL)clear;

@end
