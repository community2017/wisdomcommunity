//
//  KLocationDataModel.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KLocationDataModel.h"
#import "KCityModel.h"

@implementation KLocationDataModel

-(BOOL)insertObject:(KCityModel*)object{
    __block BOOL isSuccess = NO;
    if ([self isContainObject:object]) {
        return YES;
    }
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString* sql = [NSString stringWithFormat:@"INSERT INTO kcities_table (zip,areaCode,areaName,areaType,parentArea,areaDesc) VALUES (?,?,?,?,?,?)"];
        if (![db executeUpdate:sql,object.zip,object.areaCode,object.areaName,@(object.areaType),object.parentArea,object.areaDesc]) {
            KLog(@"ERROR:%@",[db lastErrorMessage]);
        }else{
            isSuccess = YES;
        }
    }];
    
    return isSuccess;
}

-(BOOL)isContainObject:(KCityModel*)object{
    __block BOOL isContain = NO;
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT * FROM kcities_table WHERE zip = ? and areaCode = ? and areaName = ? and areaType = ? and parentArea = ? and areaDesc = ?";
        FMResultSet* rs = [db executeQuery:sql,object.zip,object.areaCode,object.areaName,@(object.areaType),object.parentArea,object.areaDesc];
        if (!rs) {
            [rs close];
            return;
        }
        while ([rs next]) {
            NSString* zip = [rs stringForColumn:@"zip"];
            NSString* areaCode = [rs stringForColumn:@"areaCode"];
            NSString* areaName = [rs stringForColumn:@"areaName"];
            NSInteger areaType = [rs intForColumn:@"areaType"];
            NSString* parentArea = [rs stringForColumn:@"parentArea"];
            NSString* areaDesc = [rs stringForColumn:@"areaDesc"];
            
            if ([object.zip isEqualToString:zip] && [object.areaCode isEqualToString:areaCode] && [object.areaName isEqualToString:areaName] && object.areaType == areaType && [object.parentArea isEqualToString:parentArea] && [areaDesc isEqualToString:areaDesc]) {
                isContain = YES;
            }
        }
        [rs close];
    }];
    
    return isContain;
}

-(BOOL)deleteObjet:(KCityModel*)object{
    __block BOOL isDelete = NO;
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString* sql = @"DELETE FROM kcities_table WHERE zip = ? and areaCode = ? and areaName = ? and areaType = ? and parentArea = ? and areaDesc = ?";
        isDelete = [db executeUpdate:sql,object.zip,object.areaCode,object.areaName,@(object.areaType),object.parentArea,object.areaDesc];
        if (isDelete) {
            KLog(@"DELETE %@%@ SUCCESSFUL",object.areaName,object.areaCode);
        }else{
            KLog(@"ERROR:%@",[db lastErrorMessage]);
        }
    }];
    
    return isDelete;
}

-(NSArray *)getProviceData{
    __block NSMutableArray* provices = nil;
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        //NSString* sql = @"SELECT * FROM kcities_table WHERE areaType = ? and parentArea = ?";
        //FMResultSet* rs = [db executeQuery:sql,@(1),@"-1"];
        NSString* sql = @"SELECT * FROM kcities_table WHERE areaType = ?";
        FMResultSet* rs = [db executeQuery:sql,@(1)];
        if (!rs) {
            [rs close];
            return ;
        }
        provices = [NSMutableArray arrayWithCapacity:34];
        while ([rs next]) {
            NSString* zip = [rs stringForColumn:@"zip"];
            NSString* areaCode = [rs stringForColumn:@"areaCode"];
            NSString* areaName = [rs stringForColumn:@"areaName"];
            NSInteger areaType = [rs intForColumn:@"areaType"];
            NSString* parentArea = [rs stringForColumn:@"parentArea"];
            NSString* areaDesc = [rs stringForColumn:@"areaDesc"];
            
            KCityModel* provice = [[KCityModel alloc]init];
            provice.zip = zip;
            provice.areaCode = areaCode;
            provice.areaName = areaName;
            provice.areaType = areaType;
            provice.parentArea = parentArea;
            provice.areaDesc = areaDesc;
            
            [provices addObject:provice];
        }
        [rs close];
    }];
    
    return provices;
}

-(NSArray *)getCitiesDataWithProvince:(KCityModel *)province{
    __block NSMutableArray* cities = nil;
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT * FROM kcities_table WHERE areaType = ? and parentArea = ?";
        FMResultSet* rs = [db executeQuery:sql,@(2),province.areaCode];
        if (!rs) {
            [rs close];
            return ;
        }
        cities = [NSMutableArray array];
        while ([rs next]) {
            NSString* zip = [rs stringForColumn:@"zip"];
            NSString* areaCode = [rs stringForColumn:@"areaCode"];
            NSString* areaName = [rs stringForColumn:@"areaName"];
            NSInteger areaType = [rs intForColumn:@"areaType"];
            NSString* parentArea = [rs stringForColumn:@"parentArea"];
            NSString* areaDesc = [rs stringForColumn:@"areaDesc"];
            
            KCityModel* city = [[KCityModel alloc]init];
            city.zip = zip;
            city.areaCode = areaCode;
            city.areaName = areaName;
            city.areaType = areaType;
            city.parentArea = parentArea;
            city.areaDesc = areaDesc;
            
            [cities addObject:city];
        }
        [rs close];
    }];
    
    return cities;
}

-(NSArray *)getAreasDataWithCity:(KCityModel *)city{
    __block NSMutableArray* areas = nil;
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT * FROM kcities_table WHERE areaType = ? and parentArea = ?";
        FMResultSet* rs = [db executeQuery:sql,@(3),city.areaCode];
        if (!rs) {
            [rs close];
            return ;
        }
        
        areas = [NSMutableArray array];
        while ([rs next]) {
            NSString* zip = [rs stringForColumn:@"zip"];
            NSString* areaCode = [rs stringForColumn:@"areaCode"];
            NSString* areaName = [rs stringForColumn:@"areaName"];
            NSInteger areaType = [rs intForColumn:@"areaType"];
            NSString* parentArea = [rs stringForColumn:@"parentArea"];
            NSString* areaDesc = [rs stringForColumn:@"areaDesc"];
            
            KCityModel* area = [[KCityModel alloc]init];
            area.zip = zip;
            area.areaCode = areaCode;
            area.areaName = areaName;
            area.areaType = areaType;
            area.parentArea = parentArea;
            area.areaDesc = areaDesc;
            
            [areas addObject:area];
        }
        [rs close];
    }];
    
    return areas;
}

-(BOOL)clear{
    __block BOOL isClear = NO;
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString* sql = @"DROP TABLE kcities_table";
        if ([db executeUpdate:sql]) {
            KLog(@"CLEAR TABLE SUCCESSFUL");
        }
    }];
    return isClear;
}

@end
