//
//  DoorAccessCell.h
//  Keping
//
//  Created by 柯平 on 2017/7/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoorAccessCell : UICollectionViewCell

/**/
@property(nonatomic,strong)UIImageView* backView;
/**/
@property(nonatomic,strong)UILabel* titleLabel;

@end
