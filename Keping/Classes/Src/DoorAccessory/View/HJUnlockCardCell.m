//
//  HJUnlockCardCell.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJUnlockCardCell.h"

//#import "KEntranceCardsListModel.h"

@interface HJUnlockCardCell()<UIAlertViewDelegate>
/**
 图片
 */
@property(nonatomic,weak)UIImageView*iconView;
/**
 门锁号
 */
@property(nonatomic,weak)UILabel*titleLabel;
/**
 门锁时间
 */
@property(nonatomic,weak)UILabel*lockTimeLabel;
/**
 解锁按钮
 */
@property(nonatomic,weak)UIButton*unlockBtn;
@end
@implementation HJUnlockCardCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJUnlockCardCell";
    HJUnlockCardCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //图片
    UIImageView*iconView=[[UIImageView alloc]init];
    [self.contentView addSubview:iconView];
    iconView.image=[UIImage imageNamed:@"lock_card"];
    self.iconView=iconView;
    
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:20];
    self.titleLabel=titleLabel;
    
    
    //锁时间
    UILabel*lockTimeLabel=[[UILabel alloc]init];
    [self.contentView addSubview:lockTimeLabel];
    lockTimeLabel.textColor=[UIColor colorWithHexString:@"#959595"];
    lockTimeLabel.numberOfLines=0;
    lockTimeLabel.font=HJFont(K_FactorW(16));
    self.lockTimeLabel=lockTimeLabel;
    
    //解锁按钮
    UIButton*unlockBtn=[[UIButton alloc]init];
    [self.contentView addSubview:unlockBtn];
    [unlockBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    self.unlockBtn=unlockBtn;
    [unlockBtn addTarget:self action:@selector(clickUnlock) forControlEvents:UIControlEventTouchUpInside];
    
    
}
//-(void)setEntranceCardViews:(entranceCardViewsModel *)entranceCardViews
//{
//    _entranceCardViews=entranceCardViews;
//    self.titleLabel.text=entranceCardViews.cardNo;
//
//    self.lockTimeLabel.text=entranceCardViews.startTime;
//
//}
-(void)clickUnlock
{
//    NSString*title=[NSString stringWithFormat:@"Are you sure you want to unlock your card %@?",self.entranceCardViews.cardNo];
//    UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"Unlock a Card" message:title delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"Unlock", nil];
//    [alert show];
}
#pragma mark --UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        //开锁
        if ([_delegate respondsToSelector:@selector(unlockCardCellClickUnlock:)]) {
            [_delegate unlockCardCellClickUnlock:self.entranceCardViews];
        }
    }
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat imgWH=K_FactorW(65);
    CGFloat margin=K_FactorW(20);
    self.iconView.frame=(CGRect){{K_FactorW(15),(self.height-imgWH)/2},{imgWH,imgWH}};
    
    imgWH=K_FactorW(20);
    self.unlockBtn.frame=(CGRect){{self.width-margin-imgWH,self.iconView.y},{imgWH,imgWH}};
    
    self.titleLabel.frame=(CGRect){{margin+CGRectGetMaxX(self.iconView.frame),self.iconView.y},{CGRectGetMinX(self.unlockBtn.frame)-CGRectGetMaxX(self.iconView.frame)-2*margin,K_FactorH(25)}};
    
    self.lockTimeLabel.frame=(CGRect){{self.titleLabel.x,CGRectGetMaxY(self.titleLabel.frame)},{self.titleLabel.width,CGRectGetMaxY(self.iconView.frame)-CGRectGetMaxY(self.titleLabel.frame)}};
}
@end
