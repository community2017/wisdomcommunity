//
//  DoorAccessCell.m
//  Keping
//
//  Created by 柯平 on 2017/7/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "DoorAccessCell.h"

@implementation DoorAccessCell

-(UIImageView *)backView
{
    if (!_backView) {
        _backView = [[UIImageView alloc]init];
        _backView.contentMode = UIViewContentModeScaleAspectFill;
        _backView.clipsToBounds = YES;
        [self.contentView addSubview:_backView];
    }
    return _backView;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemFont;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = kBoldFont(17);
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(void)layoutSubviews{
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(0);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-5);
    }];
    
}

@end
