//
//  HJUnlockCardCell.h
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>
@class entranceCardViewsModel;
@class HJUnlockCardCell;
@protocol HJUnlockCardCellDelegate <NSObject>

-(void)unlockCardCellClickUnlock:(entranceCardViewsModel*)entranceCardViews;

@end
@interface HJUnlockCardCell : UITableViewCell
+(instancetype)cellWithTableView:(UITableView*)tableView;
/**
 门禁卡模型
 */
@property(nonatomic,strong)entranceCardViewsModel*entranceCardViews;
/**
 代理
 */
@property(nonatomic,weak)id<HJUnlockCardCellDelegate>delegate;
@end
