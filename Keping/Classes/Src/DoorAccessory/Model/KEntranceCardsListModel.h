//
//  KEntranceCardsListModel.h
//  Keping
//
//  Created by a on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@class entranceCardViewsModel;
@interface KEntranceCardsListModel : NSObject
/**
 提示
 */
@property(nonatomic,copy)NSString*msg;
/**
 @mock=200
 */
@property(nonatomic,assign)NSInteger statusCode;
/**
 门禁卡数组
 */
@property(nonatomic,copy)NSArray<entranceCardViewsModel*>*entranceCardViews;

@end
@interface entranceCardViewsModel : NSObject
/**
 门禁卡号
 */
@property(nonatomic,copy)NSString*cardNo;
/**
 社区id
 */
@property(nonatomic,copy)NSString*communityId;
/**
 结束时间
 */
@property(nonatomic,copy)NSString*endTime;
/**
 门禁卡id
 */
@property(nonatomic,copy)NSString* id;
/**
 开始时间
 */
@property(nonatomic,copy)NSString*startTime;
/**
 N=未锁定 Y=锁定
 */
@property(nonatomic,copy)NSString*status;
@end
