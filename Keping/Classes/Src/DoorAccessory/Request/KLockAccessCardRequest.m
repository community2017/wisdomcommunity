//
//  KLockAccessCardRequest.m
//  Keping
//
//  Created by a on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KLockAccessCardRequest.h"

@implementation KLockAccessCardRequest
{
    NSString*_requestUrl;
}
-(instancetype)initWithCarId:(NSString *)carId{
    if (![carId isNotBlank]) return nil;
    if (self) {
        NSString*url=[NSString stringWithFormat:@"%@",HJEntranceCardsUpdateURL];
        _requestUrl=[[url stringByReplacingOccurrencesOfString:@"carId" withString:carId] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    return self;
}
-(NSString *)requestUrl
{
    return _requestUrl;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}
-(id)requestArgument
{
    return self.params;
}
@end
