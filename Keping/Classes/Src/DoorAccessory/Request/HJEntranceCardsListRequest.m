//
//  HJEntranceCardsListRequest.m
//  Keping
//
//  Created by a on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJEntranceCardsListRequest.h"

@implementation HJEntranceCardsListRequest
-(NSString *)requestUrl
{
    return KPentranceCardsListURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}
@end
