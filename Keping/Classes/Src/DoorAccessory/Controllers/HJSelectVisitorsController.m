//
//  HJSelectVisitorsController.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJSelectVisitorsController.h"

#import "HJChooseVisitorsCell.h"
#import "HJVisitFriendViewCell.h"


@interface HJSelectVisitorsController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据源
 */
@property(nonatomic,strong)NSMutableArray*dataAry;
/**
 测试数据
 */
@property(nonatomic,strong)NSArray*ceshiAry;
@end

@implementation HJSelectVisitorsController
-(NSArray *)ceshiAry
{
    if (!_ceshiAry) {
        _ceshiAry=@[@"Williams",@"Smith",@"Rodriguez",@"Johnson"];
    }
    return _ceshiAry;
}
-(NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=[NSMutableArray arrayWithObjects:@[@"Choose from Previous Guest Lists"],@[@"Choose from Previous Visitors"],@[@"Choose from Friends"], nil];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Select visitors";
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
    //添加table尾部
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, K_FactorH(100))];
    tableView.tableFooterView=footerView;
    footerView.backgroundColor=[UIColor clearColor];
    CGFloat margin =K_FactorW(20);
    UIButton*addBtn=[[UIButton alloc]initWithFrame:CGRectMake(margin, K_FactorH(30), footerView.width-2*margin, footerView.height-K_FactorH(30)-margin)];
    [footerView addSubview:addBtn];
    addBtn.layer.cornerRadius=addBtn.height/2;
    addBtn.layer.masksToBounds=YES;
    [addBtn setBackgroundColor:[UIColor colorWithHexString:@"#6853fa"] forState:UIControlStateNormal];
    [addBtn setTitle:@"Send Invitation" forState:UIControlStateNormal];
    addBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [addBtn addTarget:self action:@selector(clickSendInvitation) forControlEvents:UIControlEventTouchUpInside];
}
-(void)clickSendInvitation
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataAry.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataAry[section] count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        HJVisitFriendViewCell*cell=[HJVisitFriendViewCell cellWithTableView:tableView];
        cell.title=self.dataAry[indexPath.section][indexPath.row];
        return cell;
    }else
    {
        HJChooseVisitorsCell*cell=[HJChooseVisitorsCell cellWithTableView:tableView];
        cell.title=self.dataAry[indexPath.section][indexPath.row];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
            HJVisitFriendViewCell*cell=[tableView cellForRowAtIndexPath:indexPath];
            cell.isSelect=!cell.isSelect;
            if (cell.isSelect) {
                //插入好友数据
                NSMutableArray*ary=[self.dataAry[indexPath.section] mutableCopy];
                [ary addObjectsFromArray:self.ceshiAry];
                [self.dataAry replaceObjectAtIndex:indexPath.section withObject:ary];
                //刷新界面
                [self.tableView reloadData];
            }else
            {
                //删除好友数据
                NSMutableArray*ary=[self.dataAry[indexPath.section] mutableCopy];
                [ary removeObjectsInArray:self.ceshiAry];
                [self.dataAry replaceObjectAtIndex:indexPath.section withObject:ary];
                //刷新界面
                [self.tableView reloadData];
                
            }
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return K_FactorH(60);
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return 0.01f;
    }
    return K_FactorH(10);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
@end
