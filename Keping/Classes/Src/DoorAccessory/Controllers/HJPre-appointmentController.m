//
//  HJPre-appointmentController.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJPre-appointmentController.h"
#import "HJVisitFriendController.h"
#import "HJInviteGuestController.h"
#import "HJContractorSeasonPassController.h"
#import "HJAppointmentsController.h"

#import "HJPre_appointmentCell.h"

@interface HJPre_appointmentController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据源
 */
@property(nonatomic,strong)NSArray*dataAry;
@end

@implementation HJPre_appointmentController
-(NSArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=@[@"visit_friend",@"invite_guest",@"CS_pass",@"appointments"];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Pre-appointment";
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
}
#pragma mark --UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HJPre_appointmentCell*cell=[HJPre_appointmentCell cellWithTableView:tableView];
    cell.imgStr=self.dataAry[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(150);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            HJVisitFriendController*vc=[HJVisitFriendController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:
        {
            HJInviteGuestController*vc=[HJInviteGuestController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            HJContractorSeasonPassController*vc=[HJContractorSeasonPassController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:
        {
            HJAppointmentsController*vc=[HJAppointmentsController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
            
        default:
            break;
    }
}
@end
