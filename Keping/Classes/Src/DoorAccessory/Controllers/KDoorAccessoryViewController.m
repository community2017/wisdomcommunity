//
//  KDoorAccessoryViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KDoorAccessoryViewController.h"
#import "HJMyAccessController.h"
#import "HJInOrOutRecordsController.h"
#import "HJPre-appointmentController.h"


#import "DoorAccessCell.h"

@interface KDoorAccessoryViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray* items;
}
@property(nonatomic,strong)UICollectionView* collectionView;

@end

@implementation KDoorAccessoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Door Access";
    [self.collectionView registerClass:[DoorAccessCell class] forCellWithReuseIdentifier:[DoorAccessCell className]];
    items = @[@"My Access Cards",@"In/Out Records",@"Pre-appointment"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return items.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.row];
    DoorAccessCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DoorAccessCell className] forIndexPath:indexPath];
    if ([title isEqualToString:@"In/Out Records"]) {
        cell.backView.image = [UIImage imageNamed:@"InOut Records"];
    }else{
        cell.backView.image = [UIImage imageNamed:title];
    }
    cell.titleLabel.text = title;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            HJMyAccessController*vc=[HJMyAccessController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:
        {
            HJInOrOutRecordsController*vc=[HJInOrOutRecordsController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            HJPre_appointmentController*vc=[HJPre_appointmentController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat width = kScreenWidth-20;
        layout.itemSize = CGSizeMake(width, width/2.0);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-64-49) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.backgroundColor = kWhiteColor;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
