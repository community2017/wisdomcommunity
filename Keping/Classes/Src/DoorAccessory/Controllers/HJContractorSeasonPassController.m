//
//  HJContractorSeasonPassController.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJContractorSeasonPassController.h"

#import "HJCompanyController.h"


#import "HJSelectDateView.h"
#import "HJHeaderImgView.h"

@interface HJContractorSeasonPassController ()<UITableViewDataSource,UITableViewDelegate,HJSelectDateViewDelegate>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据源
 */
@property(nonatomic,strong)NSMutableArray*dataAry;
@end

@implementation HJContractorSeasonPassController
-(NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=[NSMutableArray arrayWithObjects:@[@"Company"],@[@"Start: Date & Time",@"End: Date & Time"], nil];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Contractor / Season Pass";
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
    //设置头部
    HJHeaderImgView*headerView=[HJHeaderImgView headerImgView];
    tableView.tableHeaderView=headerView;
    
    //添加table尾部
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, K_FactorH(100))];
    tableView.tableFooterView=footerView;
    footerView.backgroundColor=[UIColor clearColor];
    CGFloat margin =K_FactorW(20);
    UIButton*addBtn=[[UIButton alloc]initWithFrame:CGRectMake(margin, K_FactorH(30), footerView.width-2*margin, footerView.height-K_FactorH(30)-margin)];
    [footerView addSubview:addBtn];
    addBtn.layer.cornerRadius=addBtn.height/2;
    addBtn.layer.masksToBounds=YES;
    [addBtn setBackgroundColor:[UIColor colorWithHexString:@"#6853fa"] forState:UIControlStateNormal];
    [addBtn setTitle:@"Submit" forState:UIControlStateNormal];
    addBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [addBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
}
-(void)clickSubmit
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataAry.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataAry[section] count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString*ID=@"cell";
    UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text=self.dataAry[indexPath.section][indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0) {
        HJCompanyController*vc=[HJCompanyController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==1){
        [self creatDatePicker:UIDatePickerModeDate];
    }
}
-(void)creatDatePicker:(UIDatePickerMode)dateModel
{
    HJSelectDateView*dateView=[[HJSelectDateView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, APP_Frame_Height)];
    dateView.delegate=self;
    dateView.DateModel=dateModel;
    [self.navigationController.view addSubview:dateView];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(60);
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return K_FactorH(10);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
#pragma mark -- HJSelectDateViewDelegate
-(void)selectDateView:(HJSelectDateView *)selectDateView clickSelectDate:(NSString *)dateStr
{
    ALERT(dateStr);
    if (selectDateView.DateModel == UIDatePickerModeDate) {
        [self creatDatePicker:UIDatePickerModeTime];
    }
}
@end
