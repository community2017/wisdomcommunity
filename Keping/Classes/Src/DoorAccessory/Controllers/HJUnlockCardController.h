//
//  HJUnlockCardController.h
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KViewController.h"
@class entranceCardViewsModel;
@interface HJUnlockCardController : KViewController
/**
 门禁卡列表
 */
@property(nonatomic,strong)NSArray<entranceCardViewsModel*>*entranceCardViews;
@end
