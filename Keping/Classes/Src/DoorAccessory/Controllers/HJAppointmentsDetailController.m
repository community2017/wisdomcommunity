//
//  HJAppointmentsDetailController.m
//  Keping
//
//  Created by a on 2017/8/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJAppointmentsDetailController.h"

#import "HJAppointmentsDetailCell.h"

@interface HJAppointmentsDetailController ()<UITableViewDelegate,UITableViewDataSource>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据源
 */
@property(nonatomic,strong)NSArray*dataAry;
/**
 测试数据
 */
@property(nonatomic,strong)NSArray*ceshiAry;
@end

@implementation HJAppointmentsDetailController
-(NSArray *)ceshiAry
{
    if (!_ceshiAry) {
        _ceshiAry=@[@"Mike Philips",@"159 1858 9563",@"Malaysia Airlines",@"KH2568  ",@"06 Jan 2017 04:36 pm",@"02:34 pm - 06:34 pm"];
    }
    return _ceshiAry;
}
-(NSArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=@[@"Full Name",@"Mobile phone Number",@"Email Address",@"Vehicle Plate Number",@"Time",@"Validity"];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Details";
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    [self.view addSubview:tableView];
    self.tableView=tableView;
}
#pragma mark --UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HJAppointmentsDetailCell*cell=[HJAppointmentsDetailCell cellWithTableView:tableView];
    cell.title=self.dataAry[indexPath.row];
    cell.content=self.ceshiAry[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(50);
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return K_FactorH(40);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return K_FactorH(230/2);
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView*footerView=[[UIView alloc]init];
    footerView.backgroundColor=[UIColor whiteColor];
    UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.width,  K_FactorH(230/2))];
    label.text=@"Vehicle Plate Number ";
    label.textColor=[UIColor colorWithHexString:@"#4c4c4c"];
    [footerView addSubview:label];

    return footerView;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView*headerView=[[UIView alloc]init];
    headerView.backgroundColor=[UIColor clearColor];
    UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.width,  K_FactorH(40))];
    label.text=@"status: Upcoming";
    label.textColor=[UIColor colorWithHexString:@"#666668"];
    label.textAlignment=NSTextAlignmentCenter;
    [headerView addSubview:label];
    return headerView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
