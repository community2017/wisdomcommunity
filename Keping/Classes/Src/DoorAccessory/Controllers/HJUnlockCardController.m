//
//  HJUnlockCardController.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJUnlockCardController.h"

#import "HJUnlockCardCell.h"

//#import "KLockAccessCardRequest.h"

//#import "KEntranceCardsListModel.h"
#import "KUnlockCardModel.h"
@interface HJUnlockCardController ()<UITableViewDelegate,UITableViewDataSource,HJUnlockCardCellDelegate>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
@end

@implementation HJUnlockCardController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title=@"Unlock a Card";
    
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.tableFooterView=[UIView new];
    [self.view addSubview:tableView];
    self.tableView=tableView;
}
#pragma mark --UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.entranceCardViews.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HJUnlockCardCell*cell=[HJUnlockCardCell cellWithTableView:tableView];
    cell.delegate=self;
    cell.entranceCardViews=self.entranceCardViews[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(90);
}
#pragma mark --HJUnlockCardCellDelegate
-(void)unlockCardCellClickUnlock:(entranceCardViewsModel *)entranceCardViews
{
//    KLockAccessCardRequest* lockRequest = [[KLockAccessCardRequest alloc]initWithCarId:entranceCardViews.cardNo];
//    lockRequest.params=[NSDictionary dictionaryWithObjectsAndKeys:entranceCardViews.startTime,@"startTime",entranceCardViews.endTime,@"endTime",@"Y",@"status",nil];
//    WEAKSELF;
//    [lockRequest startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
//        [self hideHUD];
//        //type1
//        KUnlockCardModel*unlockModel = [KUnlockCardModel modelWithJSON:request.responseData];
//        if (unlockModel.statusCode == 200) {
//            [weakSelf.navigationController popViewControllerAnimated:YES];
//
//        }else{
//            [weakSelf showErrorText:unlockModel.msg];
//        }
//
//    } failure:^(__kindof KBaseRequest * _Nonnull request) {
//        [self hideHUD];
//        [self showError:request.error];
//    }];

}
@end
