//
//  HJVisitFriendController.m
//  Keping
//
//  Created by a on 2017/8/28.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJVisitFriendController.h"
#import "HJVisitFriendInfoController.h"


#import "HJVisitFriendViewCell.h"
#import "HJHeaderImgView.h"

#import "HJSelectDateView.h"

@interface HJVisitFriendController ()<UITableViewDataSource,UITableViewDelegate,HJSelectDateViewDelegate>
/**
 tableView
 */
@property(nonatomic,weak)UITableView*tableView;
/**
 数据源
 */
@property(nonatomic,strong)NSMutableArray*dataAry;
/**
 测试数据
 */
@property(nonatomic,strong)NSArray*ceshiAry;
@end

@implementation HJVisitFriendController
-(NSArray *)ceshiAry
{
    if (!_ceshiAry) {
        _ceshiAry=@[@"Williams",@"Smith",@"Rodriguez",@"Johnson"];
    }
    return _ceshiAry;
}
-(NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry=[NSMutableArray arrayWithObjects:@[@"* Select a friend"],@[@"* Date",@"* Time"], nil];
    }
    return _dataAry;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
-(void)setUI
{
    
    self.navigationItem.title=@"Visit a Friend";
    UITableView*tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.backgroundColor=[UIColor colorWithHexString:@"#f2f1f6"];
    [self.view addSubview:tableView];
    self.tableView=tableView;
    
    //设置头部
    HJHeaderImgView*headerView=[HJHeaderImgView headerImgView];
    tableView.tableHeaderView=headerView;
    
    //添加table尾部
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, K_FactorH(80))];
    tableView.tableFooterView=footerView;
    footerView.backgroundColor=[UIColor clearColor];
    CGFloat margin =K_FactorW(20);
    UIButton*addBtn=[[UIButton alloc]initWithFrame:CGRectMake(margin, K_FactorH(30), footerView.width-2*margin, footerView.height-K_FactorH(30))];
    [footerView addSubview:addBtn];
    addBtn.layer.cornerRadius=addBtn.height/2;
    addBtn.layer.masksToBounds=YES;
    [addBtn setBackgroundColor:[UIColor colorWithHexString:@"#6853fa"] forState:UIControlStateNormal];
    [addBtn setTitle:@"Next Step" forState:UIControlStateNormal];
    addBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [addBtn addTarget:self action:@selector(clickNextStep) forControlEvents:UIControlEventTouchUpInside];
}
-(void)clickNextStep
{
    HJVisitFriendInfoController*vc=[HJVisitFriendInfoController new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark --UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataAry.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataAry[section] count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (indexPath.section==0&&indexPath.row!=0) {
        //显示好友cell
        static NSString*ID=@"cell";
        UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
        if (!cell) {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        }
        cell.textLabel.text=self.dataAry[indexPath.section][indexPath.row];
        return cell;
    }else
    {
        HJVisitFriendViewCell*cell=[HJVisitFriendViewCell cellWithTableView:tableView];
        cell.title=self.dataAry[indexPath.section][indexPath.row];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0&&indexPath.row==0) {
        HJVisitFriendViewCell*cell=[tableView cellForRowAtIndexPath:indexPath];
        cell.isSelect=!cell.isSelect;
        if (cell.isSelect) {
            //插入好友数据
            NSMutableArray*ary=[self.dataAry[0] mutableCopy];
            [ary addObjectsFromArray:self.ceshiAry];
            [self.dataAry replaceObjectAtIndex:0 withObject:ary];
            //刷新界面
            [self.tableView reloadData];
        }else
        {
            //删除好友数据
            NSMutableArray*ary=[self.dataAry[0] mutableCopy];
            [ary removeObjectsInArray:self.ceshiAry];
            [self.dataAry replaceObjectAtIndex:0 withObject:ary];
            //刷新界面
            [self.tableView reloadData];
            
        }
    }else if (indexPath.section==1)
    {
        //弹出时间选择器
        if (indexPath.row==0) {
            //弹出年月日
            [self creatDatePicker:UIDatePickerModeDate];
        }else if (indexPath.row==1)
        {
            //弹出时分秒
            [self creatDatePicker:UIDatePickerModeTime];
        }
    }else
    {
        //选择好友
        ALERT(self.dataAry[indexPath.section][indexPath.row]);
        
    }
}
-(void)creatDatePicker:(UIDatePickerMode)dateModel
{
    HJSelectDateView*dateView=[[HJSelectDateView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, APP_Frame_Height)];
    dateView.delegate=self;
    dateView.DateModel=dateModel;
    [self.navigationController.view addSubview:dateView];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return K_FactorH(60);
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return K_FactorH(10);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//
//    HJVisitHeaderFootView*headerView=[HJVisitHeaderFootView headerFootView:tableView];
//    headerView.title=ary[section];
//    return headerView;
//}
#pragma mark -- HJSelectDateViewDelegate
-(void)selectDateView:(HJSelectDateView *)selectDateView clickSelectDate:(NSString *)dateStr
{
    ALERT(dateStr);
}
@end
