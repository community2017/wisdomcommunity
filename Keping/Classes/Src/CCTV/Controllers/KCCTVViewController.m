//
//  KCCTVViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCCTVViewController.h"
#import "CCTVSettingViewController.h"
#import "KCCTVRequest.h"
#import "KCCTVsData.h"
#import "KCCTVContentView.h"

@interface KCCTVViewController ()

@property(nonatomic,assign)KCCTVShowMode showMode;
@property(nonatomic,strong)KCCTVContentView* cctvView;
@property(nonatomic,strong)KLiveModeView* modeView;
@property(nonatomic,strong)KCCTVsData* cctvData;
@end

@implementation KCCTVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUI];
    //[self getData];
    [self testData];
    _showMode = KCCTVShowModeOne;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)getData
{
    [super getData];
    KCCTVRequest* request = [[KCCTVRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KCCTVsData* cctvData = [KCCTVsData modelWithJSON:request.responseData];
        self.cctvData = cctvData;
        _cctvView.cctvData = cctvData;
        if (cctvData.statusCode != 200) {
            [self showErrorText:cctvData.msg errCode:cctvData.statusCode];
        }
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self showErrorText:
         request.error.localizedDescription?:@"Network Error!"];
    }];
}

-(void)testData{
    KCCTVsData* cctvData = [KCCTVsData new];
    NSMutableArray* arr = [NSMutableArray array];
    for (NSInteger i = 0; i < 15; i++) {
        KCctvEntityView* cctvObj = [KCctvEntityView new];
        cctvObj.address = @"HongKong";
        cctvObj.time = @"2017-09-01 03:45";
        NSInteger j = i%4;
        if (j == 0) {
            cctvObj.testUrl = @"rtsp://admin:Admin12345@linktime1.zapto.org:8050/cam/realmonitor?channel=1&subtype=1";
        }else if (j == 1){
            cctvObj.testUrl = @"rtsp://admin:Admin12345@linktime2.zapto.org:8051/cam/realmonitor?channel=1&subtype=1";
        }else if (j == 2){
            cctvObj.testUrl = @"rtsp://admin:Admin12345@linktime3.zapto.org:8052/cam/realmonitor?channel=1&subtype=1";
        }else if (j == 3){
            cctvObj.testUrl = @"rtsp://admin:Admin12345@linktime1.zapto.org:8053/cam/realmonitor?channel=1&subtype=1";
        }else{
            cctvObj.testUrl = @"rtsp://admin:Admin12345@linktime1.zapto.org:8050/cam/realmonitor?channel=1&subtype=1";
        }
        [arr addObject:cctvObj];
    }
    cctvData.cctvEntityViews = arr;
    _cctvView.cctvData = cctvData;
}

-(void)setUI
{
    self.navigationItem.title=@"CCTV";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cctv_enum.png"] style:UIBarButtonItemStylePlain target:self action:@selector(enumAction)];
    
    self.view.backgroundColor = KBackColor;
    
    _cctvView = [KCCTVContentView new];
    _cctvView.backgroundColor = kBlackColor;
    [self.view addSubview:_cctvView];

    _modeView = [KLiveModeView new];
    _modeView.backgroundColor = kWhiteColor;
    [self.view addSubview:_modeView];
    
    _cctvView.top = 0;
    _cctvView.left = 0;
    _modeView.top = _cctvView.bottom;
    _modeView.left = _cctvView.left;
}


-(void)enumAction
{
    CCTVSettingViewController* vc = [CCTVSettingViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
