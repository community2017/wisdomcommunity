//
//  CCTVSettingViewController.m
//  Keping
//
//  Created by 柯平 on 2017/7/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "CCTVSettingViewController.h"
#import "HJCCTVAddDeviceController.h"



#import "CCTVSettingCell.h"

@interface CCTVSettingViewController ()
{
    NSArray* devices;
}

@end

@implementation CCTVSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUI];
}
-(void)setUI
{
    self.navigationItem.title = @"CCTV Settings";
    self.groupView.refreshEnable = NO;
    self.groupView.loadmoreEnable = NO;
    self.groupView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.groupView.separatorInset = UIEdgeInsetsZero;
    [self.view addSubview:self.groupView];
    
    devices = @[@"CCTV 01",@"CCTV 02",@"CCTV 03",@"CCTV 04",@"CCTV 05"];
    
    //添加尾部
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, K_FactorH(80))];
    self.groupView.tableFooterView=footerView;
    footerView.backgroundColor=[UIColor clearColor];
    CGFloat margin =K_FactorW(20);
    UIButton*addBtn=[[UIButton alloc]initWithFrame:CGRectMake(margin, K_FactorH(30), footerView.width-2*margin, footerView.height-K_FactorH(30))];
    [footerView addSubview:addBtn];
    addBtn.layer.cornerRadius=addBtn.height/2;
    addBtn.layer.masksToBounds=YES;
    [addBtn setBackgroundColor:[UIColor colorWithHexString:@"#6853fa"] forState:UIControlStateNormal];
    [addBtn setTitle:@"Add New Device" forState:UIControlStateNormal];
    addBtn.titleLabel.font=[UIFont boldSystemFontOfSize:20];
    [addBtn addTarget:self action:@selector(clickAddDevice) forControlEvents:UIControlEventTouchUpInside];

}
-(void)clickAddDevice
{
    HJCCTVAddDeviceController*vc=[HJCCTVAddDeviceController new];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 40.0f;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Device Manager";
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return devices.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCTVSettingCell* cell = [tableView dequeueReusableCellWithIdentifier:[CCTVSettingCell className]];
    if (!cell) {
        cell = [[CCTVSettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[CCTVSettingCell className]];
    }
    cell.titleLabel.text = devices[indexPath.row];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
