//
//  KCCTVRequest.m
//  Keping
//
//  Created by 柯平 on 2017/9/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCCTVRequest.h"

@implementation KCCTVRequest

-(NSString *)requestUrl
{
    return KUserCCTVsURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}

@end
