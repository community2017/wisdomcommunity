//
//	KCCTVsData.h
//
//	Create by 平 柯 on 21/8/2017
//	Copyright © 2017. All rights reserved.
//

//	Model file generated by 平 柯: xtxh@outlook.com

#import <UIKit/UIKit.h>

@interface KCctvEntityView : NSObject

@property (nonatomic, strong) NSString * cctvPartyId;
@property (nonatomic, strong) NSString * communityId;
@property (nonatomic, strong) NSString * idField;
@property (nonatomic, strong) NSString * ip;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * passWord;
@property (nonatomic, strong) NSString * port;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * userName;

/*测试地址*/
@property(nonatomic,copy)NSString* testUrl;
/**/
@property(nonatomic,copy)NSString* address;
/**/
@property(nonatomic,copy)NSString* time;

@end

@interface KCCTVsData : NSObject

@property (nonatomic, strong) NSArray <KCctvEntityView*>* cctvEntityViews;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, assign) NSInteger statusCode;



@end
