//
//  HJAddDeviceCell.m
//  Keping
//
//  Created by a on 2017/8/25.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJAddDeviceCell.h"

@interface HJAddDeviceCell()
/**
 标题
 */
@property(nonatomic,weak)UILabel*titleLabel;

@end
@implementation HJAddDeviceCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString*ID=@"HJAddDeviceCell";
    HJAddDeviceCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell=[[self alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        //添加子控件
        [self setAddSubView];
    }
    return self;
}
-(void)setAddSubView
{
    //标题
    UILabel*titleLabel=[[UILabel alloc]init];
    [self.contentView addSubview:titleLabel];
    titleLabel.textColor=[UIColor colorWithHexString:@"#343434"];
    self.titleLabel=titleLabel;
    CGFloat margin =K_FactorW(15);
    WEAKSELF;
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf).offset(margin);
        make.width.mas_equalTo(K_FactorW(120));
        make.top.bottom.mas_equalTo(weakSelf);
    }];
    
    //输入框
    UITextField*contentField=[[UITextField alloc]init];
    [self.contentView addSubview:contentField];
    [contentField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel.mas_right).offset(margin);
        make.right.mas_equalTo(weakSelf).offset(-margin);
        make.top.bottom.mas_equalTo(weakSelf);
    }];
}
-(void)setTitle:(NSString *)title
{
    _title=title;
    self.titleLabel.text=title;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
}
@end
