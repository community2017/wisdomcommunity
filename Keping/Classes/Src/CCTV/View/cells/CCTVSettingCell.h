//
//  CCTVSettingCell.h
//  Keping
//
//  Created by 柯平 on 2017/7/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface CCTVSettingCell : KTableViewCell
/**/
@property(nonatomic,strong)UILabel* titleLabel;

/**/
@property(nonatomic,strong)UISwitch* uiSwitch;

/**/
@property(nonatomic,copy)void (^switchBlock)(BOOL isOn);
@end
