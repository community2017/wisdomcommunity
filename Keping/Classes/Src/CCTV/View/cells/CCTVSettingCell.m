//
//  CCTVSettingCell.m
//  Keping
//
//  Created by 柯平 on 2017/7/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "CCTVSettingCell.h"

@implementation CCTVSettingCell

@synthesize titleLabel = _titleLabel;
@synthesize uiSwitch = _uiSwitch;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemFont;
        _titleLabel.textColor = [UIColor darkTextColor];
        [self.contentView addSubview:_titleLabel];
        
        _uiSwitch = [[UISwitch alloc]init];
        [_uiSwitch addTarget:self action:@selector(valueChanged) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:_uiSwitch];
        
    }
    return self;
}

-(void)layoutSubviews{
    [self.uiSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(30);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(self.uiSwitch.mas_left).offset(-5);
        make.bottom.mas_equalTo(0);
    }];

}

-(void)valueChanged{
    BOOL isOn = [_uiSwitch isOn];
    if (self.switchBlock) {
        self.switchBlock(isOn);
    }
}

@end
