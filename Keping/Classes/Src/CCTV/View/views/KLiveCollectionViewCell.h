//
//  KLiveCollectionViewCell.h
//  Keping
//
//  Created by 柯平 on 2017/9/2.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KCctvEntityView;

@interface KLiveCollectionViewCell : UICollectionViewCell

/**/
@property(nonatomic,strong)UILabel* timeLabel;
/**/
@property(nonatomic,strong)UILabel* placeLabel;

/**/
@property(nonatomic,strong)KCctvEntityView* cctvObj;

@end
