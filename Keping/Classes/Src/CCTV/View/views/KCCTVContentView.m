//
//  KCCTVContentView.m
//  Keping
//
//  Created by 柯平 on 2017/9/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KCCTVContentView.h"
#import "KCCTVsData.h"
#import "KLiveCollectionViewCell.h"

CGFloat const bottomMargin = 34;
CGFloat const gapMargin = 3.0;

@implementation KLiveModeView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kScreenWidth;
        frame.size.height = 60;
    }
    self = [super initWithFrame:frame];
    
    CGSize buttonSize = CGSizeMake(30, 30);
    _oneButton = [UIButton commonButtonWithTitle:@"01"];
    [_oneButton setBackgroundImage:[UIImage imageWithColor:UIColorHex(6354F9)] forState:UIControlStateSelected];
    [_oneButton setBackgroundImage:[UIImage imageWithColor:UIColorHex(CECFCE)] forState:UIControlStateNormal];
    [_oneButton setTitleColor:kWhiteColor forState:UIControlStateSelected];
    [_oneButton setTitleColor:kDarkGrayColor forState:UIControlStateNormal];
    _oneButton.size = buttonSize;
    _oneButton.layer.cornerRadius = 3.0;
    _oneButton.clipsToBounds = YES;
    [_oneButton addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    _playButton = [UIButton commonButtonWithTitle:@""];
    [_playButton setBackgroundImage:[UIImage imageWithColor:UIColorHex(6354F9)] forState:UIControlStateNormal];
    _playButton.size = CGSizeMake(20, 20);
    _playButton.clipsToBounds = YES;
    _playButton.layer.cornerRadius = _playButton.size.height/2.0;
    
    _fourButton = [UIButton commonButtonWithTitle:@"04"];
    [_fourButton setBackgroundImage:[UIImage imageWithColor:UIColorHex(6354F9)] forState:UIControlStateSelected];
    [_fourButton setBackgroundImage:[UIImage imageWithColor:UIColorHex(CECFCE)] forState:UIControlStateNormal];
    [_fourButton setTitleColor:kWhiteColor forState:UIControlStateSelected];
    [_fourButton setTitleColor:kDarkGrayColor forState:UIControlStateNormal];
    _fourButton.size = buttonSize;
    _fourButton.layer.cornerRadius = 3.0;
    _fourButton.clipsToBounds = YES;
    [_fourButton addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _playButton.centerX = self.width/2;
    _playButton.centerY = self.height/2;
    
    _oneButton.centerX = self.width/2-60;
    _fourButton.centerX = self.width/2+60;
    _oneButton.centerY = _playButton.centerY;
    _fourButton.centerY = _playButton.centerY;
    
    [self addSubview:_oneButton];
    [self addSubview:_playButton];
    [self addSubview:_fourButton];
    
    _oneButton.selected = YES;
    
    return self;
}


-(void)buttonTap:(UIButton*)sender{
    if ([sender.titleLabel.text isEqualToString:@"01"]) {
        sender.selected = YES;
        _fourButton.selected = NO;
        _fourButton.layer.borderWidth = 0.5;
        _fourButton.layer.borderColor = kGrayColor.CGColor;
        sender.layer.borderWidth = 0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"KShowModeOfOneSingleNotification" object:nil];
    }else if ([sender.titleLabel.text isEqualToString:@"04"]){
        _oneButton.selected = NO;
        sender.selected = YES;
        _oneButton.layer.borderWidth = 0.5;
        _oneButton.layer.borderColor = kGrayColor.CGColor;
        sender.layer.borderWidth = 0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"KShowModeOfFourNotification" object:nil];
    }
}

@end

@interface KCCTVContentView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation KCCTVContentView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kScreenWidth;
        frame.size.height = kScreenWidth+bottomMargin*2;
    }
    self = [super initWithFrame:frame];
    
    self.backgroundColor = kBlackColor;
    
    _countLabel = [UILabel new];
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.textColor = kWhiteColor;
    _countLabel.frame = CGRectMake(0, self.height-bottomMargin, kScreenWidth, bottomMargin);
    _countLabel.font = kFont(14);
    [self addSubview:_countLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setOneNotification) name:@"KShowModeOfOneSingleNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setFourNotification) name:@"KShowModeOfFourNotification" object:nil];
    
    self.showMode = KCCTVShowModeOne;
    
    return self;
}

-(void)setOneNotification{
    self.showMode = KCCTVShowModeOne;
}
-(void)setFourNotification{
    self.showMode = KCCTVShowModeFour;
}

-(void)setCctvData:(KCCTVsData *)cctvData
{
    if (!cctvData) return;
    _cctvData = cctvData;
    [self reloadData];
}

-(void)reloadData
{
    if (_collectionView) {
        _collectionView.delegate = nil;
        _collectionView.dataSource = nil;
        [_collectionView removeFromSuperview];
        _collectionView = nil;
    }

    CGFloat width = 0;
    if (self.showMode) {//four
        width = (kScreenWidth-3*gapMargin)/2.0;
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(width, width);
        layout.minimumLineSpacing = gapMargin;
        layout.minimumInteritemSpacing = gapMargin;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(gapMargin, gapMargin, gapMargin, gapMargin);
        UICollectionView* collectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, bottomMargin, kScreenWidth, kScreenWidth) collectionViewLayout:layout];
        collectView.delegate = self;
        collectView.dataSource = self;
        collectView.pagingEnabled = YES;
        collectView.backgroundColor = kGrayColor;
        collectView.showsHorizontalScrollIndicator = NO;
        [self addSubview:collectView];
        self.collectionView = collectView;
        
        self.countLabel.text = [NSString stringWithFormat:@"1/%ld",self.cctvData.cctvEntityViews.count/4+1];
    }else{
        width = kScreenWidth-gapMargin*2;
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(width, width);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(gapMargin, gapMargin, gapMargin, gapMargin);
        UICollectionView* collectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, bottomMargin, kScreenWidth, kScreenWidth) collectionViewLayout:layout];
        collectView.showsHorizontalScrollIndicator = NO;
        collectView.delegate = self;
        collectView.dataSource = self;
        collectView.pagingEnabled = YES;
        collectView.backgroundColor = kGrayColor;
        [self addSubview:collectView];
        self.collectionView = collectView;
        self.countLabel.text = [NSString stringWithFormat:@"1/%ld",self.cctvData.cctvEntityViews.count];
    }
    [self.collectionView registerClass:[KLiveCollectionViewCell class] forCellWithReuseIdentifier:[KLiveCollectionViewCell className]];
    [self.collectionView reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.showMode) {
        return ceil(self.cctvData.cctvEntityViews.count/4.0);
    }else{
        return 1;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.showMode) {
        if (section < floor(self.cctvData.cctvEntityViews.count/4.0)) {
            return 4;
        }else{
            return self.cctvData.cctvEntityViews.count - floor(self.cctvData.cctvEntityViews.count/4.0)*4;
        }
    }else{
        return self.cctvData.cctvEntityViews.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KLiveCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[KLiveCollectionViewCell className] forIndexPath:indexPath];
    if (!cell) {
        cell = [[KLiveCollectionViewCell alloc] init];
    }
    [cell setCctvObj:
     self.cctvData.cctvEntityViews[indexPath.row]];
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat contentOffX = scrollView.contentOffset.x;
    KLog(@"%f",contentOffX/self.containerView.width);
    if (self.showMode) {
        self.countLabel.text = [NSString stringWithFormat:@"%.0f/%zd",contentOffX/kScreenWidth+1,self.cctvData.cctvEntityViews.count/4+1];
    }else{
        self.countLabel.text = [NSString stringWithFormat:@"%.0f/%ld",contentOffX/kScreenWidth+1,(unsigned long)self.cctvData.cctvEntityViews.count];
    }
}

-(void)dealloc
{
    if (self.collectionView) {
        self.collectionView.delegate = nil;
        self.collectionView.dataSource = nil;
        [self.collectionView removeFromSuperview];
        self.collectionView = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setShowMode:(KCCTVShowMode)showMode
{
    _showMode = showMode;
    [self reloadData];
}

@end
