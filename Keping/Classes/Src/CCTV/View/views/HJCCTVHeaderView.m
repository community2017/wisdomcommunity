//
//  HJCCTVHeaderView.m
//  Keping
//
//  Created by a on 2017/8/24.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "HJCCTVHeaderView.h"

static NSString*ID=@"cellId";
@interface HJCCTVHeaderView()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
/**
 
 */
@property(nonatomic,weak)UICollectionView*collectionView;
/**
 页码
 */
@property(nonatomic,weak)UILabel*pageLabel;
/**
 item宽
 */
@property(nonatomic,assign)CGFloat itemW;
/**
 item高
 */
@property(nonatomic,assign)CGFloat itemH;
/**
 item间距
 */
@property(nonatomic,assign)CGFloat itemMargin;
@end
@implementation HJCCTVHeaderView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor blackColor];
        [self setUI];
    }
    return self;
}
-(void)setUI
{
    self.dataAry=@[@"1_16",@"directory_Foods_back",@"1_16",@"directory_Foods_back",@"1_16",@"directory_Foods_back",@"1_16"];
    //1.初始化layout
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置collectionView滚动方向
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    
    //2.初始化collectionView
    CGFloat margin= K_FactorH(50);
    UICollectionView*collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,margin, self.width, self.height-2*margin) collectionViewLayout:layout];
    collectionView.pagingEnabled=YES;
    self.collectionView=collectionView;
    [self addSubview:collectionView];
//    collectionView.backgroundColor = [UIColor redColor];
    
    
    //3.注册collectionViewCell
    //注意，此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致 均为 cellId
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:ID];
    
    //    //注册headerView  此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致  均为reusableView
    //    [mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView"];
    
    //4.设置代理
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    //页码
    UILabel*pageLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(collectionView.frame), self.width, self.height-CGRectGetMaxY(collectionView.frame))];
    pageLabel.backgroundColor=[UIColor clearColor];
    pageLabel.textColor=[UIColor whiteColor];
    pageLabel.textAlignment=NSTextAlignmentCenter;
    self.pageLabel=pageLabel;
    [self addSubview:pageLabel];

}
-(void)setCctvStatus:(NSInteger)cctvStatus
{
    _cctvStatus=cctvStatus;
    switch (cctvStatus) {
        case 0:
            //CGSizeMake(collectionView.width, collectionView.height)
            self.itemW=self.collectionView.width;
            self.itemH=self.collectionView.height;
            self.itemMargin=0;
            //主动赋值页码
            self.pageLabel.text = [NSString stringWithFormat:@"%zd/%zd",1,self.dataAry.count];
            break;
        case 1:
        {
           self.itemMargin =K_FactorW(5);
            self.itemW=(self.collectionView.width-self.itemMargin)/2;
            self.itemH=(self.collectionView.height-self.itemMargin)/2;
            //主动赋值页码
            self.pageLabel.text = [NSString stringWithFormat:@"%zd/%.0f",1,ceilf((self.dataAry.count*1.0/4))];
        }
            break;
            
        default:
            break;
    }
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
}
#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    switch (self.cctvStatus) {
        case 0:
            return self.dataAry.count;
            break;
        case 1:
            return ceilf((self.dataAry.count*1.0/4));
            break;

            
        default:
            return 0;
            break;
    }
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (self.cctvStatus) {
        case 0:
            return 1;
            break;
        case 1:
            return 4;
            break;
            
        default:
            return 0;
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    cell.backgroundColor=[UIColor greenColor];
//    UIImageView*imgView=[[UIImageView alloc]initWithFrame:cell.bounds];
//    imgView.image=[UIImage imageNamed:self.dataAry[indexPath.row]];
//    [cell.contentView addSubview:imgView];
    return cell;
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.itemW,self.itemH);
}

//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(self.itemMargin, self.itemMargin, self.itemMargin, self.itemMargin);
//}
////设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return self.itemMargin;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return self.itemMargin;
}


//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    UICollectionView *cell = (MyCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//    NSString *msg = cell.botlabel.text;
//    NSLog(@"%@",msg);
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger currentPage=roundf(scrollView.contentOffset.x/scrollView.width+1);
    NSInteger totalPage =roundf(scrollView.contentSize.width/scrollView.width);
    self.pageLabel.text = [NSString stringWithFormat:@"%zd/%zd",currentPage,totalPage]; // 分页控制器当前显示的页数

}

@end
