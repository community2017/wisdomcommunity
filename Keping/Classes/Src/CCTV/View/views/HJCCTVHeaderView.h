//
//  HJCCTVHeaderView.h
//  Keping
//
//  Created by a on 2017/8/24.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger,HJCCTVStatus) {
    HJCCTVOnePageStatus = 1,
    HJCCTVFourPageStatus = 2
};
@interface HJCCTVHeaderView : UIView
/**
 数据源
 */
@property(nonatomic,copy)NSArray*dataAry;
/**
 分页状态
 */
@property(nonatomic,assign)NSInteger cctvStatus;
@end
