//
//  KCCTVContentView.h
//  Keping
//
//  Created by 柯平 on 2017/9/1.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KCCTVsData;

@interface KLiveModeView : UIView

/**/
@property(nonatomic,strong)UIButton* oneButton;
/**/
@property(nonatomic,strong)UIButton* fourButton;
/**/
@property(nonatomic,strong)UIButton* playButton;

@end

@interface KCCTVContentView : UIView

/**/
@property(nonatomic,strong)UIView* containerView;
/**/
@property(nonatomic,strong)UILabel* countLabel;
/**/
@property(nonatomic,strong)UICollectionView* collectionView;

/**/
@property(nonatomic,strong)KCCTVsData* cctvData;
/**/
@property(nonatomic,assign)KCCTVShowMode showMode;

@end
