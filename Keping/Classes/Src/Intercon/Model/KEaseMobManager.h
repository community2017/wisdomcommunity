//
//  KEaseMobManager.h
//  Keping
//
//  Created by 柯平 on 2017/10/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KEaseMobManager : NSObject

+(void)easeMobLoginUsername:(NSString*)username
                   password:(NSString*)password
                 controller:(KViewController*)controller;

+(void)loginStateChanged:(BOOL)loginSuccess;

+(void)easeMobLogout;

@end
