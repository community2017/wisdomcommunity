//
//  KConversationsData.m
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KConversationsData.h"

@implementation KConversationsData

+(NSDictionary*)modelContainerPropertyGenericClass{
    return @{@"relationSessionViews":[KPartyView class]};
}

@end
