//
//  KChatManager.h
//  Keping
//
//  Created by 柯平 on 2017/10/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Hyphenate/Hyphenate.h>

@interface KChatManager : NSObject<EMClientDelegate,EMContactManagerDelegate,EMChatManagerDelegate>

+(instancetype)shareManager;

-(void)asyncPushOptions;

-(void)asyncConversationData;

@end
