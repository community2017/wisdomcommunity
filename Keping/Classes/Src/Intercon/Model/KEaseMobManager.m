//
//  KEaseMobManager.m
//  Keping
//
//  Created by 柯平 on 2017/10/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KEaseMobManager.h"
#import <Hyphenate/Hyphenate.h>
#import "KChatManager.h"

@implementation KEaseMobManager

+(void)easeMobLoginUsername:(NSString *)username
                   password:(NSString *)password
                 controller:(KViewController *)controller
{
    // 已经绑定环信账号
    if ([username isNotBlank]) {
        [controller showLoading:@"Is landing..."];
        [[EMClient sharedClient] loginWithUsername:username password:password completion:^(NSString *aUsername, EMError *aError) {
            [controller hideHUD];
            if (!aError) {
                [[EMClient sharedClient].options setIsAutoLogin:YES];
                [self loginStateChanged:YES];
                [self changeRootToHomeViewController];
            }else{
                if (aError.code == EMErrorUserAlreadyLogin) {
                    [[EMClient sharedClient] logout:YES completion:^(EMError *aError) {
                        if (!aError) {
                            [[EMClient sharedClient] loginWithUsername:username password:password completion:^(NSString *aUsername, EMError *aError) {
                                [self changeRootToHomeViewController];
                                [self loginStateChanged:YES];
                            }];
                        }
                    }];
                }else if (aError.code == EMErrorUserNotFound){
                    [[EMClient sharedClient] registerWithUsername:username password:password completion:^(NSString *aUsername, EMError *aError) {
                        if (!aError) {
                            [[EMClient sharedClient] loginWithUsername:username password:password completion:^(NSString *aUsername, EMError *aError) {
                                if (!aError) {
                                    [self changeRootToHomeViewController];
                                    [self loginStateChanged:YES];
                                }else{
                                    [controller hideHUD];
                                    [controller showErrorText:aError.errorDescription];
                                }
                            }];
                        }
                    }];
                }
                else{
                    [controller hideHUD];
                    [controller showErrorText:
                     aError.errorDescription];
                }
            }
        }];
    }
}

+(void)loginStateChanged:(BOOL)loginSuccess
{
    if (loginSuccess) {
        [[KChatManager shareManager] asyncPushOptions];
        [[KChatManager shareManager] asyncConversationData];
    }
}

+(void)easeMobLogout
{
    [[EMClient sharedClient] logout:YES completion:^(EMError *aError) {
        if (aError) {
            KLog(@"Logout Error:%@",aError.errorDescription);
        }
    }];
}


@end
