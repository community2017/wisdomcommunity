//
//  KConversationsData.h
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KUserData.h"

@interface KConversationsData : NSObject

/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,strong)NSArray<KPartyView*>* relationSessionViews;

@end
