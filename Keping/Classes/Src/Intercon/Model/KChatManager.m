//
//  KChatManager.m
//  Keping
//
//  Created by 柯平 on 2017/10/15.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KChatManager.h"

@implementation KChatManager

-(instancetype)init
{
    self = [super init];
    if (self) {
        [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
        [[EMClient sharedClient].contactManager addDelegate:self delegateQueue:nil];
        [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    }
    return self;
}

+(instancetype)shareManager
{
    static KChatManager* _manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[KChatManager alloc] init];
    });
    return _manager;
}

-(void)asyncPushOptions
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = nil;
        [[EMClient sharedClient] getPushOptionsFromServerWithError:&error];
    });
}

-(void)asyncConversationData
{
    @weakify(self);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray* array = [[EMClient sharedClient].chatManager getAllConversations];
        [array enumerateObjectsUsingBlock:^(EMConversation* conversation, NSUInteger idx, BOOL * _Nonnull stop) {
            if (conversation.latestMessage == nil) {
                [[EMClient sharedClient].chatManager deleteConversation:conversation.conversationId isDeleteMessages:NO completion:nil];
            }
        }];
    });
    
}

@end
