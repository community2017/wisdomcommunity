//
//  KContactsData.h
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KUserData.h"

@interface KContactsData : NSObject
/*通讯录*/
@property(nonatomic,strong)NSArray* bookViews;//
/*社区*/
@property(nonatomic,strong)NSArray* communityViews;
/*好友*/
@property(nonatomic,strong)NSArray<KPartyView*>* friendViews;
/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,assign)NSInteger statusCode;//

@end
