//
//  KGetContactsRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KGetContactsRequest.h"

@implementation KGetContactsRequest

-(NSString *)requestUrl
{
    return KChatContactsURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId};
}

-(id)requestArgument
{
    return nil;
}

@end
