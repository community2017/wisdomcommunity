//
//  KUploadImageRequest.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KUploadImageRequest.h"

@implementation KUploadImageData

@end


@implementation KUploadImageRequest
{
    NSString *_image;
    
}

-(instancetype)initWithUploadImage:(UIImage *)image compression:(CGFloat)compression
{
    if (self = [super init]) {
        NSData* imageData = UIImageJPEGRepresentation(image, compression);
        _image = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    return self;
}

- (instancetype)initWithUploadImage:(UIImage *)image {
    return [self initWithUploadImage:image compression:1.0];
}

- (KRequestMethod)requestMethod {
    return KRequestMethodPOST;
}

- (NSTimeInterval)requestTimeoutInterval
{
    return 60;
}

-(id)requestArgument
{
    return @{@"suffix":@"jpg",@"image":_image};
}


//-(SERVERCENTER_TYPE)centerType
//{
//    return PICTURE_SERVERCENTER;
//}

- (NSString *)requestUrl
{
    return KUploadImageURL;
}

@end
