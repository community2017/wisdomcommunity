//
//  KConversationRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/16.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KConversationRequest.h"

@implementation KConversationRequest

-(NSString *)requestUrl
{
    return KChatHistoryURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return nil;
}

-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary
{
    return @{@"Cookie":[KAppConfig sharedConfig].sessionId?:@""};
}

@end
