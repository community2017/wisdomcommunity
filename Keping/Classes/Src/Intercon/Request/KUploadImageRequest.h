//
//  KUploadImageRequest.h
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//  base64上传图片

#import "KRequest.h"

@interface KUploadImageData : NSObject

/**/
@property(nonatomic,copy)NSString* msg;
/**/
@property(nonatomic,assign)NSInteger statusCode;
/**/
@property(nonatomic,copy)NSString* imageUrl;

@end


@interface KUploadImageRequest : KRequest

- (instancetype)initWithUploadImage:(UIImage *)image;
- (instancetype)initWithUploadImage:(UIImage *)image compression:(CGFloat)compression;

@end
