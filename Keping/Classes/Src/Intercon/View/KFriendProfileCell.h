//
//  KFriendProfileCell.h
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KFriendProfileCell : KTableViewCell

/**/
@property(nonatomic,strong)UIImageView* avatar;
/**/
@property(nonatomic,strong)UILabel* nicknameLabel;
/**/
@property(nonatomic,strong)UILabel* phoneLabel;
/**/
@property(nonatomic,strong)UILabel* nameLabel;

@end
