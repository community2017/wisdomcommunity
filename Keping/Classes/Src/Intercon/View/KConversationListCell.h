//
//  KConversationListCell.h
//  Keping
//
//  Created by 柯平 on 2017/9/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@class KPartyView;

@interface KConversationListCell : KTableViewCell

/**/
@property(nonatomic,strong)UIImageView* avatar;
/**/
@property(nonatomic,strong)UILabel* nameLabel;
/**/
@property(nonatomic,strong)UILabel* messageLabel;
/**/
@property(nonatomic,strong)UILabel* countLabel;

-(void)setUser:(KPartyView*)user message:(NSString*)msg;

@end
