//
//  KFootView.m
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KFootView.h"

@implementation KFootView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kScreenWidth;
        frame.size.height = K_FactorH(120);
    }
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(instancetype)init
{
    return [self initWithFrame:CGRectMake(0, 0, kScreenWidth, K_FactorH(120))];
}

-(UIButton *)footButton
{
    if (!_footButton) {
        _footButton = [UIButton commonButtonWithTitle:@""];
        [_footButton setTitleColor:kWhiteColor forState:UIControlStateNormal];
        _footButton.size = CGSizeMake(self.width - 30, 44);
        _footButton.centerY = self.height/2;
        _footButton.centerX = self.width/2;
        _footButton.layer.cornerRadius = 3.0f;
        _footButton.backgroundColor = kClearColor;
        _footButton.titleLabel.font = kFont(17);
        [_footButton addTarget:self action:@selector(footAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_footButton];
    }
    return _footButton;
}

-(void)footAction{
    if (self.footClick) {
        self.footClick();
    }
}

@end
