//
//  KSwitchCell.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KSwitchCell.h"

@implementation KSwitchCell

-(void)openSwitchOrNot:(UISwitch*)sender{
    if (self.operateBlock) {
        self.operateBlock(sender.isOn);
    }
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemFont;
        _titleLabel.textColor = kBlackColor;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UISwitch *)switchIb
{
    if (!_switchIb) {
        _switchIb = [[UISwitch alloc] init];
        //_switchIb.onTintColor = kThemeColor;
        [_switchIb addTarget:self action:@selector(openSwitchOrNot:) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:_switchIb];
    }
    return _switchIb;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.switchIb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
//        make.width.mas_equalTo(50);
//        make.height.mas_equalTo(30);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(self.switchIb.mas_left).offset(-10);
        make.top.bottom.mas_equalTo(0);
    }];
}

@end
