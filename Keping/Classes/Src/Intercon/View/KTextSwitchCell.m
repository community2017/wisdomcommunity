//
//  KTextSwitchCell.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTextSwitchCell.h"

@implementation KTextSwitchCell

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = SystemFont;
        _titleLabel.textColor = kBlackColor;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UILabel *)infoLabel
{
    if (!_infoLabel) {
        _infoLabel = [UILabel new];
        _infoLabel.font = SystemSmallFont;
        _infoLabel.textColor = kDarkGrayColor;
        _infoLabel.numberOfLines = 0;
        [self.contentView addSubview:_infoLabel];
    }
    return _infoLabel;
}

-(UISwitch *)switchIb
{
    if (!_switchIb) {
        _switchIb = [[UISwitch alloc] init];
        _switchIb.onTintColor = kThemeColor;
        [self.contentView addSubview:_switchIb];
    }
    return _switchIb;
}


-(void)layoutSubviews
{
    [self.switchIb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.titleLabel.mas_centerY);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.switchIb.mas_left).offset(-10);
    }];
    [self.infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.left.mas_equalTo(self.titleLabel.mas_left);
        make.right.mas_equalTo(self.switchIb.mas_right);
        make.bottom.mas_equalTo(-15);
    }];
}

-(CGSize)sizeThatFits:(CGSize)size
{
    CGFloat totalHeight = 0;
    totalHeight += 30;
    totalHeight += 20;
    CGFloat infoHeight = [self.infoLabel sizeThatFits:size].height;;
    if (infoHeight > 0) {
        totalHeight += (infoHeight+10);
    }
    
    return CGSizeMake(size.width, totalHeight);
}

@end
