//
//  KPureInputTextCell.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KPureInputTextCell.h"

@implementation KPureInputTextCell

-(UITextField *)txfield
{
    if (!_txfield) {
        _txfield = [[UITextField alloc] init];
        _txfield.clearButtonMode = UITextFieldViewModeWhileEditing;
        _txfield.font = SystemFont;
        _txfield.delegate = self;
        [self.contentView addSubview:_txfield];
    }
    return _txfield;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isNotBlank]) {
        if (self.textBlock) {
            self.textBlock(textField.text);
        }
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.txfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(0);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
    }];
}

@end
