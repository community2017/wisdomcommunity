//
//  KPureInputTextCell.h
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KPureInputTextCell : KTableViewCell<UITextFieldDelegate>

/**/
@property(nonatomic,copy)void(^textBlock) (NSString* text);
/**/
@property(nonatomic,strong)UITextField* txfield;

@end
