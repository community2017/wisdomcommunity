//
//  KTextSwitchCell.h
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KTextSwitchCell : KTableViewCell

/**/
@property(nonatomic,strong)UILabel* titleLabel;
/**/
@property(nonatomic,strong)UISwitch* switchIb;
/**/
@property(nonatomic,strong)UILabel* infoLabel;

@end
