//
//  KConversationListCell.m
//  Keping
//
//  Created by 柯平 on 2017/9/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KConversationListCell.h"
#import "KUserData.h"

@implementation KConversationListCell

-(void)setUser:(KPartyView *)user message:(NSString *)msg
{
    [self.avatar setImageWithURL:[NSURL URLWithString:user.profile] placeholder:[UIImage imageNamed:KImagePlaceholder]];
    self.nameLabel.text = user.nickName;
    self.messageLabel.text = msg;
}

-(UIImageView *)avatar
{
    if (!_avatar) {
        _avatar = [UIImageView new];
        _avatar.clipsToBounds = YES;
        [self.contentView addSubview:_avatar];
    }
    return _avatar;
}

-(UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = SystemBoldFont;
        _nameLabel.textColor = kBlackColor;
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

-(UILabel *)messageLabel
{
    if (!_messageLabel) {
        _messageLabel = [UILabel new];
        _messageLabel.textColor = kDarkGrayColor;
        _messageLabel.font = SystemSmallFont;
        [self.contentView addSubview:_messageLabel];
    }
    return _messageLabel;
}

-(UILabel *)countLabel
{
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.textColor = kWhiteColor;
        _countLabel.font = kFont(10);
        _countLabel.backgroundColor = kRedColor;
        _countLabel.layer.cornerRadius = 12;
        _countLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_countLabel];
    }
    return _countLabel;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.avatar.mas_right).offset(10);
        make.right.mas_equalTo(self.countLabel.mas_left).offset(-5);
        make.bottom.mas_equalTo(self.avatar.mas_centerY);
        make.top.mas_equalTo(self.avatar.mas_top);
    }];
    [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel.mas_left);
        make.right.mas_equalTo(self.nameLabel.mas_right);
        make.bottom.mas_equalTo(self.avatar.mas_bottom);
        make.top.mas_equalTo(self.avatar.mas_centerY).offset(5);
    }];
}

@end
