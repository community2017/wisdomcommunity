//
//  KSwitchCell.h
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KSwitchCell : KTableViewCell

/**/
@property(nonatomic,copy)void (^operateBlock)(BOOL open);

/**/
@property(nonatomic,strong)UILabel* titleLabel;
/**/
@property(nonatomic,strong)UISwitch* switchIb;

@end
