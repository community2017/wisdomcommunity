//
//  KFriendProfileCell.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KFriendProfileCell.h"

@implementation KFriendProfileCell

-(UIImageView *)avatar
{
    if (!_avatar) {
        _avatar = [UIImageView new];
        _avatar.clipsToBounds = YES;
        [self.contentView addSubview:_avatar];
    }
    return _avatar;
}

-(UILabel *)nicknameLabel
{
    if (!_nicknameLabel) {
        _nicknameLabel = [UILabel new];
        _nicknameLabel.font = SystemFont;
        _nicknameLabel.textColor = kBlackColor;
        [self.contentView addSubview:_nicknameLabel];
    }
    return _nicknameLabel;
}

-(UILabel *)phoneLabel
{
    if (!_phoneLabel) {
        _phoneLabel = [UILabel new];
        _phoneLabel.font = SystemSmallFont;
        _phoneLabel.textColor = kDarkGrayColor;
        [self.contentView addSubview:_phoneLabel];
    }
    return _phoneLabel;
}

-(UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = SystemSmallFont;
        _nameLabel.textColor = kDarkGrayColor;
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat margin = 10;
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(margin);
        make.width.height.mas_equalTo(60);
    }];
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.avatar.mas_right).offset(margin);
        make.top.mas_equalTo(self.avatar.mas_top);
        make.right.mas_equalTo(-margin);
        make.height.mas_equalTo(20);
    }];
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nicknameLabel.mas_left);
        make.top.mas_equalTo(self.nicknameLabel.mas_bottom);
        make.height.mas_equalTo(self.nicknameLabel.mas_height);
        make.right.mas_equalTo(self.nicknameLabel.mas_right);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nicknameLabel.mas_left);
        make.top.mas_equalTo(self.phoneLabel.mas_bottom);
        make.height.mas_equalTo(self.nicknameLabel.mas_height);
        make.right.mas_equalTo(self.nicknameLabel.mas_right);
    }];
}

@end
