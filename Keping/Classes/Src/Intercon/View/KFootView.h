//
//  KFootView.h
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFootView : UIView

/**/
@property(nonatomic,copy)void(^footClick) (void);
/**/
@property(nonatomic,strong)UIButton* footButton;

@end
