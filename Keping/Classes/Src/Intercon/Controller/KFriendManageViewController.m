//
//  KFriendManageViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KFriendManageViewController.h"
#import <UITableView+FDTemplateLayoutCell.h>
#import "KTextSwitchCell.h"

@interface KFriendManageViewController ()

@end

@implementation KFriendManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.data = @[@"Auto Add Friends",@"Allow Others to Add",@"Block Users"].mutableCopy;
    [self.tableView registerClass:[KTextSwitchCell class] forCellReuseIdentifier:[KTextSwitchCell className]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = self.data[indexPath.section];
    if ([@"Block Users" isEqualToString:title]) {
        return 64;
    }else{
        return [tableView fd_heightForCellWithIdentifier:[KTextSwitchCell className] configuration:^(id cell) {
            [self configCell:cell atIndex:indexPath];
        }];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //@[@"Auto Add Friends",@"Allow Others to Add",@"Block Users"]
    NSString* title = self.data[indexPath.section];
    if ([@"Block Users" isEqualToString:title]) {
        KTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTableViewCell className]];
        if (!cell) {
            cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTableViewCell className]];
        }
        cell.textLabel.text = @"Block Users";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }else{
        KTextSwitchCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTextSwitchCell className]];
        [self configCell:cell atIndex:indexPath];
        return cell;
    }
    return nil;
}

-(void)configCell:(KTextSwitchCell*)cell atIndex:(NSIndexPath*)indexPath
{
    NSString* title = self.data[indexPath.section];
    cell.fd_enforceFrameLayout = NO;
    cell.titleLabel.text = title;
    if ([@"Auto Add Friends" isEqualToString:title]) {
        cell.infoLabel.text = @"Friends who also use LinkTime will be automatically added from your phonebook.";
    }else if ([@"Allow Others to Add" isEqualToString:title]){
        cell.infoLabel.text = @"Others who have your phone number or  from the same community can add you as friend.";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
