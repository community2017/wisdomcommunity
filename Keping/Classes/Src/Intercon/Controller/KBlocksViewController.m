//
//  KBlocksViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KBlocksViewController.h"

@interface KBlocksViewController ()

@end

@implementation KBlocksViewController

-(instancetype)initWithBlocks:(NSArray *)blocks
{
    self = [super init];
    if (self) {
        if (blocks.count > 0) {
            self.title = [NSString stringWithFormat:@"Block Users(%ld)",blocks.count];
            [self.data addObjectsFromArray:blocks];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title  = @"Block Users";
    [self getData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTableViewCell className]];
    if (!cell) {
        cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTableViewCell className]];
    }
    cell.imageView.image = [UIImage imageNamed:@"placeholder"];
    cell.textLabel.text = @"Franklin";
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
