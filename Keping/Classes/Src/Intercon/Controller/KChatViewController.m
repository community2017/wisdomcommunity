//
//  KChatViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KChatViewController.h"

@interface KChatViewController ()<EMClientDelegate>

@property(nonatomic)BOOL isPlayingAudio;
@property(nonatomic,copy)EaseSelectAtTargetCallback selectedCallback;


@end

@implementation KChatViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (EMConversationTypeChat == self.conversation.type) {
        NSDictionary* ext = self.conversation.ext;
        if ([ext objectForKey:@"subject"]) {
            self.title = [ext objectForKey:@"subject"];
        }
        
        if (ext && ext[@"kHaveAtMessage"]) {
            NSMutableDictionary* newExt = ext.mutableCopy;
            [newExt removeObjectForKey:@"kHaveAtMessage"];
            self.conversation.ext = newExt;
        }
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.showRefreshFooter = YES;
    self.delegate = self;
    self.dataSource = self;
    self.tableView.backgroundColor = UIColorHex(F1F2F5);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_deleteAllMessages) name:@"KDeleteAllMessagesNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_endConversation) name:@"KEndConversationNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_insertCallMessage) name:@"KInsertCallMessageNotification" object:nil];
    
}

-(void)_setupBarButtonItem
{
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:kWhiteColor,NSFontAttributeName:SystemBoldFont}];
    
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (EMConversationTypeChatRoom == self.conversation.type) {
        if (self.isJoinedChatroom) {//退出聊天室，删除回话
            NSString* chater = [self.conversation.conversationId copy];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                EMError* error = nil;
                [[EMClient sharedClient].roomManager leaveChatroom:chater error:&error];
                if (error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"Leave chatroom '%@' failed [%@]",chater,error.errorDescription] preferredStyle:UIAlertControllerStyleAlert];
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }else{
                    [[EMClient sharedClient].chatManager deleteConversation:self.conversation.conversationId isDeleteMessages:YES completion:nil];
                }
            });
        }
    }
    [[EMClient sharedClient] removeDelegate:self];
}

-(void)_deleteAllMessages
{
    
}

-(void)_endConversation{
    
}

-(void)_insertCallMessage{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
