//
//  KInterconViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KInterconViewController.h"
#import "KHistoryViewController.h"
#import "KContactsViewController.h"

@interface KInterconViewController ()<UIScrollViewDelegate>
{
    NSArray* _itemTitles;
}
/**/
@property(nonatomic,strong)UIView* titleView;
/**/
@property(nonatomic,strong)UIScrollView* contentView;


@end

@implementation KInterconViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Intercom";
    
    _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [self.view addSubview:_titleView];
    
    _contentView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _titleView.bottom, kScreenWidth, kScreenHeight-_titleView.height)];
    _contentView.delegate = self;
    _contentView.pagingEnabled = YES;
    _contentView.showsVerticalScrollIndicator = NO;
    _contentView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_contentView];
    
    //添加子控制器
    KHistoryViewController *hvc = [[KHistoryViewController alloc] init];
    hvc.title = @"History";
    [self addChildViewController:hvc];
    
    KContactsViewController *cvc = [[KContactsViewController alloc] init];
    cvc.title=@"Contacts";
    [self addChildViewController:cvc];
    _contentView.contentSize =
    CGSizeMake(self.childViewControllers.count * kScreenWidth, 0);
    
    //设置标题
    CGFloat btnW = self.view.width/self.childViewControllers.count;
    for (NSInteger i = 0; i < self.childViewControllers.count; i++) {
        UIViewController* vc = self.childViewControllers[i];
        UIButton *btn = [[UIButton alloc] init];
        [btn setTitle:[self.childViewControllers[i] title] forState:UIControlStateNormal];
        [btn setTitleColor:UIColorHex(4c5cf7) forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:vc.title forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageWithColor:kWhiteColor] forState:UIControlStateSelected];
        [btn setBackgroundImage:[UIImage imageNamed:@"navi_bar_back"] forState:UIControlStateNormal];
        btn.frame = CGRectMake(i*btnW, 0, btnW, _titleView.height);
        [btn addTarget:self action:@selector(clickTitleItem:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
        [_titleView addSubview:btn];
    }
    
    [self scrollViewDidEndScrollingAnimation:_contentView];
}

-(void)clickTitleItem:(UIButton*)sender{
    // 取出被点击label的索引
    NSInteger index = sender.tag;
    
    // 让底部的内容scrollView滚动到对应位置
    CGPoint offset = _contentView.contentOffset;
    offset.x = index * _contentView.width;
    [_contentView setContentOffset:offset animated:YES];
}

#pragma mark - <UIScrollViewDelegate>
/**
 * scrollView结束了滚动动画以后就会调用这个方法（比如- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated;方法执行的动画完毕后）
 */
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    // 一些临时变量
    CGFloat width = scrollView.frame.size.width;
    CGFloat height = scrollView.frame.size.height;
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 当前位置需要显示的控制器的索引
    NSInteger index = offsetX / width;
    
    // 让对应的顶部标题选中
    UIButton *btn = _titleView.subviews[index];
    btn.selected=YES;
    //    CGPoint titleOffset = self.titleScrollView.contentOffset;
    // 让其他按钮回到最初的状态
    for (UIButton *otherBtn in _titleView.subviews) {
        if ([otherBtn isKindOfClass:[UIButton class]]) {
            if (otherBtn != btn) otherBtn.selected=NO;
        }
    }
    // 取出需要显示的控制器
    UIViewController *willShowVc = self.childViewControllers[index];
    
    // 如果当前位置的位置已经显示过了，就直接返回
    if ([willShowVc isViewLoaded]) return;
    
    // 添加控制器的view到contentScrollView中;
    willShowVc.view.frame = CGRectMake(offsetX,0, width, height);
    [scrollView addSubview:willShowVc.view];
}

/**
 * 手指松开scrollView后，scrollView停止减速完毕就会调用这个
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
