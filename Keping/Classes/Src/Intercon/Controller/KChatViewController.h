//
//  KChatViewController.h
//  Keping
//
//  Created by 柯平 on 2017/9/14.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "EaseMessageViewController.h"

@interface KChatViewController : EaseMessageViewController<EaseMessageViewControllerDelegate,EaseMessageViewControllerDataSource>

@end
