//
//  KHistoryViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KHistoryViewController.h"
#import "KConversationListCell.h"
#import "KConversationRequest.h"
#import "KConversationsData.h"

@interface KHistoryViewController ()

@end

@implementation KHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 64;
    self.tableView.loadmoreEnable = NO;
    [self.tableView beginRefresh];
    
}

-(void)refresh
{
    [self getData];
    
}

-(void)getData
{
    [super getData];
    
    KConversationRequest* request = [[KConversationRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        KConversationsData* data = [KConversationsData modelWithJSON:request.responseData];
        if (data.statusCode == 200) {
            [self.data addObjectsFromArray:
             data.relationSessionViews];
        }else{
            [self showErrorText:data.msg];
        }
        [self.tableView endRefresh];
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        [self.tableView endRefresh];
    }];
//    NSArray* conversations = [[EMClient sharedClient].chatManager  getAllConversations];
//    if (!conversations.count) {
//        [self.tableView endRefresh];
//    }else{
//        [self.data addObjectsFromArray:conversations];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KConversationListCell* cell = [tableView dequeueReusableCellWithIdentifier:[KConversationListCell className]];
    if (!cell) {
        cell = [[KConversationListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KConversationListCell className]];
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
