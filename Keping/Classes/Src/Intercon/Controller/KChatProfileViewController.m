//
//  KChatProfileViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KChatProfileViewController.h"
#import "KFriendProfileCell.h"
#import "KSwitchCell.h"

@interface KChatProfileViewController ()

@end

@implementation KChatProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Profile";
    
    self.data = @[@"Profile",@"Mobile",@"Alias",@"Address",@"Block"].mutableCopy;
    [self getData];
}

-(void)getData
{
    [super getData];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.data.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = self.data[indexPath.section];
    if ([@"Profile" isEqualToString:title]) {
        return 80;
    }else{
        return 64;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //@[@"Profile",@"Mobile",@"Aliss",@"Address",@"Block"]
    NSString* title = self.data[indexPath.section];
    if ([@"Profile" isEqualToString:title]) {
        KFriendProfileCell* cell = [tableView dequeueReusableCellWithIdentifier:[KFriendProfileCell className]];
        if (!cell) {
            cell = [[KFriendProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KFriendProfileCell className]];
        }
    }else if ([@"Block" isEqualToString:title]){
        KSwitchCell* cell = [tableView dequeueReusableCellWithIdentifier:[KSwitchCell className]];
        if (!cell) {
            cell = [[KSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KSwitchCell className]];
        }
        return cell;
    }
    else{
        KTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTableViewCell className]];
        if (!cell) {
            cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTableViewCell className]];
        }
        cell.textLabel.text = title;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if ([@"Mobile" isEqualToString:title]){
            cell.detailTextLabel.textColor = kThemeColor;
        }else if ([@"Alias" isEqualToString:title]){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }else if ([@"Address" isEqualToString:title]){
            
        }
        return cell;
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
