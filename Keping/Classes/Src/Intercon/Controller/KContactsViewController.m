//
//  KContactsViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/12.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KContactsViewController.h"
#import "KGetContactsRequest.h"

@interface KContactsViewController ()

@end

@implementation KContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.loadmoreEnable = NO;
    self.tableView.refreshEnable = NO;
    [self getData];
    
}
-(void)getData
{
    [super getData];
    KGetContactsRequest* request = [[KGetContactsRequest alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof KBaseRequest * _Nonnull request) {
        
    } failure:^(__kindof KBaseRequest * _Nonnull request) {
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
