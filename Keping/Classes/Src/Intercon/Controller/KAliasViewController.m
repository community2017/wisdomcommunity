//
//  KAliasViewController.m
//  Keping
//
//  Created by 柯平 on 2017/9/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KAliasViewController.h"
#import "KPureInputTextCell.h"

@interface KAliasViewController ()
{
    NSString* _placeholder;
    NSString* _originText;
    NSInteger _mode;
}

@property(nonatomic,copy)NSString* text;
@end

@implementation KAliasViewController

-(instancetype)initWithPlaceholder:(NSString *)placeholder originText:(NSString *)originText inputMode:(NSInteger)mode
{
    self = [super init];
    if (self) {
        _placeholder = placeholder;
        _originText = originText;
        _mode = mode;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Alias";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    
    
}

-(void)saveAction
{
    [self.view endEditing:YES];
    if ([self.text isNotBlank]) {
        if (![self.text isEqualToString:_originText]) {
            
        }
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"\t%@",_originText];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @weakify(self);
    KPureInputTextCell* cell = [tableView dequeueReusableCellWithIdentifier:[KPureInputTextCell className]];
    if (!cell) {
        cell = [[KPureInputTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KPureInputTextCell className]];
    }
    cell.txfield.placeholder = _placeholder;
    cell.txfield.text = _originText;
    
    cell.textBlock = ^(NSString *text) {
        if ([text isNotBlank]) {
            weak_self.text = text;
        }
    };
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
