//
//  KLaunchViewCell.m
//  ChiKe
//
//  Created by Kevin on 2017/5/4.
//  Copyright © 2017年 keping. All rights reserved.
//

#import "KLaunchViewCell.h"

@interface KLaunchViewCell ()

@property(nonatomic,weak)UIImageView* imageView;

@end

@implementation KLaunchViewCell

-(UIImageView *)imageView{
    if (!_imageView) {
        UIImageView* imv = [[UIImageView alloc]initWithFrame:self.contentView.bounds];
        imv.contentMode = UIViewContentModeScaleToFill;
        imv.clipsToBounds = YES;
        [self.contentView addSubview:imv];
        _imageView = imv;
    }
    return _imageView;
}

-(void)setImage:(UIImage *)image{
    if (!image || ![image isKindOfClass:[UIImage class]]) return;
    self.imageView.image = image;
}

-(void)setImageUrl:(NSURL *)imageUrl{
    if (!imageUrl || ![imageUrl isKindOfClass:[NSURL class]]) return;
    [self.imageView setImageURL:imageUrl];
}

@end
