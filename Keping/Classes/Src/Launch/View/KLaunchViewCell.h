//
//  KLaunchViewCell.h
//  ChiKe
//
//  Created by Kevin on 2017/5/4.
//  Copyright © 2017年 keping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KLaunchViewCell : UICollectionViewCell
@property(nonatomic,strong)UIImage* image;
@property(nonatomic,strong)NSURL*   imageUrl;

@end
