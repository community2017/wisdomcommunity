//
//  KICPhotoCell.h
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KICPhotoCell : KTableViewCell

/**/
@property(nonatomic,strong)UILabel* titleLabel;
/**/
@property(nonatomic,strong)UIImageView* frontView;
/**/
@property(nonatomic,strong)UIImageView* backView;


@end
