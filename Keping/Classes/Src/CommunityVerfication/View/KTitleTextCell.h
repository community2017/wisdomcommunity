//
//  KTitleTextCell.h
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTableViewCell.h"

@interface KTitleTextCell : KTableViewCell

/**/
@property(nonatomic,strong)UILabel* titleLb;
/**/
@property(nonatomic,strong)UITextField* textTf;

@end
