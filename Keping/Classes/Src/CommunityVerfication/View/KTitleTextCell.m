//
//  KTitleTextCell.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KTitleTextCell.h"

@implementation KTitleTextCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth/2);
    }];
    [self.textTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(100);
        make.bottom.top.mas_equalTo(0);
    }];
}

-(UILabel *)titleLb
{
    if (!_titleLb) {
        _titleLb = [UILabel new];
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}

-(UITextField *)textTf
{
    if (!_textTf) {
        _textTf = [UITextField new];
        [self.contentView addSubview:_textTf];
        _textTf.textAlignment = NSTextAlignmentRight;
        _textTf.borderStyle = UITextBorderStyleNone;
    }
    return _textTf;
}

@end
