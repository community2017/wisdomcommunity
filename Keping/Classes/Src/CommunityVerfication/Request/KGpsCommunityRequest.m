//
//  KGpsCommunityRequest.m
//  Keping
//
//  Created by 柯平 on 2017/10/11.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KGpsCommunityRequest.h"

@implementation KGpsCommunityRequest

-(NSString *)requestUrl
{
    return KGetCommunityURL;
}

-(KRequestMethod)requestMethod
{
    return KRequestMethodGET;
}

-(id)requestArgument
{
    return self.params;
}

@end
