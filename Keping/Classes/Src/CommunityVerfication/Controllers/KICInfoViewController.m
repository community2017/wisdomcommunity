//
//  KICInfoViewController.m
//  Keping
//
//  Created by 柯平 on 2017/10/10.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KICInfoViewController.h"
#import "KFootView.h"

@interface KICInfoViewController ()

/**/
@property(nonatomic,strong)NSArray* items;

/**/
@property(nonatomic,strong)KFootView* footView;

@end

@implementation KICInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.items = @[@"*Full Name:",@"*Address:",@"*IC No:",@"IC Photo:"];
    self.tableView.tableFooterView = self.footView;
    
}

-(void)submitAction{
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.items.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = self.items[indexPath.section];
    if ([@"IC Photo:" isEqualToString:title]) {
        
    }else if ([@"*Address:" isEqualToString:title]){
        
    }else{
        KTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTableViewCell className]];
        if (!cell) {
            cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[KTableViewCell className]];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if ([@"*Full Name:" isEqualToString:title]) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@  %@",title,@"name"];
        }else if ([@"*IC No:" isEqualToString:title]){
            cell.textLabel.text = [NSString stringWithFormat:@"%@  %@",title,@"0000"];
        }
        return cell;
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(KFootView *)footView
{
    if (!_footView) {
        _footView = [[KFootView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
        [_footView.footButton setTitle:@"Submit" forState:UIControlStateNormal];
        _footView.footClick = ^{
            
        };
    }
    return _footView;
}

@end
