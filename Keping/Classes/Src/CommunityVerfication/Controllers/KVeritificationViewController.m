//
//  KVeritificationViewController.m
//  Keping
//
//  Created by 柯平 on 2017/6/30.
//  Copyright © 2017年 柯平. All rights reserved.
//

#import "KVeritificationViewController.h"
#import "KTitleTextCell.h"
#import "KSelectCommunityController.h"

@interface KVeritificationViewController ()
{
    NSArray* items;
}
/**/
@property(nonatomic,strong)UIView* footView;
/**/
@property(nonatomic,strong)UIButton* addButton;

@end

@implementation KVeritificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Veritification";
    
    items = @[@"*Community:",@"*Full Name:",@"Verification Code:",@"*Address:"];
    
    self.groupView.refreshEnable = NO;
    self.groupView.loadmoreEnable = NO;
    self.groupView.rowHeight = 64;
    self.groupView.tableFooterView = self.footView;
    [self.view addSubview:self.groupView];
    
}

-(void)submitVertificationInfo{
    [self.view endEditing:YES];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return 5.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0001;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return items.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.section];
    KTitleTextCell* cell = [tableView dequeueReusableCellWithIdentifier:[KTitleTextCell className]];
    if (!cell) {
        cell = [[KTitleTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UITableViewCellStyleDefault];
    }
    if ([@"*Community:" isEqualToString:title]) {
        cell.textTf.userInteractionEnabled = NO;
    }
    cell.titleLb.text = title;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = items[indexPath.section];
    if ([@"*Community:" isEqualToString:title]) {
        KSelectCommunityController* vc = [KSelectCommunityController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

-(UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 84)];
        _footView.backgroundColor = kClearColor;
        [self.addButton setFrame:CGRectMake(15, 30, kScreenWidth-30, 44)];
        [_footView addSubview:self.addButton];
    }
    return _footView;
}

-(UIButton *)addButton
{
    if (!_addButton) {
        _addButton = [UIButton commonButtonWithTitle:@"Submit"];
        [_addButton addTarget:self action:@selector(submitVertificationInfo) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addButton;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
